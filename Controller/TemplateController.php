<?php 
class TemplateController extends AppController {
	
  public $layout = null;
  public $autoRender = false;
  
  public function beforeFilter () {
    parent::beforeFilter();
   
  }

  public function afterFilter() {
    $view = str_replace('__', '/', $this->request->params['action']);
    $view = str_replace('_', '-', $view);
    $this->render($view);
  }

  // DASHBOARD
  public function dashboard__index() {}
  
  
  // ADMINISTRATOR TEMPLATES
  // users
  public function admin__users__index() {}
  public function admin__users__add() {}
  public function admin__users__edit() {}
  public function admin__users__view() {}
  // permissions
  public function admin__permissions__index() {}
  public function admin__permissions__add() {}
  public function admin__permissions__edit() {}
  public function admin__permissions__view() {}
  // employees
  public function admin__employees__index() {}
  public function admin__employees__add() {}
  public function admin__employees__edit() {}
  public function admin__employees__view() {}
  // business
  public function admin__business__index() {}
  public function admin__business__add() {}
  public function admin__business__edit() {}
  public function admin__business__view() {}
  // business services
  public function admin__business_services__index() {}
  public function admin__business_services__add() {}
  public function admin__business_services__edit() {}
  public function admin__business_services__view() {}
  // accomodations
  public function admin__room_accomodations__index() {}
  public function admin__room_accomodations__add() {}
  public function admin__room_accomodations__edit() {}
  public function admin__room_accomodations__view() {}
  // room types
  public function admin__room_types__index() {}
  public function admin__room_types__add() {}
  public function admin__room_types__edit() {}
  public function admin__room_types__view() {}
  // rooms
  public function admin__rooms__index() {}
  public function admin__rooms__add() {}
  public function admin__rooms__edit() {}
  public function admin__rooms__view() {}
  public function admin__rooms__occupied() {}
  // discounts
  public function admin__discounts__index() {}
  public function admin__discounts__add() {}
  public function admin__discounts__edit() {}
  public function admin__discounts__view() {}
  // penalties
  public function admin__penalties__index() {}
  public function admin__penalties__add() {}
  public function admin__penalties__edit() {}
  public function admin__penalties__view() {}
  // settings
  public function admin__settings__index() {}
  public function admin__settings__add() {}
  public function admin__settings__edit() {}
  public function admin__settings__view() {}
  // logs
  public function admin__logs__index() {}
  public function admin__logs__view() {}
  // dtr 
  public function admin__daily_time_records__index() {}
  public function admin__daily_time_records__edit() {}
  public function admin__daily_time_records__view() {}
  public function admin__daily_time_records__add() {}
  // messages
  public function admin__messages__index() {}
  public function admin__messages__add() {}
  // companies
  public function admin__companies__index() {}
  public function admin__companies__add() {}
  public function admin__companies__edit() {}
  public function admin__companies__view() {}
  
  // HOTEL TEMPLATES
  // calendar
  public function hotel__calendar__index() {}
  // bookings
  public function hotel__bookings__index() {}
  public function hotel__bookings__add() {}
  public function hotel__bookings__edit() {}
  public function hotel__bookings__view() {}
  public function hotel__bookings__book() {}
  // cashiering
  public function hotel__cashiering__index() {}
  public function hotel__cashiering__add() {}
  public function hotel__cashiering__edit() {}
  public function hotel__cashiering__view() {}
  public function hotel__cashiering__soa() {}
  // reports
  public function hotel__cashiering__report() {}
  public function hotel__cashiering__report_view() {}
  // night audit
  public function hotel__night_audits__index() {}
  // ledger
  public function hotel__ledgers__index() {}
  public function hotel__ledgers__view() {}
  // transactions
  public function hotel__transactions__index() {}
  public function hotel__transactions__add() {}
  public function hotel__transactions__view() {}
  public function hotel__transactions__edit() {}
  // guests
  public function hotel__guests__index    () {}
  public function hotel__guests__incoming () {}
  public function hotel__guests__outgoing () {}
  public function hotel__guests__list     () {}
  public function hotel__guests__view     () {}
  public function hotel__guests__edit     () {}
  // print
  // public function hotel__cashiering__print__cashiering_report (){}


  // RESTO TEMPLATES
  // menus
  public function resto__menus__index() {}
  public function resto__menus__add() {}
  public function resto__menus__edit() {}
  public function resto__menus__view() {} 
  // tables
  public function resto__tables__index() {}
  public function resto__tables__add() {}
  public function resto__tables__edit() {}
  public function resto__tables__view() {} 
  // suppliers
  public function resto__suppliers__index() {}
  public function resto__suppliers__add() {}
  public function resto__suppliers__edit() {}
  public function resto__suppliers__view() {} 
  // inventory
  public function resto__inventory__index() {}
  public function resto__inventory__add() {}
  public function resto__inventory__edit() {}
  public function resto__inventory__view() {} 
  public function resto__inventory__daily_consumption() {}
  // transactions
  public function resto__transactions__index() {}
  public function resto__transactions__add() {}
  public function resto__transactions__view() {}
  public function resto__transactions__edit() {}
  // pos
  public function resto__pos__index() {}
  public function resto__pos__table_view() {}
  public function resto__pos__table_transact() {}
  // REPORT
  public function resto__cashiering__report() {}
  public function resto__suppliers__report() {}
  public function resto__suppliers__report_view() {}
  public function resto__transactions__receivables() {} 
  public function resto__transactions__receivables_view() {} 

  // BAR TEMPLATES
  // menus
  public function bar__menus__index() {}
  public function bar__menus__add() {}
  public function bar__menus__edit() {}
  public function bar__menus__view() {} 
  // tables
  public function bar__tables__index() {}
  public function bar__tables__add() {}
  public function bar__tables__edit() {}
  public function bar__tables__view() {} 
  // suppliers
  public function bar__suppliers__index() {}
  public function bar__suppliers__add() {}
  public function bar__suppliers__edit() {}
  public function bar__suppliers__view() {} 
  // inventory
  public function bar__inventory__index() {}
  public function bar__inventory__add() {}
  public function bar__inventory__edit() {}
  public function bar__inventory__view() {}
  public function bar__inventory__daily_consumption() {}
  
    // transactions
  public function bar__transactions__index() {}
  public function bar__transactions__add() {}
  public function bar__transactions__view() {}
  public function bar__transactions__edit() {}
  // pos
  public function bar__pos__index() {}
  public function bar__pos__table_view() {}
  public function bar__pos__table_transact() {}  
  // ACCOUNTING
  public function accounting__statement_of_accounts() {}
  // REPORT
  public function bar__cashiering__report() {}
  public function bar__suppliers__report() {}
  public function bar__suppliers__report_view() {}
  public function bar__transactions__receivables() {} 
  public function bar__transactions__receivables_view() {} 
  
  // PRINT
  public function print__hotel__folio() {
    $folioId = $this->request->params['pass'][0];
    $Folio = ClassRegistry::init('Folio');
    
    $folio = $Folio->find('first', array(
      'contain' => array(
        'Guest',
        'ReservationRoom' => array(
          'Room' => array(
            'RoomType'  
          )
        )
      ),
      'conditions' => array(
        'Folio.id' => $folioId
      )
    ));
    
    $this->set(compact('folio'));
  }
  

}