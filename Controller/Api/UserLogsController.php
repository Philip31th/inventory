<?php
class UserLogsController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
  
    // default conditions
    $conditions = array();
    $conditions['UserLog.visible'] = true;
    $conditions['User.visible'] = true;

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'User.lastName LIKE' => "%$search%",
        'User.firstName LIKE' => "%$search%",
        'UserLog.action LIKE' => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'contain' => array(
        'User'
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'UserLog.created' => 'DESC'
      )
    );
    $modelName = 'UserLog';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData   = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $logs = array();
    foreach ($tmpData as $data) {
      $log    = $data['UserLog'];
      $user    = $data['User'];

      $logs[] = array(
        'id'          => $log['id'],
        'user'        => $user['firstName'].' '.$user['lastName'],
        'action'      => $log['action'],
        'role'        => $user['role'],
        'description' => $log['description'],
        'created'     => date('M d,Y h:i:s A', strtotime($log['created'])),
      );
    }

    $response = array(
      'ok'   => true,
      'data' => $logs,
      'paginator' => $paginator,
    );
        
    $this->set(array(
        'response'   => $response,
        '_serialize' => 'response'
    ));
  }


}