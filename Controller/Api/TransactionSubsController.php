<?php 
class TransactionSubsController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  // public $uses = array('TransactionPayment');

  public function view($id = null) {

    $data = $this->TransactionSub->find('first',array(
      'conditions'  =>  array(
        'TransactionSub.id' =>  $id
      )
      )); 

    $response = array(
        'ok'      =>  true,
        'data'    =>  $data
      );
    $this->set(array(
      'response'    =>  $response,
      '_serialize'  =>  'response'
    ));

  }
  public function add() {   
    if ($this->TransactionSub->save($this->request->data['TransactionSub'])) {
      $response = array (
        'ok'  => true,
        'msg' => 'TransactionSubs has been saved'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'TransactionSubs cannot be save this time'
      );
    }
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));

  }

  public function edit($id = null) {  
    $this->request->data['TransactionSub']['id']=$id;
    if ($this->TransactionSub->save($this->request->data['TransactionSub'])) {
      $response = array (
        'ok'  => true,
        'msg' => 'TransactionSub has been saved'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'TransactionSub cannot be save this time'
      );
    }
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    // check subs
    $subs = $this->checkSub($id);

    foreach ($subs as $i => $data) {
      $this->request->data['TransactionSub'][$i]['id'] = $subs[$i]['TransactionSub']['id']; 

      $hide = $this->TransactionSub->hide($this->request->data['TransactionSub'][$i]['id']);
    }

    // check payments
      $payments = $this->getPayments($id);

      foreach ($payments as $i => $data) {
        $this->request->data['TransactionPayment'][$i]['id'] = $payments[$i]['TransactionPayment']['id']; 
            $hide = $this->TransactionSub->TransactionPayment->hide($this->request->data['TransactionPayment'][$i]['id']);
      }

      if($hide) {
        $response = array(
          'ok'      =>  true,
          'datas'   =>  $subs,
          'pay' => $payments
        );

        $this->set(array(
          'response'    =>  $response,
          '_serialize'  =>  'response'
        ));
      }
  }


  public function checkSub($id = null) {
    $subId = $this->TransactionSub->find('first',array(
      'conditions'  => array(
        'id' => $id
      )
    ));

    $subs = $this->TransactionSub->find('all',array(
      'conditions'  =>  array(
        'transactionId' => $subId['TransactionSub']['transactionId'],
        'particulars'   => $subId['TransactionSub']['particulars']    
      )
    ));

    return $subs;
  }  

  public function getPayments($id=null) {
    $paymentId = $this->TransactionSub->TransactionPayment->find('all',array(
      'conditions'  =>  array(
        'subId' =>  $id
      )
    ));
    return $paymentId;
  }

  
}
