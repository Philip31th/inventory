<?php 
class FolioSubsController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  
  public function add() {
    // $this->FolioSub->create();
    if ($this->FolioSub->save($this->request->data['FolioSub'])) {
      $this->FolioSub->Folio->save(array(
        'id'              => $this->request->data['FolioSub']['folioId'],
        // 'closed'          => true,
        'transferedBills' => true
      ));
      
      $response = array (
        'ok'  => true,
        // 'msg' => 'Folio Sub has been saved'
      );
    } else {
      $response = array(
        'ok'  => false,
        // 'msg' => 'Folio Sub cannot be save this time'
      );
    }
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->FolioSub->hide($id)) {
       $response = array(
        'ok'      =>  true,
        'message' =>  'FolioSub has been deleted.'
      );
      $this->set(array(
        'response'    =>  $response,
        '_serialize'  =>  'response'
      ));
    }
  }
  
}
