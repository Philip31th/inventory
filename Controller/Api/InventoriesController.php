<?php
class InventoriesController extends AppController {
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
  
    // default conditions
    $conditions = array();
    $conditions['Inventory.visible'] = true;

    // check department
    if (isset($this->request->query['code'])) {
      $code = $this->request->query['code'];
      $conditions['Inventory.departmentId'] = $this->Inventory->Department->get($code);
    } else {
      $code = null;
    }

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Inventory.name LIKE' => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'contain' => array(
        'Department',
        'InventorySub'  =>  array(
          'conditions'  =>  array(
            'InventorySub.visible'  => true
          )
        ),
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Inventory.name' => 'ASC'
      )
    );
    $modelName = 'Inventory';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData   = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $inventories = array();
    foreach ($tmpData as $data) {
      $inventory     = $data['Inventory'];
      $department    = $data['Department'];
      $inventorySubs = $data['InventorySub'];

      // sum quantity
      $quantity = 0;
      $cost = 0;
      $totalAmount = 0;
      $qty1 = 0;
      foreach ($inventorySubs as $item) {
        if ($item['type']) {
          $quantity += $item['quantity'];
          $qty1 += $item['quantity'];
          $totalAmount += $item['cost']*$item['quantity']; 
        } else {
          $quantity -= $item['quantity'];
        }  
      }
      if($totalAmount!=0||$qty1!=0) {
        $cost = $totalAmount/$qty1;
      }
      
       $inventories[] = array(
        'id'          => $inventory['id'],
        'name'        => $inventory['name'],
        'description' => $inventory['description'],
        'unit'        => $inventory['unit'],
        'quantity'    => $quantity,
        'cost'        => $cost,
        'department'  => $department['name'],
      );
    }

    $response = array(
      'ok'   => true,
      'code' => $code,
      'data' => $inventories,
      'paginator' => $paginator,
    );
        
    $this->set(array(
        'response'   => $response,
        '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    // conditions
    $conditions = array();
    $conditions['Inventory.id'] = $id;
    $conditions['Inventory.visible'] = true;
    
    // load inventory
    $data = $this->Inventory->find('first',array(
      'contain' => array(
        'Department',
        'InventorySub'  =>  array(
          'conditions'  =>  array(
            'InventorySub.visible'  =>  true
          )
        )
        ),
      'conditions'=> $conditions
    ));

    $quantity  = 0;
    $received  = 0;
    $delivered = 0; 
    $inventory = array();

    $inventory['Inventory'] = array(
      'id'            =>  $data['Inventory']['id'],
      'departmentId'  =>  $data['Inventory']['departmentId'],
      'code'          =>  $data['Inventory']['code'],
      'name'          =>  $data['Inventory']['name'],
      'description'   =>  $data['Inventory']['description'],
      'unit'          =>  strtoupper($data['Inventory']['unit'])
    );

    $inventory['Department']['name'] = $data['Department']['name'];
      $averageCost = 0;
      $qty1 = 0;
      $totalAmount = 0;
      $cost = '';
    foreach ($data['InventorySub'] as $sub) {
     
      if ($sub['type']) {
        $quantity += $sub['quantity'];
        $qty1 += $sub['quantity'];
        $totalAmount += $sub['cost']*$sub['quantity']; 
        $cost = 'PHP '. number_format($sub['cost'],2);
      } else {
        $quantity -= $sub['quantity'];  
        $delivered -= $quantity;  
        $cost = '';
      }
      
      if($totalAmount!=0||$qty1!=0) {
        $averageCost = $totalAmount/$qty1;
      }
      
      $inventory['InventorySub'][] = array(
        'id'        =>  $sub['id'],
        'type'      =>  $sub['type'],
        'quantity'  =>  $sub['quantity'],
        'cost'      =>  $cost,
        'cost_n'    =>  str_replace('PHP ','',$cost),
        'date'      =>  date('M d,Y h:i A',strtotime($sub['created']))
      );  
      
      $inventory['Inventory']['averageCost'] = 'PHP '. number_format($averageCost,2);
    }

    $inventory['TotalQuantity'] = $quantity;
    
    $response = array(
      'ok'   => true,
      'data' => $inventory,
       'tmp' => $sub
    );

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function add() {
    $this->request->data['Inventory']['departmentId'] = $this->Inventory->Department->get($this->request->data['Inventory']['departmentId']);
    $save = $this->Inventory->validSave($this->request->data['Inventory']);

    if($save) {
      $this->request->data['InventorySub']['inventoryId'] = $this->Inventory->getLastInsertId();
      $this->Inventory->InventorySub->save($this->request->data['InventorySub']);
    }

    $response = $save;

    $this->set(array(
      'response'    => $response,
      '_serialize'  => 'response'
    ));
  }

  public function edit($id = null) {
    $this->request->data['Inventory']['id'] = $id;
    $save = $this->Inventory->validSave($this->request->data['Inventory']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->Inventory->hide($id)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Inventory has been deleted.',
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Inventory cannot be delete this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function api_consumptions() {
    
    $conditions = array();
    $conditions['Inventory.visible'] = true;
    
    // check department
    if (isset($this->request->query['code'])) {
      $code = $this->request->query['code'];
      $conditions['Inventory.departmentId'] = $this->Inventory->Department->get($code);
    } else {
      $code = null;
    }
    
    $date = date('Y-m-d',strtotime($this->request->query['date']));

    $data = $this->Inventory->find('all',array(
      'contain' => array(
        'Department',
        'InventorySub'  =>  array(
          'conditions'  =>  array(
            'InventorySub.visible'      => true,
            'InventorySub.created LIKE' => "$date%",
            'InventorySub.type'         => false
          )
        ),
      ),
      'conditions' => $conditions
    ));
    
    // transform data
    $cons = array();
    foreach ($data as $con) {
      
      if(!empty($con['InventorySub'])) {
        
        $delivered = 0;
        foreach ($con['InventorySub'] as $sub) {
          $delivered +=  $sub['quantity'];
        }
        
        $cons[] = array(
          'id'          => $con['Inventory']['id'],
          'name'        => $con['Inventory']['name'],
          'description' => $con['Inventory']['description'],
          'unit'        => strtoupper($con['Inventory']['unit']),
          'consumed'    => $delivered
          
        );
      }
    }
    
    $response = array(
      'ok'   => true,
      'data' => $cons,
      'date' => $date
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

}