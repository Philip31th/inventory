<?php 
class DashboardController extends AppController {
	  
	public $uses = array(
	 'ReservationRoom',
	 'Reservation',
	 'Folio',
	 'Room',
  );
	public $layout = null;
	public $components = array('Paginator', 'RequestHandler');
	
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
    $dateNow = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])):date('Y-m-d');
    $edate   = date('Y-m-d', strtotime('-1day',strtotime($dateNow)));

    // available rooms
    $availableRooms = $this->Room->find('all', array(
      'contain' => array(
        'Accomodation' => array(
          'name'
        ),
        'RoomType' => array(
          'name', 
          'rate'
        ),
        'Folio' => array(
          'id',
          'arrival', 
          'departure',
          'closed',
          'transferedBills',
          'Guest'  => array(
            'lastName', 
            'firstName', 
            'middleName'
          ),
          'conditions'=> array(
		        'OR' => array(
              'Folio.arrival   <=' => $dateNow,
              'Folio.departure >=' => $edate,
			      ),
			      'closed' => false,
			      'visible' => true
          )
        ),
        'ReservationRoom'=>array(
          'id',
          'arrival',
          'departure',
          'Reservation'=>array(
            'lastName', 
            'firstName', 
            'middleName'
          ),
          'conditions'=>array(
		        'OR'=>array(
              'ReservationRoom.arrival   <=' => $dateNow,
              'ReservationRoom.departure >=' => $edate,
			      ),
			      'closed'    => false,
			      'checkedIn' => true
          )
        ),
      ),
      'order'=>array('Room.name'=>'ASC'),
      'conditions'  =>  array(
        'Room.visible'  =>  true
      )
    ));
    foreach($availableRooms as $i=>$room){
      if(count($room['Folio']) > 0 ){
        unset($availableRooms[$i]);
      }
    }
    $availableRooms = array_values($availableRooms);
    // .available rooms

    
    // incoming guest
    $incomingList = $this->Reservation->find('all', array(
      'contain'=>array(
        'ReservationRoom'=>array(
          'Folio'=>array('Guest'),
          'Room'=>array(
            'RoomType',
            'conditions'=>array(
              'Room.visible'=>true  
            )
          ),
          'conditions'=>array(
            'ReservationRoom.checkedIn'=>false,
            'ReservationRoom.arrival'=>date('Y-m-d'),
            'ReservationRoom.visible'=> true
          )
         )
      ),
      'conditions'=>array(
        'Reservation.closed'=>false,
        'Reservation.visible' => true
      )
    ));
    $incomingGuests = array();
    foreach($incomingList as $reservation){
      foreach($reservation['ReservationRoom'] as $room){
        $incomingGuests[] = array(
          'id'=>$room['id']
        );
      }
    }
    // .incoming guest
    
    
    // outgoing guest
    $conditions = array();
    $conditions['ReservationRoom.checkedIn'] = true;
    $conditions['ReservationRoom.checkOutDate'] = null;
    $conditions['ReservationRoom.departure <='] = $edate;
    $conditions['ReservationRoom.visible'] = true;

    $outgoingList = $this->ReservationRoom->find('all', array(
      'contain'=>array(
        'Room'=> array(
            'RoomType',          
            'conditions'  =>  array('Room.visible' => true)
          ),
        'Reservation' => array('conditions'=>array('Reservation.visible'=>true)),
        'Folio'=>array(
          'Guest' => array(
            'conditions'=>array('Guest.visible'=>true)
           ),
          'conditions'=>array('Folio.visible'=>true)
        )
      ),
      'conditions'=>$conditions
    ));
    $outgoingGuests = array();
    foreach($outgoingList as $data){
      $outgoingGuests[] = array(
        'id'=>$data['ReservationRoom']['id']
      );
    }
  // .outgoing guests
    
    
    // occupied rooms
    $occupiedRooms = $this->Folio->find('all', array(
      'conditions'=>array(
        'Folio.closed'=>false,
        'Folio.arrival <='=>$dateNow,
        'Folio.departure >'=>$edate,
        'Folio.visible'    => true
      )
    ));
    
    foreach ($occupiedRooms as $i=>$occupied) {
      $occupiedRooms[$i] = array(
        'id'=>$occupied['Folio']['id']
      );
    }
    // .occupied rooms
    
    $availableRooms = count($availableRooms) - count($incomingGuests);
    $incomingGuests = count($incomingGuests);
    $outgoingGuests = count($outgoingGuests);
    $occupiedRooms  = count($occupiedRooms);
    // $occupancyRate  = ($occupiedRooms / ($availableRooms )) * 100;
    $occupancyRate  = ($occupiedRooms + $outgoingGuests ) / ($availableRooms) * 100;
    
    $data = array(
      'availableRooms' => $availableRooms,
      'incomingGuests' => $incomingGuests,
      'outgoingGuests' => $outgoingGuests,
      'occupiedRooms'  => $occupiedRooms,
      'uncleanedRooms' => 0,
      'occupancyRate'  => number_format($occupancyRate),
    );


    $response = array(
      'response'=>true,
      'result'=>$data,
      'test' => $edate,
      'data' => $outgoingList
    );
    
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
	}
  
}
