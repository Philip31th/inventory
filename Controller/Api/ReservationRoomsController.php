<?php 
class ReservationRoomsController extends AppController {
	
	public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
		
	}

	public function view($id = null) {

	}

  public function add() {
    $save = $this->ReservationRoom->saveMany($this->request->data['ReservationRoom']);
    if ($save) {
      $response = array(
        'ok'  => true,
        'msg' => 'Reservation of room has been saved.',
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Reservation of room cannot be saved this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {

  }

  public function delete($id = null) {
    if ($this->superUser())
      $delete = $this->ReservationRoom->delete($id);
    else
      $delete = $this->ReservationRoom->hide($id);

    if ($delete) {
      $response = array(
        'ok'  => true,
        'msg' => 'Reservation of room has been deleted.',
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Reservation of room cannot be deleted this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

}
