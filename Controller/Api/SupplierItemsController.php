<?php 
class SupplierItemsController extends AppController {
  
  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }

  public function add() {

    if ($this->SupplierItem->save($this->request->data['SupplierItem'])) {
      $response = array (
        'ok'  => true,
        'msg' => 'Supplier Item has been saved',
        'data'=>  $this->request->data['SupplierItem']
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Supplier Item cannot be save this time'
      );
    }
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));

  }

  public function edit($id = null) {      

    // if ($this->SupplierItem->saveMany($this->request->data['SupplierItem'])) {
      $response = array (
        'ok'  => true,
        'msg' => 'SupplierItem has been saved',
        'data'=>  $this->request->data['SupplierItem']
      );
    // } else {
    //   $response = array(
    //     'ok'  => false,
    //     'msg' => 'SupplierItem cannot be save this time'
    //   );
    // }
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->SupplierItem->hide($id)) {
       $response = array(
        'ok'      =>  true,
        'message' =>  'SupplierItem has been deleted.'
      );
      $this->set(array(
        'response'    =>  $response,
        '_serialize'  =>  'response'
      ));
    }
  }
}