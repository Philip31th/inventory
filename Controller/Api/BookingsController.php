<?php 
class BookingsController extends AppController {
  
  public $uses = array('Reservation','Guest');
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {

    // default conditions
    $conditions = array();
    $conditions['Reservation.closed'] = false;
    if (!$this->superUser())
      $conditions['Reservation.visible'] = true;

    // search conditions
    if (isset($this->request->query['search'])) {
      if (!empty($this->request->query['search'])) {
        $search = date('Y-m-d', strtotime($this->request->query['search']));
        $conditions['Reservation.arrival <='] = $search;
        $conditions['Reservation.departure >='] = $search;
      }
    }

    // paginate data
    $paginatorSettings = array(
      'contain'=>array(
        'ReservationRoom'=>array(
          'conditions'=>array(
            'ReservationRoom.visible'=>true
          )
        )
      ),
      'conditions'=>$conditions 
    );
    $modelName = 'Reservation';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $reservations = array();
    foreach($tmpData as $data) {
      $reservation      = $data['Reservation'];
      $reservationRooms = $data['ReservationRoom'];
      $rooms = count($reservationRooms);
      $checkedInRooms = 0;
      $status = '';

      foreach ($reservationRooms as $rr) {
        if ($rr['checkedIn'])
          $checkedInRooms += 1;
      }

      if (date('Y-m-d', strtotime($reservation['arrival'])) < date('Y-m-d') && ($rooms - $checkedInRooms) != 0) {
        $status = 'expired';
        $departure = date('m/d/Y', strtotime('+1day', strtotime($reservation['departure'])));
        $days = '';
      }

      if (date('Y-m-d', strtotime('+1day', strtotime($reservation['departure']))) != date('Y-m-d') && ($rooms - $checkedInRooms) == 0) {
        $departure = date('m/d/Y');
        $days = round(((strtotime(date('Y-m-d')) - (strtotime('+1day',strtotime($reservation['departure']))))/86400),0);
      } 

      if (date('Y-m-d', strtotime('+1day', strtotime($reservation['departure']))) >= date('Y-m-d')) {
        $departure = date('m/d/Y', strtotime('+1day', strtotime($reservation['departure'])));
        $days = '';
      } 


      $reservations[] = array(
        'id'             => $reservation['id'],
        'name'           => $reservation['firstName'] . ' ' . $reservation['lastName'],
        'rooms'          => $rooms,
        'arrival'        => date('m/d/Y', strtotime($reservation['arrival'])),
        'departure'      => $departure,
        'extensions'      => $days, 
        'multiple'       => $rooms > 1? true : false,
        'checkedInRooms' => $checkedInRooms,
        'pendingRooms'   => $rooms - $checkedInRooms,
        'source'         => $reservation['bookingSource'],
        'status'         => $status, 
        'visible'        => $reservation['visible'],
      );
    }
    
    $response = array(
      'ok'        => true,
      'data'      => $reservations,
      'paginator' => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $reservation = $this->Reservation->find('first', array(
      'contain' => array(
        'ReservationRoom' => array(
          'Folio' => array(
            'conditions' => array(
              'Folio.visible' => true
            ),
            'Guest'
          ),
          'conditions' => array(
            'ReservationRoom.visible' => true
          ),
          'Room' => array(
            'id', 'name', 'rate',
            'RoomType' => array(
              'name', 'rate'
            ),
            'Accomodation' => array(
              'name',
              'number',
            ),
            'conditions' => array(
              'Room.visible' => true
            )
          )
        ),
        'Company'
      ),
      'conditions' => array(
        'Reservation.id'      => $id,
        'Reservation.visible' => true
      )
    ));

    // transform data
    $reservation['Reservation']['arrival'] = date('m/d/Y', strtotime($reservation['Reservation']['arrival']));
    $reservation['Reservation']['departure'] = date('m/d/Y', strtotime('+1day', strtotime($reservation['Reservation']['departure'])));

    $status = '';
    foreach ($reservation['ReservationRoom'] as $k => $data) {
      $reservation['ReservationRoom'][$k]['arrival'] = fdate($data['arrival']);

      if (date('Y-m-d',strtotime($reservation['Reservation']['arrival'])) < date('Y-m-d') && !$reservation['ReservationRoom'][$k]['checkedIn']) {
         $reservation['ReservationRoom'][$k]['status'] = 'expired';
         $departure = date('m/d/Y', strtotime('+1day', strtotime($data['departure'])));
         $reservation['ReservationRoom'][$k]['departure'] = $departure;
      }  else {
        $reservation['ReservationRoom'][$k]['status']='';
      }

      if (date('Y-m-d', strtotime('+1day', strtotime($data['departure']))) != date('Y-m-d')) {
        $departure = date('m/d/Y');
        $days = round(((strtotime(date('Y-m-d')) - (strtotime('+1day',strtotime($data['departure']))))/86400),0);
        $reservation['ReservationRoom'][$k]['departure'] = $departure;
        $reservation['Reservation']['nights'] = (int)$reservation['Reservation']['nights'] + $days;  
      } 

      if (date('Y-m-d', strtotime('+1day', strtotime($data['departure']))) > date('Y-m-d')) {
        $departure = date('m/d/Y', strtotime('+1day', strtotime($data['departure'])));
        @$days = '';
        $reservation['ReservationRoom'][$k]['departure'] = $departure;
        $reservation['Reservation']['nights'] = round(((strtotime($reservation['Reservation']['departure']) - strtotime($reservation['Reservation']['arrival']))/86400),0);
      } 

      if (empty($data['Folio'])) {
        $reservation['Reservation']['nights'] = round(((strtotime($reservation['Reservation']['departure']) - strtotime($reservation['Reservation']['arrival']))/86400),0);
      } 

      // if (date('Y-m-d', strtotime('+1day', strtotime($data['']))) == date('Y-m-d')) {
      //   $reservation['Reservation']['nights'] = round(((strtotime($reservation['Reservation']['departure']) - strtotime($reservation['Reservation']['arrival']))/86400),0);
      // }

      @$reservation['ReservationRoom'][$k]['extensions'] = @$days; 
      $reservation['ReservationRoom'][$k]['nights'] = $reservation['Reservation']['nights'];  

      $reservation['ReservationRoom'][$k]['Room']['rate'] = number_format($reservation['ReservationRoom'][$k]['Room']['rate']*$reservation['Reservation']['nights'],2);
    }

    if (date('Y-m-d',strtotime($reservation['Reservation']['arrival'])) < date('Y-m-d')) {
      $reservation['Reservation']['status'] = 'expired';
      $reservation['Reservation']['departure'] = date('m/d/Y', strtotime('+1day', strtotime($data['departure'])));
      @$days = '';
    } else 

     @$reservation['Reservation']['gender'] = @$reservation['Reservation']['gender'];
     
     $reservation['Reservation']['arrivalNot'] = false;
     if (date('Y-m-d',strtotime($reservation['Reservation']['arrival'])) != date('Y-m-d')) {
      $reservation['Reservation']['arrivalNot'] = true;
     } 

     @$reservation['Reservation']['identity'] = @$reservation['Reservation']['identity'];
    $response = array(
      'ok'   => true,
      'data' => @$reservation,
      'reservation_room'=>$reservation['ReservationRoom']
    );
    
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function add() {
    // transform arrival and departure date
    $this->request->data['Reservation']['arrival'] = fdate($this->request->data['Reservation']['arrival'], 'Y-m-d');
    $this->request->data['Reservation']['departure'] = date('Y-m-d', strtotime('-1day', strtotime($this->request->data['Reservation']['departure'])));
    
    if (!empty($this->request->data['Company'])) {
      if($this->request->data['Company']['id']=='') {
        $this->Reservation->Company->save($this->request->data['Company']);
        $this->request->data['Reservation']['companyId'] = $this->Reservation->Company->getLastInsertId();
      } else {
        $this->request->data['Reservation']['companyId'] = $this->request->data['Company']['id'];
      }
    }

    // reservation data
    $reservation = $this->request->data['Reservation'];

    // reservation rooms data
    $reservationRooms = array();
    foreach($this->request->data['ReservationRoom'] as $room){
      $reservationRooms[] = array(
        'roomId'    => $room['id'],
        'arrival'   => $this->request->data['Reservation']['arrival'],
        'departure' => $this->request->data['Reservation']['departure']
      );
    }

    $save = $this->Reservation->saveAll(array(
      'Reservation'     => $reservation,
      'ReservationRoom' => $reservationRooms,

    ));

    if ($save) {
      $response = array(
        'ok'  => true,
        'msg' => 'Booking has been saved.',
        'data' => $save
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Booking cannot be saved this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    $this->request->data = $this->request->input('json_decode', true);
   
    // transform date
    $this->request->data['Reservation']['arrival']   = date('Y-m-d',strtotime($this->request->data['Reservation']['arrival']));
    $this->request->data['Reservation']['departure'] = date('Y-m-d',strtotime('-1day',strtotime($this->request->data['Reservation']['departure'])));
    
    // if company kalalagay lng 
    if (!empty($this->request->data['Company'])) {
      if ($this->request->data['Company']['id'] == '' && $this->request->data['Company']['name'] != '') {
          $this->Reservation->Company->save($this->request->data['Company']);
          $this->request->data['Reservation']['companyId'] = $this->Reservation->Company->getLastInsertId();
      } else {
        if ($this->request->data['Company']['id']!='' && $this->request->data['Company']['name']=='') {
          $this->request->data['Company']['id'] = null;
        }
        // update company details
        @$this->request->data['Reservation']['companyId'] = $this->request->data['Company']['id'];
        $this->Reservation->Company->save($this->request->data['Company']);
      }
    }  
    
    if($this->Reservation->save($this->request->data)){

      // if checked in , update guest data if theres update there
      if(isset($this->request->data['Reservation']['checked'])) {
        $guestId = $this->Reservation->getGuestId($id);

        $guest = $this->Guest->save(array(
          'id'         => $guestId,
          'title'      => $this->request->data['Reservation']['title'],
          'email'      => $this->request->data['Reservation']['email'],
          'lastName'   => $this->request->data['Reservation']['lastName'],
          'firstName'  => $this->request->data['Reservation']['firstName'],
          'middleName' => $this->request->data['Reservation']['middleName'],
          'gender'     => $this->request->data['Reservation']['gender'],
          'nationality'=> $this->request->data['Reservation']['nationality'],
          'address'    => $this->request->data['Reservation']['address'],
          'companyId'  => $this->request->data['Reservation']['companyId'],
          'phone'      => $this->request->data['Reservation']['phone'],
          'mobile'     => $this->request->data['Reservation']['mobile']
        ));
      } else {
        $guest = '';
      }

      $response = array(
        'ok'       => true,
        'msg'      =>'Reservation has been saved.',
        'tst' => $guest
      );
    }
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->superUser()) {
      $delete = $this->Reservation->delete($id);
      $deleteReservationRoom = $this->Reservation->ReservationRoom->deleteAll(array(
        'ReservationRoom.reservationId' => $id
      ), false);
    } else {
      $delete = $this->Reservation->hide($id);
      $deleteReservationRoom = $this->Reservation->ReservationRoom->updateAll(array(
        'ReservationRoom.visible' => false
      ), array(
        'ReservationRoom.reservationId' => $id
      ));
    }

    if ($delete AND $deleteReservationRoom) {
      $response = array(
        'ok'  => true,
        'msg' => 'Reservation has been deleted.'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Reservation cannot be deleted this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function available() {
    $availableRooms = $this->Room->query("
      SELECT
        Room.id,
        Room.code,
        RoomType.name,
        RoomType.rate,
        Accomodation.name
      FROM
        (rooms as Room LEFT JOIN
        room_types as RoomType ON Room.room_type_id = RoomType.id) LEFT JOIN
        accomodations as Accomodation ON Accomodation.id = RoomType.accomodation_id
    ");
    
    foreach($availableRooms as $key=>$room){
      $availableRooms[$key] = array(
        'id'=>$room['Room']['id'],
        'code'=>$room['Room']['code'],
        'type'=>$room['RoomType']['name'],
        'rate'=>$room['RoomType']['rate'],
        'accomodation'=>$room['Accomodation']['name']
      );
    }
    
    $response = array(
        'response'=>true,
        'message'=>'Getting data successful.',
        'result'=>$availableRooms
      );
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function createFolio() {
    echo 'test';
  }

}

