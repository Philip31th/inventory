<?php
class UsersController extends AppController {

  public $layout = null;
  
  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }
  
  public function test () {
    $this->autoRender = false;
    var_dump($this->Session->read('Auth.User')) ;
  }

  public function index () {
    // default page 1
    $date = isset($this->request->query['search'])? date('Y-m-d H:i:s', strtotime($this->request->query['search'])):date('Y-m-d 00:00:00');
    
    // default conditions
    $conditions = array();
    $conditions['User.visible'] = true;
    $conditions['User.role NOT LIKE'] = 'superuser';

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];

      $conditions['OR'] = array(
        'User.username LIKE'  => "%$search%",
        'User.firstName LIKE' => "%$search%",
        'User.lastName LIKE'  => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'conditions' => $conditions,
      'order' => array(
        'User.firstName' => 'ASC',
        'User.lastName'  => 'ASC',
      )
    );
    $modelName = 'User';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData    = $this->Paginator->paginate($modelName);
    $paginator  = $this->request->params['paging'][$modelName];
    
    $users = array();
    foreach ($tmpData as $data) {
      $user = $data['User'];

      $users[] = array(
        'id'        => $user['id'],
        'username'  => $user['username'],
        'name'      => $user['firstName'] . ' ' . $user['lastName'],
        'role'      => $user['role'],
        'developer' => $user['developer'],
      );
    }

    $response = array(
      'ok'        => true,
      'data'      => $users,
      'paginator' => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function view($id = null) {
    $user = $this->User->find('first', array(
      'contain' => array(
        'UserPermission' => array(
          'Permission'
        )
      ),
      'conditions' => array(
        'User.id' => $id
      )
    ));
    unset($user['User']['password']);

    // user permission
    $disabledPermission = array();
    foreach ($user['UserPermission'] as $k => $data) {
      $disabledPermission[] = $data['Permission']['id'];
      $user['UserPermission'][$k] = array(
        'id'     => $data['id'],
        'module' => $data['Permission']['module'],
        'name'   => $data['Permission']['name'],
      );
    }

    // permission selection
    $permissionSelection = $this->User->UserPermission->Permission->find('all', array(
      'conditions' => array(
        'Permission.id !=' => $disabledPermission
      )
    ));
    foreach ($permissionSelection as $k => $data) {
      $permissionSelection[$k] = array(
        'id'     => $data['Permission']['id'],
        'module' => $data['Permission']['module'],
        'name'   => $data['Permission']['name'],
      );
    }
    $user['PermissionSelection'] = $permissionSelection;

    $response = array(
      'ok'        => true,
      'data'      => $user,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function add () {
    $save = $this->User->validSave($this->request->data['User']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function edit ($id = null) {
    if (isset($this->request->data['User']['password'])) {
      if ($this->request->data['User']['password'] == '')
        unset($this->request->data['User']['password']);
    }

    $save = $this->User->validSave($this->request->data['User']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function delete ($id = null) {
    if ($this->User->hide($id)) {
      $response = array(
        'ok'  => true,
        'msg' => 'User has been deleted.'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'User cannot be deleted this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
    
  }
  
  public function high_level ($id = null) {
   $data = $this->User->find('first', array(
    'conditions' => array(
      'User.id' => $id
    )
   ));
   
    $response = array(
      'ok'   => true,
      'data' => $data['User']['high_level']
    );
  
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

}
