<?php
class EmployeeLeavesController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  
  public function add() {
    
    if($this->EmployeeLeave->save(array(
        'employeeId' => $this->request->data['employeeId'],
        'startDate'  => date('Y-m-d',strtotime($this->request->data['startDate'])),
        'endDate'    => date('Y-m-d',strtotime($this->request->data['endDate'])),
        'leaveType'  => $this->request->data['leaveType'],
        'purpose'    => $this->request->data['purpose']
      ))){
      $response = array(
        'ok'  => true,
        // 'msg' => 'Employee Leaves succesfully saved.',
        'data' => $this->request->data
      );
    } else {
      $response = array(
        'ok'  => false,
        // 'msg' => 'Employee Leaves  cannot be save this time.'
      );
    }
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
    public function edit($id = null) {

     if ($this->EmployeeLeave->save(array(
        'id' => $this->request->data['id'],
        'startDate'  => date('Y-m-d',strtotime($this->request->data['startDate'])),
        'endDate'    => date('Y-m-d',strtotime($this->request->data['endDate'])),
        'leaveType'  => $this->request->data['leaveType'],
        'purpose'    => $this->request->data['purpose']
      ))) {
      $response = array(
        'ok'  => true,
        // 'msg' => 'Employee Remarks has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        // 'msg' => 'Employee Remarks  cannot be save this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
 
   public function delete($id = null) {
    if ($this->EmployeeLeave->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
}
