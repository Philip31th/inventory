<?php
class EmployeesController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  
  // public $uses = array(');
  
  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
    
    // default conditions
    $conditions = array();
    $conditions['Employee.visible'] = true;

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Employee.employeeNumber LIKE'  => "%$search%",
        'Employee.lastName LIKE'        => "%$search%",
        'Employee.firstName LIKE'       => "%$search%",
        'Employee.position LIKE'        => "%$search%",
        'Department.name LIKE'          => "%$search%",
      );
    }
    
    // paginate data
    $paginatorSettings = array(
      'contain'    => array(
        'Department',
        'EmployeePerformance'
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Employee.name' => 'ASC'
      )
    );
    $modelName = 'Employee';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator   = $this->request->params['paging'][$modelName];
    
    // transform data
    $employees = array();
    foreach ($tmpData as $data) {
      $employee = $data['Employee'];
      $employees[] = array(
        'id'          => $employee['id'],
        'number'      => $employee['employeeNumber'],
        'name'        => $employee['firstName'] . ' ' . $employee['lastName'],
        'department'  => $data['Department']['name'],
        'position'    => $employee['position'],
      );
    }

    $response = array(
      'ok'         => true,
      'data'       => $employees,
      'test'  =>  $tmpData,
    
      'paginator'  => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $data = $this->Employee->find('first', array(
      'contain' =>  array(
        'Department',
        'EmployeePerformance' => array(
          'conditions' => array(
            'EmployeePerformance.visible' => true  
          )  
        ),
        'EmployeeLeave' => array(
          'conditions' => array(
            'EmployeeLeave.visible' => true  
          )  
        )
      ),
      'conditions' => array(
        'Employee.id' => $id
       )
    ));
    
    $performances = array();
    foreach ($data['EmployeePerformance'] as $performance) {
      
      $performances[] = array(
        'id'           => $performance['id'],
        'remarks'      => $performance['remarks'],
        'employeeId'   => $performance['employeeId'],
        'date'         => date('M d, Y',strtotime($performance['created']))
      );
    }
    
    $leaves = array();
    foreach ($data['EmployeeLeave'] as $leave) {
      
      $leaves[] = array(
        'id'           => $leave['id'],
        'startDate'    => date('M d, Y',strtotime($leave['startDate'])),
        'endDate'      => date('M d, Y',strtotime($leave['endDate'])),
        'start_date'   => date('m/d/Y',strtotime($leave['startDate'])),
        'end_date'     => date('m/d/Y',strtotime($leave['endDate'])),
        'leaveType'    => $leave['leaveType'],
        'purpose'      => $leave['purpose']
      );
    }
    
    $data['EmployeePerformance'] = @$performances;
    $data['EmployeeLeave'] = @$leaves;

    $response = array(
      'ok'   => true,
      'data'  => $data,
      // 'datas' => $datas,
      // 'msg'  => null,
    );

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function add() {
     // transform data NOT SURE what is this 
		if(isset($_SERVER["CONTENT_TYPE"]) and strpos($_SERVER["CONTENT_TYPE"], "application/json") !== false)
      $_POST = array_merge($_POST, (array) json_decode(trim(file_get_contents('php://input')), true));
		$this->request->data = json_decode($_POST['data'], true);
		
    // transform employee data
    $employeeImage = @$_FILES['attachment']['name'];
    if (!empty($employeeImage)) {
      $attachmentExt = pathinfo($employeeImage, PATHINFO_EXTENSION);
      $this->request->data['Employee']['image'] =  $this->Employee->nextId() . '.' . $attachmentExt;
    }
    
     $this->request->data['Employee']['dateHired'] = date('Y-m-d', strtotime($this->request->data['Employee']['dateHired']));

     if($this->Employee->save($this->request->data)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Employee has been added.',
        'data' => $this->request->data

      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Employee cannot be saved this time.'
      );
    }
    
    // upload attachment
    if (!empty($employeeImage)) {
      $path = "uploads/employees";
      if(!file_exists('uploads')) mkdir('uploads');
      if(!file_exists('uploads/employees')) mkdir('uploads/employees');
      if(!file_exists($path)) mkdir($path);
      
      move_uploaded_file($_FILES['attachment']['tmp_name'], $path . '/' . $this->request->data['Employee']['image']);
    }

    $this->set(array(
        'response'   => $response,
        '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    
    // transform data NOT SURE what is this 
		// if(isset($_SERVER["CONTENT_TYPE"]) and strpos($_SERVER["CONTENT_TYPE"], "application/json") !== false)
  //     $_POST = array_merge($_POST, (array) json_decode(trim(file_get_contents('php://input')), true));
		//   $this->request->data = json_decode($_POST['data'], true);
		
  //     // transform employee data
  //     $employeeImage = @$_FILES['attachment']['name'];
  //     if (!empty($employeeImage)) {
  //       $attachmentExt = pathinfo($employeeImage, PATHINFO_EXTENSION);
  //       $this->request->data['Employee']['image'] =  $id . '.' . $attachmentExt;
  //     }
      
    
              
     if($this->Employee->save($this->request->data)) {
      $response = array(
        'ok'   => true,
        'msg'  => 'Employee has been added.',
        'data' => $this->request->data['Employee']
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Employee cannot be saved this time.'
      );
    }

    $this->set(array(
        'response'   => $response,
        '_serialize' => 'response'
    ));
    
    
    
  }
  
  public function delete($id = null) {
    if ($this->Employee->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
    
    
    
  
  
}
