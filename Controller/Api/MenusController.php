<?php 
class MenusController  extends AppController {
  
  public $layout = null;
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
   //  default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
    
     //default conditions
     $conditions = array () ;
     $conditions['Menu.visible'] = true;

    // department code condition
    if (isset($this->request->query['code']))
      $conditions['Department.code LIKE'] = $this->request->query['code'];
 
     //search conditions
     if (isset($this->request->query['search'])) {
      $conditions['OR'] = array(
         'Menu.name LIKE'=> '%' . $this->request->query['search'] . '%'
       );
     }
    
     //paginate data
     $paginatorSettings = array(
      'contain' =>(
        'Department'
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
       'Menu.name' => 'ASC'
       )
     );
     $modelName = 'Menu';
     $this->Paginator->settings = $paginatorSettings;
     $tmpData   = $this->Paginator->paginate($modelName);
     $paginator = $this->request->params['paging'][$modelName];
    
     // transform data here
     $menus = array();
     foreach ($tmpData as $data) {
     $menu = $data['Menu'];
     $department = $data['Department'];
     $menus[] = array(
       'id'          => $menu['id'],
       'name'        => $menu['name'],
       'code'        => $menu['code'],
       'amount'      => $menu['amount'],
       'department'  => $department['name'],

     );
   }
     
    $response = array(
      'ok'   => true,
      'data'       => $menus,
      'paginator'  => $paginator,
    );

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $data = $this->Menu->findById($id);

    $data['Menu']['amount'] = (float)$data['Menu']['amount'];

    $response = array(
      'ok'   => true,
      'data' => $data
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
      
    ));
  }

  public function add() {
    $this->request->data['Menu']['departmentId'] = $this->Menu->Department->get($this->request->data['Menu']['departmentId']);
    $save = $this->Menu->validSave($this->request->data['Menu']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    $this->request->data['Menu']['departmentId'] = $this->Menu->Department->get($this->request->data['Menu']['departmentId']);
    $save = $this->Menu->validSave($this->request->data['Menu']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->Menu->hide($id)) {
      $response = array(
        'ok'   => true,
        'msg'  => 'Menu has been deleted.',
      );
    } else {
      $response = array(
        'ok'   => false,
        'msg'  => 'Menu cannot be deleted this time.',
      );
    }
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

    
}
