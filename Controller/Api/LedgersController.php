<?php
class LedgersController extends AppController {
  
	public $layout = null;
	public $uses = array('TransactionPayment', 'Guest');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  
  public function index() {
    
    // search
    if (isset($this->request->query['search'])) {
      $search = strtolower($this->request->query['search']);
    } else {
      $search = '';
    }
    
    $qry = "
      SELECT
        Company.name,
        SUM(TransactionPayment.amount) as totalAmount
      FROM
        (((((transaction_payments AS TransactionPayment LEFT JOIN
        transactions AS Transaction ON Transaction.id = TransactionPayment.transactionId) LEFT JOIN
        folio_transactions as FolioTransaction ON FolioTransaction.transactionId = Transaction.id) LEFT JOIN
        folios as Folio ON Folio.id = FolioTransaction.folioId) LEFT JOIN
        guests as Guest ON Guest.id = Folio.guestId)
        LEFT JOIN companies as Company ON Company.id = Guest.companyId)
      WHERE
        TransactionPayment.visible = true AND
        TransactionPayment.paymentType = 'send bill' AND
        Company.visible = true AND
        Company.name  = $search
      GROUP BY
        Company.name
    ";
    
    $tmpData = $this->TransactionPayment->query($qry);

    $companies = array();
    $company = array();
    $totalAmount = 0;
    foreach ($tmpData as $data) {
      $company[] = array(
        'company' => properCase($data['Company']['name']),
        'coy'     => strtolower($data['Company']['name']),
        'credit'  => $data[0]['totalAmount'], 
        'debit'   => 0
      );
      
      $totalAmount += $data[0]['totalAmount'];
    }
      
      $companies['company'] = $company;
      $companies['totalDebit'] = 0;
      $companies['totalCredit'] = $totalAmount;
      $companies['totalBalance'] = $totalAmount - 0;
  
    $response = array(
      'ok'   => true,
      'data' => $companies
     );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
    
  public function api_view() {
    $companyName = strtolower($this->request->query['company']);
    
    $qry = "
      SELECT
        TransactionPayment.orNumber AS refNumber,
        TransactionPayment.amount,
        Transaction.particulars,
        Transaction.date,
        Transaction.created,
        Folio.id,
        Folio.arrival,
        Folio.departure,
        Company.name
      FROM
        (((((transaction_payments AS TransactionPayment LEFT JOIN
        transactions AS Transaction ON Transaction.id = TransactionPayment.transactionId) LEFT JOIN
        folio_transactions as FolioTransaction ON FolioTransaction.transactionId = Transaction.id) LEFT JOIN
        folios as Folio ON Folio.id = FolioTransaction.folioId) LEFT JOIN
        guests as Guest ON Guest.id = Folio.guestId)
        LEFT JOIN companies as Company ON Company.id = Guest.companyId)
      WHERE
        TransactionPayment.visible = true AND
        TransactionPayment.paymentType = 'send bill' AND
        Company.name = $companyName
      GROUP BY
        Transaction.id
    ";
  
    $tmpData = $this->TransactionPayment->query($qry);
  
    $transactions = array();
    $totalAmount = 0;
    $payments = array();
    foreach ($tmpData as $data) {
      
      // search 
      $dates = '';
      if (!strpos($data['Transaction']['particulars'],'accomodation')){
        $dates =  date('M d Y',strtotime($data['Transaction']['created']));
      } else {
        $days   = date('d',strtotime($data['Folio']['arrival'])) . '-' .date('d',strtotime('+1day',strtotime($data['Folio']['departure'])));
        $month  = date('M',strtotime($data['Folio']['arrival']));
        $year   = date('Y',strtotime($data['Folio']['arrival']));
        $dates   = $month . ' ' . $days . ' ' . $year;
      }
      
          $transactions[] = array(
          'amount'        => $data['TransactionPayment']['amount'],
          'particulars'   => $data['Transaction']['particulars'],
          'date'          => $dates,
          'company'       => $data['Company']['name'],
          'debit'         => 0,
          // 'credit'        => $data['TransactionPayment']['amount']
        );
        
        $totalAmount += $data['TransactionPayment']['amount'];
        $company = $data['Company']['name'] ;
    }
    
    $payments['debit']    = 0;
    $payments['payments'] = $totalAmount;
    $payments['company'] = $company;
    
    $response = array(
      'ok'   => true,
      'data' => $transactions,
      // 'tmpData' => $tmpData,
      'totalAmount' => $payments
    );
    
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }
}