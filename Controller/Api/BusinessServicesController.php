<?php
class BusinessServicesController extends AppController {

  public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    $conditions['BusinessService.visible'] = true;
    
    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];

      $conditions['OR'] = array(
        'BusinessService.name LIKE'  => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'contain' => array(
        'Business'
      ),
      'conditions' => $conditions,
      'limit'      => 2500,
      'page'       => $page,
      'order'      => array(
        // 'Business.name' => 'ASC',
        'BusinessService.name' => 'ASC'
      )
    );
    $modelName = 'BusinessService';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];


    // transform data
    $settings = array();
    foreach ($tmpData as $data) {
      $service = $data['BusinessService'];
      $business = $data['Business'];

      $settings[] = array(
        'id'        => $service['id'],
        'business'  => $business['name'],
        'name'      => $service['name'],
        'value'     => $service['value'],
      );
    }

    $response = array(
      'ok'        => true,
      'data'      => $settings,
      'paginator' => $paginator
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $service = $this->BusinessService->findById($id);
    $response = array(
      'ok'   => true,
      'data' => $service,
    );

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function add() {
    $save = $this->BusinessService->validSave($this->request->data['BusinessService']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    $save = $this->BusinessService->validSave($this->request->data['BusinessService']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function delete($id = null) {
    if ($this->BusinessService->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
        'msg' => 'Business service has been removed.'
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
}
