<?php 
class ReservationsController extends AppController {
	
	public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }


	public function index() {
    $response = array(
      'ok'   => true,
      'data' => array(),
    );
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
	}

	public function view($id = null) {
    $response = array(
      'ok'   => true,
      'data' => array(),
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
	}

  public function add() {
   // transform arrival and departure date
    $this->request->data['Reservation']['arrival'] = fdate($this->request->data['Reservation']['arrival'], 'Y-m-d');
    $this->request->data['Reservation']['departure'] = date('Y-m-d', strtotime('-1day', strtotime($this->request->data['Reservation']['departure'])));
    
    // company
    if(!empty($this->request->data['Company'])) {
      $coy = strtoupper($this->request->data['Company']['name']);
      // check company if exist
      $companyId = $this->Reservation->Company->get($coy);

      if ($companyId == null) {
        $this->Reservation->Company->save($this->request->data['Company']);
        $this->request->data['Reservation']['companyId'] = $this->Reservation->Company->getLastInsertId();
      } else {
        $this->request->data['Reservation']['companyId'] = $companyId;
      }
         
    }

    // reservation data
    // transform travel agency booking source
    if($this->request->data['Reservation']['bookingSource']=='Travel Agency(TVA)') {
      $this->request->data['Reservation']['bookingSource'] = strtolower((str_replace('(TVA)', '', $this->request->data['Reservation']['bookingSource'])));
    } else {
      $this->request->data['Reservation']['bookingSource'] = strtolower($this->request->data['Reservation']['bookingSource']);
    }

    $reservation = $this->request->data['Reservation'];

    // reservation rooms data
    $reservationRooms = array();
    foreach($this->request->data['ReservationRoom'] as $room){
      $reservationRooms[] = array(
        'roomId'    => $room['id'],
        'arrival'   => $this->request->data['Reservation']['arrival'],
        'departure' => $this->request->data['Reservation']['departure']
      );
    }

    $save = $this->Reservation->saveAll(array(
      'Reservation'     => $reservation,
      'ReservationRoom' => $reservationRooms,
    ));
    $book['reservation'] = $reservation;
    $book['reservationRoom'] = $reservationRooms;
    $book['company'] = @$this->request->data['Company'];
    if ($save) {
      $response = array(
        'ok'  => true,
        'msg' => 'Booking has been saved.',
        'data' => $book
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Booking cannot be saved this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));

  }

  public function edit($id = null) {
    $save = $this->Reservation->save($this->request->data);
   if($save){ 
    $response = array(
      'ok'   => true,
      'msg'  => 'Reservation has been saved'
    );
   }
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {

  }
}
