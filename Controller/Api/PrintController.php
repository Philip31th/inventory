<?php
class PrintController extends AppController {

  public $uses = array('UserLog','Folio','Room','TransactionPayment','RoomType','Transaction');
  
  public function statementofaccount($id) {
    
    $conditions['Folio.id'] = $id;

    $data = $this->Folio->find('first', array(
      'contain'=>array(
        'ReservationRoom' => array(
          'Reservation'
        ),
          'Guest' => array('Company'),
          'Room'=>array('RoomType'),
          'FolioTransaction'=>array(
            'Transaction'=>array(
              'Business' => array(
                'conditions'  =>  array(
                  'Business.visible' => true
                )
              ), 
              'TransactionSub' => array(
                'conditions'  =>  array(
                  'TransactionSub.visible' => true
                )
              ), 
              'TransactionPayment' => array(
                'conditions'  =>  array(
                  'TransactionPayment.visible' => true
                )
              ),
              'TransactionDiscount' => array(
                'conditions'  =>  array(
                  'TransactionDiscount.visible' => true
                )
              ),
              'conditions'  =>  array(
                'Transaction.visible' => true,
                'Transaction.businessId' => 1,
              )
            )
          )
        ),
        'conditions' => $conditions
      ));
    
   $cashiering = array();

    $transactionAmount = 0;
    $paidAmount = 0;
    $totalDiscount = 0;
    $transactions = array();
    $payments = array();
    $totalTransactions = 0;

    // get total nights
    if ($data['Folio']['departure'] != date('Y-m-d')) {
      $departure = strtotime('+1day',strtotime($data['Folio']['departure']));
      $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime($data['Folio']['departure']))) > date('Y-m-d')) {
      $departure = date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure'])));
      $extensions = 0;
    } 

    foreach($data['FolioTransaction'] as $transaction){
      
      if (!empty($transaction['Transaction'])) { 
        // transaction sub
        $amount = 0;
        $items = array();
        
        foreach($transaction['Transaction']['TransactionSub'] as $sub){
          // check if accomodation
          if (strrpos($sub['particulars'],'accomodation')) {
            $sub['amount'] = $sub['amount']*($data['Folio']['nights']+@$extensions);
          }

          $items[] = array(
            'id' => $sub['id'],
            'particulars'=> $sub['particulars'],
            'amount'=>$sub['amount']*$sub['quantity']
          );

          $amount+=$sub['amount']*$sub['quantity'];
        }
        
        // folio payment
        $paid = 0;
        $payments = array();
        foreach($transaction['Transaction']['TransactionPayment'] as $sub) {
          $payments[] = array(
            'orNumber'=>$sub['orNumber'],
            'amount'=>$sub['amount'],
            'date'=>date('m/d/Y', strtotime($sub['date']))
          );
          $paid += $sub['amount']-$sub['change'];
        }
        
        $discountType = 0;
        $discountValue = 0;
        $discount = 0;
        $balance = 0; 

        foreach ($transaction['Transaction']['TransactionDiscount'] as $sub) {
          $discount = $sub['type']?($sub['value']):($amount*($sub['value']/100));
          $totalDiscount += $discount;
          $balance = (($amount-$paid) - $discount) <= 0? 0:(($amount-$paid) - $discount);

          $discountType = $sub['type'];
          $discountValue = $sub['value'];
        }    

        $transactions[] = array(
          'id'              =>  $transaction['Transaction']['id'],
          'code'            =>  $transaction['Transaction']['code'],
          'business_id'     =>  $transaction['Transaction']['businessId'],
          'discount_type'   =>  $discountType,
          'discount_value'  =>  $discountValue,
          'title'           =>  $transaction['Transaction']['particulars'],
          'items'           =>  $items,
          'payments'        =>  $payments,
          'amount'          =>  $amount,
          'discount'        =>  $discount,
          'paid'            =>  $paid,
          'balance'         =>  $balance
        );
        $transactionAmount += $amount;
        $paidAmount += $paid;
      }
    }  
     
    $totalBalance = (($transactionAmount - $paidAmount) - $totalDiscount) <= 0? 0:(($transactionAmount - $paidAmount) - $totalDiscount);
    $cashiering = array(
      'id'                  =>  $data['Folio']['id'],
      'code'                =>  $data['Folio']['code'],
      'room'                =>  $data['Room']['name'],
      'type'                =>  $data['Room']['RoomType']['name'],
      'rate'                =>  $data['Room']['rate'],
      'guest'               =>  $data['Guest']['lastName'] . ', ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['middleName'],
      'company'             =>  !empty($data['Guest']['Company'])?$data['Guest']['Company']['name']:'',
      'address'             =>  $data['Guest']['address'],
      'amount'              =>  $data['Room']['rate'] * $data['Folio']['nights'],
      'arrival'             =>  date('m/d/Y', strtotime($data['Folio']['arrival'])),
      'departure'           =>  date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
      'reservation_id'      =>  @$data['ReservationRoom']['Reservation']['id'],
      'reservation_room_id' =>  @$data['ReservationRoom']['id'],
      'special_request'     =>  $data['Folio']['specialRequest'],
      'closed'              =>  $data['Folio']['closed'],
      'Transaction'         =>  array(
        'charges'           =>  $transactions,
        'amount'            =>  $transactionAmount,
        'discount'          =>  $totalDiscount,
        'paid'              =>  $paidAmount,
        'balance'           =>  $totalBalance
      ),
      'transaction_amounts' =>  $amount,
       $totalTransactions   += count($transactions)
    );
    
    $this->set(compact('cashiering'));

    $this->saveLogs('statement of account');
  
  }

  public function guestFolioSOA($id) {
    $conditions['Folio.id'] = $id;
    
    $tmp = $this->Folio->find('first', array(
      'contain'=>array(
        'ReservationRoom' => array(
          'Reservation'
        ),
        'Guest' => array(
          'Company'
        ),
        'Room' => array(
          'RoomType' => array('name')
        ),
        'FolioTransaction' => array(
          'conditions' => array(
            'visible' => true
          ),
          'Transaction' => array(
            'conditions' => array(
              'visible' => true  
            ),
            'Business',
            'TransactionSub' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionPayment' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionDiscount' => array(
              'Discount',
              'conditions' => array(
                'visible' => true
              )
            )
          )
        ),
      ),
      'conditions' => $conditions
    ));
  
    // get total nights
    if ($tmp['ReservationRoom']['departure'] != date('Y-m-d')) {
      $departure = strtotime('+1day',strtotime($tmp['ReservationRoom']['departure']));
      $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime($tmp['Folio']['departure']))) > date('Y-m-d')) {
      $departure = date('m/d/Y', strtotime('+1day', strtotime($tmp['Folio']['departure'])));
      $extensions = 0;
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime($tmp['Folio']['departure']))) < date('Y-m-d') ) {
        $departuree = date('m/d/Y');
      } else if (date('Y-m-d', strtotime('+1day', strtotime($tmp['Folio']['departure']))) >= date('Y-m-d')) {
        $departuree = date('m/d/Y', strtotime('+1day', strtotime($tmp['Folio']['departure'])));
      }

    // folio data
    $cashiering = array(
      'id'             => $tmp['Folio']['id'],
      'code'           => $tmp['Folio']['code'],
      'room'           => $tmp['Room']['name'],
      'company'        => @$tmp['Guest']['Company']['name'],
      'vehiclecolor'   => $tmp['Guest']['vehicleColor'],
      'vehicleMade'    => @$tmp['Guest']['vehicleMade'],
      'vehicleplateNo' => $tmp['Guest']['vehiclePlateNo'],
      'roomType'       => $tmp['Room']['RoomType']['name'],
      'guest'          => properCase($tmp['Guest']['firstName'] . ' ' . $tmp['Guest']['lastName']),
      'guestId'        => $tmp['Guest']['id'],
      'roomRate'       => $tmp['Room']['rate']*($tmp['Folio']['nights']+@$extensions),
      'extensions'     => @$extensions,
      'address'        => $tmp['Guest']['address'],
      'arrival'        => fdate($tmp['Folio']['arrival']),
      'departure'      => $departuree,
      'mobile'         => $tmp['Guest']['mobile'],
      'bookingSource'  => properCase($tmp['Folio']['bookingSource']),
      'specialRequest' => $tmp['Folio']['specialRequest'],
      'remarks'        => $tmp['Folio']['remarks'],
      'checkedIn'      => $tmp['ReservationRoom']['checkedIn'],
      'taxRate'        => 1.12,
      'closed'         => $tmp['Folio']['closed'],
      'transferedBills'=> $tmp['Folio']['transferedBills'],
      'reservation_room_id' =>  $tmp['ReservationRoom']['id'],
      'room_id'        => $tmp['ReservationRoom']['roomId'],
      'reservation_id' => $tmp['ReservationRoom']['reservationId']
    );
  
    //transactions
    $grandAmount   = 0;
    $grandPayment  = 0;
    $chargesGrandPayment = 0;
    $grandDiscount = 0;
    $grandChange   = 0;
    $grandInitialPayment   = 0;
    $transactions  = array();

    foreach ($tmp['FolioTransaction'] as $data) {
      $transaction = $data['Transaction'];
  
      // subs
      $totalAmount = 0;
      $transactionSubs = array();    
      foreach ($transaction['TransactionSub'] as $trans) {
        if ($transaction['particulars']=='room accomodation charges') {
          $totalAmount += $trans['amount']*((int)$tmp['Folio']['nights']+@$extensions);
        } else {
          $totalAmount += $trans['amount']*$trans['quantity'];
        }

        $transactionSubs[] = array(
          'id'          => $trans['id'],
          'particulars' => properCase($trans['particulars']),
          'amount'      => $transaction['particulars']=='room accomodation charges'? $trans['amount']*($tmp['Folio']['nights']+@$extensions):$trans['amount']*$trans['quantity'],
          'nights'      => (int)$tmp['Folio']['nights'], 
          'quantity'    => (int)$trans['quantity'] 
        );
      }
  
      // payments
      $totalChange = 0;
      $payment = 0;
      $totalbarChange = 0;
      $totalPayment = 0;
      $totalbarPayment = 0;
      $hotelInitialPayment = 0;
      $totalhotelChange = 0;
      $totalhotelPayment = 0;
      $transactionPayments = array();
      foreach ($transaction['TransactionPayment'] as $pay) {
        if ($data['cleanTable']) { 
          if(date('Y-m-d h:i', strtotime($pay['created'])) <= date('Y-m-d h:i', strtotime($data['modified']))) {
            $totalbarChange += $pay['change'];
            $totalbarPayment += ($pay['amount'] - $pay['change']);
          } else {
            $totalhotelChange += $pay['change'];
            $totalhotelPayment += ($pay['amount']);

            $totalChange += $pay['change'];
            $payment = $pay['amount'] - $pay['change'];
            $totalPayment += $payment;
      
            $transactionPayments[] = array(
              'id'       => $pay['id'],
              'orNumber' => $pay['orNumber'],
              'amount'   => floatval($pay['amount']),
              'change'   => floatval($pay['change']),
              'type'     => $pay['paymentType'],
              'date'     => date('Y-m-d h:i A',strtotime($pay['created'])),
              'business'  => $transaction['Business']['name']
            ); 
          }   
        } else {
            if (date('Y-m-d h:i:s', strtotime($pay['created'])) == date('Y-m-d h:i:s', strtotime($data['created']))) {
              $totalbarChange += $pay['change'];
              $totalbarPayment += ($pay['amount'] - $pay['change']); 

              $hotelInitialPayment += ($pay['amount'] - $pay['change']);
            } else {
              $totalChange += $pay['change'];
              $payment = $pay['amount'] - $pay['change'];
              $totalPayment += $payment;
            }

            $transactionPayments[] = array(
              'id'       => $pay['id'],
              'orNumber' => $pay['orNumber'],
              'amount'   => floatval($pay['amount']),
              'change'   => floatval($pay['change']),
              'type'     => $pay['paymentType'],
              'date'     => date('Y-m-d h:i A',strtotime($pay['created'])),
              'business'  => $transaction['Business']['name']
            );
        }         
      }
  
      // discounts
      $totalDiscount = 0;
      $transactionDiscounts = array();
      $percent = ''; $fixed = '';
      $str = '';
      $totalPercent = 0;
      $percentRate = 0;
      foreach ($transaction['TransactionDiscount'] as $dis) {
        $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $totalAmount * ($dis['value']/100);
        $totalDiscount += $discountAmount;
        if($dis['type'] == 'percent') {
          $percentRate = $dis['value']/100;
          $totalPercent += $percentRate;
        }
  
        $transactionDiscounts[] = array(
          'id'     => $dis['id'],
          'name'   => ucwords($dis['Discount']['name']),
          'value'  => $dis['value'],
          'rate'   => $percentRate,
          'type'   => $dis['type'],
          'amount' => $discountAmount
        );
        // $discountTypes .= ' '. $dis['type'];
        if($dis['type']=='percent') {
          $percent = ' ' .'percent';
        } else if($dis['type']=='fixed') {
          $fixed = ' ' .'fixed';
        }

        $str = $percent . ',' . $fixed;
      }
      
      $transactions[] = array(
        'id'            => $transaction['id'],
        'code'          => $transaction['code'],
        'business'      => $transaction['Business']['name'],
        'particulars'   => properCase($transaction['particulars']),
        'totalbarPayment' => @$totalbarPayment,
        'totalhotelPayment' => $totalhotelPayment,
        'totalChange'   => $totalChange,
        'totalAmount'   => $totalAmount,
        'totalPayment'  => $totalPayment,
        'totalDiscount' => $totalDiscount,
        'totalBalance'  => $totalAmount - ($totalPayment + @$totalbarPayment + $totalDiscount),
        'subs'          => $transactionSubs,
        'payments'      => $transactionPayments,
        'discounts'     => $transactionDiscounts
      );
  
      $grandAmount   += $totalAmount;
      $chargesGrandPayment += $totalPayment;
      $grandPayment  += $totalPayment + $hotelInitialPayment;
      $grandDiscount += $totalDiscount;
      $grandChange   += $totalChange;
      $grandInitialPayment += $totalbarPayment;
    }
  
    $cashiering['Transactions']['charges']        = $transactions;
    $cashiering['Transactions']['totalAmount']    = $grandAmount;
    $cashiering['Transactions']['totalPayment']   = $grandPayment;
    $cashiering['Transactions']['chargesTotalPayment']   = $chargesGrandPayment;
    $cashiering['Transactions']['totalDiscount']  = $grandDiscount;
    $cashiering['Transactions']['totalInitialPayment']  = $grandInitialPayment;
    $cashiering['Transactions']['discountPercent'] = $totalPercent;    
    $cashiering['Transactions']['totalChange']     = $grandChange;
    $cashiering['Transactions']['totalBalance']   = $grandAmount - ($grandPayment + $grandInitialPayment + $grandDiscount);

    $received = $this->Folio->FolioSub->find('all', array(
      'contain' =>  array(
        'Folio' =>  array(
          'FolioTransaction'  =>  array(
            'Transaction' =>  array(
              'Business',
              'TransactionSub' => array(
                'conditions'=>array(
                  'visible' => true
                )
              ),
              'TransactionPayment' => array(
                'conditions' => array(
                  'visible' => true
                )
              ) ,
              'TransactionDiscount' => array(
                'conditions' => array(
                  'visible' => true
                )
              )  
            )
          )
        )
      ),
      'conditions'  =>  array(
        'FolioSub.parentId' => $id,

      )
    ));

    // received transactions
    if(!empty($received)) {
      $receivedTransactions = array();
      $tgrandAmount   = 0;
      $tgrandPayment  = 0;
      $tgrandDiscount = 0;
      $tgrandChange   = 0;
      $tgrandChildPayment = 0;
      $tgrandBarPayment = 0;
    foreach ($received as $rcv) {

    // get total nights
    if ($rcv['Folio']['departure'] != date('Y-m-d')) {
      $departure = strtotime('+1day',strtotime($rcv['Folio']['departure']));
      $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime($rcv['Folio']['departure']))) > date('Y-m-d')) {
      $departure = date('m/d/Y', strtotime('+1day', strtotime($rcv['Folio']['departure'])));
      $extensions = 0;
    } 
  
    foreach($rcv['Folio']['FolioTransaction'] as $fTransaction) {
       
      // subs
      $tTotalAmount = 0;
      $tTransactionSubs = array();  
      foreach ($fTransaction['Transaction']['TransactionSub'] as $trans) {
        
        if($trans['paid']&&$fTransaction['Transaction']['paidByParentId']) {
          if (strrpos($trans['particulars'], 'accomodation')) {
            $trans['amount'] = $trans['amount']*($rcv['Folio']['nights']+@$extensions);
          }

          $tTotalAmount += $trans['amount']*$trans['quantity'];

          $tTransactionSubs[] = array(
            'id'          => $trans['id'],
            'particulars' => properCase($trans['particulars']),
            'amount'      => floatval($trans['amount'])*$trans['quantity'],
            'paid'        => $trans['paid']
          );
        }

       if(!$trans['paid']) {
        if (strrpos($trans['particulars'], 'accomodation')) {
          $trans['amount'] = $trans['amount']*($rcv['Folio']['nights']+@$extensions);
        }

          $tTotalAmount += $trans['amount']*$trans['quantity'];

          $tTransactionSubs[] = array(
            'id'          => $trans['id'],
            'particulars' => properCase($trans['particulars']),
            'amount'      => floatval($trans['amount'])*$trans['quantity'],
            'paid'        => $trans['paid']
          );
       }  
      }
      
      // payments
        $tTotalPayment = 0;
        $tpayment = 0;
        $tTotalChange = 0;
        $tTotalChildFolioPayment = 0;
        $tTotalChildFolioChange = 0;
        $tTotalOtherBusinessChange = 0;
        $tTotalOtherBusinessPayment = 0;
        $tTransactionPayments = array();
        foreach ($fTransaction['Transaction']['TransactionPayment'] as $tpay) {
 
         // $tpay['created'] < $fTransaction['created'] || 
          if ($tpay['created'] < $rcv['FolioSub']['created']) {
            $tTotalChildFolioChange += $tpay['change'];
            $tTotalChildFolioPayment += ($tpay['amount'] - $tpay['change']);
            
            // if ($tpay['created'] < $fTransaction['created']) {
            //   $tTotalOtherBusinessChange += $tpay['change'];
            //   $tTotalOtherBusinessPayment += ($tpay['amount'] - $tTotalOtherBusinessChange);
            // }

          } else {

            $tTotalChange += $tpay['change'];
            $tpayment = $tpay['amount'] - $tTotalChange;
            $tTotalPayment += $tpayment;
            
            $tTransactionPayments[] = array(
              'id'       => $tpay['id'],
              'orNumber' => $tpay['orNumber'],
              'amount'   => floatval($tpay['amount'])-$tpay['change'],
              'change'   => floatval($tpay['change']),
              'type'     => $tpay['paymentType'],
              'date'     => $tpay['created']
            );
          }  
          // } 
        }

        // discounts
        $tTotalDiscount = 0;
        $tTotalPercent = 0;
        $tPercentRate  = 0;
        $tTransactionDiscounts = array();
        foreach ($fTransaction['Transaction']['TransactionDiscount'] as $tdis) {
          $tdiscountAmount = $tdis['type'] == 'fixed'? $tdis['value'] : $tTotalAmount * ($tdis['value']/100);
          $tTotalDiscount += $tdiscountAmount;
      
          if($tdis['type'] == 'percent') {
            $tPercentRate = $tdis['value']/100;
            $tTotalPercent += $tPercentRate;
          }
        
          $tTransactionDiscounts[] = array(
            'id'     => $tdis['id'],
            'value'  => $tdis['value'],
            'type'   => $tdis['type'],
            'amount' => $tdiscountAmount
          );
          
          if($tdis['type']=='percent') {
            $percent = ' ' .'percent';
          } else if($tdis['type']=='fixed') {
            $fixed = ' ' .'fixed';
          }
  
          $str = $percent . ',' . $fixed;
        
        }

           if(!empty($tTransactionSubs)) {   
              $receivedTransactions[] = array(
                'id'            => $fTransaction['Transaction']['id'],
                'code'          => $fTransaction['Transaction']['code'],
                'FolioSubCreated'  => $rcv['FolioSub']['created'],
                'FolioTransactionCreated' => $fTransaction['created'],
                'folioCode'     => $rcv['Folio']['code'],
                'paidByParentId'=> $fTransaction['Transaction']['paidByParentId'],
                'business'      => $fTransaction['Transaction']['Business']['name'],
                'particulars'   => properCase($fTransaction['Transaction']['particulars']),
                'TotalChildFolioChange' => $tTotalChildFolioChange,
                'TotalChildFolioPayment' => $tTotalChildFolioPayment,
                'totalAmount'   => $tTotalAmount - $tTotalChildFolioPayment,
                'totalPayment'  => $tTotalPayment,
                'totalDiscount' => $tTotalDiscount,
                'totalBalance'  => $tTotalAmount - ($tTotalPayment + $tTotalChildFolioPayment + $tTotalDiscount), 
                'subs'          => $tTransactionSubs,
                'payments'      => $tTransactionPayments,
                'discounts'     => $tTransactionDiscounts
              );
              
      $tgrandAmount   += $tTotalAmount - $tTotalChildFolioPayment;
      $tgrandPayment  += $tTotalPayment;
      $tgrandChildPayment  += $tTotalChildFolioPayment;
      $tgrandDiscount += $tTotalDiscount;
      $tgrandChange   += $tTotalChange;
       }
      }
    }  

      $cashiering['ReceivedTransactions']['charges']         = $receivedTransactions;
      $cashiering['ReceivedTransactions']['totalAmount']     = $tgrandAmount;
      $cashiering['ReceivedTransactions']['totalPayment']    = $tgrandPayment;
      $cashiering['ReceivedTransactions']['totalDiscount']   = $tgrandDiscount;
      $cashiering['ReceivedTransactions']['discountPercent'] = $tTotalPercent;  
      $cashiering['ReceivedTransactions']['totalChange']     = $tgrandChange;
      $cashiering['ReceivedTransactions']['totalInitialPayment']     = $tgrandChildPayment;
      $cashiering['ReceivedTransactions']['totalBalance']    = $tgrandAmount - ($tgrandPayment + $tgrandDiscount);    


    // $receivedTransactions

    }
 
    $cashiering['discountTypes']   = $str;

      if(empty($receivedTransactions) || $receivedTransactions == null || $receivedTransactions == '') {
        $cashiering['receivedTransactions'] = false;
      } else {
        $cashiering['receivedTransactions'] = true;
      }

    $payIds = '';
    // payments
    foreach ($transactions as $t) {
      foreach ($t['payments'] as $p) {
        $payIds .= $p['id'] . ',';
      }
    }
    
    if(isset($receivedTransactions)) {
      foreach ($receivedTransactions as $r) {
        foreach ($r['payments'] as $py) {
          $payIds .= $py['id'] . ',';
        }
      }  
    } 
    
    $pay =array_map('trim', explode(',',$payIds));
    
    $cpayments = $this->TransactionPayment->find('all',array(
      'contain' =>  array(
        'Transaction' => array(
          'Business',
          'FolioTransaction'
        )
      ),
      'conditions' => array(
        'TransactionPayment.id'      => $pay,
        'TransactionPayment.visible' => true
      )
    ));
    
    // transform cpayments
    $cTotalPayment = 0;
    $cpayment = 0;
    $cTotalChange = 0;
    $cTransactionPayments = array();
   
    foreach ($cpayments as $cdata) {
      $cpay = $cdata['TransactionPayment'];

      if($cpay['visible']==true) {
         $paidInOtherBusiness = false;
        if ($cdata['Transaction']['FolioTransaction']['cleanTable']) { 
          if(date('Y-m-d h:i', strtotime($cpay['created'])) <= date('Y-m-d h:i', strtotime($cdata['Transaction']['FolioTransaction']['modified']))) {
            $paidInOtherBusiness = true;
          }  
        }
          
        $cTotalChange += $cpay['change'];
        $cpayment = $cpay['amount'] - $cTotalChange;
        $cTotalPayment += $cpayment;

        $cTransactionPayments['payments'][] = array(
          'id'       => $cpay['id'],
          'orNumber' => $cpay['orNumber'],
          'amount'   => floatval($cpay['amount']),
          'change'   => floatval($cpay['change']),
          'type'     => $cpay['paymentType'],
          'code'     => $cdata['Transaction']['code'],
          'date'     => date('M d,2015 h:i A',strtotime($cpay['created'])),
          'business' => propercase($cdata['Transaction']['Business']['name']),
          'paidInOtherBusiness' => $paidInOtherBusiness,
          'visible'  => $cpay['visible']
        );
      }
    }
    
    // . payments
    $cashiering['cPayments'] = $cTransactionPayments;

    // checks if returning or not
    $returning = $this->Folio->find('all',array(
      'conditions' => array(
        'Folio.guestId' => $tmp['Guest']['id']
      )
    ));

    // transform folio
    $returned = false;
    if(count($returning)>1) {
      $returned = true;
    } 

    $cashiering['History'] = $returned;

    if(empty($received)) {
      $cashiering['ReceivedTransactions'] = null;
    }
  
    $this->set(compact('cashiering','tmp'));

    $this->saveLogs('guest folio statement of account');
  }

  public function audit() {
    // default date
    $date = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])):date('Y-m-d');
  
    // default conditions
    $conditions = array();
    $conditions['Room.visible'] = true;
    
    // load rooms
   $tmpData = $this->Room->find('all', array(
      'contain'=>array(
        'Accomodation' => array(
          'name'
        ),
        'RoomType' => array(
          'name',
          'rate'
        ),
        'Folio'=>array(
          'Guest' => array(
            'lastName',
            'firstName',
            'middleName'
          ),
          'conditions' => array(
            'Folio.arrival   <=' => $date,
            'Folio.departure >=' => $date,
            'Folio.closed'       => false
          ),
          'fields' => array(
            'arrival','departure','id','closed'
          ),
        ),
        'ReservationRoom'=>array(
          'id',
          'arrival',
          'departure',
          'Reservation' => array(
            'lastName',
            'firstName',
            'middleName'
          ),
          'conditions' => array(
            'ReservationRoom.arrival <='   => $date,
            'ReservationRoom.departure >=' => $date,
            'ReservationRoom.closed'       => false
          )
        ),
      ),
      'conditions' => $conditions,
      'order' => array(
        'Room.name' => 'ASC'
      ),
      'fields' => array(
        'COUNT(Room.roomTypeId) as TotalRooms',
        'COUNT(CASE uncleaned when "1" then 1 else null end) as TotalUncleaned',
        'COUNT(CASE outOfOrder when "1" then 1 else null end) as TotalOutOfOrder',
        'uncleaned','outOfOrder','name','rate','visible','Room.id'
      ),
    'group' => array(
          'Room.roomTypeId'  
        )
    ));

    // initialization
    $audits = array();
    $audit = array();

    $available = 0;
    $occupied = 0;
    $gro = 0;
    $grandTotalRooms = 0;
    $grandTotalOccupied = 0;
    $grandTotalAvailable = 0;
    $grandDir = 0;
    $grandOoo = 0;
    $grandRack = 0;
    $grandGro = 0;
    $grandDisc = 0;
    $grandRevenue = 0;
    $discount = 0;
    $discountAmt = 0;
    $revenue = 0;
    
    // transformation
    foreach ($tmpData as $data) {
      $discounts = $this->folios($data['RoomType']['name']);
      $occupied = $this->occupied($data['RoomType']['name'],$date);
      $available  = $data[0]['TotalRooms'] - $occupied;
      
      $grandTotalRooms += $data[0]['TotalRooms'];
      $grandTotalOccupied += $occupied;
      $grandTotalAvailable += $available;

      $gro = $occupied * $data['RoomType']['rate'];
      $grandGro += $gro;
      $grandDisc += $discounts;
      $revenue = $gro - $discounts;
      $grandRevenue += $revenue;   
      
        $audit[] = array(
        'room'                => $data['RoomType']['name'],
        'total_rooms'         => $data[0]['TotalRooms'],
        'total_occupied'      => $occupied,
        'total_available'     => $available,
        'dirty'               => $data[0]['TotalUncleaned'],
        'out_of_order'        => $data[0]['TotalOutOfOrder'],
        'rate'                => $data['RoomType']['rate'],
        'gro'                 => $gro,
        'discount'            => $discounts,
        'revenue'             => $revenue
      );
      
      $grandDir += $data[0]['TotalUncleaned'];
      $grandOoo += $data[0]['TotalOutOfOrder'];
    
    }
        $audits['audits'] = $audit;
        $audits['grandTotalRooms']     = $grandTotalRooms;
        $audits['grandTotalOccupied']  = $grandTotalOccupied;
        $audits['grandTotalAvailable'] = $grandTotalAvailable;
        $audits['grandTotalDir'] = $grandDir;
        $audits['grandTotalOoo'] = $grandOoo;
        $audits['grandGro'] = $grandGro;
        $audits['grandDisc'] = $grandDisc;
        $audits['grandRevenue'] = $grandRevenue;
  
    $this->set(compact('date','audits'));
    $this->saveLogs('hotel night audit report');
  }
  
  public function ledger_all() {

    $qry = "
      SELECT
        SUM(TransactionPayment.amount) AS totalAmount,
        Company.name
      FROM
        (((((transaction_payments AS TransactionPayment LEFT JOIN
        transactions AS Transaction ON Transaction.id = TransactionPayment.transactionId) LEFT JOIN
        folio_transactions as FolioTransaction ON FolioTransaction.transactionId = Transaction.id) LEFT JOIN
        folios as Folio ON Folio.id = FolioTransaction.folioId) LEFT JOIN
        guests as Guest ON Guest.id = Folio.guestId)
        LEFT JOIN companies as Company ON Company.id = Guest.companyId)
      WHERE
        TransactionPayment.visible = true AND
        TransactionPayment.paymentType = 'send bill'
      GROUP BY
        Company.name
    ";
  
    $tmpData = $this->TransactionPayment->query($qry);
    
   $companies = array();
    $company = array();
    $totalAmount = 0;
    foreach ($tmpData as $data) {
      $company[] = array(
        'company' => properCase($data['Company']['name']),
        'coy'     => strtolower($data['Company']['name']),
        'credit'  => $data[0]['totalAmount'], 
        'debit'   => 0
      );
      $totalAmount += $data[0]['totalAmount'];
    }
      
    $companies['company'] = $company;
    $companies['totalDebit'] = 0;
    $companies['totalCredit'] = $totalAmount;
    $companies['totalBalance'] = $totalAmount - 0;
    
    
    $this->set(compact('companies'));
    $this->saveLogs('hotel city ledger report');
  }
  
  public function ledger_view($company) {
    $companyName = strtolower($company);

   $qry = "
    SELECT
      TransactionPayment.orNumber AS refNumber,
      TransactionPayment.amount,
      Transaction.particulars,
      Transaction.date,
      Folio.id,
      Folio.arrival,
      Folio.departure,
      Company.name
    FROM
      (((((transaction_payments AS TransactionPayment LEFT JOIN
      transactions AS Transaction ON Transaction.id = TransactionPayment.transactionId) LEFT JOIN
      folio_transactions as FolioTransaction ON FolioTransaction.transactionId = Transaction.id) LEFT JOIN
      folios as Folio ON Folio.id = FolioTransaction.folioId) LEFT JOIN
      guests as Guest ON Guest.id = Folio.guestId)
      LEFT JOIN companies as Company ON Company.id = Guest.companyId)
    WHERE
      TransactionPayment.visible = true AND
      TransactionPayment.paymentType = 'send bill' AND
      Company.name LIKE '%$companyName%'
    GROUP BY
      Transaction.id
  ";
  
    $tmpData = $this->TransactionPayment->query($qry);
  
    $transactions = array();
    $totalAmount = 0;
    $payments = array();
    foreach ($tmpData as $data) {
      
         // search 
      $dates = '';
      if (!strpos($data['Transaction']['particulars'],'accomodation')){
        $dates =  date('M d Y',strtotime($data['Transaction']['created']));
      } else {
        $days   = date('d',strtotime($data['Folio']['arrival'])) . '-' .date('d',strtotime('+1day',strtotime($data['Folio']['departure'])));
        $month  = date('M',strtotime($data['Folio']['arrival']));
        $year   = date('Y',strtotime($data['Folio']['arrival']));
        $dates   = $month . ' ' . $days . ' ' . $year;
      }
      
          $transactions[] = array(
          'amount'        => $data['TransactionPayment']['amount'],
          'particulars'   => $data['Transaction']['particulars'],
          'date'          => $dates,
          'company'       => $data['Company']['name'],
          'debit'         => 0,
          // 'credit'        => $data['TransactionPayment']['amount']
        );
        
        $totalAmount += $data['TransactionPayment']['amount'];
        $company = $data['Company']['name'] ;
    }
    
    $payments['debit']    = 0;
    $payments['payments'] = $totalAmount;
    $payments['company'] = $company;
    
    $this->set(compact('transactions','payments'));
    $this->saveLogs('hotel city ledger per company');
  }
  
  public function logs() {
    // default conditions
    $conditions = array();
    $conditions['UserLog.visible'] = true;
    $conditions['User.visible'] = true;

    // paginate data
    $paginatorSettings = array(
      'contain' => array(
        'User'
      ),
      'conditions' => $conditions,
      'order'      => array(
        'UserLog.created' => 'DESC'
      )
    );
    $modelName = 'UserLog';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData   = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $logs = array();
    foreach ($tmpData as $data) {
      $log    = $data['UserLog'];
      $user    = $data['User'];

      $logs[] = array(
        'id'          => $log['id'],
        'user'        => $user['firstName'].' '.$user['lastName'],
        'action'      => $log['action'],
        'role'        => $user['role'],
        'description' => $log['description'],
        'date'       => date('M d, y h:i:s A', strtotime($log['created'])),
      );
    }

    $response = array(
      'ok'   => true,
      'data' => $logs,
    );
        
    $this->set(compact('logs'));
    $this->saveLogs('user logs');
  }

  public function saveLogs($description) {
    $this->UserLog->save(array(
      'userId'     =>  $this->Session->read('Auth.User.id'),
      'action'     =>  'Print',
      'description'=>  $description
    ));
  }

    public function occupied($type=null,$date=null) {  

      $conditions['RoomType.visible'] = true;
      
      $roomTypes = $this->RoomType->find('all', array(
        'contain'=>array(
          'Accomodation',
          'Room'=>array(
            'Folio'=>array(
              'id',
              'conditions'=>array(
                'OR'=>array(
                   'Folio.arrival   <=' => $date,
                   'Folio.departure >=' => $date,
                ),
                'visible'=>true,
                'closed' => false
              ),
              'FolioTransaction' => array('Transaction'=>array('TransactionDiscount'))
            ),
            'ReservationRoom'=>array(
              'id',
              'conditions'=>array(
                'OR'=>array(
                  'ReservationRoom.arrival <='   => $date,
                  'ReservationRoom.departure >=' => $date
                ),
                'visible'   => true,
                'closed'    => false,
                )
            )
          )
        )
      ));

      $RoomType = array();
      foreach($roomTypes as $a=>$roomType){
        $Room = array();
        foreach($roomType['Room'] as $b=>$room){
          $roomType['Folio'] = $room['Folio'];
          
          if(count($room['Folio'])>0 AND count($room['ReservationRoom'])>0 ) 
            $Room[] = $room;
          }
        
        $roomType['Room'] = count($Room);
        if(count($Room)>0){
          $roomType['room_id'] =  $Room[0]['id'];
          $RoomType[] = $roomType;
        }
      }
      $roomTypes = $RoomType;

      $totalOccupied = 0;
      foreach ($roomTypes as $data) {
        if($data['RoomType']['name']==$type) {
          $totalOccupied += 1;
        }
      }
      
      return $totalOccupied;
    }
    
    public function folios($type=null,$date=null) {
      $conditions['RoomType.visible'] = true;

      $roomTypes = $this->RoomType->find('all', array(
        'contain'=>array(
          'Accomodation',
          'Room'=>array(
            'Folio'=>array(
              'id',
              'conditions'=>array(
                'OR'=>array(
                   'Folio.arrival   <=' => $date,
                   'Folio.departure >=' => $date,
                ),
                'visible'=>true,
                'closed' => false,
                'paid'   => false
              ),
              'FolioTransaction' => array(
                'Transaction'=>array(
                  'TransactionDiscount'
                  )
                )
            ),
            'ReservationRoom'=>array(
              'id',
              'conditions'=>array(
                'OR'=>array(
                  'ReservationRoom.arrival <='   => $date,
                  'ReservationRoom.departure >=' => $date,
                  'ReservationRoom.closed'       => false
                ),
                'visible'   => true,
                'closed'     => false
                )
            )
          )
        )
      ));

      $RoomType = array();
      $rooms =array();
      foreach($roomTypes as $a=>$roomType){
        
        $Room = array();
         $discounts = 0;
        foreach($roomType['Room'] as $b=>$room){
          foreach ($room['Folio'] as $c=>$folio) {
            foreach ($folio['FolioTransaction'] as $ft=>$tr) {
              foreach ($tr['Transaction']['TransactionDiscount'] as $disc) {
                $disco = $disc['type'] == 'fixed'? $disc['value'] : (int)$room['rate']* ((int)$disc['value']/100);
                $discounts += $disco; 
                
              }
            }
              
            }
          if(count($room['Folio'])>0 AND count($room['ReservationRoom'])>0 ) 
            $Room[] = $room;
            $roomType['Discounts'] = $discounts; 
          }
          
          // $RoomType[] = $rooms;

          $roomType['Room'] = count($Room);
         
            if(count($Room)>0){
              $roomType['room_id'] =  $Room[0]['id'];
              // $roomType['discounts'] =  $Room[0]['id'];
              $RoomType[] = $roomType;
            }
      }
          $roomTypes = $RoomType;
          
          $totalDiscounts = 0;
          foreach ($roomTypes as $data) {
            if($data['RoomType']['name']==$type) {
              $totalDiscounts = $data['Discounts'];
            }
          }
            

      return $totalDiscounts;
    }

  public function cashiering_report($date=null,$startTime=null,$endTime=null,$business=null,$id=null) {

    $conditions = array();
    $conditions_payments = array();
    $conditions['Folio.visible'] = true;
    $conditions['Folio.arrival <='] = date('Y-m-d',strtotime($date));
    $conditions['Folio.departure >='] = date('Y-m-d', strtotime('-1day',strtotime($date)));
    $conditions_payments['TransactionPayment.visible'] = true;
    $conditions_payments['TransactionPayment.date'] = date('Y-m-d',strtotime($date));
    // print_r($this->request->params);
    if ($id != null) {
      $conditions['Folio.id'] = $id;   
    }

    if(isset($this->request->params['pass'][1])) {
      $conditions_payments['TransactionPayment.created >='] = $date . ' ' . date("H:i:s", $startTime/1000);
      $conditions_payments['TransactionPayment.created <='] = $date . ' ' . date("H:i:s", $endTime/1000);
    }

   $cashier = $this->Folio->find('all', array(
      'contain'=>array(
        'ReservationRoom'=>array('Reservation'),
        'Guest'=>array('Company'),
        'Room'=>array('RoomType'),
        'FolioTransaction'=>array(
          'Transaction'=>array(
            'Business', 
            'TransactionSub' => array('conditions' => array('TransactionSub.visible'=> true)),
            'TransactionPayment' => array(
              'conditions' => $conditions_payments
              ),
            'TransactionDiscount' => array('Discount','conditions' => array('TransactionDiscount.visible'=> true)),           
            'conditions' => array(
              'Transaction.visible'=> true,
              'Transaction.businessId' => $this->request->params['named']['business']        
            )
          ),
          'conditions' => array('FolioTransaction.visible'=> true)
        ),
      ),
      'conditions'=>$conditions
    ));
    
    $cashiering = array();
    $summary = array();
    $sum = '';
    $cash = 0;
    $totalCash = 0;
    $totalSendBill = 0;
    $totalCard = 0;
    $totalDebit = 0;
    foreach($cashier as $data){
      $transactionAmount = 0;
      $paidAmount = 0;
      $paymentType = '';
      $str = '';
      $transactions = array();
      $payments = array();
      
      // get total nights
      if ($data['ReservationRoom']['departure'] != date('Y-m-d')) {
        $departure = strtotime('+1day',strtotime($data['ReservationRoom']['departure']));
        $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
      } 

      if (date('Y-m-d', strtotime('+1day', strtotime($data['Folio']['departure']))) > date('Y-m-d')) {
        $departure = date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure'])));
        $extensions = 0;
      } 
  
      foreach($data['FolioTransaction'] as $transaction){
        
        if(!empty($transaction['Transaction'])) {

          // subs
          $amount = 0;
          $items = array();
          foreach($transaction['Transaction']['TransactionSub'] as $sub){
            $items[] = array(
              'particulars'=>$sub['particulars'],
              'amount'=>$sub['amount']
            );
            
            if (strrpos($sub['particulars'], 'accomodation')) {
              $sub['amount'] = $sub['amount']*($data['Folio']['nights']+@$extensions);
            }

            $amount+=$sub['amount'];
          }
          
          // discounts
          $totalDiscount = 0;
          $transactionDiscounts = array();
          $totalPercent = 0;
          $percentRate = 0;
          foreach ($transaction['Transaction']['TransactionDiscount'] as $dis) {
            $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $amount * ($dis['value']/100);
            $totalDiscount += $discountAmount;
            if($dis['type'] == 'percent') {
              $percentRate = $dis['value']/100;
              $totalPercent += $percentRate;
            }
      
            $transactionDiscounts[] = array(
              'id'     => $dis['id'],
              'name'   => ucwords($dis['Discount']['name']),
              'value'  => $dis['value'],
              'rate'   => $percentRate,
              'type'   => $dis['type'],
              'amount' => $discountAmount
            );
          }


          // payments
          $paid = 0;
          $payments = array();
         
          $totalChange = 0;
          $sendBill = false;
          foreach($transaction['Transaction']['TransactionPayment'] as $pay){

              $totalChange += $pay['change'];

              $payments[] = array(
                'or_number'=>$pay['orNumber'],
                'amount'=>$pay['amount'] - $pay['change'],
                'date'=>date('m/d/Y', strtotime($pay['date'])),
                'time'=>date('h:i:s A', strtotime($pay['created'])),
                'paymentType'=>$pay['paymentType']
              );
              if($pay['paymentType']=='send bill') {
                $str .= 'send bill';
                $sendBill = true;
              } else {
                $str .= $pay['paymentType'];
                $paid += ($pay['amount']-$pay['change']);
              }

              if(strpos($str,'send bill')) {
                $paymentType = 'send bill';
              } else {
                $paymentType = $pay['paymentType'];
              }

                // summary tally
                if($pay['paymentType']=='cash' || $pay['paymentType']=='cheque'||$pay['paymentType']==null) {
                  $totalCash += ($pay['amount']-$pay['change']); 
                } 
                if ($pay['paymentType']=='credit card') {
                  $totalCard += ($pay['amount']-$pay['change']);
                } 
                if ($pay['paymentType']=='send bill') {
                  $totalSendBill += ($pay['amount']-$pay['change']);
                }

                if ($pay['paymentType']=='debit') {
                  $totalDebit += ($pay['amount']-$pay['change']);
                }

          }
        }  
          
          $subTitle = !empty($transaction['Transaction']['title'])? ' ('.$transaction['Transaction']['title'].')':'';
          
          if(@$transaction['Transaction']['businessId']!=null) {

          $transactions[] = array(
            'id'=>@$transaction['Transaction']['id'],
            'code'=>@$transaction['Transaction']['code'],
            'business_id'=>@$transaction['Transaction']['businessId'],
            'title'=>@$transaction['Transaction']['Business']['name'] . $subTitle,
            'items'=>$items,
            'payments'=>$payments,
            'amount'=>$amount,
            'discount'=>$totalDiscount,
            'paid'=>$paid,
            'balance'=>($amount-$paid)-($totalDiscount-$totalChange)
          );
          $transactionAmount += $amount;
           $paidAmount += $paid;
          }

      }
       
      if ($data['ReservationRoom']['checkedIn']==true) {
        $status = 'IN-HOUSE';
      } 
      if ($data['ReservationRoom']['checkOutDate']!=null) {
        $status = 'CHECK-OUT';
      } 

      if(@$paidAmount!=0 || @$sendBill==true) {
        $cashiering[] = array(
          'id'=>$data['Folio']['id'],
          'code'=>$data['Folio']['code'],
          'room'=>$data['Room']['name'],
          'type'=>$data['Room']['RoomType']['name'],
          'rate'=>$data['Room']['rate'],
          'guest'=>ucwords($data['Guest']['lastName'] . ', ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['middleName']),
          'company'=>!empty($data['Guest']['Company'])?$data['Guest']['Company']['name']:'',
          'address'=>$data['Guest']['address'],
          'amount'=>($data['Room']['rate'] )*($data['Folio']['nights']),
          'arrival'=>date('m/d/Y', strtotime($data['Folio']['arrival'])),
          'departure'=>date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
          'reservation_id'=>$data['ReservationRoom']['reservationId'],
          'reservation_room_id'=>$data['ReservationRoom']['id'],
          'status' => $status,
          'closed'=>$data['Folio']['closed'],
          'transferedBills' => $data['Folio']['transferedBills'],
          'Transaction'=>array(
            'charges'=>$transactions,
            'amount'=>$transactionAmount,
            'paidAmount'=> $paidAmount,
            'discount'=> $totalDiscount,
            'balance'=> ($transactionAmount - $paidAmount  - $totalDiscount),
            'paymentType'=>$paymentType
          )
        );    

      @$summary = array(
        'TotalCash'     =>  @$totalCash,
        'TotalSendBill' =>  @$totalSendBill,
        'TotalCard'     =>  @$totalCard,
        'TotalDebit'    =>  @$totalDebit,
        'Total'         =>  @$totalCash + $totalSendBill + $totalCard  + $totalDebit
      );      
      }

    }
         
    if(empty($summary)) {
      $summary = array(
        'TotalCash'     =>  0,
        'TotalSendBill' =>  0,
        'TotalCard'     =>  0,
        'TotalDebit'    =>  0,
        'Total'         =>  0
      );
    }

    $this->set(compact('summary','cashiering'));

    $this->saveLogs('hotel cashiering report');
 
    return $cashiering;
  }

  public function bar_resto_cashiering_report($date=null,$startTime=null,$endTime=null,$business=null,$id=null) {

      $edate = date('Y-m-d', strtotime('-1day',strtotime($date)));

      $conditions['Folio.visible'] = true;
      $conditions['Folio.arrival <='] = $date;
      $conditions['Folio.departure >='] = $edate;
      $conditions_payments = array();
      $conditions_payments['TransactionPayment.visible'] = true;
      $conditions_payments['TransactionPayment.date'] = $date;

      if ($id != null) {
        $conditions['Folio.id'] = $id;   
      }

      if (isset($this->request->params['pass'][1])) {
        $start = $date . ' ' . date("H:i:s", $startTime/1000);
        $end = $date . ' ' . date("H:i:s", $endTime/1000);
        $conditions_payments['TransactionPayment.created >='] = $start;
        $conditions_payments['TransactionPayment.created <='] = $end;
      }

      $cashier = $this->Folio->find('all', array(
        'contain'=>array(
          'ReservationRoom'=>array('Reservation'),
          'Guest'=>array('Company'),
          'Room'=>array('RoomType'),
          'FolioTransaction'=>array(
            'Transaction'=>array(
              'Business', 
              'TransactionSub' => array('conditions' => array('TransactionSub.visible'=> true)),
              'TransactionPayment' => array(
                'conditions' => $conditions_payments 
                ),
              'TransactionDiscount' => array('Discount','conditions' => array('TransactionDiscount.visible'=> true)),           
              'conditions' => array(
                'Transaction.visible'=> true,
                'Transaction.businessId' => $this->request->params['named']['business']      
              )
            ),
            'conditions' => array('FolioTransaction.visible'=> true)
          ),
        ),
        'conditions'=>$conditions
      ));
      
      $cashiering = array();
      $summary = array();
      $sum = '';
      $cash = 0;
      $totalCash = 0;
      $totalCard = 0;
      $totalDebit = 0;
      
      foreach($cashier as $data){
        $transactionAmount = 0;
        $paidAmount = 0;
        $paymentType = '';
        $str = '';
        $transactions = array();
        $payments = array();
        $totalReceivable= 0;
    
        foreach($data['FolioTransaction'] as $transaction){
          
          if(!empty($transaction['Transaction'])) {
            // subs
            $amount = 0;
            $items = array();
            foreach($transaction['Transaction']['TransactionSub'] as $sub){
              $items[] = array(
                'particulars'=>$sub['particulars'],
                'amount'=>$sub['amount'],
                'quantity'=> $sub['quantity']
              );
              $amount+=$sub['amount']*$sub['quantity'];
            }
            
            // discounts
            $totalDiscount = 0;
            $transactionDiscounts = array();
            $totalPercent = 0;
            $percentRate = 0;
            foreach ($transaction['Transaction']['TransactionDiscount'] as $dis) {
              $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $amount * ($dis['value']/100);
              $totalDiscount += $discountAmount;
              if($dis['type'] == 'percent') {
                $percentRate = $dis['value']/100;
                $totalPercent += $percentRate;
              }
        
              $transactionDiscounts[] = array(
                'id'     => $dis['id'],
                'name'   => ucwords($dis['Discount']['name']),
                'value'  => $dis['value'],
                'rate'   => $percentRate,
                'type'   => $dis['type'],
                'amount' => $discountAmount
              );
            }


            // payments
            $paid = 0;
            $payments = array();
            $totalChange = 0;
            foreach($transaction['Transaction']['TransactionPayment'] as $pay){

                $totalChange += (float)$pay['change'];

                $payments[] = array(
                  'or_number'=>$pay['orNumber'],
                  'amount'=>$pay['amount'],
                  'date'=>date('m/d/Y', strtotime($pay['date'])),
                  'change'=>$pay['change'],
                  'time'=>date('h:i:s A', strtotime($pay['created'])),
                  'paymentType'=>($pay['paymentType'])
                );

                $paid += $pay['amount']-(float)$pay['change'];

                  // summary tally
                  if($pay['paymentType']=='cash' || $pay['paymentType']=='cheque'||$pay['paymentType']==null) {
                    $totalCash += ($pay['amount']-$pay['change']);
                    $paymentType = 'cash'; 
                  } 
                  if ($pay['paymentType']=='credit card') {
                    $totalCard += ($pay['amount']-$pay['change']);
                    $paymentType = 'credit card';
                  } 

                  if ($pay['paymentType']=='debit') {
                    $totalDebit += ($pay['amount']-$pay['change']);
                    $paymentType = 'debit';
                  }
           }
          }  
            
            if(@$transaction['Transaction']['businessId']!=null) {

            $transactions[] = array(
              'id'=>@$transaction['Transaction']['id'],
              'code'=>@$transaction['Transaction']['code'],
              'business_id'=>@$transaction['Transaction']['businessId'],
              'title'=>@$transaction['Transaction']['title'],
              'items'=>$items,
              'payments'=>$payments,
              'amount'=>$amount,
              'discount'=>$totalDiscount,
              'paid'=>$paid,
              'balance'=>($amount-$paid)-($totalDiscount)
            );

            $transactionAmount += $amount;
             $paidAmount += $paid;

            }
        }
         
            if ($data['ReservationRoom']['checkedIn']==true) {
              $status = 'IN-HOUSE';
              // $totalReceivable += ($transactionAmount - $paidAmount  - $totalDiscount);
            } 
            if ($data['ReservationRoom']['checkOutDate']!=null) {
              $status = 'CHECK-OUT';
            }

            if ($data['ReservationRoom']['checkOutDate']!=null&&$data['Folio']['transferedBills'] || $data['ReservationRoom']['checkedIn']==true) {
              $totalReceivable += ($transactionAmount - $paidAmount  - @$totalDiscount);
            }

        if($paidAmount!=0 || @$sendBill) {
          $cashiering[] = array(
            'id'=>$data['Folio']['id'],
            'code'=>$data['Folio']['code'],
            'room'=>$data['Room']['name'],
            'type'=>$data['Room']['RoomType']['name'],
            'rate'=>$data['Room']['rate'],
            'guest'=>ucwords($data['Guest']['lastName'] . ', ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['middleName']),
            'company'=>!empty($data['Guest']['Company'])?$data['Guest']['Company']['name']:'',
            'address'=>$data['Guest']['address'],
            'arrival'=>date('m/d/Y', strtotime($data['Folio']['arrival'])),
            'departure'=>date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
            'reservation_id'=>$data['ReservationRoom']['reservationId'],
            'reservation_room_id'=>$data['ReservationRoom']['id'],
            'status' => $status,
            'closed'=>$data['Folio']['closed'],
            'transferedBills' => $data['Folio']['transferedBills'],
            'Transaction'=>array(
              'charges'=>$transactions,
              'amount'=>$transactionAmount,
              'paid'=> $paidAmount,
              'discount'=> $totalDiscount,
              'balance'=> ($transactionAmount - $paidAmount  - $totalDiscount),
              'paymentType'=>$paymentType
            ),
          );    
        
        $summary = array(
          'TotalCash'     =>  $totalCash,
          'TotalCard'     =>  $totalCard,
          'TotalDebit'    =>  $totalDebit,
          'TotalReceivable' =>  $totalReceivable,
          'Total'         =>  $totalCash + $totalCard  + $totalDebit
        );  

        }
      } 

      if(empty($summary)) {
        $summary = array(
          'TotalCash'     =>  0,
          'TotalCard'     =>  0,
          'TotalDebit'    =>  0,
          'TotalReceivable' => 0,
          'Total'         =>  0
        );  
      }


    @$non_guest = @$this->non_guest_report($date,$this->request->params['named']['business'],@$start,@$end);

    $this->set(compact('cashiering','non_guest','summary'));

    $this->saveLogs('hotel cashiering report');
  }

  public function non_guest_report($date=null,$business=null,$startTime=null,$endTime=null) {
      $conditions_payments = array();
      $conditions_payments['TransactionPayment.visible'] = true;
      $conditions_payments['TransactionPayment.date'] = $date;

      if($startTime!=null||$startTime!='') {
        $conditions_payments['TransactionPayment.created >='] = $startTime;
        $conditions_payments['TransactionPayment.created <='] = $endTime;
      }

      $cashier = $this->Transaction->find('all', array(
        'contain' => array(
            'FolioTransaction' => array('conditions'=>array('FolioTransaction.visible')),
            'Business', 
            'TransactionSub' => array('conditions' => array('TransactionSub.visible'=> true)),
            'TransactionPayment' => array(
              'conditions' => $conditions_payments 
              ),
            'TransactionDiscount' => array('Discount','conditions' => array('TransactionDiscount.visible'=> true)),            
        ),
        'conditions' => array(
            'Transaction.visible'=> true,
            'Transaction.businessId' => $business          
          )
        ));

    $transactionAmount = 0;
    $totalChange = 0;
    $payments = array(); 
    $cashiering = array();
    $summary = array();
    $sum = '';
    $cash = 0;
    $totalCash = 0;
    $totalCard = 0;
    $totalDebit = 0;
    $totalReceivable= 0;
    $transactions = array();
    foreach($cashier as $transaction){

      if ($transaction['FolioTransaction']['id'] == null) {
        $paidAmount = 0;
        $paymentType = '';
        $str = '';
         

        // subs
        $amount = 0;
        $items = array();
        foreach($transaction['TransactionSub'] as $sub){
          $items[] = array(
            'particulars'=>$sub['particulars'],
            'amount'=>$sub['amount'],
            'quantity'=> $sub['quantity']
          );
          $amount+=$sub['amount']*$sub['quantity'];
        }

        // discounts
        $totalDiscount = 0;
        $transactionDiscounts = array();
        $totalPercent = 0;
        $percentRate = 0;
        foreach ($transaction['TransactionDiscount'] as $dis) {
          $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $amount * ($dis['value']/100);
          $totalDiscount += $discountAmount;
          if($dis['type'] == 'percent') {
            $percentRate = $dis['value']/100;
            $totalPercent += $percentRate;
        }

        $transactionDiscounts[] = array(
            'id'     => $dis['id'],
            'name'   => ucwords($dis['Discount']['name']),
            'value'  => $dis['value'],
            'rate'   => $percentRate,
            'type'   => $dis['type'],
            'amount' => $discountAmount
          );
        }

        // payments
        foreach($transaction['TransactionPayment'] as $pay){

            $totalChange += $pay['change'];

            $payments[] = array(
              'or_number'=>$pay['orNumber'],
              'amount'=>$pay['amount'],
              'date'=>date('m/d/Y', strtotime($pay['date'])),
              'change'=>$pay['change'],
              'time'=>date('h:i:s A', strtotime($pay['created'])),
              'paymentType'=>($pay['paymentType'])
            );

            @$paid += $pay['amount']-$pay['change'];

              // summary tally
              if($pay['paymentType']=='cash' || $pay['paymentType']=='cheque'||$pay['paymentType']==null) {
                $totalCash += ($pay['amount']-$pay['change']);
                $paymentType = 'cash'; 
              } 
              if ($pay['paymentType']=='credit card') {
                $totalCard += ($pay['amount']-$pay['change']);
                $paymentType = 'credit card';
              } 

              if ($pay['paymentType']=='debit') {
                $totalDebit += ($pay['amount']-$pay['change']);
                $paymentType = 'debit';
              }
        }

        if(!empty($transaction['TransactionPayment'])) {
          $transactions[] = array(
              'id'=>@$transaction['Transaction']['id'],
              'code'=>@$transaction['Transaction']['code'],
              'business_id'=>@$transaction['Transaction']['businessId'],
              'title'=>@$transaction['Transaction']['title'],
              'date'  => @$transaction['Transaction']['date'],
              'paymentType'=>$paymentType,
              'items'=>$items,
              'payments'=>$payments,
              'amount'=>$amount,
              'discount'=>$totalDiscount,
              'paid'=>$paid,
              'balance'=>($amount-$paid)-($totalDiscount)
            );
            $transactionAmount += $amount;
            $paidAmount += @$paid;
        }

        if(@$paidAmount!=0) {
          $cashiering['Transactions'] = array(

            'charges'=>@$transactions,
            'amount'=>@$transactionAmount,
            'paid'=> @$paidAmount,
            'discount'=> $totalDiscount,
            'folio'=>$transaction['FolioTransaction'],
            'balance'=> ($transactionAmount - $paidAmount  - $totalDiscount),
          );    

        @$cashiering['Summary'] = array(
          'TotalCash'     =>   @$totalCash,
          'TotalCard'     =>  @$totalCard,
          'TotalDebit'    =>  @$totalDebit,
          'Total'         =>  (@$totalCash + $totalCard  + $totalDebit)
        );  

        }         
      }
    }

    if(!isset($cashiering['Transactions']) || !isset($cashiering['Summary'])) {
      $cashiering['Transactions']['charges'] = array();
      $cashiering['Summary'] = array(
        'TotalCash'     =>  0,
        'TotalCard'     =>  0,
        'TotalDebit'    =>  0,
        'Total'         =>  0
      );
    }


      return $cashiering;

  }
 } 