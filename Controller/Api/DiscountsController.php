<?php 
class DiscountsController extends AppController {
	
	public $layout = null;
	public $components = array('Paginator', 'RequestHandler');
	
  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }

	public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    $conditions['Discount.visible'] = true;
    
    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];

      $conditions['OR'] = array(
        'Discount.name LIKE'  => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Discount.name' => 'ASC'
      )
    );
    $modelName = 'Discount';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];
		
		// transform data
    $discounts = array();
		foreach ($tmpData as $data) {
      $discount = $data['Discount'];

			$discounts[] = array(
			'id'    => $discount['id'],
			'name'  => $discount['name'],
			'value' => $discount['value'],
			'type'  => $discount['type'],
			);
		}
		
		$response = array(
			'ok'        => true,
			'data'      => $discounts,
      'paginator' => $paginator,
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
	}

  public function view($id = null) {
    $discount = $this->Discount->findById($id);

  	$response = array(
  		'ok'   => true,
  		'data' => $discount,
  	);
  	
  	$this->set(array(
  		'response'=>$response,
  		'_serialize'=>'response'
  	));
  }

  public function add() {
		if ($this->Discount->save($this->request->data)) {
			$response = array(
				'ok'  => true,
				'msg' => 'Discount has been saved.'
			);
		}

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }

  public function edit($id = null) {
		if($this->Discount->save($this->request->data)) {
			$response = array(
				'ok'  => true,
				'msg' => 'Discount has been saved.'
			);
		}

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }

  public function delete($id = null) {
    if ($this->Discount->hide($id)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Discount has been deleted.'
      );
    }

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  } 
    
}
