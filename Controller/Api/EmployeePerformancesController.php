<?php
class EmployeePerformancesController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  
  // public function index(){
  

  //   $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
    
  //   // default conditions
  //   $conditions['EmployeePerformance.visible'] = true;
     
  //   $paginatorSettings = array(
  //     'conditions' => $conditions,
      
  //     'order' => array(
  //       'EmployeePerformance.id' => 'ASC',
  //         )
  //     );
      
  //     $modelName = 'EmployeePerformance';
  //     $this->Paginator->settings = $paginatorSettings;
  //     $tmpData    = $this->Paginator->paginate($modelName);
  //     $paginator  = $this->request->params['paging'][$modelName];
        
  //     $vehicletypes = array();
  //       foreach ($tmpData as $data) {
  //         $vehicletype = $data['EmployeePerformance'];
        
  //         $vehicletypes[] = array(
  //           'id'        => $vehicletype['employeeId'],
  //           'name'      => $vehicletype['remarks'],
  //         );
  //       }

  //   $response = array(
  //       'ok'  => true,
  //       'data'      => $vehicletypes,
  //       'test'  =>  $tmpData,
  //       'paginator' => $paginator,
  //   );

  //   $this->set(array(
  //     'response'   => $response,
  //     '_serialize' => 'response'
  //   ));
  // }
  
   public function view($id = null) {
    $performance = $this->EmployeePerformance->findById($id);

  	$response = array(
  		'ok'   => true,
  		'data' => $performance,
  	);
  	
  	$this->set(array(
  		'response'=>$response,
  		'_serialize'=>'response'
  	));
  }
  
  
   public function add() {
    if(isset($this->request->data['vid'])) {
      $this->request->data['EmployeePerformance']['id'] = $this->request->data['vid'];
      $this->request->data['EmployeePerformance']['remarks'] = $this->request->data['vremarks'];
    } else {
      $this->request->data['EmployeePerformance']['remarks']    = $this->request->data['remarks'];
      $this->request->data['EmployeePerformance']['employeeId'] = $this->request->data['employeeId'];
      // $this->request->data['EmployeePerformance']['created']    = $this->request->data['created']);
   
    }
    if ($this->EmployeePerformance->save( $this->request->data['EmployeePerformance'])) {
      $response = array(
        'ok'  => true,
        'msg' => 'Employee Remarks has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Employee Remarks  cannot be save this time.'
      );
    }
      
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
    public function edit($id = null) {
     if ($this->EmployeePerformance->save(array(
      'remarks'    => $this->request->data['performance']['remarks'],
      ))) {
      $response = array(
        'ok'  => true,
        'msg' => 'Employee Remarks has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Employee Remarks  cannot be save this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
 
   public function delete($id = null) {
    if ($this->EmployeePerformance->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
}
