<?php 
class CalendarsController extends AppController {
	
	public $layout = null;
	public $uses = array(
		'Room',
		'Folio',
		'ReservationRoom'
	);
	
	
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
		// start Dates
		$date = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])):date('Y-m-d');
		$datee = date('Y-m-d');
		$endDate = date('Y-m-d', strtotime('+6days', strtotime($datee)));

		$dates = array();
		$start = strtotime($date);
		for($i=0; $i<7; $i++){
			$dates[] = array(
				'date'=>date('Y-m-d', $start)
			);
			$start += 86400;
		}
		// end Dates

		// occupied
		$occupied = array();
		$occupancy = $this->Folio->find('all', array(
			'conditions' => array(
				'OR' => array(
					"arrival >= '$date' AND arrival <= '$endDate'",
					"departure <= '$date' AND departure <= '$endDate'",
					"arrival <= '$date' AND departure >= '$date'",
					"arrival <= '$endDate' AND departure >= '$endDate'"
				),
				'visible'=>true,
				'closed'=>false
			),
			'fields'=>array(
			  'id',
				'roomId',
				'arrival',
				'departure',
				'nights'
			)
		));
    
		foreach ($occupancy as $o=>$occ) {
			$arrival = strtotime($occ['Folio']['arrival']);
			$departure = strtotime('+1day',strtotime($occ['Folio']['departure']));

			if ($departure < strtotime(date('Y-m-d'))) {
				$extension = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
				$days = $extension + $occ['Folio']['nights'] + 1;
			}

			if ($departure >= strtotime(date('Y-m-d'))) {
				$departure = strtotime('+1day',strtotime($occ['Folio']['departure']));
				$days = ceil(($departure - $arrival)/86400) + 1;
	      $extension = 0;
	    }

			// $days = ceil(($departure - $arrival)/86400) + 1;
			for ($i=0; $i<$days; $i++) {
				$occupied[$occ['Folio']['roomId'] .'-'. date('Y-m-d', strtotime("+$i days", $arrival))] = $occ['Folio']['id'];
				// $occupied[$occ['Folio']['roomId'] .'-'. date('Y-m-d', strtotime("+$i days", $arrival))] = $occ['Folio']['id'] .'-'.$extension.'-'.$days;
			}
		}
		// occupied
		
		
		// reserved
		$reserved = array();
		$reserves = $this->ReservationRoom->find('all', array(
			'conditions'=>array(
				'OR'=>array(
					// "arrival >= '$date'",
					// "departure <= '$date' AND departure <= '$endDate'",
					// "arrival >= '$date' AND departure >= '$date'",
					// "arrival <= '$endDate' AND departure >= '$endDate'"
				),
				'arrival >='	=>	$date,	
				'visible'=>true,
				'closed'=>false
			),
			'fields'=>array(
				'roomId',
				'arrival',
				'departure'
			)
		));
		foreach($reserves as $r=>$res){
			$arrival = strtotime($res['ReservationRoom']['arrival']);
			$departure = strtotime('+1day',strtotime($res['ReservationRoom']['departure']));
			$days = ceil(($departure - $arrival)/86400) + 1;
			//$occupancy[$o]['days'] = $days;

			for($i=0; $i<$days; $i++){
				$reserved[] = $res['ReservationRoom']['roomId'] .'-'. date('Y-m-d', strtotime("+$i days", $arrival));
			}
		}
		// reserved
	
	
		// start Rooms
		$conditions['Room.visible'] = true;
		if(isset($this->request->query['search'])){
			$conditions['OR'] = array(
				'Room.code LIKE'=> '%' . $this->request->query['search'] . '%',
				'Room.name LIKE'=> '%' . $this->request->query['search'] . '%'
			);
		}

		$rooms = $this->Room->find('all', array(
      'contain'=>array(
      	'RoomType'	=>	array(
      		'conditions'  =>  array(
            'RoomType.visible'	=>	true
          )  
      	)
      ),
      'conditions'=>$conditions
    ));

		foreach($rooms as $key=>$room){
			$rooms[$key] = array(
				'id'=>$room['Room']['id'],
				'name'=>$room['Room']['name'],
				'type'=>$room['RoomType']['name']
			);
			foreach($dates as $d){
				$roomKey = $room['Room']['id'] .'-'. $d['date'];
        $folioId = null;
				if (array_key_exists($roomKey, $occupied)) {
				  $roomStatus = 'occupied';
          $folioId = $occupied[$roomKey];
        } elseif(in_array($roomKey, $reserved)) {
          $roomStatus = 'reserved';
				} else {
				  $roomStatus = 'available';
				}
				$rooms[$key]['availability'][$roomKey] = array(
					'status'   => $roomStatus,
					'folioId' => $folioId
				);
			}
		}
		// end Rooms

		$response = array(
			'date'=>$date,
		// 	'temp'=>$occupancy,
			'occupied'=>@$occupied,
			'reserved'=>@$reserved,
			'response'=>true,
			'message'=>'Getting data successful.',
			'dates'=>$dates,
			'result'=>$rooms,
			'endDate'=>$endDate
		);
		
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
	}

	public function view($id = null){
		$data = $this->Department->findById($id);
		$response = array(
			'response'=>true,
			'result'=>$data
		);
		
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
	}

    public function add(){
		$this->request->data = $this->request->input('json_decode', true);
		$this->Department->create();
		if($this->Department->save($this->request->data)){
			$response = array(
				'response'=>true,
				'message'=>'Department has been saved.'
			);
		}
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
    }

    public function edit($id = null){
		$this->request->data = $this->request->input('json_decode', true);
		if($this->Department->save($this->request->data)){
			$response = array(
				'response'=>true,
				'message'=>'Department has been saved.'
			);
		}
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
    }

    public function delete($id = null){
		$this->Department->visible($id, false);
		$response = array(
			'response'=>true,
			'message'=>'Department has been deleted.'
		);
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
    }

}
