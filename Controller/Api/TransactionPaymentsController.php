<?php 
class TransactionPaymentsController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public $uses = array('Business','Department','TransactionSub','Transaction','TransactionPayment', 'TransactionDiscount','TableTransaction');

  public function index() {
    // search OR NUMBER if exist
    $orNumber = false;
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $businessId = $this->Business->get($this->request->query['businessId']);
      $departmentId = $this->Department->get($this->request->query['businessId']);
      $data = $this->TransactionPayment->find('first', array(
        'contain' =>  array(
          'Transaction' =>  array(
            'conditions'  =>  array(
              'Transaction.businessId' => $businessId,
              'Transaction.visible'    => true
            )
          ),
          'SupplierPayment' =>  array(
             'conditions'  =>  array(
                'SupplierPayment.visible'  => true
              ),
            'Supplier'  =>  array(
              'Department',
              'conditions'  =>  array(
                'Supplier.visible'  => true
              )
            )
          )
        ),
        'conditions'  =>  array(
          'TransactionPayment.orNumber' => $search,
          'TransactionPayment.visible'  => true
        )
      ));



      if (!empty($data['TransactionPayment'])&&$data['Transaction']['id']!=null) {
        $orNumber = true;
      }
      if(!empty($data['SupplierPayment'])){
        if(@$data['SupplierPayment'][0]['Supplier']['departmentId']==$departmentId) {
          $orNumber = true;
        }
      }
      
    }

    $response = array(
      'ok'        =>  true,
      'data'      =>  $orNumber,
      'code'      =>   $data
    );

    $this->set(array(
      'response'    =>  $response,
      '_serialize'  =>  'response'
    ));
  }

  public function view($id=null) {
    $payment = $this->TransactionPayment->find('first',array(
      'conditions'  =>  array(
        'TransactionPayment.id' =>  $id
        // 'TransactionPayment.transactionId' =>  1
      )
    ));

    // search OR NUMBER if exist
    $orNumber = false;
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $data = $this->TransactionPayment->find('first', array(
        'conditions'  =>  array(
          'TransactionPayment.orNumber LIKE' => "%$search%",
          'TransactionPayment.visible'       => true
        )
      ));
    }

    $response = array(
      'ok'        =>  true,
      'data'      =>  $payment
    );

    $this->set(array(
      'response'    =>  $response,
      '_serialize'  =>  'response'
    ));
  }

  public function add() {
    $this->request->data['TransactionPayment']['date'] = date('Y-m-d',strtotime($this->request->data['TransactionPayment']['date']));

    if ($this->TransactionPayment->save($this->request->data['TransactionPayment'])) {
      
      if (isset($this->request->data['TransactionPayment']['supplierId'])) {
        // save supplier payment
        $this->TransactionPayment->SupplierPayment->save(array(
          'supplierId'  =>  $this->request->data['TransactionPayment']['supplierId'],
          'paymentId'   =>  $this->TransactionPayment->getLastInsertId()
        ));
      }

      // get total payments
      $totalPayments = $this->TransactionPayment->find('all',array(
        'conditions' => array(
          'TransactionPayment.transactionId' => $this->request->data['TransactionPayment']['transactionId'],
          'TransactionPayment.visible' => true
        ),
        'fields' => array(
          'SUM(TransactionPayment.amount) as totalAmount',
          'SUM(TransactionPayment.change) as totalChange'
        )
      ));

      // get total discounts
      $totalDiscounts = $this->TransactionDiscount->find('all',array(
        'conditions' => array(
          'TransactionDiscount.transactionId' => $this->request->data['TransactionPayment']['transactionId'],
          'TransactionDiscount.visible' => true
        )
      ));

      // get transaction sub 
      $subAmount = $this->TransactionSub->get($this->request->data['TransactionPayment']['transactionId']);

      // transform discount
      $totalDiscount = 0;
      foreach ($totalDiscounts as $dis) {
          $discountAmount = $dis['TransactionDiscount']['type'] == 'fixed'? (int)$dis['TransactionDiscount']['value'] : $subAmount * ($dis['TransactionDiscount']['value']/100); 
          $totalDiscount += $discountAmount;
      }
      

        $grandTotalPayment = @$totalPayments[0][0]['totalAmount'] - @$totalPayments[0][0]['totalChange'];

      if($subAmount == ($grandTotalPayment+$totalDiscount)) {
        $transactionSubId = $this->TransactionSub->getSub($this->request->data['TransactionPayment']['transactionId']);

        $this->request->data['TransactionSub']['id'] = $transactionSubId;
        $this->request->data['TransactionSub']['paid'] = true;

          // if may folio code
          if (!empty($this->request->data['TransactionPayment']['folioCode'])) {
            $transactionId = $this->request->data['TransactionPayment']['transactionId'];
            $this->Transaction->save(array(
              'id'    => $transactionId,
              'paidByParentId' => true
            ));  
          }

          $this->TransactionSub->save($this->request->data['TransactionSub']); 

          // $this->TableTransaction->save(array(
          //   'id'    =>  $this->request->data['TransactionPayment']['tableTransactionId'],
          //   'paid'  =>  true
          // ));

        $result = true;
      } else {
        // if(isset($this->request->data['TransactionPayment']['tableTransactionId'])) {
        //   $this->TableTransaction->save(array(
        //     'id'    =>  $this->request->data['TransactionPayment']['tableTransactionId'],
        //     'paid'  =>  false
        //   ));
        // }
        $result = false;
      }

      if(empty($this->request->data['TransactionPayment']['folioCode'])) {
        $this->request->data['TransactionPayment']['folioCode'] = '';
      }

      $response = array (
        'ok'  => true,
        'msg' => 'Transaction Payment has been saved',
        'totalPayments' => $grandTotalPayment,
        'discount' => $totalDiscount ,
        'sub' => $subAmount,
        'folioCode' => $this->request->data['TransactionPayment']['folioCode'],
        'result' => $result
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Transaction Payment cannot be save this time'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));

  }

  public function edit($id = null) {  
    $this->request->data['TransactionPayment']['id']=$id;

    if ($this->TransactionPayment->save($this->request->data['TransactionPayment'])) {
      $response = array (
        'ok'  => true,
        'msg' => 'Transaction Payment has been saved'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Transaction Payment cannot be save this time'
      );
    }
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

    public function delete($id = null) {

    if ($this->TransactionPayment->hide($id)) {
       $response = array(
        'ok'      =>  true,
        'id'      => $id,
        'message' =>  'TransactionPayment has been deleted.'
      );
    }
     $this->set(array(
      'response'    =>  $response,
      '_serialize'  =>  'response'
    ));
  }

  public function checkPaid() {

  } 

}
