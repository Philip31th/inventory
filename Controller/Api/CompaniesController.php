<?php 
class CompaniesController extends AppController {
  
  public $layout = null;  
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {

    // COMPANY LIST 
    if (!isset($this->request->query['company']) && !isset($this->request->query['coy'])){
      $data = $this->Company->find('all',array(
        'conditions'  =>  array(
          'visible'   =>  true
        )
      ));
    
    $result = array();
      if(!empty($data)) {
       foreach ($data as $value) {
          $result[] = strtoupper($value['Company']['name']);
       }
      }
      $response = array('data'=>$result);

    } else {
      // default page 1
      $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

      // default conditions
      $conditions = array();
      $conditions['Company.visible'] = true;

      // search conditions
      if (isset($this->request->query['search'])) {
        $search = $this->request->query['search'];
        $conditions['OR'] = array(
          'Company.name LIKE'        => "%$search%",
        );
      }

      // paginate data
      $paginatorSettings = array(
        'conditions' => $conditions,
        'limit'      => 25,
        'page'       => $page,
        'order'      => array(
          'Company.name' => 'ASC'
        )
      );
      $modelName = 'Company';
      $this->Paginator->settings = $paginatorSettings;
      $tmpData     = $this->Paginator->paginate($modelName);
      $paginator = $this->request->params['paging'][$modelName];

      foreach ($tmpData as $key => $value) {
        $tmpData[$key]['Company']['date'] = date('M d, Y h:i A',strtotime($tmpData[$key]['Company']['created']));
      }

      $response = array(
        'ok'  =>true,
        'data'=>$tmpData,
        'paginator'  => $paginator
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {

    $company = $this->Company->findById($id);

    $coy = $company['Company'];
    $con = $coy['contactPersonName'];
    $title   = $coy['contactPersonTitle'] . ' ';
     
    $company['Company'] = array(
      'id'            =>  $coy['id'],
      'name'          =>  properCase($coy['name']),
      'contactPerson' =>  $con!=null?  $title . properCase($con): '',
      'contactPersonTitle' => $coy['contactPersonTitle'],
      'contactPersonName' => properCase($coy['contactPersonName']),
      'email'         =>  strtolower($coy['email']),  
      'mobile'        =>  $coy['mobile'],  
      'phone'         =>  $coy['phone'],  
      'fax'           =>  $coy['fax'],  
      'address'       =>  properCase($coy['address']),  
      'date'       =>  date('M d, Y h:i A',strtotime($coy['created']))
    );

    $response = array(
      'ok'   => true,
      'data' => $company,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));

    return $company;
  }


  public function add() {
    if ($this->Company->save($this->request->data)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Company has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Company cannot be save this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    if($this->Company->save($this->request->data)){
      $response = array(
        'ok'  => true,
        'msg' => 'Company has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Company cannot be save this time.'
      );
    }

    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }
}