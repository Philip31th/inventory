<?php 
class RoomsController extends AppController {
  
	public $layout = null;
	public $uses = array('Room', 'Folio','ReservationRoom');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
    // default date
    $date = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])) : date('Y-m-d');
    $edate   = date('Y-m-d', strtotime('-1day',strtotime($date)));

    // default conditions
    $conditions = array();
    if (!$this->superUser())
      $conditions['Room.visible'] = true;

    // load rooms
		$tmpData = $this->Room->find('all', array(
      'contain'=>array(
        'Accomodation' => array(
          'name'
        ),
        'RoomType' => array(
          'name',
          'rate'
        ),
        'Folio'=>array(
          'id',
          'arrival',
          'departure',
          'closed',
          'visible',
          'Guest' => array(
            'lastName',
            'firstName',
            'middleName'
          ),
          'conditions' => array(
            'OR'  =>  array(
               // 'Folio.departure >=' => $edate,
               // 'Folio.departure <=' => $edate,
                "arrival >= '$date' AND arrival <= '$edate'",
                "departure <= '$date' AND departure <= '$edate'",
                "arrival <= '$date' AND departure >= '$date'",
                "arrival <= '$edate' AND departure >= '$edate'"
            ),
            // 'Folio.arrival'      => $date,
            'Folio.closed'       => false,
            'Folio.visible'      => true 
          )
        ),
        'ReservationRoom'=>array(
          'id',
          'arrival',
          'departure',
          'closed',
          'checkedIn',
          'checkOutDate',
          'Reservation' => array(
            'lastName',
            'firstName',
            'middleName'
          ),
          'conditions' => array(
            // 'ReservationRoom.closed' => false,
            'ReservationRoom.arrival'   => $date,
            'ReservationRoom.departure >=' => $edate,
            // 'ReservationRoom.checkedIn' => true,
            'ReservationRoom.visible' => true,
            'ReservationRoom.checkOutDate' => null
          )
        ),
      ),
      'conditions' => $conditions,
      'order' => array(
        'Room.name' => 'ASC'
      )
    ));

    // transform data
    $rooms = array();
    foreach ($tmpData as $data) {
      $room  = $data['Room'];
      $type  = $data['RoomType'];
      $accom = $data['Accomodation'];
      $rr    = $data['ReservationRoom'];

      // foreach ($data['ReservationRoom'] as $rs) {
      //   if ($rs['departure'] != date('Y-m-d')) {
      //     $departure = strtotime('+1day',strtotime($data['ReservationRoom']['departure']));
      //   }
      // }
      
      // check status
        if (count($data['Folio']) > 0) {
          $status = 'occupied';
        } elseif (count($data['ReservationRoom']) > 0 ) {
          $status = 'reserved';
        } elseif ($room['uncleaned']) {
          $status = 'uncleaned';
        } else {
          $status = 'available';
        }

      $available = $status == 'available'? true : false;

      $rooms[] = array(
        'id'           => $room['id'],
        'name'         => $room['name'],
        'type'         => $type['name'],
        'accomodation' => $accom['name'],
        'rate'         => $room['rate'],
        'status'       => $status,
        'available'    => $available,
        'visible'      => $room['visible']
      );
    }

		$response = array(
			'ok'   => true,
			'date' => $date,
			'data' => $rooms,
      'total' => count($rooms),
			'tmp'  => $tmpData
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
	}

	public function view($id = null) {
    $room = $this->Room->find('first', array(
      'contain' => array(
        'RoomType',
        'Accomodation'
      ),
      'conditions' => array(
          'Room.id' => $id
      )
    ));
      
    $response = array(
      'ok'   => true,
      'data' => $room
    );

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
	}

  public function add() {
    // get accomidation id
    $type = $this->Room->RoomType->findById($this->request->data['Room']['roomTypeId']);
    $this->request->data['Room']['accomodationId'] = $type['RoomType']['accomodationId'];
    $this->request->data['Room']['rate'] = $type['RoomType']['rate'];

    // save room
    $save = $this->Room->validSave($this->request->data['Room']);
    $response = $save;

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }

  public function edit($id = null) {
    // get accomidation id
    $type = $this->Room->RoomType->findById($this->request->data['Room']['roomTypeId']);
    $this->request->data['Room']['accomodationId'] = $type['RoomType']['accomodationId'];

    // save room
    $save = $this->Room->validSave($this->request->data['Room']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->superUser())
      $delete = $this->Room->delete($id);
    else
      $delete = $this->Room->hide($id);

    if ($delete) {
      $response = array(
        'ok'  => true,
        'msg' => 'Room has been deleted.'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Room cannot deleted this time.'
      );
    }

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }
	

 public function api_occupied() {
    $date = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])):date('Y-m-d');
    $edate = date('Y-m-d', strtotime('-1day',strtotime($date)));
		
	  $occupieds = $this->Folio->find('all', array(
			'contain'=>array(
				'Guest'  => array(
          'conditions'=>array(
            'Guest.visible'=>true
          )
        ),
				'Room'=>array(
          'RoomType',
          'conditions'=>array(
            'Room.visible'=>true
          )
        )
			),
			'conditions'=>array(
				'Folio.closed'=>false,
        'Folio.arrival <='=>$date,
        'Folio.departure >'=>$edate,
        'Folio.visible'    => true
			)
	  ));

    $conditions = array();
    $conditions['ReservationRoom.checkedIn'] = true;
    $conditions['ReservationRoom.checkOutDate'] = null;
    $conditions['ReservationRoom.departure <='] = $edate;
    $conditions['ReservationRoom.visible'] = true;

    $outgoingList = $this->ReservationRoom->find('all', array(
      'contain'=>array(
        'Room'=> array(
            'RoomType',          
            'conditions'  =>  array('Room.visible' => true)
          ),
        'Reservation' => array('conditions'=>array('Reservation.visible'=>true)),
        'Folio'=>array(
          'Guest' => array(
            'conditions'=>array('Guest.visible'=>true)
           ),
          'conditions'=>array('Folio.visible'=>true)
        )
      ),
      'conditions'=>$conditions
    ));

    $occupiedss = array();
		foreach($occupieds as $i=>$occupied){
        $occupiedss[$i]=array(
            'id'=>$occupied['Folio']['id'],
            'room'=>$occupied['Room']['code'],
            'type'=>$occupied['Room']['RoomType']['name'],
            'guest'=>$occupied['Guest']['lastName']. ', ' . $occupied['Guest']['firstName'],
            'arrival'=>date('m/d/Y', strtotime($occupied['Folio']['arrival'])),
            'departure'=>date('m/d/Y', strtotime('+1day', strtotime($occupied['Folio']['departure']))),
            'closed' => $occupied['Folio']['closed'],
            'transfered' => $occupied['Folio']['transferedBills'],        
          );
		}
    
    
    $outgoingGuests = array();
    foreach($outgoingList as $data){
    
    if ($data['ReservationRoom']['departure']!=date('Y-m-d')) {
      $departure = strtotime('+1day',strtotime($data['ReservationRoom']['departure']));
      $days = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);

    } else { $days = 0; }
      
      $outgoingGuests[] = array(
        'id'  =>$data['Folio']['id'],
        'room'=>$data['Room']['code'],
        'type'=>$data['Room']['RoomType']['name'],
        'guest'=>$data['Folio']['Guest']['lastName']. ', ' . $data['Folio']['Guest']['firstName'],
        'arrival'=>date('m/d/Y', strtotime($data['Folio']['arrival'])),
        'departure'=>date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
        'closed' => $data['Folio']['closed'],
        'transfered' => $data['Folio']['transferedBills'],
        'extended' => $days==0?'':$days . ' day(s)'
      );
    }

    $response = array(
      'response'  => true,
      'message'   => 'occupied rooms',
      'count'     =>count($outgoingList + $occupiedss),
      'occupied'  => $occupiedss,
      'out'       => $outgoingGuests,
      'date'      => $date,
      'edate'     => $edate
    );
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
	}

}
