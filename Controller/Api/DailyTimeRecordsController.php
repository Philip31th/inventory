<?php 
class DailyTimeRecordsController extends AppController {
  
  public $components = array('Paginator', 'RequestHandler');
  public $uses       = array('Employee', 'DailyTimeRecord');

  public function beforeFilter () {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $date = isset($this->request->query['search'])? date('Y-m-d H:i:s', strtotime($this->request->query['search'])):date('Y-m-d 00:00:00');
    
    // default conditions
    $conditions = array();
    $conditions['DailyTimeRecord.visible'] = true;

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];

      $conditions['OR'] = array(
        'Employee.firstName LIKE' => "%$search%",
        'Employee.lastName LIKE'  => "%$search%"
      );
    }

    // paginate data
    $paginatorSettings = array(
      'conditions' => $conditions,
      'contain' => array(
        'Employee'
      ),
      'order' => array(
        'Employee.firstName' => 'ASC',
        'DailyTimeRecord.id' => 'ASC'
      )
    );
     
    $modelName = 'DailyTimeRecord';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData    = $this->Paginator->paginate($modelName);
    $paginator  = $this->request->params['paging'][$modelName];
    
    // transform data
    $records = array();
    foreach ($tmpData as $data) {
      $emp = $data['Employee'];
      $dtr = $data['DailyTimeRecord'];
    
      // total hours
      $timeIn  = strtotime('+8 hours',strtotime($dtr['timeIn'])); 
      $timeOut = strtotime('+8 hours',strtotime($dtr['timeOut']));

      $hours         = (($timeOut - $timeIn)/3600);
      $minutes       = ($hours - floor($hours)) * 60; 
      $final_hours   = round($hours,0);
      $final_minutes = round($minutes);

      $records[] = array(
        'id'          => $dtr['id'],
        'name'        => $emp['firstName'] . ' ' . $emp['lastName'],
        'timeIn'      => date('M d,Y h:i:s A', strtotime('+8 hours',strtotime($dtr['timeIn']))),
        'timeOut'     => date('M d,Y h:i:s A', strtotime('+8 hours',strtotime($dtr['timeOut']))),
        'total_hours' => $final_hours . 'H ' . $final_minutes . 'm'

      );
    }

    $response = array(
      'ok'        => true,
      'data'      => $records,
      'paginator' => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function view($id = null) {
    // default conditions
    $conditions = array();
    $conditions['DailyTimeRecord.visible'] = true;
    $conditions['DailyTimeRecord.id'] = $id;
  
    // load data
    $datas = $this->DailyTimeRecord->find('first',array(
      'contain'   => array(
        'Employee'  => array('lastName','firstName')
      ),
      'conditions'=> $conditions
    ));
    
    foreach ($datas as $k => $data) {
      $dtr = $datas['DailyTimeRecord'];
      $emp = $datas['Employee'];
      
      // total hours
      $timeIn  = strtotime('+8 hours',strtotime($dtr['timeIn'])); 
      $timeOut = strtotime('+8 hours',strtotime($dtr['timeOut']));
    
      $hours = ((($timeOut - $timeIn)/3600));
      $minutes = ($hours - floor($hours)) * 60; 
      $final_hours = round($hours,0);
      $final_minutes = round($minutes);
      
      $datas[$k] = array(
        'id'           => $dtr['id'],
        'name'         => $emp['firstName'] . ' ' . $emp['lastName'],
        'in'           => date('M d,Y h:i:s A', strtotime('+8 hours',strtotime($dtr['timeIn']))),
        'out'          => date('M d,Y h:i:s A', strtotime('+8 hours',strtotime($dtr['timeOut']))),
        'total_hours'  => $final_hours . 'H ' . $final_minutes . 'm',
        'timeIn'       => $dtr['timeIn'],
        'timeOut'      => $dtr['timeOut']
      );
    }  
        
    $response = array(
      'ok'        => true,
      'data'      => $datas,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function add() {
// 2015-07-19T21:56:01.243Z
    // $this->DailyTimeRecord->data['DailyTimeRecord']['timeIn'] = date('Y-m-dTH:i:sZ', strtotime('+8 hours',strtotime($this->DailyTimeRecord->data['DailyTimeRecord']['timeIn'])));

    if($this->DailyTimeRecord->save($this->request->data)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Daily Time Record has been added.',
        'data' => $this->request->data
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Daily Time Record cannot be saved this time.',
        'data'=> $this->request->data
      );
    }

    // $response = array(
    //     'ok'  => true,
    //     // 'msg' => 'Daily Time Record cannot be saved this time.',
    //     'data'=> $this->request->data
    //   );

    $this->set(array(
        'response'   => $response,
        '_serialize' => 'response'
    ));
  }
  
  public function edit($id = null) {

    if ($this->DailyTimeRecord->save($this->request->data['DailyTimeRecord'])) {
      $response = array(
        'ok'   => true,
        'msg'  => 'DailyTimeRecord has been updated.',
      );
    } else {
      $response = array(
        'ok'   => false,
        'msg'  => 'DailyTimeRecord cannot be saved this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->DailyTimeRecord->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
        'msg'  => 'DTR has been deleted.',
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
        'msg'  => 'DTR cannot be deleted this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  } 
}
  


