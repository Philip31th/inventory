<?php
class SelectController extends AppController {

  public $layout = null;

  public $uses = array(
    'Department',
    'Accomodation',
    'RoomType',
    'Room',
    'Employee',
    'Penalty',
    'BusinessService',
    'Setting',
    'Guest',
    'Folio',
    'Discount',
    'Business',
    'Inventory',
    'Menu',
    'Company'

  );
  
  public function beforeFilter () {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index () {
    $datas = array();
    $code  = null;
    
    if (isset($this->request->query['code']))
      $code = $this->request->query['code'];
    
    // departments
    if ($code == 'departments') {
      $tmp = $this->Department->find('all', array(
        'conditions' => array(
          'Department.visible' => true
        ),
        'order' => array(
          'Department.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Department']['id'],
          'value' => $data['Department']['name'],
        );
      }

    // room accomodations
    } elseif ($code == 'accomodations') {
      $tmp = $this->Accomodation->find('all', array(
        'conditions' => array(
          'Accomodation.visible' => true
        ),
        'order' => array(
          'Accomodation.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Accomodation']['id'],
          'value' => $data['Accomodation']['name'],
        );
      }

    // room types
    } elseif ($code == 'room-types') {
      $tmp = $this->RoomType->find('all', array(
        'conditions' => array(
          'RoomType.visible' => true
        ),
        'order' => array(
          'RoomType.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['RoomType']['id'],
          'value' => $data['RoomType']['name'],
        );
      }

    // employees
    } elseif ($code == 'employees') {
      $tmp = $this->Employee->find('all', array(
        'conditions' => array(
          'Employee.visible' => true
        ),
        'order' => array(
          'Employee.firstName' => 'ASC',
          'Employee.lastName' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Employee']['id'],
          'value' => $data['Employee']['firstName'] . ' ' . $data['Employee']['lastName'],
          'name'  => ucwords($data['Employee']['firstName'] . ' ' . $data['Employee']['lastName'])
        );
      }
      
    // penalties
    } elseif ($code == 'penalties') {
      $tmp = $this->Penalty->find('all', array(
        'conditions' => array(
          'Penalty.visible' => true
        ),
        'order' => array(
          'Penalty.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Penalty']['id'],
          'name'  => $data['Penalty']['name'],
          'type'  => $data['Penalty']['type'],
          'value' => $data['Penalty']['value'],
        );
      }
      
    // business service
    } elseif ($code == 'business-services') {
      $tmp = $this->BusinessService->find('all', array(
        'contain' => array(
          'Business'
        ),
        'conditions' => array(
          'Business.visible'        => true,
          'BusinessService.visible' => true
        ),
        'order' => array(
          'Business.name'        => 'ASC',
          'BusinessService.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'         => $data['BusinessService']['id'],
          'businessId' => $data['Business']['id'],
          'business'   => $data['Business']['name'],
          'name'       => $data['BusinessService']['name'],
          'type'       => $data['BusinessService']['type'],
          'value'      => $data['BusinessService']['value'],
        );
      }

    // available rooms
    } elseif ($code == 'available-rooms') {

      $arrival = isset($this->request->query['arrival'])? date('Y-m-d', strtotime($this->request->query['arrival'])) : date('Y-m-d');
      $departure = isset($this->request->query['departure'])? date('Y-m-d', strtotime($this->request->query['departure'])) : date('Y-m-d', strtotime('+1day' , strtotime($arrival)));
      $conditions['Room.visible'] = true;
      
      $rooms = $this->Room->find('all', array(
        'contain' => array(
          'RoomType' => array('name', 'rate'),
          'Accomodation' => array('name'),
          'Folio' => array(
            'id',
            'conditions' => array(
              'OR' => array(
                "arrival >= '$arrival' AND arrival <= '$departure'",
                "departure <= '$arrival' AND departure <= '$departure'",
                "arrival <= '$arrival' AND departure >= '$arrival'",
                "arrival <= '$departure' AND departure >= '$departure'"
              ),
              'visible' => true,
              'closed'     => false
            )
          ),
          'ReservationRoom'=>array(
            'id',
            'conditions' => array(
              'OR' => array(
                "arrival >= '$arrival' AND arrival <= '$departure'",
                "departure <= '$arrival' AND departure <= '$departure'",
                "arrival <= '$arrival' AND departure >= '$arrival'",
                "arrival <= '$departure' AND departure >= '$departure'",
              ),
              'visible' => true,
              'closed'     => false
            )
          ),
        ),
        'conditions' => $conditions
      ));
      $availableRooms = array();
      foreach ($rooms as $key => $room) {
        if (count($room['Folio']) == 0 AND count($room['ReservationRoom']) == 0) {
          $availableRooms[] = array(
            'id'           => $room['Room']['id'],
            'code'         => $room['Room']['code'],
            'type'         => $room['RoomType']['name'],
            'rate'         => $room['RoomType']['rate'],
            'accomodation' => $room['Accomodation']['name']
          );  
        }
        
      }
      
      $datas = $availableRooms;
      $response['arrival']   = fdate($arrival);
      $response['departure'] = fdate($departure);
      $response['total']     = count($availableRooms);

    // booking source
    } elseif ($code == 'booking-sources') {
      $tmp = $this->Setting->get('booking-sources');
      $tmp = explode(',', $tmp);
      foreach ($tmp as $data) {
        $datas[] = array(
          'id'    => strtolower($data),
          'value' => properCase($data),
        );
      }

    // payment types
    } elseif ($code == 'payment-types') {
      if(isset($this->request->query['pos'])||isset($this->request->query['supplierPayment'])) {
        $tmp = $this->Setting->get('payment-types2');
      } else {
        $tmp = $this->Setting->get('payment-types');
      }
      
      $tmp = explode(',', $tmp);
      foreach ($tmp as $data) {
        $datas[] = array(
          'id'    => strtolower($data),
          'value' => properCase($data),
        );
      }

    // guests
    } elseif ($code == 'guests') {
      $tmp = $this->Folio->find('all', array(
        'contain' => array(
          'Guest' => array(
            'conditions' => array(
              'Guest.visible' => true
            ),
          ),
          'ReservationRoom'
        ),
        'conditions' => array(
          'Folio.closed' => true,
          'ReservationRoom.checkedIn' => false,
          // 'ReservationRoom.checkOut !=' => null
          ),
        'order' => array(
          'Guest.lastName' => 'ASC',
          'Guest.firstName' => 'ASC',
        ),
        'group' => array(
          'Guest.lastName',
          'Guest.firstName'
        )
      ));

      
      foreach ($tmp as $k => $data) {
        $guest = properCase($data['Guest']['lastName'] . ', ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['middleName']);
     
        $datas[] = array(
          'id'     => $data['Guest']['id'],
          'value'  => $guest, 
        );
      }

    // guest data
    } elseif ($code == 'guest-data') {
      $tmp = $this->Guest->find('first', array(
        'contain' => array(
          'Company',
        ),
        'conditions' => array(
          'Guest.id' => $this->request->query['id']
        )
      ));
      $datas = $tmp;
    
    } elseif ($code == 'folio-data') {
      $tmp = $this->Guest->find('first', array(
        'contain' => array(
          'Company',
          'Folio' => array(
            'ReservationRoom' => array(
               'Room' => array('RoomType'),
                'Reservation',
            ),
            'conditions' => array(
              'Folio.closed' => false
            )
          )
        ),
        'conditions' => array(
          'Guest.id' => $this->request->query['id']
        )
      ));
      // foreach ($tmp as $qry) {
        // $tmps= $tmp['Guest']['Folio'][0]['ReservationRoom']['departure'];
      // }

      $datas = $tmp;

    // folios for bills transferring
    } elseif ($code == 'folio') {
        $tmp = $this->Folio->find('all', array(
        'contain' => array(
          'Guest' => array(
            'firstName','lastName'
          )
        ),  
        'conditions' => array(
          'Folio.id !='   => $this->request->query['excludeId'],
          'Folio.closed'  => false,
          'Folio.visible' => true,
          'Folio.transferedBills !=' => true
        ),
        'order' => array(
          'Folio.code' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Folio']['id'],
          'value' => $data['Folio']['code'] . ' - ' . properCase($data['Guest']['firstName'] . ' ' . $data['Guest']['lastName']),
          'code'  => $data['Folio']['code']
        );
      }

    // folios main  // transactions
    } elseif ($code == 'folios') {
      $conditions = array();
      $conditions['Folio.visible'] = true;
      $conditions['Folio.closed'] = false;
      $conditions['Folio.transferedBills'] = false;

        $tmp = $this->Folio->find('all', array(
          'contain' => array(
            'Guest' => array(
              'firstName','lastName'
            )
          ),
          'conditions' => $conditions,
          'order' => array(
            'Folio.code' => 'ASC',
          )
        ));
        foreach ($tmp as $k => $data) {
          $datas[] = array(
            'id'    => $data['Folio']['id'],
            'value' => $data['Folio']['code'] . ' - ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['lastName'],
            'code'  => $data['Folio']['code']
          );
        }

      // folios for bar/resto pos  
    } elseif ($code == 'pos_folios') {
      $conditions = array();
      $conditions['Folio.visible'] = true;
      $conditions['Folio.closed'] = false;
      $conditions['Folio.transferedBills'] = false;

        $tmp = $this->Folio->find('all', array(
          'contain' => array(
            'Guest' => array(
              'firstName','lastName'
            ),
            'Room' => array(
              'conditions' => array(
                'Room.visible' => true
              )
            )
          ),
          'conditions' => $conditions,
          'order' => array(
            'Folio.code' => 'ASC',
          )
        ));
        foreach ($tmp as $k => $data) {
          $datas[] = array(
            'id'    => $data['Folio']['id'],
            'value' => $data['Folio']['code'] . ' - Room #' . $data['Room']['name']. ' - ' .$data['Guest']['firstName'] . ' ' . $data['Guest']['lastName'],
            'code'  => $data['Folio']['code']
          );
        }
    
    // discounts
    } elseif ($code == 'discounts') {
        $tmp = $this->Discount->find('all', array(
        'conditions' => array(
          'Discount.visible' => true,
        ),
        'order' => array(
          'Discount.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Discount']['id'],
          'type'  => $data['Discount']['type'],
          'name'  => $data['Discount']['name'],
          'value' => $data['Discount']['value'],
        );
      }
      
    // business  
    }  elseif ($code == 'business') {
        $tmp = $this->Business->find('all', array(
        'conditions' => array(
          'Business.visible' => true,
        ),
        'order' => array(
          'Business.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Business']['id'],
          'value' => $data['Business']['name'],
        );
      }
    // menus
    } elseif ($code == 'menus') {
       $tmp = $this->Menu->find('all', array(
        'conditions' => array(
          'Menu.visible' => true,
          'Menu.departmentId' => $this->request->query['departmentId']
        ),
        'order' => array(
          'Menu.name' => 'ASC',
        )
      ));
      foreach ($tmp as $k => $data) {
        $datas[] = array(
          'id'    => $data['Menu']['id'],
          'value' => $data['Menu']['name'],
          'amount'=> (int)$data['Menu']['amount']
        );
      }

     // companies
    } elseif ($code == 'companies') {
      // $search = $this->request->query['search'];
      
      $tmp = $this->Company->find('all', array(
        'conditions' => array(
          'Company.visible' => true,
          // 'Company.name LIKE' => "%$search%"
        ),
        'order' => array(
          'Company.name' => 'ASC',
        )
      ));
      if (!empty($tmp)) {
        foreach ($tmp as $k => $data) {
          $datas[] = $data['Company']['name'];
        }  
      }

      // company data
    } elseif ($code == 'company') {
      $id = $this->Company->get($this->request->query['coy']);
      
      $tmp = $this->Company->findById($id);

      if (empty($tmp)) {

        $tmp['Company'] = array(
          'id'=>'',
          'address'=>'',
          'contactPersonName'=>'',
          'mobile'=>'',
          'phone'=>'',
          'email'=>'',
          'fax'=>''
        );
      }

      $datas = $tmp;
      
    } else {
      $datas = array();
    }

    $response['ok']   = true;
    $response['data'] = $datas;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response',
    ));
  }
  
}
