<?php 
class BarMenuInventoriesController extends AppController {
 
  public $layout = null;
  public $components = array('Paginator', 'RequestHandler');
    
  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }

  public function index() {
    
    $conditions = array();
    $conditions['BarMenus.visible'] = true;

    $datas = $this->Inventory->find('all', array(
        'contain' => array(
            'InventorySub',
            'Department'
        ),
        'conditions' => $conditions
    ));

    foreach ($datas as $k => $data) {
        $quantity = 0;
        foreach ($data['InventorySub'] as $sub) {
            if ($sub['type'])
                $quantity += $sub['quantity'];
            else
                $quantity -= $sub['quantity'];
        }

        $datas[$k] = array(
            'id'         => $data['Inventory']['id'],
            'code'       => $data['Inventory']['code'],
            'name'       => $data['Inventory']['name'],
            'quantity'   => $quantity,
            'department' => $data['Department']['name']
        );
    }

    $response = array(
        'ok'   => true,
        'data' => $datas,
    );
        
    $this->set(array(
        'response'   => $response,
        '_serialize' => 'response'
    ));
 } 
}
  
  