<?php 
class FoliosController extends AppController {
	
  public $uses = array(
    'Folio',
    'Transaction',
    'Business',
    'Reservation',
    'ReservationRoom'
  );

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
		$response = array(
			'ok'   => true,
			'data' => null,
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
	}

	public function view($id = null) {
    $response = array(
      'ok'   => true,
      'data' => null,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
	}

	
  public function add() {
    // transform data
		if(isset($_SERVER["CONTENT_TYPE"]) and strpos($_SERVER["CONTENT_TYPE"], "application/json") !== false)
      $_POST = array_merge($_POST, (array) json_decode(trim(file_get_contents('php://input')), true));
		$this->request->data = json_decode($_POST['data'], true);

    // transform folio data
		$this->request->data['Folio']['code'] = $this->Folio->generateCode(5);
    $this->request->data['Folio']['arrival'] = fdate($this->request->data['Folio']['arrival'], 'Y-m-d');
    $this->request->data['Folio']['departure'] = date('Y-m-d', strtotime('-1day', strtotime($this->request->data['Folio']['departure'])));

    // transform guest data
    $this->request->data['Guest']['code'] = $this->Folio->Guest->generateCode(5);
    $attachmentName = @$_FILES['attachment']['name'];
    if (!empty($attachmentName)) {
      $attachmentExt = pathinfo($attachmentName, PATHINFO_EXTENSION);
      $this->request->data['Guest']['attachment'] = $this->Folio->Guest->nextId() . '.' . $attachmentExt;
    }

    $folioData = array();
    // check if returning customer
    if (!isset($this->request->data['Guest']['id']) or $this->request->data['Guest']['id'] == null or $this->request->data['Guest']['id'] == '')
      {
        
        $this->request->data['Guest']['companyId'] = $this->request->data['Company']['id'];
        $folioData['Guest'] = $this->request->data['Guest'];
       } 
    else {
      $this->request->data['Folio']['guestId'] = $this->request->data['Guest']['id'];
    }

    // add folio data
    $folioData['Folio'] = $this->request->data['Folio'];

    // save folio and guest
    $saveFolio = $this->Folio->saveAll($folioData);
    $folioId = $this->Folio->getLastInsertId();

    // update email in reservation 
    if(!empty($this->request->data['Guest']['email'])) {
      $this->request->data['Reservation']['email'] = $this->request->data['Guest']['email'];
    }

    // update reservation if returning guest
    $this->Reservation->save(array(
        'id'          => $this->request->data['Reservation']['id'],
        'lastName'    => $this->request->data['Guest']['lastName'], 
        'firstName'   => $this->request->data['Guest']['firstName'],
        'middleName'  => $this->request->data['Guest']['middleName'],
        'address'     => $this->request->data['Guest']['address'],
        'gender'      => $this->request->data['Guest']['gender'],
        'mobile'      => $this->request->data['Guest']['mobile'],
        'nationality' => $this->request->data['Guest']['nationality'],
        'companyId'   => $this->request->data['Guest']['companyId']
      ));

    // update reservation room to checked in
    $updateReservationRoom = $this->Folio->ReservationRoom->save(array(
      'id'            => $this->request->data['Folio']['reservationRoomId'],
      'checkedIn'     => true,
      'checkedInDate' => date('Y-m-d'),
    ));

    // upload attachment
    if ($saveFolio and !empty($attachmentName)) {
      $path = "uploads/guests/$folioId";
      if(!file_exists('uploads')) mkdir('uploads');
      if(!file_exists('uploads/guests')) mkdir('uploads/guests');
      if(!file_exists($path)) mkdir($path);
      move_uploaded_file($_FILES['attachment']['tmp_name'], $path . '/' . $this->request->data['Guest']['attachment']);
    }

    // transaction data
    $start = strtotime($this->request->data['Folio']['arrival']) / 86400;
    $end = strtotime($this->request->data['Folio']['departure']) / 86400;
    $room = $this->Folio->Room->findById($this->request->data['Folio']['roomId']);

    // transaction subs data
    $transactionSubs = array();
    for ($i = 0; $i <= $end; $i+=86400) {
      $transactionSubs[] = array(
        'particulars' => 'room #' . $room['Room']['name'] . ' accomodation charge',
        'amount'      => $room['Room']['rate'],
      );
    }
    // save transaction
    $saveTransaction = $this->Transaction->saveAll(array(
      'Transaction' => array(
        'code'        => $this->Folio->FolioTransaction->Transaction->generateCode(6),
        'title'       => 'hotel charges',
        'businessId'  => $this->Business->generateId('hotel'),
        'particulars' => 'room accomodation charges',
        'date'        => date('Y-m-d'),
      ),
      'TransactionSub' => $transactionSubs
    ));
    $transactionId = $this->Folio->FolioTransaction->Transaction->getLastInsertId();

    // save folio transaction
    $saveFolioTransaction = $this->Folio->FolioTransaction->save(array(
      'folioId'       => $folioId,
      'transactionId' => $transactionId,
    ));

    if(!empty($this->request->data['TransactionPayment'])) {
      $this->request->data['TransactionPayment']['transactionId'] = $transactionId;
      $this->request->data['TransactionPayment']['date'] = date('Y-m-d');
      $this->Folio->FolioTransaction->Transaction->TransactionPayment->save($this->request->data['TransactionPayment']);
    }

    if ($saveFolio and $updateReservationRoom and $saveTransaction and $saveFolioTransaction) {
      $response = array(
        'ok'    => true,
        'msg'   => 'Room booking has been checked in.',
        'data'  => $transactionSubs,
      );
    } else {
      $response = array(
        'ok'    => false,
        'msg'   => 'Room booking cannot be check in this time.',
      );
    }

    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
		$this->request->data = $this->request->input('json_decode', true);
		if($this->Department->save($this->request->data)){
			$response = array(
				'response'=>true,
				'message'=>'Department has been saved.'
			);
		}
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
    }

  public function delete($id = null) {
    $folio = $this->Folio->findById($id);

    if ($this->superUser()){
      $delete = $this->Folio->delete($id);
    }
    else {
      $delete = $this->Folio->hide($id);
    }

    $resetReservationRoom = $this->Folio->ReservationRoom->save(array(
      'id'            => $folio['Folio']['reservationRoomId'],
      'checkedIn'     => false,
      'checkedInDate' => null,
    ));

    if ($delete and $resetReservationRoom) {
      $response = array(
        'ok'  => true,
        'msg' => 'Folio has been deleted.'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Folio cannot deleted this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
	
	public function api_available(){
		$availableRooms = $this->Room->query("
			SELECT
				Room.id,
				Room.code,
				RoomType.name,
				RoomType.rate,
				Accomodation.name
			FROM
				(rooms as Room LEFT JOIN
				room_types as RoomType ON Room.room_type_id = RoomType.id) LEFT JOIN
				accomodations as Accomodation ON Accomodation.id = RoomType.accomodation_id
		");
		
		foreach($availableRooms as $key=>$room){
			$availableRooms[$key] = array(
				'id'=>$room['Room']['id'],
				'code'=>$room['Room']['code'],
				'type'=>$room['RoomType']['name'],
				'rate'=>$room['RoomType']['rate'],
				'accomodation'=>$room['Accomodation']['name']
			);
		}
		
		$response = array(
				'response'=>true,
				'message'=>'Getting data successful.',
				'result'=>$availableRooms
			);
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
	}

  
  public function api_arrival(){
    $arrivalDate = isset($this->request->query['arrival'])? date('Y-m-d', strtotime($this->request->query['arrival'])):date('Y-m-d');
    $conditions = array();

    $conditions['ReservationRoom.checkedIn'] = false;
    $conditions['ReservationRoom.arrival'] = $arrivalDate;
    
    $list = $this->Reservation->find('all', array(
      'contain'=>array(
        'ReservationRoom'=>array(
          'Folio',
          'Room'=>array('RoomType'),
          'conditions'=>$conditions
         )
      ),
      'conditions'=>array(
        'Reservation.closed'=>false,
        'Reservation.visible'=>true
      )
    ));
    $arrivals = array();
    foreach($list as $reservation){
      foreach($reservation['ReservationRoom'] as $room){
        $arrivals[] = array(
          'id'=>$room['id'],
          'code'=>$room['Room']['code'],
          'type'=>$room['Room']['RoomType']['name'],
          'guest'=>empty($room['Folio'])? $reservation['Reservation']['lastName']. ', ' .$reservation['Reservation']['firstName']:$room['Folio']['lastName']. ', ' .$room['Folio']['firstName'],
          'arrival'=>date('m/d/Y', strtotime($room['arrival'])),
          'departure'=>date('m/d/Y', strtotime('+1day', strtotime($room['departure']))),
          'reservation_id' => $reservation['Reservation']['id'],
        );
      }
    }
    $response = array(
      'response'=>true,
      'message'=>'arrival list',
      'result'=>$arrivals
    );
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function api_departure(){
    $departureDate = isset($this->request->query['departure'])? date('Y-m-d', strtotime($this->request->query['departure'])):date('Y-m-d');
    $departureDate = date('Y-m-d', strtotime('-1day',strtotime($departureDate)));
    $conditions = array();
    // $conditions['ReservationRoom.checkOutDate'] = null;
    // $conditions['ReservationRoom.checkedIn'] = true;
    // $conditions['ReservationRoom.departure <='] = $departureDate;
    // $conditions['ReservationRoom.visible'] = true;

    $conditions['ReservationRoom.checkedIn'] = true;
    $conditions['ReservationRoom.checkOutDate'] = null;
    $conditions['ReservationRoom.departure <='] = $departureDate;
    $conditions['ReservationRoom.visible'] = true;

    $list = $this->ReservationRoom->find('all', array(
      'contain'=>array(
        'Room'=>array(
          'RoomType',
          'conditions'  =>  array('Room.visible' => true)
        ),
        'Reservation' => array(
          'conditions'=>array(
            'Reservation.visible'=>true
          )
        ),
        'Folio' => array(
          'Guest' => array(
            'conditions'=>array('Guest.visible'=>true)
           ),
          'conditions'=>array('Folio.visible'=>true)
        )
      ),
      'conditions'=>$conditions
    ));
    $departures = array();
    foreach($list as $data){

      if ($data['ReservationRoom']['departure']!=date('Y-m-d')) {
        $departure = strtotime('+1day',strtotime($data['ReservationRoom']['departure']));
        $days = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);

      } else { $days = 0; }

      $departures[] = array(
        'id'=>$data['ReservationRoom']['id'],
        'code'=>$data['Room']['code'],
        'type'=>$data['Room']['RoomType']['name'],
        'guest'=>empty($data['Folio'])? @$data['Reservation']['lastName']. ', ' . @$data['Reservation']['firstName']:@$data['Folio']['Guest']['lastName']. ', ' .@$data['Folio']['Guest']['firstName'],
        'arrival'=>date('m/d/Y', strtotime($data['Folio']['arrival'])),
        // 'departure'=>date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
        'departure' => date('m/d/Y'),
        'extended' => $days==0?'':$days . ' day(s)'
      );
    }
    $response = array(
      'response'=>true,
      'message'=>'departure list',
      'result'=>$departures
    );
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }
  
  
  public function api_occupied(){
	  $occupieds = $this->Folio->find('all', array(
			'contain'=>array(
				'Customer',
				'Room'=>array('RoomType')
			),
			'conditions'=>array(
				'Folio.closed'=>false
			)
	  ));
		foreach($occupieds as $i=>$occupied){
			$occupieds[$i]=array(
				'id'=>$occupied['Folio']['id'],
				'room'=>$occupied['Room']['code'],
				'type'=>$occupied['Room']['RoomType']['name'],
				'guests'=>$occupied['Customer']['last_name']. ', ' . $occupied['Customer']['first_name'],
				'arrival'=>date('m/d/Y', strtotime($occupied['Folio']['arrival'])),
				'departure'=>date('m/d/Y', strtotime('+1day', strtotime($occupied['Folio']['departure']))),
			);

		}
    $response = array(
      'response'=>true,
      'message'=>'occupied rooms',
      'result'=>$occupieds
    );
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
	}
  
 public function allguestlist(){
   $allguestlist = $this->Customer->find('all',array(
   'contain' => array( 
    'Folio'=>array(
      'arrival',
      'departure'
        )
      )
   ));
   
  
   $response= array(
    'response'=>true,
     'message'=>'sucess',
     'result'=>$allguestlist
   );
   $this->set(array(
    'response'=>$response,
    '_serialize'=>'response'
   ));
   
   
   
 }

}
