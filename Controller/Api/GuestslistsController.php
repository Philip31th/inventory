<?php 
class GuestslistsController extends AppController {
	
	public $components = array('Paginator', 'RequestHandler', 'Thumbnail');
  public $uses = array('Folio', 'Reservation', 'ReservationRoom', 'Transaction','Customer');
	
  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }

	public function index() {
	  $allguestlist = $this->Customer->find('all',array(
   'contain' => array( 
    'Folio'=>array(
      'arrival',
      'departure'
        )
      )
   ));
   
  
   $response= array(
    'response'=>true,
     'message'=>'sucess',
     'result'=>$allguestlist
   );
   $this->set(array(
    'response'=>$response,
    '_serialize'=>'response'
   ));
   
	}

	public function view($id = null){
		$data = $this->Customer->find('first', array(
		'contain'=>array(
     'Folio'=>array(
        'ReservationRoom'=>array(
           'Reservation'
        )
      )  
    ),
			'conditions'=>array(
				'Customer.id'=>$id,
				'Customer.visible'=> true
				
			)
		));
    
    $customerId  = $data['Customer']['id'];
    $customerImg = $data['Customer']['attachment'];
    $data['Customer']['attachment'] = $this->Session->read('server') . $this->Thumbnail->render("/uploads/customers/$customerId/$customerImg", array('path' => '', 'width'=>100, 'height'=>100, 'resize'=>'crop', 'quality'=>'100'), array());
		$response = array(
			'response'=>true,
			'result'=>$data
		);
		
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
	}
	
  public function add(){
    
  }

  public function edit($id = null){
		$this->request->data = $this->request->input('json_decode', true);
    if($this->Customer->save($this->request->data)){
        //$this->Reservation->save($this->request->data->Folio[0]->ReservationRoom);
      
      
      $response = array(
        'response'=>true,
        'message'=>'Customer Information has been saved.'
      );
    }
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
    }

  public function delete($id = null){
		$this->Department->visible($id, false);
		$response = array(
			'response'=>true,
			'message'=>'Department has been deleted.'
		);
		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
		}

}
