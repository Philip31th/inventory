<?php
class PosController extends AppController {
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  public $uses = array('Table','Department','Transaction','Folio','FolioTransaction','TableTransaction');
  
  public function index() {
    
    // default conditions
    $conditions = array();
    $conditions['Table.visible'] = true;
    
    // filter per department
    if (isset($this->request->query['code'])) {
      $code =  $this->request->query['code'];
      $conditions['Table.departmentId'] = $this->Department->get($code);
    } else {
      $code = null;
    }
    
    // get table list
    $tables = $this->Table->find('all',array(
      'contain' =>  array(
        'TableTransaction' => array(
          'Transaction' => array(
            'TransactionSub'  =>  array(
              'conditions'  =>  array(
                'TransactionSub.visible'  =>  true
              )
            ),
            'FolioTransaction' => array(
              'conditions'  =>  array(
                'FolioTransaction.folioId !=' => null 
              )
            )
          ),
          'conditions'  =>  array(
            'TableTransaction.visible'  =>  true
          )
        )
      ),
      'conditions'  =>  $conditions
    ));
    
    // transform data
    $status = '';
    $available = 0;
    $occupied = 0; 
    $tablee = array();
    $paid = false;
    $payments = array();
    $subs = array();
    $TotalPays = 0;
    $TotalBills = 0;
    $folio = array();
    // $transactionId = 0;
    foreach ($tables as $data) {
      
      // get if occupied or not the table
      if($data['TableTransaction']!=null) {
        $status = true;
        $occupied += count($status);
      } else {
        $status = false;
        $paid = false;
        $tableTransactionId = '';
        $available += count($status);
      }

      if($status) {
        foreach ($data['TableTransaction'] as $tableTrans) {
            // $paid = $tableTrans['paid'];
            $tableTransactionId = $tableTrans['id'];
        }
      }

        $totalAmnt = $this->view($data['Table']['id'],'totalAmount');
        $totalPay  = $this->view($data['Table']['id'],'totalPayment');

        if($totalAmnt!=0&&($totalAmnt-$totalPay)<=0) {
          $paid = true;
        } else {
          $paid = false;
        }

        if(!empty($tableTrans['Transaction']['FolioTransaction'])&&!empty($data['TableTransaction'])) {
          $fol = true;
          $folio = array(
            'id'                  =>  $tableTrans['Transaction']['FolioTransaction']['folioId'],
            'folioTransactionId'  =>  $tableTrans['Transaction']['FolioTransaction']['id']
          );
        } else {
          $fol = false;
        }

        $tablee[] = array(
          'tableTransactionId' => $tableTransactionId,
          'tableId'       => $data['Table']['id'],
          'tableName'     => $data['Table']['name'],
          'occupied'      => $status,
          'paid'          => $paid,
          'folio'         => $fol,  
          'Folio'        =>  @$folio
        );
    
      }
       
    $table['Tables']    = $tablee;   
    $table['Available'] = $available;  
    $table['Occupied']  = $occupied;
    
    $response = array(
			'ok'   => true,
			'table' => $table,
			'temp' => $tables,
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }
  
  public function view($id=null,$specs=null) {
    // default conditions
    $conditions = array();
    $conditions['Table.visible'] = true;
    $conditions['Table.id'] = $id;
     
     $tables = $this->Table->find('first',array(
      'contain' =>  array(
        'TableTransaction'  => array(
          'Transaction'  =>  array(
            'FolioTransaction'  =>  array(
              'Folio' => array(
                'fields' => array('code','id'),
                'Guest' =>  array(
                  'fields'  =>  array('lastName','firstName')  
                ),
                'Room' => array(
                  'conditions' => array(
                    'Room.visible' => true
                  )
                )  
              ),
              'conditions' => array(
                // 'FolioTransaction.visible' => false,
                'FolioTransaction.cleanTable' => false
              )
            )  
          ),
          'conditions'  =>  array(
            'TableTransaction.visible' => true
          )
        )  
      ),
      'conditions'  =>  $conditions
    ));

    // transform data
    
    $tablees = array();
    $table = array();
    $subs = array();
    $payments = array();
    $discounts = array();
    $TotalBills = 0;
    $TotalPays = 0;
    $TotalDisc = 0;
    $TotalBills = 0;
    $folio = '';
    $total = 0;
    $totalPayments  = 0;
    $totalChange    = 0;
    $folios = array();
    foreach ($tables['TableTransaction'] as $data) {
 
    }
      $folios = array(
        'id'      => @$data['Transaction']['FolioTransaction']['folioId'],
        'value'   => @$data['Transaction']['FolioTransaction']['Folio']['code'] . ' - Room #' . @$data['Transaction']['FolioTransaction']['Folio']['Room']['name']. ' - ' . @$data['Transaction']['FolioTransaction']['Folio']['Guest']['firstName'] . ' ' . @$data['Transaction']['FolioTransaction']['Folio']['Guest']['lastName'],
        'code'    => @$data['Transaction']['FolioTransaction']['Folio']['code'],
        'folioTransactionId' => @$data['Transaction']['FolioTransaction']['id']
      );
      // transform table
      $tablees = array(
        'tableId'            => $tables['Table']['id'],
        'tableTransactionId' => @$data['id'],        
        'name'               => $tables['Table']['name'],
        // 'paid'               => $data['paid'],
        'transactionId'      => @$data['Transaction']['id'],
        'TransactionSub'     => @$this->getTransactionSub($data['Transaction']['id']),
        'TransactionPayment' => @$this->getTransactionPayment($data['Transaction']['id']),
        'folio'              => @$data['Transaction']['FolioTransaction'],
        'Folio'              => @$folios
      );
      
    foreach ($tablees['TransactionSub'] as $tabs => $data) {
      $total += $data['amount']*$data['totalQuantity'];
      $tablees['TransactionSub'][$tabs]['totalQuantity'] = (int)$data['totalQuantity']; 
    }
    foreach ($tablees['TransactionPayment'] as $pays=> $data) {
      $totalPayments += $data['amount']; 
      $totalChange   += $data['change'];       
      $tablees['TransactionPayment'][$pays]['change'] = $data['change'];       
    }

    
    if(!empty($data['Transaction']['FolioTransaction'])) {
      $fol = true;
    } else {
      $fol = false;
    }

    $response = array(
      'ok'   => true,
      'table' => $tablees,
      'total' => $total,
      'totalPayments' =>  $totalPayments,  
      'totalChange'   =>  $totalChange,
      'folio'         =>  $fol
      // 'tb'      => $tables
    );

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));

    if($specs!=null) {
      if($specs=='totalAmount') {
        return $total;
      }
      if($specs=='totalPayment') {
        return $totalPayments;
      }
    }
  }
  
  public function getTransactionSub($transactionId=null) {
    $subs = $this->Transaction->TransactionSub->find('all',array(
      'contain' =>  array(
        'TransactionPayment' => array(
          'conditions'  =>  array(
             'TransactionPayment.visible'  =>  true
          ),
        )
      ),
      'conditions'  =>  array(
        'TransactionSub.transactionId'  =>  $transactionId,
         'TransactionSub.visible'  =>  true
      ),
      'fields'  =>  array(
        'SUM(TransactionSub.amount) as totalAmount','SUM(TransactionSub.quantity) as totalQuantity',
        'id','particulars','amount','quantity'
      ),
      'group' =>  array(
        'TransactionSub.particulars'
      )
    ));

    $datas = array();
    $total = 0;
    
    foreach ($subs as $sub) {
        
        $payments = array();
        $totalPayments = 0;
        foreach ($sub['TransactionPayment'] as $payment) {
            $payments[] = array(
              'id'  =>  $payment['id'],
              'amount'  =>  $payment['amount']-$payment['change'],
              'orNumber'  =>  $payment['orNumber'],
              'change'  =>  $payment['change'],
              'subId'  =>  $payment['subId'],
            );
            $totalPayments += $payment['amount']-$payment['change'];
        }

        $datas[] = array(
          'id'            =>  $sub['TransactionSub']['id'],
          'particulars'   =>  $sub['TransactionSub']['particulars'],
          'amount'        =>  @$sub['TransactionSub']['amount'],
          'quantity'      =>  $sub['TransactionSub']['quantity'],
          'totalQuantity' =>  $sub[0]['totalQuantity'],
          'totalAmount'   =>  $sub['TransactionSub']['amount']*$sub[0]['totalQuantity'],
          'totalPayments' =>  $totalPayments,
          'payments'      =>  $payments
        );
    }

    return $datas;
  }

  public function getTransactionPayment($transactionId=null) {
    $payments = $this->Transaction->TransactionPayment->find('all',array(
      'contain' =>  array(
        'TransactionSub'  => array(
          'conditions'  =>  array(
            'TransactionSub.visible' => true
          )
        )
      ),
      'conditions'  =>  array(
        'TransactionPayment.transactionId'  =>  $transactionId,
        'TransactionPayment.visible'  =>  true,
      )
    ));

    $datas = array();
    foreach ($payments as $payment) {

        $datas[] = array(
          'id'            =>  $payment['TransactionPayment']['id'],
          'amount'        =>  $payment['TransactionPayment']['amount']-$payment['TransactionPayment']['change'],
          'orNumber'      =>  $payment['TransactionPayment']['orNumber'],
          'paymentType'   =>  $payment['TransactionPayment']['paymentType'],
          'date'          =>  $payment['TransactionPayment']['date'],
          'change'        =>  $payment['TransactionPayment']['change'],
          'subId'         =>  $payment['TransactionPayment']['subId'],
        );
    }

    return $datas;
  }

  public function add() {
    $this->request->data['Transaction']['code'] = $this->Transaction->generateCode();
    
    // if merong folio
    if(isset($this->request->data['Folio']['code'])){
      $this->request->data['FolioTransaction']['folioId'] = $this->Folio->generateId($this->request->data['Folio']['code']);
    }
    
    // if merong date
    if(!isset($this->request->data['Transaction']['date'])){
      $this->request->data['Transaction']['date'] = date('Y-m-d');
    }else{
      $this->request->data['Transaction']['date'] = date('Y-m-d', strtotime($this->request->data['Transaction']['date']));
    }

    
    if($this->Transaction->save($this->request->data['Transaction'])){
      $transactionId = $this->Transaction->getLastInsertId();

      if(!empty($this->request->data['TransactionSub'])){

        foreach($this->request->data['TransactionSub'] as $i=>$data){
          $this->request->data['TransactionSub'][$i]['transactionId'] = $transactionId;
          $this->request->data['TransactionSub'][$i]['amount'] = $this->request->data['TransactionSub'][$i]['amount'];
        }

        $this->request->data['TableTransaction']['transactionId'] = $transactionId;
        $this->request->data['TableTransaction']['tableId'] = $this->request->data['tableId'];

        // if($this->request->data)
        
        $this->Transaction->TableTransaction->save($this->request->data['TableTransaction']);
        
        $this->Transaction->TransactionSub->saveMany($this->request->data['TransactionSub']);
      }
      if(!empty($this->request->data['FolioTransaction'])){
        $this->request->data['FolioTransaction']['transactionId'] = $transactionId;
        $this->request->data['FolioTransaction']['visible'] = false;
        $this->request->data['FolioTransaction']['cleanTable'] = false;
        $this->FolioTransaction->save($this->request->data['FolioTransaction']);
      }
  
      $response = array(
        'ok'=>true,
        'message'=>'Transaction has been saved.',
        'data'   => $this->request->data
      );
    }

    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function edit($id=null) {
    $this->request->data['Transaction']['id'] = $id;


   //  if(empty($this->request->data['FolioCode']) && isset($this->request->data['Folio'])){
   //    $this->request->data['FolioTransaction']['folioId'] = $this->request->data['Folio']['id'];      
   //    $this->request->data['FolioTransaction']['ordered'] = false; 
   //    $this->Transaction->FolioTransaction->delete($this->request->data['Folio']['folioTransactionId']);
   //  } else if (isset($this->request->data['Folio'])) {
   //    $this->request->data['FolioTransaction']['folioId'] = $this->Folio->generateId($this->request->data['Folio']['code']);
   //    $this->request->data['FolioTransaction']['visible'] = false; 
   //  }

   // if(!empty($this->request->data['FolioTransaction'])||!empty($this->request->data['Folio'])){

   //    $this->request->data['FolioTransaction']['id'] = $this->request->data['Folio']['folioTransactionId'];

   //    $this->request->data['FolioTransaction']['transactionId'] = $id;
   //    $this->FolioTransaction->save($this->request->data['FolioTransaction']);
   //  }

    // TRANSACTION SUB 
    if(isset($this->request->data['TransactionSub'])) {

      // foreach ($this->request->data['TransactionSub'] as $i=>$data) {
        $this->request->data['TransactionSub']['transactionId'] = $id;                    
        $this->request->data['TransactionSub']['quantity'] = $this->request->data['TransactionSub']['totalQuantity'];   
      // }

      $this->Transaction->TransactionSub->save($this->request->data['TransactionSub']); 
    }  

    // TRANSACTION PAYMENT
    if(!empty($this->request->data['TransactionPayment'])) {

      foreach ($this->request->data['TransactionPayment'] as $i=>$data) {
        // add or update
        if(!isset($this->request->data['TransactionPayment'][$i]['id'])) {
          $this->request->data['TransactionPayment'][$i]['transactionId'] = $id;
          $this->request->data['TransactionPayment'][$i]['date'] = date('Y-m-d'); 
        } 
      }

      $this->Transaction->TransactionPayment->saveMany($this->request->data['TransactionPayment']);
    }  
    
    $response = array(
      'ok'=>true,
      // 'message'=>'TransactionSub has been saved.',
      'data'   => $this->request->data
    ); 

    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }  

  public function delete($id = null) {

    if ($this->TableTransaction->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
        'msg'  => 'Table has been cleaned.',
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
        'msg'  => 'Table cannot be cleaned this time.',
      );
    }

   $response = array(
        'ok'   => true,
        'id' => $id
        // 'msg'  => 'Table has been cleaned.',
      );
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }  
}
