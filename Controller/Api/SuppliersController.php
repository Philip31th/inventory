<?php 
class SuppliersController extends AppController {
  
  public $layout = null;
  public $uses = array('Department','Supplier','TransactionPayment');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

   // default conditions
    $conditions = array();
    $conditions['Supplier.visible'] = true;

    // check suppliers
    if (isset($this->request->query['code'])) {
      $code = $this->request->query['code'];
      $conditions['Supplier.departmentId'] = $this->Department->get($this->request->query['code']);
    } else {
      $code = null;
    }

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Supplier.name LIKE'      => "%$search%",
        'Supplier.address LIKE'   => "%$search%"
      );
    }

    // paginate data
    $paginatorSettings = array(
      'contain' => array(
        'Department',
        'SupplierItem'  =>  array(
          'conditions'  =>  array('SupplierItem.visible'=>true)
        ),
        'SupplierPayment' => array(
          'TransactionPayment' => array(
            'conditions'  =>  array('TransactionPayment.visible'=>true),
          ),
          'conditions'  =>  array('SupplierPayment.visible'=>true),
        )
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Supplier.name' => 'ASC'
      )
    );
    $modelName = 'Supplier';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $suppliers = array();
    $payables = array();
    $grandTotalpayables = 0;
    $supplier_payments = array();
    foreach ($tmpData as $data) {
      $supplier = $data['Supplier'];
      $supplier_payment = $data['SupplierPayment'];

      $items = array();
      $totalAmount  = 0;
      $totalPayments = 0;
      $totalPayables = 0;

      foreach ($data['SupplierItem'] as $item) {
        $totalAmount += $item['price']*$item['quantity'];

        $items[] = array(
          'id'    =>    $item['id'],
          'amount'=> $item['price']
        );

      }
    
    if(!empty($supplier_payment)) {  
      foreach ($supplier_payment as $sp) {
          @$totalPayments += @$sp['TransactionPayment']['amount'] - @$sp['TransactionPayment']['change'];
         
      } 
    }  

      // for suplliers mngmt
      $suppliers[] = array(
          'id'         => $supplier['id'],
          'name'       => $supplier['name'],
          'contact'    => $supplier['contactNumber'],
          'address'    => $supplier['address'],
          'department' => $supplier['name'],
          'items'     =>  $items,
          'totalAmount'=> $totalAmount,
          'totalPayments' => $totalPayments,
          'totalPayables'=> $totalAmount - $totalPayments
        );

      // for suppliers payables
      if($totalAmount - $totalPayments > 0) { 

        $payables[] = array(
          'id'         => $supplier['id'],
          'name'       => $supplier['name'],
          'contact'    => $supplier['contactNumber'],
          'address'    => $supplier['address'],
          'department' => $supplier['name'],
          'items'     =>  $items,
          'totalAmount'=> $totalAmount,
          'totalPayments' => $totalPayments,
          'totalPayables'=> $totalAmount - $totalPayments
        );

        $grandTotalpayables += ($totalAmount - $totalPayments);
      }
    }

    // SUPPLIES PAYMENTS HISTORY
    $conditions_suppliers = array();
    $conditions_suppliers['Supplier.visible'] = true;
    $condition_transactionPayments = array();
    $condition_transactionPayments['TransactionPayment.visible'] = true;

     // check suppliers
    if (isset($this->request->query['code'])) {
      $code = $this->request->query['code'];
      $conditions_suppliers['Supplier.departmentId'] = $this->Department->get($this->request->query['code']);
    } else {
      $code = null;
    }

    // search conditions
    if(isset($this->request->query['searchDate'])) {
      $date = date('Y-m-d',strtotime($this->request->query['searchDate']));
      $condition_transactionPayments['TransactionPayment.date']  = $date; 
    }

    if(isset($this->request->query['searchName'])) {
      $searchName = $this->request->query['searchName'];
      $conditions_suppliers['Supplier.name LIKE']  = "%$searchName%"; 
    }

    $datas = $this->Supplier->SupplierPayment->find('all',array(
      'contain' =>  array(
        'Supplier' => array(
          'conditions'  =>  $conditions_suppliers
        ),
        'TransactionPayment'  => array(
          'conditions' => $condition_transactionPayments,            
          'order'      =>  array('TransactionPayment.created'  =>  'DESC')
        )  
      ),
      'conditions'  =>  array(
        'SupplierPayment.visible' => true
      )
    ));

    $supplier_payments = array();
    foreach ($datas as $data) {

      if($data['TransactionPayment']['id'] != null && $data['Supplier']['id'] !=null ) {
       @$data['TransactionPayment']['datetime'] = date('M d, Y h:i A', strtotime(@$data['TransactionPayment']['created']));

        $supplier_payments[] = array(
          'Supplier'  =>  $data['Supplier'],
          'TransactionPayment'  =>  $data['TransactionPayment']
        );
      }
    } 

    $response = array(
        'ok'        => true,
        'code'      => $code,
        'data'      => $suppliers,
        'payables'  => $payables,
        'totalPayables' => $grandTotalpayables,
        'supplies_payments'   => $supplier_payments,
        'paginator' => $paginator
    );
        
    $this->set(array(
        'response'   => $response,
        '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $supplier = $this->Supplier->find('first', array(
      'contain' => array(
        'Department',
        'SupplierItem'  =>  array('conditions'=>array('SupplierItem.visible'=> true)),
        'SupplierPayment' => array(
          'TransactionPayment' => array(
            'conditions'  =>  array('TransactionPayment.visible'=>true)
          ),
          'conditions'  =>  array('SupplierPayment.visible'=>true)
        )
      ),
      'conditions' => array(
        'Supplier.id' => $id
      )
    ));

    $payments = array();
    $totalPayments = 0;
    $totalChange = 0;
    foreach ($supplier['SupplierPayment'] as $data) {
      $payment = $data['TransactionPayment'];

      if(!empty($payment)){
        @$payment['datetime'] = date('M d, Y h:i A', strtotime(@$payment['created']));
        @$totalPayments += ($payment['amount']-@$payment['change']);
        @$totalChange += @$payment['change'];
        $payments[] = $payment;
      }   
    }

    $totalAmount = 0;
    foreach ($supplier['SupplierItem'] as $items) {
      $totalAmount += ($items['price']*$items['quantity']);
    }

    foreach ($supplier['SupplierItem'] as $key => $value) {

      $supplier['SupplierItem'][$key]['started'] = date('M d, Y h:i A', strtotime($supplier['SupplierItem'][$key]['created']));
    }

    $totals = array(
      'totalPayments' =>  $totalPayments,
      'totalChange'   =>  $totalChange,
      'totalAmount'   =>  $totalAmount
    );

    $supplier['Supplier']['started'] = date('M d, Y', strtotime($supplier['Supplier']['created']));
    

    $response = array(
      'ok'    => true,
      'data'  => $supplier,
      'payments' => @$payments,
      'totals' => @$totals,

    );
     
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function add() {
    $this->request->data['Supplier']['departmentId'] = $this->Supplier->Department->get($this->request->data['Supplier']['departmentId']);

    $save = $this->Supplier->validSave($this->request->data['Supplier']);

    if ($save) {
      if(isset($this->request->query['supplierId'])) {
        $supplierId = $this->request->query['supplierId'];
      } else {
        $supplierId =  $this->Supplier->getLastInsertId();
      }

      if(!empty($this->request->data['SupplierItem'])){
        foreach($this->request->data['SupplierItem'] as $i=>$data){
          $this->request->data['SupplierItem'][$i]['supplierId'] = $supplierId;
        }
        $this->Supplier->SupplierItem->saveMany($this->request->data['SupplierItem']);
      }
    }
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    if(!isset($this->request->query['supplierId'])) {
      $this->request->data['Supplier']['id'] = $id;
    } else {
      $this->request->data['Supplier']['id'] = $this->request->query['supplierId'];
    }

    $save = $this->Supplier->validSave($this->request->data['Supplier']);
    if ($save) {
      if(isset($this->request->query['supplierId'])) {
        $supplierId = $this->request->query['supplierId'];
      } else {
        $supplierId =  $id;
      }
      
      if(!empty($this->request->data['SupplierItem'])){
        foreach($this->request->data['SupplierItem'] as $i=>$data){
          $this->request->data['SupplierItem'][$i]['supplierId'] = $supplierId;
        }
        $this->Supplier->SupplierItem->saveMany($this->request->data['SupplierItem']);
      }
    }

    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->Supplier->hide($id)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Supplier has been deleted.'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Supplier cannot deleted this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function report() {
    $response = array(
        'ok'  => true,
        // 'data'
      );

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
}
