<?php
class InventorySubsController extends AppController {
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function add() {
    
    if ($this->InventorySub->saveMany($this->request->data['InventorySub'])) {
        $response = array (
          'ok'  => true,
          'msg' => 'Inventory Sub has been saved',
          'data' => $this->request->data['InventorySub']
        );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Inventory Sub cannot be save this time',
        'data' => $this->request->data['InventorySub']
      );
    }
    
    // $response = array (
    //     'ok'  => true,
    //     // 'msg' => 'Inventory Sub has been saved',
    //     'data' => $this->request->data['InventorySub']
    //   );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->InventorySub->hide($id)) {
      $response = array(
        'ok'  => true,
        'msg' => 'InventorySub has been deleted.'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'InventorySub cannot deleted this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

}