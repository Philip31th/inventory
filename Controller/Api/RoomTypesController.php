<?php 
class RoomTypesController extends AppController {
	
	public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
    // default page
	  $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    if (!$this->superUser())
      $conditions['RoomType.visible'] = true;

    // search conditions
  	if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];

  		$conditions['OR'] = array(
				'RoomType.name LIKE' => "%$search%"
  		);
  	}
		
    // paginate data
  	$paginatorSettings = array(
  		'contain'=>array(
    		'Accomodation',
				'Room'
  		),
  		'conditions'     => $conditions,
      'limit'          => 25,
      'page'           => $page,
      'order'          => array(
        'RoomType.name' => 'ASC'
      )
    );
    $modelName = 'RoomType';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData    = $this->Paginator->paginate($modelName);
    $paginator  = $this->request->params['paging'][$modelName];
	
    // transform data
    $roomTypes = array();
    foreach ($tmpData as $data) {
      $type         = $data['RoomType'];
      $accomodation = $data['Accomodation'];
      $room         = $data['Room'];

      $roomTypes[] = array(
        'id'            => $type['id'],
        'name'          => $type['name'],
        'accomodation'  => $accomodation['name'],
        'rate'          => $type['rate'],
        'count'         => count($room),
        'visible'       => $type['visible'],
      );
    }
		
		$response = array(
			'ok'        => true,
			'paginator' => $paginator,
			'data'      => $roomTypes,
      'settings'  => $paginatorSettings
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
	}

  public function view($id = null) {
    // conditions
    $conditions = array();
    $conditions ['RoomType.id'] = $id;

    $roomConditions = array(); 
    if (!$this->superUser())
      $roomConditions['Room.visible'] = true;


    $data = $this->RoomType->find('first', array(
      'contain' => array(
        'Accomodation',
        'Room' => array(
          'conditions' => $roomConditions
        ),
      ),
      'conditions' => $conditions
    ));
  	$response = array(
  		'ok'   => true,
  		'data' => $data
  	);
  	
  	$this->set(array(
  		'response'   => $response,
  		'_serialize' => 'response'
  	));
  }

  public function add() {
    $save = $this->RoomType->validSave($this->request->data['RoomType']);
    $response = $save;

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }

  public function edit($id = null) {
    $save = $this->RoomType->validSave($this->request->data['RoomType']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {
    if ($this->superUser())
      $delete = $this->RoomType->delete($id);
    else
      $delete = $this->RoomType->hide($id);

		if ($delete) {
      $response = array(
        'ok'  => true,
        'msg' => 'Room Type has been deleted.'
      );
    } else {
      $response = array(
        'ok'  => false,
        'msg' => 'Room Type cannot deleted this time.'
      );
    }

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }
  
  public function api_available() {
    $arrival = isset($this->request->query['arrival'])? date('Y-m-d 00:00:00', strtotime($this->request->query['arrival'])):date('Y-m-d 00:00:00');
    $departure = isset($this->request->query['departure'])? date('Y-m-d 00:00:00', strtotime($this->request->query['departure'])):date('Y-m-d 00:00:00');
    $departure = date('Y-m-d 00:00:00', strtotime('-1day', strtotime($departure)));
    
    $conditions['RoomType.visible'] = true;
    
    $roomTypes = $this->RoomType->find('all', array(
      'contain'=>array(
        'Accomodation',
        'Room'=>array(
          'conditions'  => array('Room.visible'=>true),
          'Folio'=>array(
            'id',
            'conditions'=>array(
              'OR'=>array(
              // "arrival >= '$arrival' AND arrival <= '$departure'",
              //   "departure >= '$arrival' AND departure <= '$departure'",
              //   "arrival <= '$arrival' AND departure >= '$arrival'",
              //   "arrival <= '$departure' AND departure >= '$departure'"
                
                "arrival >= '$arrival' AND arrival <= '$departure'",
      					"departure <= '$arrival' AND departure <= '$departure'",
      					"arrival <= '$arrival' AND departure >= '$arrival'",
      					"arrival <= '$departure' AND departure >= '$departure'"
              ),
              'visible'=>true,
              'closed' => false
            )
          ),
          'ReservationRoom'=>array(
            'id','departure','arrival',
            'conditions'=>array(
              'OR'=>array( 
                "arrival >= '$arrival' AND arrival <= '$departure'",
                "departure <= '$arrival' AND departure <= '$departure'",
                "arrival <= '$arrival' AND departure >= '$arrival'",
                "arrival <= '$departure' AND departure >= '$departure'",
              ),
              'visible'   => true,
              'closed'     => false
              )
          )
        )
      ),
      'conditions'  =>  $conditions
    ));
    
    $RoomType = array();
    $total = 0;
    foreach($roomTypes as $a=>$roomType){
      $Room = array();
      foreach($roomType['Room'] as $b=>$room){
        if(count($room['Folio'])<=0 AND count($room['ReservationRoom'])<=0 ) 
          $Room[] = $room;
      }
      $roomType['Room'] = count($Room);
      if(count($Room)>0){
        $roomType['room_id'] =  $Room[0]['id'];
        $RoomType[] = $roomType;
      }
      $total +=  $roomType['Room'];
    }
    // $roomTypes = $RoomType;
    
 
    $response = array(
      'response'=>true,
      'message'=>'Getting data successful.',
      'arrival'=>$arrival,
      'departure'=>$departure,
      'result'=>$RoomType,
      'roomttype' => $roomTypes,
      'total' => $total
    );
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }
}
