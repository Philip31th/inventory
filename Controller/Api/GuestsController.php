<?php 
class GuestsController extends AppController {
	
	public $layout = null;
	
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
		// default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    $conditions['Folio.visible'] = true;
    $conditions['Folio.closed']  = false;


    // search conditions
    if (isset($this->request->query['search'])) {
      if (!empty($this->request->query['search'])) {
        $search = date('Y-m-d', strtotime($this->request->query['search']));
        $conditions['Folio.arrival   <='] = $search;
    	  $conditions['Folio.departure >='] = $search;
      }
    } else {
    	$search = date('Y-m-d');
    	$conditions['OR'] = array(
    	  'Folio.arrival   <=' => $search,
    	  'Folio.departure >=' => $search
    	 );
    // 	$conditions['Folio.arrival   <='] = $search;
    // 	$conditions['Folio.departure >='] = $search;
    }

    // paginate data
    $paginatorSettings = array(
    	'contain' => array (
    		'Guest',
    		'Room' => array(
    			'RoomType'
    		)
    	),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Guest.name' => 'ASC'
      )
    );
    $modelName = 'Folio';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData   = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $guests = array();
    foreach ($tmpData as $data) {
      $guest = $data['Guest'];
      $folio = $data['Folio'];
      $room  = $data['Room'];

      $guests[] = array(
        'id'        => $guest['id'],
        'code'      => $guest['code'],
        'name'      => $guest['firstName'] . ' ' . $guest['lastName'],
       	'room'		  => $room['name'],
       	'type'		  => $room['RoomType']['name'],
       	'arrival'	  => fdate($folio['arrival']),
       	'departure' => date('m/d/Y', strtotime('+1day', strtotime($folio['departure'])))
      );
    }

		$response = array(
			'ok'   			=> true,
			'search' 		=> fdate($search),
			'data' 			=> $guests,
			'paginator' => $paginator
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
	}

	public function view($id = null) {
    // default conditions
    $conditions = array();
    $conditions['Folio.visible'] = true;
    $conditions['Folio.closed']  = false;
    $conditions['Guest.id'] = $id;

    // paginate data
    $paginatorSettings = array(
      'contain' => array (
        'Guest',
        'Room' => array(
          'RoomType'
        )
      ),
      'conditions' => $conditions,
      'order'      => array(
        'Guest.name' => 'ASC'
      )
    );
    $modelName = 'Folio';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData   = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $guests = array();
    foreach ($tmpData as $data) {
      $guest = $data['Guest'];
      $folio = $data['Folio'];
      $room  = $data['Room'];

      $guests[] = array(
        'id'        => $guest['id'],
        'code'      => $guest['code'],
        'name'      => ucwords($guest['firstName'] . ' ' . $guest['lastName']),
        'room'      => $room['name'],
        'room_type' => $room['RoomType']['name'],
        'arrival'   => fdate($folio['arrival']),
        'departure' => date('m/d/Y', strtotime('+1day', strtotime($folio['departure'])))
      );
    }

    $response = array(
      'ok'        => true,
      'data'      => $guests,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
	}
	
  public function add() {

  }

  public function edit($id = null) {

  }
	
	public function delete($id = null) {
		$response = array(
			'ok'   => true,
			'data' => array(),
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' =>'response'
		));
	}

}
