<?php 
class FolioTransactionsController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function add() {
     

   if(isset($this->request->data['Folio']['id'])) {
    if($this->request->data['Folio']['id']!=null) {
      $this->request->data['FolioTransaction']['id'] = $this->request->data['Folio']['folioTransactionId'];
      $this->request->data['FolioTransaction']['cleanTable'] = true;
      $this->request->data['FolioTransaction']['visible'] = true;
    }  
   } else {
      if (isset($this->request->data['FolioTransactionId'])) {
        // if(isset($this->request->data['FolioCode'])) {
        //   $folioId = $this->FolioTransaction->Folio->generateId($this->request->data['FolioCode']);
        // } else {
        //   $folioId = null;
        // }

        if(($this->request->data['FolioTransactionId']!=null) || $this->request->data['FolioTransactionId']!='') {
          $this->request->data['FolioTransaction']['id'] = $this->request->data['FolioTransactionId']; 
          $this->request->data['FolioTransaction']['folioId'] = $this->FolioTransaction->Folio->generateId($this->request->data['FolioCode']);        
        } 
      } elseif(@$this->request->data['paymentStatus']==true) {
        $this->request->data['FolioTransaction']['id'] = $this->request->data['FolioTransactionId']; 
        $this->request->data['FolioTransaction']['folioId'] = null;
      } else {
        // if(isset($this->request->data['FolioCode']) || isset($this->request->data['transactionId'])) {
        //   $folioId = $this->FolioTransaction->Folio->generateId($this->request->data['FolioCode']);
        //   $transactionId = $this->request->data['transactionId'];
        // } else {
        //   $folioId = null;
        //   $transactionId = null;
        // }
        $this->request->data['FolioTransaction']['folioId'] = $this->FolioTransaction->Folio->generateId($this->request->data['FolioCode']) ;        
        @$this->request->data['FolioTransaction']['transactionId'] = @$this->request->data['transactionId']; 
        $this->request->data['FolioTransaction']['visible'] = false;
        $this->request->data['FolioTransaction']['cleanTable'] = false;
      }   
   }

    $this->FolioTransaction->save($this->request->data['FolioTransaction']);

     $response = array (
        'ok'  => true,
        'data'  =>  $this->request->data
      );

     $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }  

  public function edit($id=null) {

    if($this->FolioTransaction->save(array(
      'id'  =>  $id,
      'cleanTable' => true,
      'visible' => true
    ))) {
       $response = array (
        'ok'  => true,
        'data'  =>  $this->request->data,
        // 'msg' => "Success"
      );
    } else {
       $response = array (
        'ok'  => true,
        'data'  =>  $this->request->data,
        // 'msg' => "UnSuccess"
      );
    }

    

     $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
}  