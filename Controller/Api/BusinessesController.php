<?php
class BusinessesController extends AppController {

  public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    $conditions['Business.visible'] = true;

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Business.code LIKE'        => "%$search%",
        'Business.name LIKE'        => "%$search%",
        'Business.description LIKE' => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Business.name' => 'ASC'
      )
    );
    $modelName = 'Business';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $businesses = array();
    foreach ($tmpData as $data) {
      $business = $data['Business'];

      $businesses[] = array(
        'id'          => $business['id'],
        'code'        => $business['code'],
        'name'        => $business['name'],
        'description' => $business['description'],
      );
    }

    $response = array(
      'ok'         => true,
      'data'       => $businesses,
      'paginator'  => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $business = $this->Business->find('first', array(
      'contain' => array(
        'BusinessService'
      ),
      'conditions' => array(
        'Business.id' => $id
      )
    ));

    // transform data
    foreach ($business['BusinessService'] as $k => $service) {
      $business['BusinessService'][$k] = array(
        'id'     => $service['id'],
        'name'   => $service['name'],
        'amount' => $service['value'],
      );
    }

    $response = array(
      'ok'   => true,
      'data' => $business
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function add() {
    // $save = $this->Business->validSave($this->request->data['Business']);
    // $response = $save;

    // $this->set(array(
    //   'response'   => $response,
    //   '_serialize' => 'response'
    // ));
  }

  public function edit($id = null) {
    $save = $this->Business->validSave($this->request->data['Business']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function delete($id = null) {
    if ($this->MembershipType->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
}
