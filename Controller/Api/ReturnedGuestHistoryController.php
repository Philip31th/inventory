<?php 
class ReturnedGuestHistoryController extends AppController {
 
 public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  public $uses = array('Folio','FolioSub');

  public function view($id = null) {
    // checks if returning or not
    $returning = $this->Folio->find('all',array(
      'contain' => array(
        'FolioTransaction' => array(
          'Transaction' => array(
            'Business',
             'TransactionSub' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionPayment' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionDiscount' => array(
              'conditions' => array(
                'visible' => true
              )
            )
          )
        ),
        'Guest',
        'Room' => array(
          'RoomType' => array(
            'fields' => array(
              'name'
            )
          )
        )
      ),
      'conditions' => array(
        'Folio.guestId' => $id,
        'Folio.closed'  => true
      )
    ));

    $folios = array();
    $ftransactions = array();
    $fullTransactions = array();

    foreach ($returning as $folio) {

      foreach ($folio['FolioTransaction'] as $ftransaction) {    

        // subs
        $totalAmount = 0;
        foreach ($ftransaction['Transaction']['TransactionSub'] as $sub) {
          $totalAmount += $sub['amount'];
        }    

        // payments
        $totalPayment = 0;
        $totalChange = 0;    
        foreach ($ftransaction['Transaction']['TransactionPayment'] as $pay) {
          $totalChange += $pay['change'];
          $payment = $pay['amount'] - $totalChange;
          $totalPayment += $payment;
        }    

        // discounts
        $totalDiscount = 0;
        foreach ($ftransaction['Transaction']['TransactionDiscount'] as $disc) {
          $tdiscountAmount = $disc['type'] == 'fixed'? $disc['value'] : $totalAmount * ($disc['value']/100);
          $totalDiscount += $tdiscountAmount;
        }


        // search if accomodation 
        $date = '';
        if (!strpos($ftransaction['Transaction']['particulars'],'accomodation')){
          $date =  date('M d, Y',strtotime($ftransaction['Transaction']['created']));
        } else {
          $date   = date('M d, Y',strtotime($folio['Folio']['arrival'])) . ' - ' .date('M d, Y',strtotime('+1day',strtotime($folio['Folio']['departure'])));
        }
        
        $ftransactions[] = array(
          'id'              => $ftransaction['Transaction']['id'],
          // 'code'            => $ftransaction['Transaction']['code'],
          'business'        => $ftransaction['Transaction']['Business']['name'],
          'particulars'     => properCase($ftransaction['Transaction']['particulars']),
          'totalAmount'     => $totalAmount,
          'totalPayment'    => $totalPayment,
          'totalDiscount'   => $totalDiscount,
          'totalBalance'    => $totalAmount - ($totalPayment - $totalDiscount),
          'id'              => $folio['Folio']['id'],
          'code'            => $folio['Folio']['code'],
          'guestId'         => $folio['Folio']['guestId'],
          'arrival'         => date('M d,Y',strtotime($folio['Folio']['arrival'])),
          'departure'       => date('M d,Y',strtotime('+1day',strtotime($folio['Folio']['departure']))),
          'nights'          => $folio['Folio']['nights'],
          'room'            => $folio['Folio']['roomId'],  
          'roomType'        => $folio['Room']['RoomType']['name'],
          'bookingSource'   => properCase($folio['Folio']['bookingSource']),
          'date'            => $date,
          'transferedBills' => $folio['Folio']['transferedBills'],
          'paidByParent'    => $ftransaction['Transaction']['paidByParentId'],
          'parentFolio'     => $this->getFolio($folio['Folio']['id'],$ftransaction['Transaction']['paidByParentId'])
        );  
      }  
  
        $fullTransactions['transactions'] = $ftransactions; 
        $fullTransactions['folios'] = $folios;      
     }

      $history['GuestName']     =  properCase($folio['Guest']['lastName'] . ' ' . $folio['Guest']['firstName']);
      $history['Folio']         =  $ftransactions; 

 
    $foliosubs = $this->Folio->find('all',array(
        'contain' => array(
          'FolioSub' => array(
            'Folio' => array(
              'code'  
            )  
          ),
          'FolioTransaction' => array(
          'Transaction' => array(
            'Business',
             'TransactionSub' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionPayment' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionDiscount' => array(
              'conditions' => array(
                'visible' => true
              )
            )
          )
          )
        ),
        'conditions' => array(
          'Folio.guestId' => $id
        )
    ));
    
    
     
    // transform
    $received = array();
    foreach ($foliosubs as $fs) {
      if(!empty($fs['FolioSub'])) {
        
        foreach ($fs['FolioTransaction'] as $ft) {
          
           // subs
            $tAmount = 0;
            foreach ($ft['Transaction']['TransactionSub'] as $s) {
              $tAmount += $s['amount'];
            }    
    
            // payments
            $tPayment = 0;
            $tChange = 0;    
            foreach ($ft['Transaction']['TransactionPayment'] as $p) {
              $tChange += $p['change'];
              $pay = $p['amount'] - $tChange;
              $tPayment += $pay;
            }    
    
            // discounts
            $tDiscount = 0;
            foreach ($ft['Transaction']['TransactionDiscount'] as $d) {
              $tdisc = $d['type'] == 'fixed'? $d['value'] : $tAmount * ($d['value']/100);
              $tDiscount += $tdisc;
              }
            
          // search if accomodation 
        $dates = '';
        if (!strpos($ft['Transaction']['particulars'],'accomodation')){
          $dates =  date('M d, Y',strtotime($ft['Transaction']['created']));
        } else {
          $dates   = date('M d, Y',strtotime($fs['Folio']['arrival'])) . ' - ' .date('M d, Y',strtotime('+1day',strtotime($fs['Folio']['departure'])));
        }
        
           $received[] = array(
            'folioId'         => $fs['Folio']['code'],
            'senderFolio'     => $this->getSenderFolio($fs['FolioSub']),
            'business'        => $ft['Transaction']['Business']['name'],
            'particulars'     => properCase($ft['Transaction']['particulars']),
            'totalAmount'     => $tAmount,
            'totalPayment'    => $tPayment,
            'totalDiscount'   => $tDiscount,
            'totalBalance'    => $tAmount - ($tPayment - $tDiscount),
            'date'            => $dates
        );  
          
        }
        
       
      }
    }
       
    $response = array(
      'ok'  => true,
      'data' => $history,
      'guestId' => $id,
      'received' => $received
    );

    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }
  
  public function getFolio($folioId=null,$transfered=false) {
    //transform // get foliocode
    if($transfered) {
      $foliosub = $this->FolioSub->find('first',array(
        'conditions' => array(
          'FolioSub.folioId' => $folioId  
        )
      ));
      
      $parentId = $this->Folio->find('first',array(
        'conditions' => array(
          'Folio.id' =>  $foliosub['FolioSub']['parentId']
        )  
      ));
      $parentId = $parentId['Folio']['code'];
    } else{
      $parentId = null;
    }
    
    return $parentId;
  }

   public function getSenderFolio($folioId=null) {
     
     foreach ($folioId as $f) {
       $folioId = $f['Folio']['code'];
     }
     
     return $folioId;
     
   }   
  
}

