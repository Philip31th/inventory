<?php 
class MessagesController extends AppController {
	
	public $layout = null;
	
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

	public function index() {
	  // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

		// default condition
		$conditions = array();
    if (!$this->superUser())
      $conditions['Message.visible'] = true;
		
		$conditions['Message.to'] = $this->Session->read('Auth.User.id');
		// paginate data
		$paginatorSettings = array(
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Message.created' => 'DESC'
      )
    );
    $modelName = 'Message';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

		
		//transform data
    $messages = array();
    foreach ($tmpData as $data) {
      $message = $data['Message'];

      $messages[] = array(
        'id'      => $message['id'],
        'from'    => $message['from'], 
        'to'      => $message['to'],
        'subject' => $message['subject'],
        'message' => $message['message']
      );
    }

		$response = array(
			'ok'         => true,
			'data'       => $accomodations,
      'paginator'  => $paginator,
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response',
		));
	}

	public function view($id = null) {
		$data = $this->Accomodation->findById($id);
		$response = array(
			'ok'   => true,
			'data' => $data,
		);
		
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response',
		));
	}

  public function add() {
    $save = $this->Accomodation->validSave($this->request->data['Accomodation']);
    $response = $save;

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response',
		));
  }

  public function edit($id = null) {
    $save = $this->Accomodation->validSave($this->request->data['Accomodation']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response',
    ));
  }

  public function delete($id = null) {
    if ($this->superUser())
      $delete = $this->Accomodation->delete($id);
    else
      $delete = $this->Accomodation->hide($id);

		if ($delete) {
			$response = array(
				'ok'   => true,
				'msg'  => 'Accomodation has been deleted'
			);
		} else {
			$response = array(
				'ok'  => false,
        'msg' => 'Accomodation cannot be deleted this time.'
			);
		}

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response',
		));
  }
}
