<?php 
class PenaltiesController extends AppController {
  
  public $layout = null;
  public $components = array('Paginator', 'RequestHandler');
  
  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
    
    // default conditions
    $conditions['Penalty.visible'] = true;

    // search conditions
    if (isset($this->request->query['search'])) {
      $conditions['OR'] = array(
        'Penalty.name LIKE'=> '%' . $this->request->query['search'] . '%'
      );
    }

    // paginate data
    $paginatorSettings = array(
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Penalty.name' => 'ASC'
      )
    );
    $modelName = 'Penalty';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];
    
    // transform data
    $penalties = array();
    foreach ($tmpData as $data) {
      $penalty = $data['Penalty'];

      $penalties[] = array(
        'id'     => $penalty['id'],
        'code'   => $penalty['code'],
        'name'   => $penalty['name'],
        'value'  => $penalty['value'],
        'type'   => $penalty['type'],
      );
    }

    $response = array(
      'ok'        => true,
      'data'      => $penalties,
      'paginator' => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $penalty = $this->Penalty->findById($id);

    $response = array(
      'ok'   => true,
      'data' => $penalty,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function add() {
    if ($this->Penalty->save($this->request->data)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Penalty has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Penalty cannot be save this time.'
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    if($this->Penalty->save($this->request->data)){
      $response = array(
        'ok'  => true,
        'msg' => 'Penalty has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Penalty cannot be save this time.'
      );
    }

    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function delete($id = null) {
    if ($this->Penalty->hide($id)) {
      $response = array(
        'ok'  => true,
        'msg' => 'Penalty has been removed.',
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Penalty cannot be save this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

    
}
