<?php 
class TransactionDiscountsController extends AppController {
  
  public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  
  public function add() {
    $save = $this->TransactionDiscount->saveMany($this->request->data['TransactionDiscount']);
  
    if($save) {
      $response = array(
        'ok'  => true,
        'msg' => 'Transaction Discount has been saved.'
      );
    } else {
      $response = array(
        'ok'  => true,
        'msg' => 'Transaction Discount has been saved.'
      );
    }
  
		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response'
		));
  }

   public function delete($id = null) {
    if ($this->TransactionDiscount->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
      );
  }
    $this->set(array(
      'response'  => $response,
      '_serialize'=> 'response'
    ));
  }
  
}
