<?php 
class TableTransactionsController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function edit($id = null) {
     
     $this->FolioTransaction->save(array(
      'id'  =>  $id,
      'paid' => $this->request->data['paid']
     ));

     $response = array (
        'ok'  => true,
        'data'  =>  $this->request->data
      );

     $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }  

}  