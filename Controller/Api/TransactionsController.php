<?php 
class TransactionsController extends AppController {
  
  public $uses = array('Transaction', 'FolioTransaction', 'Folio','Business');
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index(){
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
  
    // default conditions
    $conditions = array();
    $conditions['Transaction.visible'] = true;

    // check business
    if (isset($this->request->query['code'])) {
      $code = $this->request->query['code'];
      $conditions['Transaction.businessId'] = $this->Business->get($this->request->query['code']);
    } else {
      $code = null;
    }

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Transaction.particulars LIKE' => "%$search%",
        'Transaction.code LIKE'        => "%$search%"
      );
    }

    // paginate data
    $paginatorSettings = array(
      'contain'=>array(
        'Business', 
        'FolioTransaction'  =>   array(
          'Folio'
        ),
        'TransactionSub'  =>  array(
          'conditions'  =>  array('TransactionSub.visible'=>true)
        )
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Transaction.code' => 'ASC'
      )
    );

    $modelName = 'Transaction';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData   = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $transactions = array();
    foreach ($tmpData as $data) {
      $transaction = $data['Transaction'];
      $subs        = $data['TransactionSub'];
      $business   = $data['Business'];

      // get sub-total
      $subsTotal = 0;
      foreach($subs as $sub){
          $subsTotal += $sub['amount'];
      }

      $transactions[] = array(
        'id'          =>  $transaction['id'],
        'code'        =>  $transaction['code'],
        'business'    =>  $business['name'],
        'particulars' =>  $transaction['particulars'],
        'date'        =>  date('m-d-Y', strtotime($transaction['date'])),
        'amount'      =>  $subsTotal,
        'Items'       =>  $subs
      );
    }
    
    $response = array(
      'ok'        => true,
      'code'      => $code,
      'data'      => $transactions,
      'paginator' => $paginator
    );

    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  
  public function add(){
    $this->request->data['Transaction']['code'] = $this->Transaction->generateCode();
    
    // if walang selected folio 
    if(isset($this->request->data['Folio']['code'])){
      $this->request->data['FolioTransaction']['folioId'] = $this->Folio->generateId($this->request->data['Folio']['code']);
    }
    
    // if merong trans date or wala
    if(!isset($this->request->data['Transaction']['date'])){
      $this->request->data['Transaction']['date'] = date('Y-m-d');
    }else{
      $this->request->data['Transaction']['date'] = date('Y-m-d', strtotime($this->request->data['Transaction']['date']));
    }
    
    if($this->Transaction->save($this->request->data['Transaction'])){
      $transactionId = $this->Transaction->getLastInsertId();
      if(!empty($this->request->data['TransactionSub'])){
        foreach($this->request->data['TransactionSub'] as $i=>$data){
          $this->request->data['TransactionSub'][$i]['transactionId'] = $transactionId;
        }
        $this->Transaction->TransactionSub->saveMany($this->request->data['TransactionSub']);
      }
      if(!empty($this->request->data['FolioTransaction'])){
        $this->request->data['FolioTransaction']['transactionId'] = $transactionId;
        $this->FolioTransaction->save($this->request->data['FolioTransaction']);
      }
      
      if(isset($this->request->query['folioid'])) {
        $this->request->data['FolioTransaction']['folioId'] = $this->request->query['folioid'];
        $this->request->data['FolioTransaction']['transactionId'] = $transactionId; 
         
        $this->FolioTransaction->save($this->request->data['FolioTransaction']);
      }
  
      $response = array(
        'ok'=>true,
        'message'=>'Transaction has been saved.',
        'data'   => $this->request->data,
        // 'test' => $this->request->data['Penalty']
      );
    }
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
      
  }

  public function view($id=null) {
    $transaction = $this->Transaction->find('first', array(
      'contain' => array(
        'Business',
        'TransactionSub'  =>  array(
          'conditions'=> array(
            'TransactionSub.visible'=> true
          )
        ),
        'FolioTransaction'  =>  array(
          'Folio' =>  array(
            'Guest' => array(
              'firstName','lastName'
            )
          )
        ),
        'TransactionPayment'  =>  array(
          'conditions'  =>  array(
            'TransactionPayment.visible'
          )
        ),
        'TransactionDiscount'  =>  array(
          'Discount',
          'conditions'  =>  array(
            'TransactionDiscount.visible'
          )
        )
      ),
      'conditions' => array(
        'Transaction.id' => $id,
        'Transaction.visible'=> true
      )
    ));

    $transactions = array();

    // get subs amount
    $subsTotal = 0;
    foreach ($transaction['TransactionSub'] as $sub) {
          $subsTotal += $sub['amount']*$sub['quantity'];
    }

    // get payment
    $paymentTotal = 0;
    $changeTotal = 0;   
    foreach ($transaction['TransactionPayment'] as $pay) {
          $paymentTotal += (float)$pay['amount'];
          $changeTotal += $pay['change'];
    }

    // get discount
    $discountTotal = 0;
    $percentRate = 0;
    $discs = array();
    foreach ($transaction['TransactionDiscount'] as $disc) {
      $discountAmount = $disc['type'] == 'fixed'? (float)$disc['value'] : $subsTotal * ($disc['value']/100);
      $discountTotal += $discountAmount;

      // $totalDiscount += $discountAmount;
      if($disc['type'] == 'percent') {
        $percentRate = $disc['value']/100;
        // $totalPercent += $percentRate;
      } else {
        $percentRate = null;
      }

      $transactions[] = array(
        'id'     => $disc['id'],
        'name'   => ucwords($disc['Discount']['name']),
        'value'  => $disc['value'],
        'type'   => ucfirst($disc['type']),
        'rate'   => $percentRate,
        'amount' => $discountAmount
      );
    }
     
    $bar_resto_orders = false;
    if ($transaction['Transaction']['particulars']=='bar orders'||$transaction['Transaction']['particulars']=='restaurant orders') {
      $bar_resto_orders = true;
    }  

    $transaction['TransactionDiscount'] = $transactions; 
    $transaction['TotalAmount']   = $subsTotal;
    $transaction['TotalPayment']  = $paymentTotal-$changeTotal;
    $transaction['TotalDiscount'] = $discountTotal;
    $transaction['TotalBalance']  = $subsTotal - (($paymentTotal+$discountTotal)-$changeTotal);
    $transaction['TotalChange']   = $changeTotal;
    $transaction['Guest']['name'] = properCase(@$transaction['FolioTransaction']['Folio']['Guest']['firstName'] . ' ' . @$transaction['FolioTransaction']['Folio']['Guest']['lastName']);
    $transaction['Transaction']['date'] = fdate($transaction['Transaction']['created']);
     $transaction['BarRestoOrders'] = $bar_resto_orders;

     $response = array(
      'ok'    => true,
      'data'  => $transaction
    );
     
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));

  }
  
  public function edit($id = null) {

    if($this->Transaction->save($this->request->data['Transaction'])){
      $transactionId = $id;

      if(!empty($this->request->data['TransactionSub'])){

        foreach($this->request->data['TransactionSub'] as $i=>$data){
          $this->request->data['TransactionSub'][$i]['transactionId'] = $transactionId;
        }

        $this->Transaction->TransactionSub->saveMany($this->request->data['TransactionSub']);
      }
  
      $response = array(
        'ok'=>true,
        'message'=>'Transaction has been saved.',
        'data'   => $this->request->data
      );
    }

		$this->set(array(
			'response'=>$response,
			'_serialize'=>'response'
		));
  }
  
  public function delete($id = null) {
    if ($this->Transaction->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
      );
    }
    $this->set(array(
      'response'  => $response,
      '_serialize'=> 'response'
    ));
  }
  
  public function api_receivables($id = null) {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
  
    // default conditions
    $conditions = array();
    $conditions['Transaction.visible'] = true;

    if ($id!=null) {
      $conditions['Transaction.id'] = $id;
    }

    // check business
    if (isset($this->request->query['code'])) {
      $code = $this->request->query['code'];
      $conditions['Transaction.businessId'] = $this->Business->get($this->request->query['code']);
    } else {
      $code = null;
    }

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Transaction.particulars LIKE' => "%$search%",
        'Transaction.code LIKE'        => "%$search%"
      );
    }

    // paginate data
    $paginatorSettings = array(
      'contain'=>array(
        'Business', 
        'FolioTransaction'  =>   array(
          'Folio'
        ),
        'TransactionSub'  =>  array(
          'conditions'  =>  array('TransactionSub.visible'=>true)
        ),
        'TransactionPayment'  =>  array(
          'conditions'  =>  array('TransactionPayment.visible'=>true)
        ),
        'TransactionDiscount'  =>  array(
          'conditions'  =>  array('TransactionDiscount.visible'=>true)
        )
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Transaction.created' => 'DESC'
      )
    );

    $modelName = 'Transaction';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData   = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    $transactions = array();
    $totalReceivables = 0;
    foreach ($tmpData as $data) {
      $transaction = $data['Transaction'];

      // get total Amount
      $totalAmount = 0;
      $subs = array();
       foreach ($data['TransactionSub'] as $sub) {
          $totalAmount += ($sub['amount']*$sub['quantity']); 

          $subs[] = array(
            'id'          =>  $sub['id'],
            'particulars' =>  properCase($sub['particulars']),
            'amount'      =>  $sub['amount'],
            'quantity'    =>  (int)$sub['quantity'],
            'date'        =>  date('M d, Y h:i A',strtotime($sub['created']))
          );
        }

      // get total Payment
      $totalPayment = 0;
      $initPayment = 0;
      $payments = array();
      foreach ($data['TransactionPayment'] as $pay) {

        // initial payment
        if ($pay['created'] < $data['FolioTransaction']['modified']) {
           $initPayment += $pay['amount']-$pay['change'];
        } else {
          $totalPayment += $pay['amount']-$pay['change'];  

          $payments[] = array(
            'id'          =>  $pay['id'],
            'date'        =>  date('M d, Y h:i A',strtotime($pay['created'])),
            'orNumber'    =>  $pay['orNumber'],
            'paymentType' =>  $pay['paymentType'],
            'amount'      =>  $pay['amount']-$pay['change'],
            'change'      =>  $pay['change'],
          );
        }     
      }  

      // get total Discount
      $totalDiscount = 0;
      foreach ($data['TransactionDiscount'] as $disc) {
        $discountAmount = $disc['type'] == 'fixed'? (float)$disc['value'] : $totalAmount * ($disc['value']/100);
        $totalDiscount += $discountAmount;
      }

      $totalBalance = $totalAmount - ($totalPayment + $initPayment + $totalDiscount);

      if ($totalBalance > 0 && @$data['FolioTransaction']['Folio']['code']!=null) {
        $transactions[] = array(
          'id'           => $transaction['id'],
          'folio_code'   => $data['FolioTransaction']['Folio']['code'],
          'title'        => properCase($transaction['particulars']),
          'business'     => properCase($data['Business']['name']),
          'datetime'     => date('M d, Y h:i A',strtotime($transaction['created'])),
          'totalAmount'  => $totalAmount,
          'initPayment'  => $initPayment,
          'totalPayment' => $totalPayment,
          'totalBalance' => $totalBalance,
          'TransactionSub'=> $subs,
          'TransactionPayment' => $payments
        );
        $totalReceivables += $totalBalance;
      }

      
    }

    $response = array(
      'ok'   => true,
      'data' => $transactions,
      'totalReceivables' => $totalReceivables,
      'tmp'=> $tmpData
    );

    $this->set(array(
      'response'  => $response,
      '_serialize'=> 'response'
    ));

  }
}  
  