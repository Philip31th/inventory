<?php 
class BarRestoCashieringReportController extends AppController {
 
  public $layout = null;
  public $components = array('Paginator', 'RequestHandler');
  public $uses = array('Folio','Transaction');

  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }

  public function index($id=null) {
      $date  = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])):date('Y-m-d');
      $edate = date('Y-m-d', strtotime('-1day',strtotime($date)));

      $conditions = array();
      $conditions['Folio.visible'] = true;
      $conditions['Folio.arrival <='] = $date;
      $conditions['Folio.departure >='] = $edate;

      $conditions_payments = array();
      $conditions_payments['TransactionPayment.visible'] = true;
      $conditions_payments['TransactionPayment.date'] = $date;
      
      if ($id != null) {
        $conditions['Folio.id'] = $id;   
      }

      if(isset($this->request->query['startTime'])) {
        $startTime = date('Y-m-d', strtotime($this->request->query['date'])) . ' ' . date('H:i',strtotime($this->request->query['startTime']));
        $endTime =  date('Y-m-d', strtotime($this->request->query['date'])) . ' ' . date('H:i',strtotime($this->request->query['endTime']));

        $conditions_payments['TransactionPayment.created >='] = $startTime;
        $conditions_payments['TransactionPayment.created <='] = $endTime;
      }

      $cashier = $this->Folio->find('all', array(
        'contain'=>array(
          'ReservationRoom'=>array('Reservation'),
          'Guest'=>array('Company'),
          'Room'=>array('RoomType'),
          'FolioTransaction'=>array(
            'Transaction'=>array(
              'Business', 
              'TransactionSub' => array('conditions' => array('TransactionSub.visible'=> true)),
              'TransactionPayment' => array(
                'conditions' => $conditions_payments 
                ),
              'TransactionDiscount' => array('Discount','conditions' => array('TransactionDiscount.visible'=> true)),           
              'conditions' => array(
                'Transaction.visible'=> true,
                'Transaction.businessId' => $this->request->query['business']           
              )
            ),
            'conditions' => array('FolioTransaction.visible'=> true)
          ),
        ),
        'conditions'=>$conditions
      ));
      
      $cashiering = array();
      $summary = array();
      $sum = '';
      $cash = 0;
      $totalCash = 0;
      $totalCard = 0;
      $totalDebit = 0;
      
      foreach($cashier as $data){
        $transactionAmount = 0;
        $paidAmount = 0;
        $paymentType = '';
        $str = '';
        $transactions = array();
        $payments = array();
        $totalReceivable= 0;
    
        foreach($data['FolioTransaction'] as $transaction){
          
          if(!empty($transaction['Transaction'])) {
            // subs
            $amount = 0;
            $items = array();
            foreach($transaction['Transaction']['TransactionSub'] as $sub){
              $items[] = array(
                'particulars'=>$sub['particulars'],
                'amount'=>$sub['amount'],
                'quantity'=> $sub['quantity']
              );
              $amount+=$sub['amount']*$sub['quantity'];
            }
            
            // discounts
            $totalDiscount = 0;
            $transactionDiscounts = array();
            $totalPercent = 0;
            $percentRate = 0;
            foreach ($transaction['Transaction']['TransactionDiscount'] as $dis) {
              $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $amount * ($dis['value']/100);
              $totalDiscount += $discountAmount;
              if($dis['type'] == 'percent') {
                $percentRate = $dis['value']/100;
                $totalPercent += $percentRate;
              }
        
              $transactionDiscounts[] = array(
                'id'     => $dis['id'],
                'name'   => ucwords($dis['Discount']['name']),
                'value'  => $dis['value'],
                'rate'   => $percentRate,
                'type'   => $dis['type'],
                'amount' => $discountAmount
              );
            }


            // payments
            $paid = 0;
            $payments = array();
            $totalChange = 0;
            foreach($transaction['Transaction']['TransactionPayment'] as $pay){

                $totalChange += (float)$pay['change'];

                $payments[] = array(
                  'or_number'=>$pay['orNumber'],
                  'amount'=>$pay['amount'],
                  'date'=>date('m/d/Y', strtotime($pay['date'])),
                  'change'=>$pay['change'],
                  'time'=>date('h:i:s A', strtotime($pay['created'])),
                  'paymentType'=>($pay['paymentType'])
                );

                $paid += $pay['amount']-(float)$pay['change'];

                  // summary tally
                  if($pay['paymentType']=='cash' || $pay['paymentType']=='cheque'||$pay['paymentType']==null) {
                    $totalCash += ($pay['amount']-$pay['change']);
                    $paymentType = 'cash'; 
                  } 
                  if ($pay['paymentType']=='credit card') {
                    $totalCard += ($pay['amount']-$pay['change']);
                    $paymentType = 'credit card';
                  } 

                  if ($pay['paymentType']=='debit') {
                    $totalDebit += ($pay['amount']-$pay['change']);
                    $paymentType = 'debit';
                  }
            }
          }  
            
            if(@$transaction['Transaction']['businessId']!=null) {

            $transactions[] = array(
              'id'=>@$transaction['Transaction']['id'],
              'code'=>@$transaction['Transaction']['code'],
              'business_id'=>@$transaction['Transaction']['businessId'],
              'title'=>@$transaction['Transaction']['title'],
              'items'=>$items,
              'payments'=>$payments,
              'amount'=>$amount,
              'discount'=>$totalDiscount,
              'paid'=>$paid,
              'balance'=>($amount-$paid)-($totalDiscount)
            );

            $transactionAmount += $amount;
             $paidAmount += $paid;

            }
        }
         
            if ($data['ReservationRoom']['checkedIn']==true) {
              $status = 'IN-HOUSE';
              // $totalReceivable += ($transactionAmount - $paidAmount  - $totalDiscount);
            } 
            if ($data['ReservationRoom']['checkOutDate']!=null) {
              $status = 'CHECK-OUT';
            }

            if ($data['ReservationRoom']['checkOutDate']!=null&&$data['Folio']['transferedBills'] || $data['ReservationRoom']['checkedIn']==true) {
              $totalReceivable += ($transactionAmount - $paidAmount  - @$totalDiscount);
            }

        if($paidAmount!=0 || @$sendBill) {
          $cashiering[] = array(
            'id'=>$data['Folio']['id'],
            'code'=>$data['Folio']['code'],
            'room'=>$data['Room']['name'],
            'type'=>$data['Room']['RoomType']['name'],
            'rate'=>$data['Room']['rate'],
            'guest'=>ucwords($data['Guest']['lastName'] . ', ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['middleName']),
            'company'=>!empty($data['Guest']['Company'])?$data['Guest']['Company']['name']:'',
            'address'=>$data['Guest']['address'],
            'arrival'=>date('m/d/Y', strtotime($data['Folio']['arrival'])),
            'departure'=>date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
            'reservation_id'=>$data['ReservationRoom']['reservationId'],
            'reservation_room_id'=>$data['ReservationRoom']['id'],
            'status' => $status,
            'closed'=>$data['Folio']['closed'],
            'transferedBills' => $data['Folio']['transferedBills'],
            'Transaction'=>array(
              'charges'=>$transactions,
              'amount'=>$transactionAmount,
              'paid'=> $paidAmount,
              'discount'=> $totalDiscount,
              'balance'=> ($transactionAmount - $paidAmount  - $totalDiscount),
              'paymentType'=>$paymentType
            ),
          );    
        
        $summary = array(
          'TotalCash'     =>  $totalCash,
          'TotalCard'     =>  $totalCard,
          'TotalDebit'    =>  $totalDebit,
          'TotalReceivable' => $totalReceivable,
          'Total'         =>  $totalCash + $totalCard  + $totalDebit
        );  

        }

      }

      $response = array(
        'ok'=>true,
        'date' => $date,
        'message'=>'Getting data successful.',
        'result'=>$cashiering,
        'summary'=> $summary,
        // 'data' =>$cashier,
        // 'received' => $received,
        'non_guest'=>$this->non_guest_report($date,$this->request->query['business'],@$startTime,@$endTime),
        'startTime' =>@$startTime,
        'endTime' =>  @$endTime
      );
      
      $this->set(array(
        'response'=>$response,
        '_serialize'=>'response'
      ));
  }

  public function non_guest_report($date=null,$business=null,$startTime=null,$endTime=null) {

      $conditions = array();

      $conditions_payments = array();
      $conditions_payments['TransactionPayment.visible'] = true;
      $conditions_payments['TransactionPayment.date'] = $date;

      if(isset($startTime)) {
        $startTime = date('Y-m-d', strtotime($date)) . ' ' . date('H:i',strtotime($startTime));
        $endTime =  date('Y-m-d', strtotime($date)) . ' ' . date('H:i',strtotime($endTime));

        $conditions_payments['TransactionPayment.created >='] = $startTime;
        $conditions_payments['TransactionPayment.created <='] = $endTime;
      }

      $cashier = $this->Transaction->find('all', array(
        'contain' => array(
            'FolioTransaction' => array('conditions'=>array('FolioTransaction.visible')),
            'Business', 
            'TransactionSub' => array('conditions' => array('TransactionSub.visible'=> true)),
            'TransactionPayment' => array(
              'conditions' => $conditions_payments 
              ),
            'TransactionDiscount' => array('Discount','conditions' => array('TransactionDiscount.visible'=> true)),            
        ),
        'conditions' => array(
            'Transaction.visible'=> true,
            'Transaction.businessId' => $business          
          )
        ));

      $cashiering = array();
      $summary = array();
      $sum = '';
      $cash = 0;
      $totalCash = 0;
      $totalCard = 0;
      $totalDebit = 0;
      $totalReceivable= 0;
      $transactions = array();
         
    foreach($cashier as $transaction){

      if ($transaction['FolioTransaction']['id'] == null) {
        $transactionAmount = 0;
        $paidAmount = 0;
        $paymentType = '';
        $str = '';

        $payments = array();    

        // subs
        $amount = 0;
        $items = array();
        foreach($transaction['TransactionSub'] as $sub){
          $items[] = array(
            'particulars'=>$sub['particulars'],
            'amount'=>$sub['amount'],
            'quantity'=> $sub['quantity']
          );
          $amount+=$sub['amount']*$sub['quantity'];
        }
            
        // discounts
        $totalDiscount = 0;
        $transactionDiscounts = array();
        $totalPercent = 0;
        $percentRate = 0;
        foreach ($transaction['TransactionDiscount'] as $dis) {
          $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $amount * ($dis['value']/100);
          $totalDiscount += $discountAmount;
          if($dis['type'] == 'percent') {
            $percentRate = $dis['value']/100;
            $totalPercent += $percentRate;
          }
    
          $transactionDiscounts[] = array(
            'id'     => $dis['id'],
            'name'   => ucwords($dis['Discount']['name']),
            'value'  => $dis['value'],
            'rate'   => $percentRate,
            'type'   => $dis['type'],
            'amount' => $discountAmount
          );
        }

        // payments
        $paid = 0;
        $payments = array();
        $totalChange = 0;
        foreach($transaction['TransactionPayment'] as $pay){

            $totalChange += (float)$pay['change'];

            $payments[] = array(
              'or_number'=>$pay['orNumber'],
              'amount'=>$pay['amount'],
              'date'=>date('m/d/Y', strtotime($pay['date'])),
              'change'=>$pay['change'],
              'time'=>date('h:i:s A', strtotime($pay['created'])),
              'paymentType'=>($pay['paymentType'])
            );

            $paid += $pay['amount']-$pay['change'];

              // summary tally
              if($pay['paymentType']=='cash' || $pay['paymentType']=='cheque'||$pay['paymentType']==null) {
                $totalCash += ($pay['amount']-$pay['change']);
                $paymentType = 'cash'; 
              } 
              if ($pay['paymentType']=='credit card') {
                $totalCard += ($pay['amount']-$pay['change']);
                $paymentType = 'credit card';
              } 

              if ($pay['paymentType']=='debit') {
                $totalDebit += ($pay['amount']-$pay['change']);
                $paymentType = 'debit';
              }
        } 
        if(!empty($transaction['TransactionPayment'])) {
          $transactions[] = array(
              'id'=>@$transaction['Transaction']['id'],
              'code'=>@$transaction['Transaction']['code'],
              'business_id'=>@$transaction['Transaction']['businessId'],
              'title'=>@$transaction['Transaction']['title'],
              'date'  => @$transaction['Transaction']['date'],
              'paymentType'=>$paymentType,
              'items'=>$items,
              'payments'=>$payments,
              'amount'=>$amount,
              'discount'=>$totalDiscount,
              'paid'=>$paid,
              'balance'=>($amount-$paid)-($totalDiscount)
            );
            $transactionAmount += $amount;
            $paidAmount += $paid;
        }

      if(@$paidAmount!=0) {
          $cashiering['Transactions'] = array(

            'charges'=>$transactions,
            'amount'=>$transactionAmount,
            'paid'=> $paidAmount,
            'discount'=> $totalDiscount,
            'folio'=>$transaction['FolioTransaction'],
            'balance'=> ($transactionAmount - $paidAmount  - $totalDiscount),
          );    

        $cashiering['Summary'] = array(
          'TotalCash'     =>  $totalCash,
          'TotalCard'     =>  $totalCard,
          'TotalDebit'    =>  $totalDebit,
          'Total'         =>  $totalCash + $totalCard  + $totalDebit
        );  

        }
      }
    }  

    return $cashiering;

  }

}  