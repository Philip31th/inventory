<?php
class PermissionsController extends AppController {
  
  public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }
  
  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;
    
    // default conditions
    $conditions = array();
    $conditions['Permission.visible'] = true;

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Permission.name LIKE'       => "%$search%",
        'Permission.module LIKE'     => "%$search%",
        'Permission.controller LIKE' => "%$search%",
        'Permission.action LIKE'     => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order' => array(
        'Permission.module' => 'ASC',
        'Permission.controller' => 'ASC',
        'Permission.action' => 'ASC'
      )
    );
    $modelName = 'Permission';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $permissions = array();
    foreach ($tmpData as $data) {
      $permission = $data['Permission'];
      $permissions[] = array(
        'id'         => $permission['id'],
        'name'       => $permission['name'],
        'module'     => $permission['module'],
        'controller' => $permission['controller'],
        'action'     => $permission['action'],
      );
    }

    $response = array(
      'ok'        => true,
      'data'      => $permissions,
      'paginator' => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $permission = $this->Permission->findById($id);

    $response = array(
      'ok'        => true,
      'data'      => $permission,
    );

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function add() {
    $save = $this->Permission->validSave($this->request->data['Permission']);
    $response = $save;
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function edit($id = null) {
    $save = $this->Permission->validSave($this->request->data['Permission']);
    $response = $save;
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function delete($id = null) {

  }
}
