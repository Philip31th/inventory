<?php
class NightAuditsController extends AppController {
  
  public $uses = array('Room','Folio', 'RoomType');
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

    public function index() {
    // // default date
    $date = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])) : date('Y-m-d');
    
    // // default conditions
    $conditions = array();
    $conditions['Room.visible'] = true;
    
    // load rooms
    $tmpData = $this->Room->find('all', array(
      'contain'=>array(
        'Accomodation' => array(
          'name'
        ),
        'RoomType' => array(
          'name',
          'rate'
        ),
        'Folio'=>array(
          'Guest' => array(
            'lastName',
            'firstName',
            'middleName'
          ),
          'conditions' => array(
            'Folio.arrival   <=' => $date,
            'Folio.departure >=' => $date,
            'Folio.closed'       => false
          ),
          'fields' => array(
            'arrival','departure','id','closed'
          ),
        ),
        'ReservationRoom'=>array(
          'id',
          'arrival',
          'departure',
          'Reservation' => array(
            'lastName',
            'firstName',
            'middleName'
          ),
          'conditions' => array(
            'ReservationRoom.arrival <='   => $date,
            'ReservationRoom.departure >=' => $date,
            'ReservationRoom.closed'       => false
          )
        ),
      ),
      'conditions' => $conditions,
      'order' => array(
        'Room.name' => 'ASC',
      ),
      'fields' => array(
        'COUNT(Room.roomTypeId) as TotalRooms',
        'COUNT(CASE uncleaned when "1" then 1 else null end) as TotalUncleaned',
        'COUNT(CASE outOfOrder when "1" then 1 else null end) as TotalOutOfOrder',
        'uncleaned','outOfOrder','name','rate','visible','Room.id'
      ),
    'group' => array(
          'Room.roomTypeId'  
        )
    ));

    // initialization
    $audits = array();
    $audit = array();

    $available = 0;
    $occupied = 0;
    $gro = 0;
    $grandTotalRooms = 0;
    $grandTotalOccupied = 0;
    $grandTotalAvailable = 0;
    $grandDir = 0;
    $grandOoo = 0;
    $grandRack = 0;
    $grandGro = 0;
    $grandDisc = 0;
    $grandRevenue = 0;
    $discount = 0;
    $discountAmt = 0;
    $revenue = 0;
    
    // transformation
    foreach ($tmpData as $data) {
      $discounts = $this->folios($data['RoomType']['name']);
      $occupied = $this->occupied($data['RoomType']['name']);
      $available  = $data[0]['TotalRooms'] - $occupied;
      
      $grandTotalRooms += $data[0]['TotalRooms'];
      $grandTotalOccupied += $occupied;
      $grandTotalAvailable += $available;

      $gro = $occupied * $data['RoomType']['rate'];
      $grandGro += $gro;
      $grandDisc += $discounts;
      $revenue = $gro - $discounts;
      $grandRevenue += $revenue;
      
        $audit[] = array(
        'room'                => $data['RoomType']['name'],
        'total_rooms'         => $data[0]['TotalRooms'],
        'total_occupied'      => $occupied,
        'total_available'     => $available,
        'dirty'               => $data[0]['TotalUncleaned'],
        'out_of_order'        => $data[0]['TotalOutOfOrder'],
        'rate'                => $data['RoomType']['rate'],
        'gro'                 => $gro,
        'discount'            => $discounts,
        'revenue'             => $revenue
      );
      
      $grandDir += $data[0]['TotalUncleaned'];
      $grandOoo += $data[0]['TotalOutOfOrder'];
    
    }
        $audits['audits'] = $audit;
        $audits['grandTotalRooms']     = $grandTotalRooms;
        $audits['grandTotalOccupied']  = $grandTotalOccupied;
        $audits['grandTotalAvailable'] = $grandTotalAvailable;
        $audits['grandTotalDir'] = $grandDir;
        $audits['grandTotalOoo'] = $grandOoo;
        $audits['grandGro'] = $grandGro;
        $audits['grandDisc'] = $grandDisc;
        $audits['grandRevenue'] = $grandRevenue;
  

    $response = array(
      'ok'   => true,
      'date' => $date,
      'data' => $audits,
      // 'audit' => $this->occupied('Presidential'),
    );
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function occupied($type=null) {  
    $conditions['RoomType.visible'] = true;
    
    $roomTypes = $this->RoomType->find('all', array(
      'contain'=>array(
        'Accomodation',
        'Room'=>array(
          'Folio'=>array(
            'id',
            'conditions'=>array(
              'OR'=>array(
                 'Folio.arrival   <=' => $this->request->query['date'],
                 'Folio.departure >=' => $this->request->query['date'],
              ),
              'visible'=>true,
              'closed' => false
            ),
            'FolioTransaction' => array('Transaction'=>array('TransactionDiscount'))
          ),
          'ReservationRoom'=>array(
            'id',
            'conditions'=>array(
              'OR'=>array(
                'ReservationRoom.arrival <='   => $this->request->query['date'],
                'ReservationRoom.departure >=' => $this->request->query['date']
              ),
              'visible'   => true,
              'closed'    => false,
              )
          )
        )
      )
    ));

    $RoomType = array();
    foreach($roomTypes as $a=>$roomType){
      $Room = array();
      foreach($roomType['Room'] as $b=>$room){
        $roomType['Folio'] = $room['Folio'];
        
        if(count($room['Folio'])>0 AND count($room['ReservationRoom'])>0 ) 
          $Room[] = count($room['Folio']);
        }
      
      $roomType['Room'] = count($Room);
      if(count($Room)>0){
        $roomType['room_id'] =  $Room[0]['id'];
        $RoomType[] = $roomType;
      }
    }
    $roomTypes = $RoomType;

    $totalOccupied = 0;
    foreach ($roomTypes as $data) {
      if($data['RoomType']['name']==$type) {
        $totalOccupied = $data['Room'];
      }
    }
    
    return $totalOccupied;
  }
  
  public function folios($type=null) {
    $conditions['RoomType.visible'] = true;
    
    $roomTypes = $this->RoomType->find('all', array(
      'contain'=>array(
        'Accomodation',
        'Room'=>array(
          'Folio'=>array(
            'id',
            'conditions'=>array(
              'OR'=>array(
                 'Folio.arrival   <=' => $this->request->query['date'],
                 'Folio.departure >=' => $this->request->query['date'],
              ),
              'visible'=>true,
              'closed' => false,
              'paid'   => false
            ),
            'FolioTransaction' => array(
              'Transaction'=>array(
                'TransactionDiscount'
                )
              )
          ),
          'ReservationRoom'=>array(
            'id',
            'conditions'=>array(
              'OR'=>array(
                'ReservationRoom.arrival <='   => $this->request->query['date'],
                'ReservationRoom.departure >=' => $this->request->query['date'],
                'ReservationRoom.closed'       => false
              ),
              'visible'   => true,
              'closed'     => false
              )
          )
        )
      )
    ));

    $RoomType = array();
    $rooms =array();
    foreach($roomTypes as $a=>$roomType){
      // $rooms['RoomType'] = $roomType['RoomType'];audit[1].RoomType.rate
      $Room = array();
       $discounts = 0;
      foreach($roomType['Room'] as $b=>$room){
        foreach ($room['Folio'] as $c=>$folio) {
          foreach ($folio['FolioTransaction'] as $ft=>$tr) {
            foreach ($tr['Transaction']['TransactionDiscount'] as $disc) {
              $disco = $disc['type'] == 'fixed'? $disc['value'] : (int)$room['rate']* ((int)$disc['value']/100);
              $discounts += $disco; 
              
            }
          }
            
          }
        if(count($room['Folio'])>0 AND count($room['ReservationRoom'])>0 ) 
          $Room[] = $room;
          $roomType['Discounts'] = $discounts; 
        }
        
        // $RoomType[] = $rooms;

        $roomType['Room'] = count($Room);
       
          if(count($Room)>0){
            $roomType['room_id'] =  $Room[0]['id'];
            // $roomType['discounts'] =  $Room[0]['id'];
            $RoomType[] = $roomType;
          }
    }
        $roomTypes = $RoomType;
        
        $totalDiscounts = 0;
        foreach ($roomTypes as $data) {
          if($data['RoomType']['name']==$type) {
            $totalDiscounts = $data['Discounts'];
          }
        }
          

    return $totalDiscounts;
  }
  
}
