<?php 
class CashieringController extends AppController {
  
  public $uses = array('Folio','Reservation','Room','RoomType', 'Transaction','TransactionPayment','Guest');
  
  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    $conditions['Folio.visible'] = true;
    $conditions['Folio.paid']    = false;
    $conditions['Folio.closed']  = false;
    $conditions['Folio.transferedBills']  = false;

    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Guest.firstName LIKE'  => "%$search%",
        'Guest.lastName LIKE'   => "%$search%"
      );
    }
    // paginate data
    $paginatorSettings = array(
      'contain'    => array(
        'FolioTransaction'=>array(
            'conditions' => array('visible' => true),
          'Transaction' => array(
            'conditions' => array('visible' => true),
              'Business',
              'TransactionSub'
            )
        ),
        'Guest',
        'Room' => array('RoomType')
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Folio.code' => 'ASC'
      )
    );

    $modelName = 'Folio';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator   = $this->request->params['paging'][$modelName];
    
    $vehicleMade = '';
    if(!empty($tmp['Guest']['vehicleMade'])) {
      $vehicleMade = $tmpData['Guest']['vehicleMade'];
    }
    
    // transform folios
    $cashiering = array();
    foreach ($tmpData as $data => $cash) {
      $transactionAmount = 0;
      $transactions = array();
      $payments = array();


      // trasform folio transaction
      foreach ($cash['FolioTransaction'] as $transaction) {
        

        if (!empty($transaction['Transaction']['TransactionSub'])) {
          $items = array();
        $amount = 0;
          // transform transaction sub
          foreach ($transaction['Transaction']['TransactionSub'] as $sub) {
            $items[] = array(
             'particulars'  =>  $sub['particulars'],
             'amount'       =>  floatval($sub['amount'])  
            );
            $amount += $sub['amount'];
          }

          // transform transaction
          $transactions[] = array(
            'title'       => $transaction['Transaction']['title'],
            'particulars' => $transaction['Transaction']['particulars'],
            'items'       => $items,
            'amount'      => floatval($amount)
          );
          $transactionAmount += $amount;
        }
      }

        if (date('Y-m-d', strtotime('+1day', strtotime($cash['Folio']['departure']))) < date('Y-m-d') ) {
          $departure = date('m/d/Y');
          $days = round(((strtotime(date('Y-m-d')) - (strtotime('+1day',strtotime($cash['Folio']['departure']))))/86400),0);
        } else if (date('Y-m-d', strtotime('+1day', strtotime($cash['Folio']['departure']))) >= date('Y-m-d')) {
          $departure = date('m/d/Y', strtotime('+1day', strtotime($cash['Folio']['departure'])));
          $days = '';
        }

        // transform cashiering
        $cashiering[] = array(
          'id'        => $cash['Folio']['id'],  
          'code'      => $cash['Folio']['code'],
          'room'      => $cash['Room']['name'],
          'type'      => $cash['Room']['RoomType']['name'],
          'name'      => $cash['Guest']['firstName'] . ' ' . $cash['Guest']['lastName'],
          'arrival'   => date('m/d/Y', strtotime($cash['Folio']['arrival'])),
          'departure' =>$departure,
          'extended'  => @$days,
          'Transactions'  =>  array(
            'charges'   => $transactions,
            'amount'    => $this->view($cash['Folio']['id']),  
          ),
          'Payments'  => $payments
        );
      // }  
    }

    $response = array(
      'ok'         => true,
      'data'       => $cashiering,
      'paginator'  => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $conditions['Folio.id'] = $id;
    
    $tmp = $this->Folio->find('first', array(
      'contain'=>array(
        'ReservationRoom' => array(
          'Reservation'
        ),
        'Guest' => array(
          'Company'
        ),
        'Room' => array(
          'RoomType' => array('name')
        ),
        'FolioTransaction' => array(
          'conditions' => array(
            'visible' => true
          ),
          'Transaction' => array(
            'conditions' => array(
              'visible' => true  
            ),
            'Business',
            'TransactionSub' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionPayment' => array(
              'conditions' => array(
                'visible' => true
              )
            ),
            'TransactionDiscount' => array(
              'Discount',
              'conditions' => array(
                'visible' => true
              )
            )
          )
        ),
      ),
      'conditions' => $conditions
    ));
  
    // get total nights
    if (@$tmp['ReservationRoom']['departure'] != date('Y-m-d')) {
      $departure = strtotime('+1day',strtotime(@$tmp['ReservationRoom']['departure']));
      $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime(@$tmp['Folio']['departure']))) > date('Y-m-d')) {
      $departure = date('m/d/Y', strtotime('+1day', strtotime($tmp['Folio']['departure'])));
      $extensions = 0;
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime(@$tmp['Folio']['departure']))) < date('Y-m-d') ) {
      $departuree = date('m/d/Y');
    } else if (date('Y-m-d', strtotime('+1day', strtotime(@$tmp['Folio']['departure']))) >= date('Y-m-d')) {
      $departuree = date('m/d/Y', strtotime('+1day', strtotime(@$tmp['Folio']['departure'])));
    }

    // folio data
    $cashiering = array(
      'id'             => $tmp['Folio']['id'],
      'code'           => $tmp['Folio']['code'],
      'room'           => $tmp['Room']['name'],
      'company'        => @$tmp['Guest']['Company']['name'],
      'vehiclecolor'   => $tmp['Guest']['vehicleColor'],
      'vehicleMade'    => @$tmp['Guest']['vehicleMade'],
      'vehicleplateNo' => $tmp['Guest']['vehiclePlateNo'],
      'roomType'       => $tmp['Room']['RoomType']['name'],
      'guest'          => properCase($tmp['Guest']['firstName'] . ' ' . $tmp['Guest']['lastName']),
      'guestId'        => $tmp['Guest']['id'],
      'roomRate'       => $tmp['Room']['rate']*($tmp['Folio']['nights']+@$extensions),
      'extensions'     => @$extensions,
      'address'        => $tmp['Guest']['address'],
      'arrival'        => fdate($tmp['Folio']['arrival']),
      'departure'      => $departuree,
      'mobile'         => $tmp['Guest']['mobile'],
      'bookingSource'  => properCase($tmp['Folio']['bookingSource']),
      'specialRequest' => $tmp['Folio']['specialRequest'],
      'remarks'        => $tmp['Folio']['remarks'],
      'checkedIn'      => $tmp['ReservationRoom']['checkedIn'],
      'taxRate'        => 1.12,
      'closed'         => $tmp['Folio']['closed'],
      'transferedBills'=> $tmp['Folio']['transferedBills'],
      'reservation_room_id' =>  $tmp['ReservationRoom']['id'],
      'room_id'        => $tmp['ReservationRoom']['roomId'],
      'reservation_id' => $tmp['ReservationRoom']['reservationId']
    );
  
    //transactions
    $grandAmount   = 0;
    $grandPayment  = 0;
    $chargesGrandPayment = 0;
    $grandDiscount = 0;
    $grandChange   = 0;
    $grandInitialPayment   = 0;
    $grandBalance = 0;
    $grandHotelCharge = 0;
    $grandVat = 0;
    $transactions  = array();
    foreach ($tmp['FolioTransaction'] as $data) {
      $transaction = $data['Transaction'];
      
      if (!empty($transaction)) {
        // subs
        $totalAmount = 0;
        $otherTotalAmt = 0;
        $transactionSubs = array();    
        foreach ($transaction['TransactionSub'] as $trans) {
          if ($transaction['particulars']=='room accomodation charges') {
            $totalAmount += $trans['amount']*((int)$tmp['Folio']['nights']+@$extensions);
          } 
          elseif (strpos($transaction['particulars'],'orders')) {
            $otherTotalAmt += $trans['amount']*$trans['quantity'];
          } 
          else {
            $totalAmount += $trans['amount']*$trans['quantity'];
          }

          $transactionSubs[] = array(
            'id'          => $trans['id'],
            'particulars' => properCase($trans['particulars']),
            'amount'      => $transaction['particulars']=='room accomodation charges'? $trans['amount']*($tmp['Folio']['nights']+@$extensions):$trans['amount']*$trans['quantity'],
            'nights'      => (int)$tmp['Folio']['nights'], 
            'quantity'    => (int)$trans['quantity'] 
          );
        }
    
        // payments
        $totalChange = 0;
        $payment = 0;
        $totalbarChange = 0;
        $totalPayment = 0;
        $totalInitialPayment = 0;
        $hotelInitialPayment = 0;
        $totalhotelChange = 0;
        $totalhotelPayment = 0;
        $transactionPayments = array();
        foreach ($transaction['TransactionPayment'] as $pay) {
          if ($data['cleanTable']) { 
            if(date('Y-m-d h:i:s', strtotime($pay['created'])) <= date('Y-m-d h:i:s', strtotime($data['modified']))) {
              $totalbarChange += $pay['change'];
              $totalInitialPayment += ($pay['amount'] - $pay['change']);
            } else {
              $totalhotelChange += $pay['change'];
              $totalhotelPayment += ($pay['amount']);

              $totalChange += $pay['change'];
              $payment = $pay['amount'] - $pay['change'];
              $totalPayment += $payment;
        
              $transactionPayments[] = array(
                'id'       => $pay['id'],
                'orNumber' => $pay['orNumber'],
                'amount'   => floatval($pay['amount']),
                'change'   => floatval($pay['change']),
                'type'     => $pay['paymentType'],
                'date'     => date('Y-m-d h:i A',strtotime($pay['created'])),
                'business'  => $transaction['Business']['name']
              ); 
            }   
          } else {
              if (date('Y-m-d h:i:s', strtotime($pay['created'])) <= date('Y-m-d h:i:s', strtotime($data['created']))) {
                $totalbarChange += $pay['change'];
                $totalInitialPayment += ($pay['amount'] - $pay['change']); 

                $hotelInitialPayment += ($pay['amount'] - $pay['change']);
              } else {
                $totalChange += $pay['change'];
                $payment = $pay['amount'] - $pay['change'];
                $totalPayment += $payment;
              }

              $transactionPayments[] = array(
                'id'       => $pay['id'],
                'orNumber' => $pay['orNumber'],
                'amount'   => floatval($pay['amount']),
                'change'   => floatval($pay['change']),
                'type'     => $pay['paymentType'],
                'date'     => date('Y-m-d h:i A',strtotime($pay['created'])),
                'business'  => $transaction['Business']['name']
              );
          }         
        }
    
        // discounts
        $totalDiscount = 0;
        $transactionDiscounts = array();
        $percent = ''; $fixed = '';
        $str = '';
        $totalPercent = 0;
        $percentRate = 0;
        foreach ($transaction['TransactionDiscount'] as $dis) {
          $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $totalAmount * ($dis['value']/100);
          $totalDiscount += $discountAmount;
          if($dis['type'] == 'percent') {
            $percentRate = $dis['value']/100;
            $totalPercent += $percentRate;
          }
    
          $transactionDiscounts[] = array(
            'id'     => $dis['id'],
            'name'   => ucwords($dis['Discount']['name']),
            'value'  => $dis['value'],
            'rate'   => $percentRate,
            'type'   => $dis['type'],
            'amount' => $discountAmount
          );
          // $discountTypes .= ' '. $dis['type'];
          if($dis['type']=='percent') {
            $percent = ' ' .'percent';
          } else if($dis['type']=='fixed') {
            $fixed = ' ' .'fixed';
          }

          $str = $percent . ',' . $fixed;
        }

        $totalAmt = !strchr($transaction['particulars'],'orders')?($totalAmount + $otherTotalAmt):($totalAmount + $otherTotalAmt)-@$totalInitialPayment;
        $totalHotelAmount = strchr($transaction['Business']['name'],'Hotel')?$totalAmount :0;

        $transactions[] = array(
          'id'            => $transaction['id'],
          'code'          => $transaction['code'],
          'business'      => $transaction['Business']['name'],
          'particulars'   => properCase($transaction['particulars']),
          'deletable'     =>  !strchr($transaction['particulars'],'hotel')?false:true,
          'totalInitialPayment' => @$totalInitialPayment,
          'totalhotelPayment' => $totalhotelPayment,
          'hotelInitialPayment'=> @$hotelInitialPayment,
          'otherPayment'  => $otherTotalAmt,
          'totalChange'   => $totalChange,
          'totalAmount'   => $totalAmt,
          'totalHotelAmount'=> $totalHotelAmount,
          'totalPayment'  => $totalPayment,
          'totalDiscount' => $totalDiscount,
          'totalBalance'  => ($totalAmount + $otherTotalAmt) - ($totalPayment + @$totalInitialPayment + $totalDiscount),
          'subs'          => $transactionSubs,
          'payments'      => $transactionPayments,
          'discounts'     => $transactionDiscounts
        );
        
        $grandAmount   +=  $totalAmt;
        $chargesGrandPayment += $totalPayment;
        $grandPayment  += $totalPayment + $hotelInitialPayment;
        $grandDiscount += $totalDiscount;
        $grandChange   += $totalChange;
        $grandVat      += $otherTotalAmt; 
        $grandBalance  +=  ($totalAmount + $otherTotalAmt) - ($totalPayment + @$totalInitialPayment+ $totalDiscount);
        $grandInitialPayment += @$totalInitialPayment;
        $grandHotelCharge += $totalHotelAmount;
      }  
    }
  
    $cashiering['Transactions']['charges']        = @$transactions;
    $cashiering['Transactions']['totalAmount']    = @$grandAmount;
    $cashiering['Transactions']['totalPayment']   = @$grandPayment;
    $cashiering['Transactions']['chargesTotalPayment'] = @$chargesGrandPayment;
    $cashiering['Transactions']['totalDiscount']  = @$grandDiscount;
    $cashiering['Transactions']['totalInitialPayment']  = $grandInitialPayment;
    $cashiering['Transactions']['discountPercent'] = $totalPercent;    
    $cashiering['Transactions']['totalChange']     = $grandChange;
    // $cashiering['Transactions']['totalBalance']    = $grandAmount - ($grandPayment + $grandInitialPayment + $grandDiscount);
    $cashiering['Transactions']['totalBalance'] = $grandBalance;
    $cashiering['Transactions']['totalVat']        = $grandAmount-$grandVat;
    $cashiering['Transactions']['totalCharge']        = $grandHotelCharge;

    $received = $this->Folio->FolioSub->find('all', array(
      'contain' =>  array(
        'Folio' =>  array(
          'FolioTransaction'  =>  array(
            'Transaction' =>  array(
              'Business',
              'TransactionSub' => array(
                'conditions'=>array(
                  'visible' => true
                )
              ),
              'TransactionPayment' => array(
                'conditions' => array(
                  'visible' => true
                )
              ) ,
              'TransactionDiscount' => array(
                'conditions' => array(
                  'visible' => true
                )
              )  
            )
          )
        )
      ),
      'conditions'  =>  array(
        'FolioSub.parentId' => $id,

      )
    ));

    // received transactions
    if(!empty($received)) {
      $receivedTransactions = array();
      $tgrandAmount   = 0;
      $tgrandPayment  = 0;
      $tgrandDiscount = 0;
      $tgrandChange   = 0;
      $tgrandChildPayment = 0;
      $tgrandBarPayment = 0;
      $tgrandVat   = 0;
      $tgrandHotelCharge = 0;

    foreach ($received as $rcv) {

    // get total nights
    if ($rcv['Folio']['departure'] != date('Y-m-d')) {
      $departure = strtotime('+1day',strtotime($rcv['Folio']['departure']));
      $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime($rcv['Folio']['departure']))) > date('Y-m-d')) {
      $departure = date('m/d/Y', strtotime('+1day', strtotime($rcv['Folio']['departure'])));
      $extensions = 0;
    } 

    foreach($rcv['Folio']['FolioTransaction'] as $fTransaction) {
       
      // subs
      $tTotalAmount = 0;
      $othertTotalAmt = 0;
      $tTransactionSubs = array();  
      foreach ($fTransaction['Transaction']['TransactionSub'] as $trans) {
        
        if($trans['paid']&&$fTransaction['Transaction']['paidByParentId']) {
          // if (strrpos($trans['particulars'], 'accomodation')) {
          //   $trans['amount'] = $trans['amount']*($rcv['Folio']['nights']+@$extensions);
          // }

          // if ($transaction['particulars']=='room accomodation charges') {
          //   $tTotalAmount += $trans['amount']*((int)$rcv['Folio']['nights']+@$extensions);
          // } else if ($transaction['particulars']=='bar orders' || $transaction['particulars']=='restaurant orders') {
          //   $othertTotalAmt += $trans['amount']*$trans['quantity'];
          // } else {
          //   $tTotalAmount += $trans['amount']*$trans['quantity'];
          // }
          if ($transaction['particulars']=='room accomodation charges') {
            $tTotalAmount += $trans['amount']*((int)$rcv['Folio']['nights']+@$extensions);
          } 
          elseif (strpos($transaction['particulars'],'orders')) {
            $othertTotalAmt += $trans['amount']*$trans['quantity'];
          } 
          else {
            $tTotalAmount += $trans['amount']*$trans['quantity'];
          } 

          // $tTotalAmount += $trans['amount']*$trans['quantity'];

          $tTransactionSubs[] = array(
            'id'          => $trans['id'],
            'particulars' => properCase($trans['particulars']),
            'amount'      => floatval($trans['amount'])*$trans['quantity'],
            'paid'        => $trans['paid']
          );
        }

       if(!$trans['paid']) {

          if ($transaction['particulars']=='room accomodation charges') {
            $tTotalAmount += $trans['amount']*((int)$rcv['Folio']['nights']+@$extensions);
          } 
          elseif (strpos($transaction['particulars'],'orders')) {
            $othertTotalAmt += $trans['amount']*$trans['quantity'];
          } 
          else {
            $tTotalAmount += $trans['amount']*$trans['quantity'];
          } 

          // $tTotalAmount += $trans['amount']*$trans['quantity'];

          $tTransactionSubs[] = array(
            'id'          => $trans['id'],
            'particulars' => properCase($trans['particulars']),
            'amount'      => floatval($trans['amount'])*$trans['quantity'],
            'paid'        => $trans['paid']
          );
       }  
      }
      
      // payments
        $tTotalPayment = 0;
        $tpayment = 0;
        $tTotalChange = 0;
        $tTotalChildFolioPayment = 0;
        $tTotalChildFolioChange = 0;
        $tTotalOtherBusinessChange = 0;
        $tTotalOtherBusinessPayment = 0;
        $tTransactionPayments = array();
        foreach ($fTransaction['Transaction']['TransactionPayment'] as $tpay) {
 
         // $tpay['created'] < $fTransaction['created'] || 
          if ($tpay['created'] < $rcv['FolioSub']['created']) {
            $tTotalChildFolioChange += $tpay['change'];
            $tTotalChildFolioPayment += ($tpay['amount'] - $tpay['change']);

          } else {

            $tTotalChange += $tpay['change'];
            $tpayment = $tpay['amount'] - $tTotalChange;
            $tTotalPayment += $tpayment;
            
            $tTransactionPayments[] = array(
              'id'       => $tpay['id'],
              'orNumber' => $tpay['orNumber'],
              'amount'   => floatval($tpay['amount'])-$tpay['change'],
              'change'   => floatval($tpay['change']),
              'type'     => $tpay['paymentType'],
              'date'     => $tpay['created']
            );
          }  
        }

        // discounts
        $tTotalDiscount = 0;
        $tTotalPercent = 0;
        $tPercentRate  = 0;
        $tTransactionDiscounts = array();
        foreach ($fTransaction['Transaction']['TransactionDiscount'] as $tdis) {
          $tdiscountAmount = $tdis['type'] == 'fixed'? $tdis['value'] : $tTotalAmount * ($tdis['value']/100);
          $tTotalDiscount += $tdiscountAmount;
      
          if($tdis['type'] == 'percent') {
            $tPercentRate = $tdis['value']/100;
            $tTotalPercent += $tPercentRate;
          }
        
          $tTransactionDiscounts[] = array(
            'id'     => $tdis['id'],
            'value'  => $tdis['value'],
            'type'   => $tdis['type'],
            'amount' => $tdiscountAmount
          );
          
          if($tdis['type']=='percent') {
            $percent = ' ' .'percent';
          } else if($tdis['type']=='fixed') {
            $fixed = ' ' .'fixed';
          }
  
          $str = $percent . ',' . $fixed;
        
        }

           if(!empty($tTransactionSubs)) {   

              $totaltHotelAmount = strchr($fTransaction['Transaction']['Business']['name'],'Hotel')?$tTotalAmount - $tTotalChildFolioPayment:0;

              $receivedTransactions[] = array(
                'id'            => $fTransaction['Transaction']['id'],
                'code'          => $fTransaction['Transaction']['code'],
                'FolioSubCreated'  => $rcv['FolioSub']['created'],
                'FolioTransactionCreated' => $fTransaction['created'],
                'folioCode'     => $rcv['Folio']['code'],
                'paidByParentId'=> $fTransaction['Transaction']['paidByParentId'],
                'business'      => $fTransaction['Transaction']['Business']['name'],
                'particulars'   => properCase($fTransaction['Transaction']['particulars']),
                'TotalChildFolioChange' => $tTotalChildFolioChange,
                'TotalChildFolioPayment' => $tTotalChildFolioPayment,
                'totalAmount'   => $tTotalAmount - $tTotalChildFolioPayment,
                'totalHotelAmount'=> $totaltHotelAmount,
                'totalPayment'  => $tTotalPayment,
                'totalDiscount' => $tTotalDiscount,
                'totalBalance'  => $tTotalAmount - ($tTotalPayment + $tTotalChildFolioPayment + $tTotalDiscount), 
                'subs'          => $tTransactionSubs,
                'payments'      => $tTransactionPayments,
                'discounts'     => $tTransactionDiscounts
              );
              
      $tgrandAmount   += $tTotalAmount - $tTotalChildFolioPayment;
      $tgrandPayment  += $tTotalPayment;
      $tgrandChildPayment  += $tTotalChildFolioPayment;
      $tgrandDiscount += $tTotalDiscount;
      $tgrandChange   += $tTotalChange;
      $tgrandVat       += @$othertTotalAmt;
      $tgrandHotelCharge += $totaltHotelAmount;
       }
      }
    }  

      $cashiering['ReceivedTransactions']['charges']         = $receivedTransactions;
      $cashiering['ReceivedTransactions']['totalAmount']     = $tgrandAmount;
      $cashiering['ReceivedTransactions']['totalPayment']    = $tgrandPayment;
      $cashiering['ReceivedTransactions']['totalDiscount']   = $tgrandDiscount;
      $cashiering['ReceivedTransactions']['discountPercent'] = $tTotalPercent;  
      $cashiering['ReceivedTransactions']['totalChange']     = $tgrandChange;
      $cashiering['ReceivedTransactions']['totalInitialPayment']     = $tgrandChildPayment;
      $cashiering['ReceivedTransactions']['totalBalance']    = $tgrandAmount - ($tgrandPayment + $tgrandDiscount);    
      $cashiering['ReceivedTransactions']['grandVat']        = $tgrandAmount - $tgrandVat ;
      $cashiering['ReceivedTransactions']['totalCharge']        = $tgrandHotelCharge;

    // $receivedTransactions

    }
 
    $cashiering['discountTypes']   = $str;

      if(empty($receivedTransactions) || $receivedTransactions == null || $receivedTransactions == '') {
        $cashiering['receivedTransactions'] = false;
      } else {
        $cashiering['receivedTransactions'] = true;
      }

    $payIds = '';
    // payments
    foreach ($transactions as $t) {
      foreach ($t['payments'] as $p) {
        $payIds .= $p['id'] . ',';
      }
    }
    
    if(isset($receivedTransactions)) {
      foreach ($receivedTransactions as $r) {
        foreach ($r['payments'] as $py) {
          $payIds .= $py['id'] . ',';
        }
      }  
    } 
    
    $pay =array_map('trim', explode(',',$payIds));
    
    $cpayments = $this->TransactionPayment->find('all',array(
      'contain' =>  array(
        'Transaction' => array(
          'Business',
          'FolioTransaction'
        )
      ),
      'conditions' => array(
        'TransactionPayment.id'      => $pay,
        'TransactionPayment.visible' => true
      )
    ));
    
    // transform cpayments
    $cTotalPayment = 0;
    $cpayment = 0;
    $cTotalChange = 0;
    $cTransactionPayments = array();
   
    foreach ($cpayments as $cdata) {
      $cpay = $cdata['TransactionPayment'];

      if($cpay['visible']==true) {
         $paidInOtherBusiness = false;
        if ($cdata['Transaction']['FolioTransaction']['cleanTable']) { 
          if(date('Y-m-d h:i:s', strtotime($cpay['created'])) <= date('Y-m-d h:i:s', strtotime($cdata['Transaction']['FolioTransaction']['modified']))) {
            $paidInOtherBusiness = true;
          }  
        }
          
        $cTotalChange += $cpay['change'];
        $cpayment = $cpay['amount'] - $cTotalChange;
        $cTotalPayment += $cpayment;

        $cTransactionPayments['payments'][] = array(
          'id'       => $cpay['id'],
          'orNumber' => $cpay['orNumber'],
          'amount'   => floatval($cpay['amount']),
          'change'   => floatval($cpay['change']),
          'type'     => $cpay['paymentType'],
          'code'     => $cdata['Transaction']['code'], 
          'date'     => date('M d,2015 h:i A',strtotime($cpay['created'])),
          'business' => propercase($cdata['Transaction']['Business']['name']),
          'paidInOtherBusiness' => $paidInOtherBusiness,
          'visible'  => $cpay['visible']
        );
      }
    }
    
    // . payments
    $cashiering['cPayments'] = $cTransactionPayments;

    // checks if returning or not
    $returning = $this->Folio->find('all',array(
      'conditions' => array(
        'Folio.guestId' => $tmp['Guest']['id']
      )
    ));

    // transform folio
    $returned = false;
    if(count($returning)>1) {
      $returned = true;
    } 

    $cashiering['History'] = $returned;
    $cashiering['totalAmount'] = $cashiering['Transactions']['totalCharge'] + @$cashiering['ReceivedTransactions']['totalCharge'];
    $cashiering['taxableAmount']  = $cashiering['totalAmount']/1.12;
    $cashiering['vat'] = $cashiering['totalAmount'] - $cashiering['taxableAmount']; 

     $response = array(
      'ok'  => true,
      'data' => $cashiering,
      // 'dat' =>  $receivedTransactions,
      // 'f'=> $tmp
      ); 
   
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
    
    if(!isset($grandAmount)||!isset($tgrandAmount)) {
      $tgrandAmount = null;
    }
    return $grandAmount + $tgrandAmount;
  }
  
  public function add() {
    $this->request->data = $this->request->input('json_decode', true);
    $this->Room->create();
    if($this->Room->save($this->request->data)){
      $response = array(
        'response'=>true,
        'message'=>'Room has been saved.'
      );
    }
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function edit($id = null) {
    $this->Folio->save(array(
      'id'=>$this->request->data['folio_id'],
      'paid'=>true,
      'closed'=>true
    ));
    
    $this->Folio->ReservationRoom->save(array(
      'id'      => $this->request->data['reservation_room_id'],
      'checkedIn' => false,
      'closed'  => true,
      'checkOutDate' => date('Y-m-d H:i:s')
    ));
    
    $rooms = $this->Folio->ReservationRoom->Reservation->find('first', array(
      'contain'=>array('ReservationRoom'),
      'conditions'=>array('Reservation.id'=>$this->request->data['reservation_id'])
    ));
    $roomCount = count($rooms['ReservationRoom']);
    $roomCounter = 0;
    foreach($rooms['ReservationRoom'] as $room){
      if($room['closed']) $roomCounter+=1;
    }
    if($roomCount == $roomCounter){
      $this->Folio->ReservationRoom->Reservation->save(array(
        'id'=>$this->request->data['reservation_id'],
        'closed'=>true
      ));
    }
    
    $response = array(
      'ok'=>true,
      'message'=>'checkout.',
      'data'=>$this->request->data
    );

    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }
  
  public function available() {
    $arrival = isset($this->request->query['arrival'])? date('Y-m-d H:i:s', strtotime($this->request->query['arrival'])):date('Y-m-d 00:00:00');
    $departure = isset($this->request->query['departure'])? date('Y-m-d H:i:s', strtotime($this->request->query['departure'])):date('Y-m-d 00:00:00');
    $conditions['Room.visible'] = true;
    
    $rooms = $this->Room->find('all', array(
      'contain'=>array(
        'RoomType'=>array('name', 'rate'),
        'Accomodation'=>array('name'),
        'Folio'=>array(
          'id',
          'conditions'=>array(
            'Folio.arrival <=' =>$arrival,
            'Folio.departure >='=>$departure
          )
        ),
        'ReservationRoom'=>array(
          'id',
          'conditions'=>array(
            'ReservationRoom.arrival <=' =>$arrival,
            'ReservationRoom.departure >='=>$departure
          )
        ),
      ),
      'conditions'=>$conditions
    ));
    
    foreach($rooms as $key=>$room){
      $rooms[$key] = array(
        'id'=>$room['Room']['id'],
        'code'=>$room['Room']['code'],
        'type'=>$room['RoomType']['name'],
        'rate'=>$room['RoomType']['rate'],
        'accomodation'=>$room['Accomodation']['name']
      );
    }
    
    $response = array(
      'response'=>true,
      'message'=>'Getting data successful.',
      'arrival'=>$arrival,
      'departure'=>$departure,
      'result'=>$rooms
    );
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function api_soa($id=null) {
    $conditions['Folio.id'] = $id;

    $data = $this->Folio->find('first', array(
      'contain'=>array(
        'ReservationRoom'=>array('Reservation'),
        // 'FolioSub' => array(
        //   'Folio' => array(
        //     'FolioTransaction' => array(
        //       'Transaction' => array(
        //           'Business' => array(
        //             'conditions'  =>  array(
        //               'Business.visible' => true
        //             )
        //           ),
        //         'TransactionSub' => array(
        //           'conditions'  =>  array(
        //             'TransactionSub.visible' => true
        //           )
        //         ),
        //         'TransactionPayment' => array(
        //           'conditions'  =>  array(
        //             'TransactionPayment.visible' => true
        //           )
        //         ),
        //         'TransactionDiscount' => array(
        //           'conditions'  =>  array(
        //             'TransactionDiscount.visible' => true
        //           )
        //         )
        //       )
        //     )  
        //   )
        // ),
        'Guest'=>array('Company'),
        'Room'=>array('RoomType'),
        'FolioTransaction'=>array(
          'Transaction'=>array(
            'Business' => array(
              'conditions'  =>  array(
                'Business.visible' => true,
              )
            ), 
            'TransactionSub' => array(
              'conditions'  =>  array(
                'TransactionSub.visible' => true
              )
            ), 
            'TransactionPayment' => array(
              'conditions'  =>  array(
                'TransactionPayment.visible' => true
              )
            ),
            'TransactionDiscount' => array(
              'conditions'  =>  array(
                'TransactionDiscount.visible' => true
              )
            ),
            'conditions'  =>  array(
              'Transaction.visible' => true,
              'Transaction.businessId' => 1,
            )
          )
        )
      ),
      'conditions'=>$conditions
    ));
    
    $cashiering = array();
    $transactionAmount = 0;
    $paidAmount = 0;
    $totalDiscount = 0;
    $transactions = array();
    $payments = array();
    $totalTransactions = 0;

    // get total nights
    if ($data['Folio']['departure'] != date('Y-m-d')) {
      $departure = strtotime('+1day',strtotime($data['Folio']['departure']));
      $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
    } 

    if (date('Y-m-d', strtotime('+1day', strtotime($data['Folio']['departure']))) > date('Y-m-d')) {
      $departure = date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure'])));
      $extensions = 0;
    } 

    foreach($data['FolioTransaction'] as $transaction){

      if (!empty($transaction['Transaction'])) {
        // transaction sub
        $amount = 0;
        $items = array();
        
        foreach($transaction['Transaction']['TransactionSub'] as $sub){
          // check if accomodation
          if (strrpos($sub['particulars'],'accomodation')) {
            $sub['amount'] = $sub['amount']*($data['Folio']['nights']+@$extensions);
          }

          $items[] = array(
            'id' => $sub['id'],
            'particulars'=> $sub['particulars'],
            'amount'=>$sub['amount']*$sub['quantity']
          );

          $amount+=$sub['amount']*$sub['quantity'];
        }
        
        // folio payment
        $paid = 0;
        $payments = array();
        foreach($transaction['Transaction']['TransactionPayment'] as $sub) {
          $payments[] = array(
            'orNumber'=>$sub['orNumber'],
            'amount'=>$sub['amount'],
            'date'=>date('m/d/Y', strtotime($sub['date']))
          );
          $paid += $sub['amount']-$sub['change'];
        }
        
        $discountType = 0;
        $discountValue = 0;
        $discount = 0;
        $balance = 0; 

        foreach ($transaction['Transaction']['TransactionDiscount'] as $sub) {
          $discount = $sub['type']?($sub['value']):($amount*($sub['value']/100));
          $totalDiscount += $discount;
          $balance = (($amount-$paid) - $discount) <= 0? 0:(($amount-$paid) - $discount);

          $discountType = $sub['type'];
          $discountValue = $sub['value'];
        }    

        $transactions[] = array(
          'id'              =>  $transaction['Transaction']['id'],
          'code'            =>  $transaction['Transaction']['code'],
          'business_id'     =>  $transaction['Transaction']['businessId'],
          'discount_type'   =>  $discountType,
          'discount_value'  =>  $discountValue,
          'title'           =>  $transaction['Transaction']['particulars'],
          'items'           =>  @$items,
          'payments'        =>  @$payments,
          'amount'          =>  @$amount,
          'discount'        =>  @$discount,
          'paid'            =>  @$paid,
          'balance'         =>  @$balance
        );
        $transactionAmount += $amount;
        $paidAmount += $paid;
      }
      
    }
     
    $totalBalance = (($transactionAmount - $paidAmount) - $totalDiscount) <= 0? 0:(($transactionAmount - $paidAmount) - $totalDiscount);
    $cashiering = array(
      'id'                  =>  $data['Folio']['id'],
      'code'                =>  $data['Folio']['code'],
      'room'                =>  $data['Room']['name'],
      'type'                =>  $data['Room']['RoomType']['name'],
      'rate'                =>  $data['Room']['rate'],
      'guest'               =>  $data['Guest']['lastName'] . ', ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['middleName'],
      'company'             =>  !empty($data['Guest']['Company'])?$data['Guest']['Company']['name']:'',
      'address'             =>  $data['Guest']['address'],
      'amount'              =>  $data['Room']['rate'] * $data['Folio']['nights'],
      'arrival'             =>  date('m/d/Y', strtotime($data['Folio']['arrival'])),
      'departure'           =>  date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
      'reservation_id'      =>  @$data['ReservationRoom']['Reservation']['id'],
      'reservation_room_id' =>  @$data['ReservationRoom']['id'],
      'special_request'     =>  $data['Folio']['specialRequest'],
      'closed'              =>  $data['Folio']['closed'],
      'Transaction'         =>  array(
        'charges'           =>  @$transactions,
        'amount'            =>  $transactionAmount,
        'discount'          =>  $totalDiscount,
        'paid'              =>  $paidAmount,
        'balance'           =>  $totalBalance
      ),
      'transaction_amounts' =>  $amount,
       $totalTransactions   += count($transactions)
    );

      // received transactions
    // $receivedTransactions = array();
    // $grandTotal = 0;
    // $grandDiscount = 0;
    // $grandPaid = 0;
    // $grandBalance = 0;
    // foreach($data['FolioSub'] as $receivedTransaction) {
    //   $folio = $receivedTransaction['Folio'];
      
    //   foreach ($folio['FolioTransaction'] as $fTransaction) {
        
    //     // subs
    //     $tTotalAmount = 0;
    //     $tTransactionSubs = array();  
    //     foreach ($fTransaction['Transaction']['TransactionSub'] as $trans) {
    //       $tTotalAmount += $trans['amount'];
    
    //       $tTransactionSubs[] = array(
    //         'id'          => $trans['id'],
    //         'particulars' => properCase($trans['particulars']),
    //         'amount'      => floatval($trans['amount']),
    //       );
    //     }
        
    //     // payments
    //     $tTotalPayment = 0;
    //     $tTransactionPayments = array();
    //     foreach ($fTransaction['Transaction']['TransactionPayment'] as $pay) {
    //       $tTotalPayment += $pay['amount'];
    
    //       $tTransactionPayments[] = array(
    //         'id'       => $pay['id'],
    //         'orNumber' => $pay['orNumber'],
    //         'amount'   => floatval($pay['amount']),
    //         'type'     => $pay['paymentType'],
    //         'date'     => fdate($pay['date']),
    //       );
    //     }
        
    //     // discounts
    //     $tTotalDiscount = 0;
    //     $tTransactionDiscounts = array();
    //     foreach ($fTransaction['Transaction']['TransactionDiscount'] as $dis) {
    //       $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $tTotalAmount * ($dis['value']/100);
    //       $tTotalDiscount += $discountAmount;
    
    //       $tTransactionDiscounts[] = array(
    //         'id'     => $dis['id'],
    //         'value'  => $dis['value'],
    //         'type'   => $dis['type'],
    //         'amount' => $discountAmount
    //       );
    //     }
        
    //     $receivedTransactions[] = array(
    //       'id'            => $fTransaction['Transaction']['id'],
    //       'code'          => $fTransaction['Transaction']['code'],
    //       'folioCode'     => $folio['code'],
    //       'business'      => $fTransaction['Transaction']['Business']['name'],
    //       'particulars'   => properCase($fTransaction['Transaction']['particulars']),
    //       'totalAmount'   => $tTotalAmount,
    //       'totalPayment'  => $tTotalPayment,
    //       'totalDiscount' => $tTotalDiscount,
    //       'totalBalance'  => $tTotalAmount - ($tTotalPayment + $tTotalDiscount),
    //       'subs'          => $tTransactionSubs,
    //       'payments'      => $tTransactionPayments,
    //       'discounts'     => $tTransactionDiscounts
    //     );
        
    //     // grandTotal
    //     $totalTransactions   += count($tTransactionSubs);
    //     $grandTotal += $tTotalAmount;
    //     $grandDiscount += $tTotalDiscount;
    //     $grandPaid += $tTotalPayment;
    //     $grandBalance = $tTotalAmount - ($tTotalPayment + $tTotalDiscount);
    //   }
    // }

    // $cashiering['ReceivedTransactions']['charges'] = $receivedTransactions;
    // $cashiering['ReceivedTransactions']['amount']  = $grandTotal;
    // $cashiering['ReceivedTransactions']['discount']  = $grandDiscount;
    // $cashiering['ReceivedTransactions']['balance']  = $grandBalance;
    // $cashiering['total']['countRows'] = $totalTransactions;

    $response = array(
      'ok'=>true,
      'message'=>'Getting data successful',
      'data'=>$cashiering,
      // 'tmp' => $data
    );
    
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    )); 
 

  }

  public function api_report($id=null) {
    $date  = isset($this->request->query['date'])? date('Y-m-d', strtotime($this->request->query['date'])):date('Y-m-d');
    $edate = date('Y-m-d', strtotime('-1day',strtotime($date)));

    $conditions = array();
    $conditions['Folio.visible'] = true;
    $conditions['Folio.arrival <='] = $date;
    $conditions['Folio.departure >='] = $edate;
    
    if ($id != null) {
      $conditions['Folio.id'] = $id;   
    }

    $conditions_payments = array();
    $conditions_payments['TransactionPayment.visible'] = true;
    $conditions_payments['TransactionPayment.date'] = $date;

    if(isset($this->request->query['startTime'])) {
      $startTime = date('Y-m-d', strtotime($this->request->query['date'])) . ' ' . date('H:i',strtotime($this->request->query['startTime']));
      $endTime =  date('Y-m-d', strtotime($this->request->query['date'])) . ' ' . date('H:i',strtotime($this->request->query['endTime']));

      $conditions_payments['TransactionPayment.created >='] = $startTime;
      $conditions_payments['TransactionPayment.created <='] = $endTime;
    }

    $cashier = $this->Folio->find('all', array(
      'contain'=>array(
        'ReservationRoom'=>array('Reservation'),
        'Guest'=>array('Company'),
        'Room'=>array('RoomType'),
        'FolioTransaction'=>array(
          'Transaction'=>array(
            'Business', 
            'TransactionSub' => array('conditions' => array('TransactionSub.visible'=> true)),
            'TransactionPayment' => array(
              'conditions' => $conditions_payments 
              ),
            'TransactionDiscount' => array('Discount','conditions' => array('TransactionDiscount.visible'=> true)),           
            'conditions' => array(
              'Transaction.visible'=> true,
              'Transaction.businessId' => $this->request->query['business']           
            )
          ),
          'conditions' => array('FolioTransaction.visible'=> true)
        ),
      ),
      'conditions'=>$conditions
    ));
    
    $cashiering = array();
    $summary = array();
    $sum = '';
    $cash = 0;
    $totalCash = 0;
    $totalSendBill = 0;
    $totalCard = 0;
    $totalDebit = 0;
    foreach($cashier as $key=>$data){
      $transactionAmount = 0;
      $paidAmount = 0;
      $initialAmount = 0;
      $paymentType = '';
      $str = '';
      $transactions = array();
      $payments = array();
      
      // get total nights
      if ($data['ReservationRoom']['departure'] != date('Y-m-d')) {
        $departure = strtotime('+1day',strtotime($data['ReservationRoom']['departure']));
        $extensions = round(((strtotime(date('Y-m-d')) - $departure)/86400),0);
      } 

      if (date('Y-m-d', strtotime('+1day', strtotime($data['Folio']['departure']))) > date('Y-m-d')) {
        $departure = date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure'])));
        $extensions = 0;
      } 

      foreach($data['FolioTransaction'] as $transaction){
        
        if(!empty($transaction['Transaction'])) {
          // subs
          $amount = 0;
          $items = array();
          foreach($transaction['Transaction']['TransactionSub'] as $sub){
            $items[] = array(
              'particulars'=>$sub['particulars'],
              'amount'=>$sub['amount']
            );

            if (strrpos($sub['particulars'], 'accomodation')) {
              $sub['amount'] = $sub['amount']*($data['Folio']['nights']+@$extensions);
            }

            $amount+=$sub['amount'];
          }
          
          // discounts
          $totalDiscount = 0;
          $transactionDiscounts = array();
          $totalPercent = 0;
          $percentRate = 0;
          foreach ($transaction['Transaction']['TransactionDiscount'] as $dis) {
            $discountAmount = $dis['type'] == 'fixed'? $dis['value'] : $amount * ($dis['value']/100);
            $totalDiscount += $discountAmount;
            if($dis['type'] == 'percent') {
              $percentRate = $dis['value']/100;
              $totalPercent += $percentRate;
            }
      
            $transactionDiscounts[] = array(
              'id'     => $dis['id'],
              'name'   => ucwords($dis['Discount']['name']),
              'value'  => $dis['value'],
              'rate'   => $percentRate,
              'type'   => $dis['type'],
              'amount' => $discountAmount
            );
          }


          // payments
          $paid = 0;
          $payments = array();
          $initialPayment = 0;
          $totalChange = 0;
          $sendBill = false;
          foreach($transaction['Transaction']['TransactionPayment'] as $sub){

              $totalChange += $sub['change'];

              $payments[] = array(
                'or_number'=>$sub['orNumber'],
                'amount'=>$sub['amount'] - $sub['change'],
                'date'=>date('m/d/Y', strtotime($sub['date'])),
                'time'=>date('h:i:s A', strtotime($sub['created'])),
                'created' => $sub['created'],
                'paymentType'=>($sub['paymentType'])
              );

              if($sub['paymentType']=='send bill') {
                $str .= 'send bill';
                $sendBill = true;
              } else {
                $str .= $sub['paymentType'];                
                  $paid += ($sub['amount']-$sub['change']);
              }

              if(strpos($str,'send bill')) {
                $paymentType = 'send bill';
              } else {
                $paymentType = $sub['paymentType'];
              }

                // summary tally
                if($sub['paymentType']=='cash' || $sub['paymentType']=='cheque'||$sub['paymentType']==null) {
                  $totalCash += ($sub['amount']-$sub['change']); 
                } 
                if ($sub['paymentType']=='credit card') {
                  $totalCard += ($sub['amount']-$sub['change']);
                } 
                if ($sub['paymentType']=='send bill') {
                  $totalSendBill += ($sub['amount']-$sub['change']);
                }

                if ($sub['paymentType']=='debit') {
                  $totalDebit += ($sub['amount']-$sub['change']);
                }

          }
        }  
          
          $subTitle = !empty($transaction['Transaction']['title'])? ' ('.$transaction['Transaction']['title'].')':'';
          
          if(@$transaction['Transaction']['businessId']!=null) {

            $transactionAmount += $amount;
            $paidAmount += $paid;
            $paidAmount = $paidAmount;

            $transactions[] = array(
              'id'=>@$transaction['Transaction']['id'],
              'code'=>@$transaction['Transaction']['code'],
              'business_id'=>@$transaction['Transaction']['businessId'],
              'title'=>@$transaction['Transaction']['Business']['name'] . $subTitle,
              'items'=>$items,
              'payments'=>$payments,
              'amount'=>$transactionAmount,
              'discount'=>$totalDiscount,
              'paid'=>$paid,
              'balance'=>($transactionAmount-$paid)-($totalDiscount-$totalChange)
            );
          }

      }
       
      if ($data['ReservationRoom']['checkedIn']==true) {
        $status = 'IN-HOUSE';
      } 
      if ($data['ReservationRoom']['checkOutDate']!=null) {
        $status = 'CHECK-OUT';
      } 

      if($paidAmount!=0 || @$sendBill) {
        $cashiering[] = array(
          'id'=>$data['Folio']['id'],
          'code'=>$data['Folio']['code'],
          'room'=>$data['Room']['name'],
          'type'=>$data['Room']['RoomType']['name'],
          'rate'=>$data['Room']['rate']*($data['Folio']['nights']+@$extensions),
          'guest'=>ucwords($data['Guest']['lastName'] . ', ' . $data['Guest']['firstName'] . ' ' . $data['Guest']['middleName']),
          'company'=>!empty($data['Guest']['Company'])?$data['Guest']['Company']['name']:'',
          'address'=>$data['Guest']['address'],
          'amount'=>(($data['Room']['rate'] )*($data['Folio']['nights']+@$extensions)),
          'arrival'=>date('m/d/Y', strtotime($data['Folio']['arrival'])),
          'departure'=>date('m/d/Y', strtotime('+1day', strtotime($data['Folio']['departure']))),
          'reservation_id'=>$data['ReservationRoom']['reservationId'],
          'reservation_room_id'=>$data['ReservationRoom']['id'],
          'status' => $status,
          'closed'=>$data['Folio']['closed'],
          'transferedBills' => $data['Folio']['transferedBills'],
          'Transaction'=>array(
            'charges'=>$transactions,
            'amount'=>$transactionAmount,
            'paid'=> $paidAmount,
            'discount'=> $totalDiscount,
            'balance'=> ($transactionAmount - $paidAmount  - $totalDiscount),
            'paymentType'=>$paymentType
          )
        );    

      $summary = array(
        'TotalCash'     =>  $totalCash,
        'TotalSendBill' =>  $totalSendBill,
        'TotalCard'     =>  $totalCard,
        'TotalDebit'    =>  $totalDebit,
        'Total'         =>  $totalCash + $totalSendBill + $totalCard  + $totalDebit
      );      
      }

    }

    $response = array(
      'ok'=>true,
      'date' => $date,
      'message'=>'Getting data successful.',
      'result'=>$cashiering,
      'summary'=> $summary,
      'data' =>$cashier,
      // 'received' => $received,
      'startTime' =>@$startTime,
      'endTime' =>  @$endTime
    );
    
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

}
