<?php
class TablesController extends AppController {

  public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }

  public function index() {
    // default page 1
    $page = isset($this->request->query['page'])? $this->request->query['page'] : 1;

    // default conditions
    $conditions = array();
    $conditions['Table.visible'] = true;

    // department code condition
    if (isset($this->request->query['code']))
      $conditions['Department.code LIKE'] = $this->request->query['code'];
 
    // search conditions
    if (isset($this->request->query['search'])) {
      $search = $this->request->query['search'];
      $conditions['OR'] = array(
        'Table.name LIKE'        => "%$search%",
        'Table.description LIKE' => "%$search%",
      );
    }

    // paginate data
    $paginatorSettings = array(
      'contain' => array(
        'Department'
      ),
      'conditions' => $conditions,
      'limit'      => 25,
      'page'       => $page,
      'order'      => array(
        'Table.name' => 'ASC'
      )
    );
    $modelName = 'Table';
    $this->Paginator->settings = $paginatorSettings;
    $tmpData     = $this->Paginator->paginate($modelName);
    $paginator = $this->request->params['paging'][$modelName];

    // transform data
    $tables = array();
    foreach ($tmpData as $data) {
      $table   = $data['Table'];
      $department = $data['Department'];

      $tables[] = array(
        'id'          => $table['id'],
        'code'        => $table['code'],
        'name'        => $table['name'],
        'description' => $table['description'],
        'department'  => $department['name'],
      );
    }

    $response = array(
      'ok'         => true,
      'data'       => $tables,
      'paginator'  => $paginator,
    );
    
    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }

  public function view($id = null) {
    $data = $this->Table->findById($id);
    $response = array(
      'response'=>true,
      'result'=>$data
    );
    
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }
  
  
  public function add() {
    $this->request->data['Table']['departmentId'] = $this->Table->Department->get($this->request->data['Table']['departmentId']);
    $save = $this->Table->validSave($this->request->data['Table']);
    $response = $save;

     $this->set(array(
      'response'   => $response,
       '_serialize' => 'response'
     ));
  }

  public function edit($id = null) {
    $this->request->data['Table']['departmentId'] = $this->Table->Department->get($this->request->data['Table']['departmentId']);
    $save = $this->Table->validSave($this->request->data['Table']);
    $response = $save;

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
  
  public function delete($id = null) {
    if ($this->Table->hide($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
        'msg'  => 'Table has been deleted.',
      );

    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
        'msg'  => 'Table cannot be deleted this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response'
    ));
  }
}
