<?php

class UserPermissionsController extends AppController {
	
	public $layout = null;

  public function beforeFilter() {
    parent::beforeFilter();
    $this->RequestHandler->ext = 'json';
  }


  public function add() {
		$this->UserPermission->create();
		if ($this->UserPermission->saveMany($this->request->data['UserPermission'])) {
			$response = array(
        'ok'   => true,
        'msg'  => 'Permission has been added',
      );
		} else {
      $response = array(
        'ok'   => false,
        'data' => $this->request->data,
        'msg'  => 'Permission cannot be added this time.',
      );
    }

		$this->set(array(
			'response'   => $response,
			'_serialize' => 'response',
		));
  }

  public function delete($id = null) {
    if ($this->UserPermission->delete($id)) {
      $response = array(
        'ok'   => true,
        'data' => $id,
        'msg'  => 'Permission has been deleted.',
      );
    } else {
      $response = array(
        'ok'   => false,
        'data' => $id,
        'msg'  => 'Permission cannot be delete this time.',
      );
    }

    $this->set(array(
      'response'   => $response,
      '_serialize' => 'response',
    ));
  }
}
