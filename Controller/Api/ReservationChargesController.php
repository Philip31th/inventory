<?php 
class ReservationChargesController extends AppController {
	
	public $uses = array('Folio');
	public $layout = null;
	public $components = array('Paginator', 'RequestHandler');
	
  public function beforeFilter() {
      parent::beforeFilter();
      $this->RequestHandler->ext = 'json';
  }

	public function index() {
    
	}

  public function view($id = null){

  }

  public function add(){

  }

  public function edit($id = null){
    $this->request->data = $this->request->input('json_decode', true);
    $folio = $this->Folio->find('first', array('conditions'=>array('Folio.id'=>$id)));
    $data = json_decode($folio['Folio']['data'], true);
    
    if(!array_key_exists('OtherCharges', $data)){
      $data['OtherCharges'] = array();
    }
    
    $data['OtherCharges'][] = $this->request->data['OtherCharges'];
    
    $this->Folio->save(array(
      'id'=>$id,
      'data'=>json_encode($data)
    ));
    
    $response = array(
      'response'=>true,
      'message'=>'Data has been saved.',
      'data'=>$data
    );
   
    $this->set(array(
      'response'=>$response,
      '_serialize'=>'response'
    ));
  }

  public function delete($id = null){

  }

}
