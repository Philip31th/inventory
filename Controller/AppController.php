<?php
class AppController extends Controller {
  
  public $layout = null;
  
  public $components = array(
    'Session',
    'Auth' => array(
      'loginAction' => array(
        'controller' => 'main',
        'action'     => 'login'
      ),
      'logoutRedirect' => array(
        'controller' => 'main',
        'action'     => 'login'
      ),
      'authenticate' => array(
        'Form' => array(
          'scope' => array(
            'User.activated' => true,
            'User.visible'   => true
          )
        ),
      )
    ),
    'Paginator',
    'RequestHandler',
    'Thumbnail',
  );

  public $helpers = array('Application');
  public $uses = array('User', 'Setting');
  
  public function beforeFilter () {

    if ($this->Session->check('Auth.User.id')) {
    // current user
    $currentUser = $this->User->find('first', array(
      'contain' => array(
        'UserPermission' => array(
          'Permission'
        )
      ),
      'conditions' => array(
        'User.id' => $this->Session->read('Auth.User.id')
      )
    ));

    // transform user permission
    foreach ($currentUser['UserPermission'] as $k => $permission) {
      $currentUser['UserPermission'][$k] = $permission['Permission']['module'] . '/' . $permission['Permission']['controller'] . '/' . $permission['Permission']['action'];
    }

    $this->set(compact('currentUser'));
    }

  }

  public function serverUrl() {
    // check secure connection
    $secureConnection = false;
    if(isset($_SERVER['SERVER_PORT']))
      if ($_SERVER['SERVER_PORT'] == 443)
        $secureConnection = true;
    
    $secureConnection = $secureConnection? 'https':'http';
    $server = $secureConnection. '://'. $_SERVER['SERVER_NAME'].$this->base . '/';

    return $server;
  }

  public function superUser() {
    $role = strtolower($this->Session->read('Auth.User.role'));
    $result = $role == 'superuser'? true : false;
    return $result;
  }
  
}
