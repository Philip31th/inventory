<?php
class MainController extends AppController {
	
	public $uses = array(
    'User',
    'Reservation',
    'Setting',
    'UserLog',
    'TableTransaction'
  );

  public $layout = null;
	public $components = array('RequestHandler');
    

  public function index() {
    $base = $this->serverUrl();
    $api  = $this->serverUrl() . 'api/';
    $tmp  = $this->serverUrl() . 'template/';

    $this->set(compact(
      'base',
      'api',
      'tmp'
    ));
  }

	public function login() {
    $this->User->save(array(
      'id'       => 1,
      'password' => 'password',
    ));

		$ehrms = $this->Setting->get('ehrms');
		$systemTitle = $this->Setting->get('system_title');
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
			  $this->User->save(array(
                'id'         => $this->Session->read('Auth.User.id'),
                'last_login' => date('Y-m-d H:i:s'),
              ));
        
        $this->UserLog->save(array(
          'userId'    =>  $this->Session->read('Auth.User.id'),
          'action'    =>  'Log In',
          'description'=> 'logged in',
          ));

				return $this->redirect(array(
          'controller' => 'main',
          'action'     =>'index',
        ));
			}
		} else {
			if ($this->Auth->loggedIn()) {
        $this->UserLog->save(array(
          'userId'    =>  $this->Session->read('Auth.User.id'),
          'action'    =>  'Log In',
          'description'=> 'logged in',
          ));
        return $this->redirect(array(
          'controller' => 'main',
          'action'     => 'index',
        ));

			}
		}
    $this->set(compact('ehrms', 'systemTitle'));
	}

	public function logout() {
    $this->UserLog->save(array(
      'userId'    =>  $this->Session->read('Auth.User.id'),
      'action'    =>  'Log Out',
      'description'=> 'logged out',
    ));

		$this->Session->destroy();
		$this->Session->delete('Auth');
		return $this->redirect($this->Auth->logout());	
	}

  public function reset($code = null) {
    $this->autoRender = false;

    if (!empty($code)) {
      if ($code = 'bookings' or $code == 'booking') {
        $this->Reservation->truncate();
        $this->Reservation->ReservationRoom->truncate();
        $this->Reservation->ReservationRoom->Folio->truncate();
        $this->Reservation->ReservationRoom->Folio->Guest->truncate();
        $this->Reservation->ReservationRoom->Folio->FolioSub->truncate();
        $this->Reservation->ReservationRoom->Folio->FolioTransaction->truncate();
        $this->Reservation->ReservationRoom->Folio->FolioTransaction->Transaction->truncate();
        $this->Reservation->ReservationRoom->Folio->FolioTransaction->Transaction->TransactionSub->truncate();
        $this->Reservation->ReservationRoom->Folio->FolioTransaction->Transaction->TransactionPayment->truncate();
        $this->Reservation->ReservationRoom->Folio->FolioTransaction->Transaction->TransactionDiscount->truncate();
        rrmdir('uploads/guests');
      }
    }
  }
  
  public function resetlogs() {
   $this->autoRender = false;
    $this->UserLog->truncate();
  }

  public function resettableTransactions() {
   $this->autoRender = false;
    $this->TableTransaction->truncate();
  }
}