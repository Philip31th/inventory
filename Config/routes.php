<?php

	Router::connect('/', array(
		'controller' => 'main',
		'action' 		 => 'index'
	));

	Router::connect('/login', array(
		'controller' => 'main',
		'action'     => 'login'
	));

	Router::connect('/lock', array(
		'controller' => 'main',
		'action'     => 'lock'
	));

	Router::connect('/logout', array(
		'controller' => 'main',
		'action' 		 => 'logout'
	));

	// API RESOURCES
	$resources = array(

		// dashboard
		'dashboard',

		// admin
		'users',

		// extra
		'select',
		'settings',

	);
	
	Router::mapResources($resources, array('prefix' => 'api'));
	Router::parseExtensions('json');
	
	CakePlugin::routes();
	require CAKE . 'Config' . DS . 'routes.php';

