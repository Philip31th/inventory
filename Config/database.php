<?php
class DATABASE_CONFIG {
  public $default = array(
    'datasource' => 'Database/Mysql',
    'persistent' => false,
    'host'     => '127.0.0.1',
    'login'    => 'edncsolutionsph',
    'password' => '',
    'database' => 'ehrms',
    'encoding' => 'UTF8'
  );
}