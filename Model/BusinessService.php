<?php
class BusinessService extends AppModel {

  public $recursive = -1;
  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'Business' => array(
      'foreignKey' => 'businessId'
    )
  );
  
  public function validSave($data) {
    $result = array();

    // transform data
    $data['code'] = slug(@$data['name']);
    $data['name'] = properCase(@$data['name']);
    $data['description'] = strtolower(@$data['description']);

    // validate business id
    if (validate(@$data['businessId'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Business ID is required.'
      );

    // validate description
    } elseif (validate(@$data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Service name is required.'
      );

    // validate description
    } elseif (validate(@$data['description'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Service description is required.'
      );

    // validate amount
    } elseif (validate(@$data['value'], 'numeric')) {
      $result = array(
        'ok'  => false,
        'msg' => 'Enter a valid service amount.'
      );
    } else {

      // check if existing
      $existingConditions = array();
      $existingConditions['code LIKE']  = $data['code'];
      $existingConditions['businessId'] = $data['businessId'];
      $existingConditions['visible'] = true;

      if (isset($data['id']))
        $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Business service already exists.'
        );
      // .check if existing
      } else {

        // save data
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Business service has been saved.'
          );
        }
      }
    }

    return $result;
  }
}
