<?php 
class RoomType extends AppModel {

  public $recursive = -1;
  public $actsAs = array('Containable');
  
  public $hasMany = array(
    'Room' => array(
      'foreignKey' => 'roomTypeId'
    )
  );

  public $belongsTo = array(
    'Accomodation' => array(
      'foreignKey' => 'accomodationId'
    )
  );
  
  public function validSave($data) {
    $result = array();

    // transform data
    $data['code'] = slug(@$data['name']);
    $data['name'] = properCase(@$data['name']);

    // validate accomodation id
    if (validate(@$data['accomodationId'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Room type accomodation is required.'
      );

    // validate name
    } elseif (validate(@$data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Room type name is required.'
      );
    } else {

      $existingConditions = array();
      $existingConditions['name LIKE'] = $data['name'];
      $existingConditions['visible'] = true;
      
      if (isset($data['id']))
        $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Room type already exists.'
        );
      } else {
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Room type has been saved.'
          );
        }
      }
    }

    return $result;
  }
}
