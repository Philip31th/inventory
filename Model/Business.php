<?php 
class Business extends AppModel {
    
	public $actsAs = array('Containable');

  public $belongsTo = array(
    'Department' => array(
      'foreignKey' => 'departmentId'
    )
  );

	public $hasMany = array(
    'Transaction' => array(
      'foreignKey' => 'businessId'
    ),
    'BusinessService' => array(
      'foreignKey' => 'businessId',
      'conditions' => array(
        'BusinessService.visible' => true
      ),
      'order' => array(
        'BusinessService.name' => 'ASC'
      )
    )
	);

  public function validSave($data) {
    $result = array();

    $existingConditions = array();
    $existingConditions['name LIKE'] = $data['name'];
    
    if (isset($data['id']))
      $existingConditions['id !='] = $data['id'];

    $existing = $this->existing($existingConditions);

    if ($existing) {
      $result = array(
        'ok'  => false,
        'msg' => 'Business already exists.'
      );
    } else {
      $data['code'] = slug($data['name']);
      $data['name'] = properCase($data['name']);
      if ($this->save($data)) {
        $result = array(
          'ok'  => true,
          'msg' => 'Business has been saved.'
        );
      }
    }
    return $result;
  }

   public function get($code = null) {
    $id = null;
    $data = $this->find('first', array(
      'conditions' => array(
        'Business.code LIKE' => $code
      )
    ));

    $id = !empty($data)? $data['Business']['id'] : null;

    return $id;
  }


}
