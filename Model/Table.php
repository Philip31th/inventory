<?php 
class Table extends AppModel {

  public $recursive = -1;
  public $actsAs = array('Containable');
  
	public $belongsTo = array(
    'Department'    => array(
      'foreignKey' => 'departmentId'
    ),
  );
  
  public $hasMany = array(
    'TableTransaction'  =>  array(
      'foreignKey'=> 'tableId'  
    )
  );
  
  public function validSave($data) {
    $result = array();

    // transform data
    $data['code'] = slug(@$data['name']);
    $data['name'] = properCase(@$data['name']);
    $data['description'] = strtolower(@$data['description']);

    // validate department id
    if (validate(@$data['departmentId'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Department ID is required.'
      );

    // validate name
    } elseif (validate(@$data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Table name is required.'
      );

    }  else {

      // check if existing
      $existingConditions = array();
      $existingConditions['code LIKE']  = $data['code'];
      $existingConditions['departmentId'] = $data['departmentId'];
      $existingConditions['visible'] = true;

      if (isset($data['id']))
        $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Table already exists.'
        );
      // .check if existing
      } else {

        // save data
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Table has been saved.'
          );
        }
      }
    }

    return $result;
  }
}
