<?php 
class Guest extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'Folio' => array(
			'foreignKey' => 'guestId'
		)
	);

  public $belongsTo = array(
    'Company' => array(
      'foreignKey' => 'companyId'
    ),
    'VehicleType' =>  array(
      'foreignKey'  =>  'vehicleTypeId'  
    )
  );

}
