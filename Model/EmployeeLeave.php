<?php
class EmployeeLeave extends AppModel {
 
  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Employee' => array(
      'foreignKey' => 'employeeId'
    )
  );
}