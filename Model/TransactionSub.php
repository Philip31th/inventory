<?php 
class TransactionSub extends AppModel {
    
	public $actsAs = array('Containable');
	
	public $belongsTo = array(
    'Transaction' => array(
      'foreignKey' => 'transactionId'
    )
  );

  public $hasMany = array(
    'TransactionPayment'  => array(
      'foreignKey'  =>  'subId'
    )
  );

  public function get($transactionId = null) {
    $data = $this->find('all', array(
      'conditions' => array(
        'TransactionSub.transactionId' => $transactionId,
        'TransactionSub.visible'  =>  true
      )
    ));
     $amt = 0;
    foreach ($data as $i => $value) {
      $amt += $data[$i]['TransactionSub']['amount']*$data[$i]['TransactionSub']['quantity'];
    }

    return $amt;
  }

  public function getSub($transactionId = null) {
    $data = $this->find('first', array(
      'conditions' => array(
        'TransactionSub.transactionId' => $transactionId,
        'TransactionSub.visible'  =>  true
      )
    ));

    $id = !empty($data)? $data['TransactionSub']['id'] : null;

    return $id;
  }

}
