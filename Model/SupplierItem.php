<?php 
class SupplierItem extends AppModel {

  public $recursive = -1;
  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Supplier' => array(
      'foreignKey' => 'supplierId'
    )
  );

}
