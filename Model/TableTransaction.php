<?php
class TableTransaction extends AppModel {
  
  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'Table' => array(
      'foreignKey' => 'tableId'
    ),
     'Transaction' => array(
      'foreignKey' => 'transactionId'
    )
  );

}