<?php 
class ReservationRoom extends AppModel {

	public $recursive = -1;
	public $actsAs = array('Containable');
	
	public $belongsTo = array(
		'Reservation' => array(
      'foreignKey' => 'reservationId'
    ),
		'Room' => array(
      'foreignKey' => 'roomId'
    )
	);
	
	public $hasOne = array(
		'Folio' => array(
      'foreignKey' => 'reservationRoomId'
    )
	);

}
