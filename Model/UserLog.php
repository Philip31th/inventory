<?php
class UserLog extends AppModel {

  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'User' => array(
      'foreignKey' => 'userId'
    )
  );

}
