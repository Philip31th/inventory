<?php 
class MenuIngredient extends AppModel {
    
  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Inventory' => array(
      'foreignKey' => 'inventoryId'
    ),
    'Menu' => array(
      'foreignKey' => 'menuId'
    )
  );
  
}  