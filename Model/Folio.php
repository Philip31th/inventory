<?php 
class Folio extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Guest' => array(
      'foreignKey' => 'guestId'
    ),
		'ReservationRoom' => array(
      'foreignKey' => 'reservationRoomId'
    ),
    'Room' => array(
      'foreignKey' => 'roomId'
    )
    );

  public $hasMany = array(
    'FolioTransaction' => array(
      'foreignKey' => 'folioId'
    ),
    'TransactionPayment' => array(
      'foreignKey' => 'transactionId'
    ),
    'FolioSub'  => array(
      'foreignKey' => 'parentId'
    )
  );
  
}
