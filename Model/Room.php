<?php 
class Room extends AppModel {

	public $actsAs = array('Containable');
	
	public $belongsTo = array(
		'RoomType' => array(
			'foreignKey' => 'roomTypeId'
		),
		'Accomodation' => array(
			'foreignKey' => 'accomodationId'
		)
	);

	public $hasMany = array(
		'Folio' => array(
      'foreignKey' => 'roomId'
    ),
		'ReservationRoom' => array(
      'foreignKey' => 'roomId'
    )
	);
	
  public function validSave($data) {
    $result = array();

    // transform data
    $data['code'] = slug(@$data['name']);
    $data['name'] = properCase(@$data['name']);
    $data['description'] = strtolower(@$data['description']);

    // validate data
    if (validate(@$data['roomTypeId'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Room type ID is required.'
      );

    // validate name
    } elseif (validate(@$data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Room number is required.'
      );
    } else {

	    $existingConditions = array();
	    $existingConditions['name LIKE'] = $data['name'];
	    $existingConditions['visible'] = true;
	    
	    if (isset($data['id']))
	      $existingConditions['id !='] = $data['id'];

	    $existing = $this->existing($existingConditions);

	    if ($existing) {
	      $result = array(
	        'ok'  => false,
	        'msg' => 'Room already exists.',
	      );
	    } else {
	      if ($this->save($data)) {
	        $result = array(
	          'ok'  => true,
	          'msg' => 'Room has been saved.'
	        );
	      }
	    }
    }

    return $result;
  }
}
