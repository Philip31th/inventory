<?php 
class Department extends AppModel {

  public $actsAs = array('Containable');
  
  public $hasMany = array(
    'Business' => array(
      'foreignKey' => 'departmentId'
    ),
    'Supplier' => array(
      'foreignKey' => 'departmentId'
    ),
    'Menu' => array(
      'foreignKey' => 'departmentId'
    ),
    'Table' => array(
      'foreignKey' => 'departmentId'
    ),
    'Employee'  =>  array(
      'foreignkey'  =>  'departmentId'  
    )
  );
  
  public function get($code = null) {
    $id = null;
    $data = $this->find('first', array(
      'conditions' => array(
        'Department.code LIKE' => $code
      )
    ));

    $id = !empty($data)? $data['Department']['id'] : null;

    return $id;
  }
  
   public function getch($id = null) {
    $data = $this->find('first', array(
      'conditions' => array(
        'Department.id' => $id
      )
    ));

    $name = $data['Department']['name'];

    return $name;
  }

}
