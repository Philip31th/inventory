<?php
class Task extends AppModel {

  public $recursive = -1;
  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'User' => array(
      'foreignKey' => 'createdBy'
    ),
    'Developer' => array(
      'foreignKey' => 'assignedTo'
    )
  );
}
