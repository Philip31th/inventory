<?php
class UserPermission extends AppModel {

	public $recursive = -1;
	public $actsAs = array('Containable');
	
	public $belongsTo = array(
    'User' => array(
      'foreignKey' => 'userId'
    ),
    'Permission' => array(
      'foreignKey' => 'permissionId'
    )
  );
}
