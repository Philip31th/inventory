<?php
class User extends AppModel {

	public $recursive = -1;
	public $actsAs = array('Containable');
	
	public $belongsTo = array(
		'Employee' => array(
			'foreignKey' => 'employeeId'
		)
	);

	public $hasMany = array(
		'UserLog' => array(
			'foreignKey' => 'userId'
		),
		'UserPermission' => array(
			'foreignKey' => 'userId'
		)
	);
	
	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}

  public function validSave($data) {
    $result = array();

    // validate username
    if (validate(@$data['username'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Username is required.'
      );

    } else {
	    $existingConditions = array();
	    $existingConditions['username LIKE'] = $data['username'];
	    $existingConditions['visible'] = true;

	    if (isset($data['id']))
	      $existingConditions['id !='] = $data['id'];

	    $existing = $this->existing($existingConditions);

	    if ($existing) {
	      $result = array(
	        'ok'  => false,
	        'msg' => 'User account already exists.'
	      );
	    } else {
	      if ($this->save($data)) {
	        $result = array(
	          'ok'  => true,
	          'msg' => 'User account has been saved.'
	        );
	      }
	    }
    }

    return $result;
  }
}
