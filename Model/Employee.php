<?php
class Employee extends AppModel {
 
  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Department' => array(
      'foreignKey' => 'departmentId'
    )
  );

  public $hasMany = array(
    'User' => array(
      'foreignKey' => 'employeeId',
    'conditions' => array(
        'User.visible' => true
      )
    ),
    'DailyTimeRecord'  =>  array(
      'foreignKey' => 'employeeId'
    ),
    'EmployeePerformance'  =>  array(
      'foreignKey' => 'employeeId'
    ),
    'EmployeeLeave'  =>  array(
      'foreignKey' => 'employeeId'
    )
  );

  public function validSave($data) {
    $result = array();

    // transform data
    // $data['code']        = slug(@$data['name']);
    $data['lastName']        = properCase(@$data['lastName']);
    $data['firtName']        = properCase(@$data['firstName']);


    // validate name
    if (validate($data['lastName'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Employee name is required.'
      );

    } elseif (validate($data['firstName'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Employee name is required.'
      );

    } else {
      $existingConditions = array();
      $existingConditions['name LIKE'] = $data['name'];
      $existingConditions['departmentId'] = @$data['departmentId'];
      $existingConditions['visible']   = true;

      if (isset($data['id']))
      $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Employee already exists.'
        );
      } else {
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Employee has been saved.'
          );
        }
      }

    }
    return $result;
  }
  
}
