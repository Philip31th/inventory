<?php 
class Discount extends AppModel {

	public $actsAs = array('Containable');
	
	public $hasMany = array(
		'TransactionDiscount' => array(
			'foreignKey' => 'discountId'
		)
	);
	 
}
