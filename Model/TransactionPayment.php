<?php 
class TransactionPayment extends AppModel {
    
  public $actsAs = array('Containable');
  
  public $hasMany = array(
    'SupplierPayment'  =>  array(
      'foreignKey'  =>  'paymentId'
    )
  );

  public $belongsTo = array(
    'Transaction' => array(
      'foreignKey' => 'transactionId'
    )
  );

}
