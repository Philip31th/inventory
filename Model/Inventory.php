<?php 
class Inventory extends AppModel {

  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Department' => array(
      'foreignKey' => 'departmentId'
    )
  );

  public $hasMany = array(
    'InventorySub' => array(
      'foreignKey' => 'inventoryId'
    ),
     'MenuIngredient' => array(
      'foreignKey' => 'inventoryId'
    )
  );

   public function validSave($data) {
    $result = array();

    // transform data
    $data['code']        = slug(@$data['name']);
    $data['name']        = properCase(@$data['name']);
    $data['description'] = strtolower(@$data['description']);

    // validate name
    if (validate($data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Inventory name is required.'
      );

    } else {
      $existingConditions = array();
      $existingConditions['name LIKE'] = $data['name'];
      $existingConditions['departmentId'] = $data['departmentId'];
      $existingConditions['visible']   = true;

      if (isset($data['id']))
      $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Inventory already exists.'
        );
      } else {
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Inventory has been saved.'
          );
        }
      }

    }
    return $result;
  }
}
