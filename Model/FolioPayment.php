<?php 
class FolioPayment extends AppModel {

  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'Folio' => array(
      'foreignKey' => 'folioId'
    ),
    'Transaction' => array(
      'foreignKey' => 'transactionId'
    )
  );
  
}