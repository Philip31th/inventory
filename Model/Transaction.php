<?php 
class Transaction extends AppModel {
    
  public $actsAs = array('Containable');
  
  public $hasMany = array(
    'TransactionSub' => array(
      'foreignKey' => 'transactionId'
    ),
    'TransactionPayment' => array(
      'foreignKey' => 'transactionId'
    ),
    'TransactionDiscount' => array(
      'foreignKey' => 'transactionId'
    ),
    'TableTransaction'  =>  array(
      'foreignKey'  =>  'transactionId'  
    )
  );
  
  public $belongsTo = array(
    'Business' => array(
      'foreignKey' => 'businessId'
    )
  );
  
  public $hasOne = array(
    'FolioTransaction' => array(
      'foreignKey' => 'transactionId'
    )
  );

}
