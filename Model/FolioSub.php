<?php 
class FolioSub extends AppModel {

  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'Folio' => array(
      'foreignKey' => 'parentId'
    ),
    'Folio' => array(
      'foreignKey' => 'folioId',
    )
  );

  
}