<?php 
class Company extends AppModel {

	public $actsAs = array('Containable');
	public $hasOne = array(
		// 'Customer'
	);

	public $hasMany = array(
		'Guest' => array(
  		'foreignKey' => 'companyId'
	),
    'Reservation' => array(
      'foreignKey' => 'companyId'
    )
	);	

  public function get($code = null) {
    $id = null;
    $data = $this->find('first', array(
      'conditions' => array(
        'Company.name' => $code
      )
    ));

    $id = !empty($data)? $data['Company']['id'] : null;

    return $id;
  }

}
