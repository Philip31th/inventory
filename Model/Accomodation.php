<?php 
class Accomodation extends AppModel {

	public $recursive = -1;
	public $actsAs = array('Containable');
	
	public $hasMany = array(
		'Room' => array(
			'foreignKey' => 'accomodationId'
		),
		'RoomType' => array(
			'foreignKey' => 'accomodationId'
		)
	);

  public function validSave($data) {
    $result = array();

    // transform data
    $data['code'] = slug(@$data['name']);
    $data['name'] = properCase(@$data['name']);

    // validate name
    if (validate(@$data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Accomodation name is required.'
      );

    // validate number
    } elseif (validate(@$data['number'], ' numberic')) {
      $result = array(
        'ok'  => false,
        'msg' => 'Accomodation number is required.'
      );

    } else {

	    $existingConditions = array();
	    $existingConditions['name LIKE'] = $data['name'];
	    $existingConditions['visible'] = true;
	    
	    if (isset($data['id']))
	      $existingConditions['id !='] = $data['id'];

	    $existing = $this->existing($existingConditions);

	    if ($existing) {
	      $result = array(
	        'ok'  => false,
	        'msg' => 'Accomodation already exists.'
	      );
	    } else {
	      if ($this->save($data)) {
	        $result = array(
	          'ok'  => true,
	          'msg' => 'Accomodation has been saved.'
	        );
	      }
	    }
    }

    return $result;
  }
  
}
