<?php 
class Menu extends AppModel {
    
  public $recursive = -1;
  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Department' => array(
      'foreignKey' => 'departmentId'
    ),
  );

  public $hasMany = array(
    'MenuIngredient' => array(
      'foreignKey' => 'menuId'
    ),
  );
  
  public function validSave($data) {
    $result = array();

    // transform data
    $data['code'] = slug(@$data['name']);
    $data['name'] = properCase(@$data['name']);
    $data['description'] = strtolower(@$data['description']);

    // validate department id
    if (validate(@$data['departmentId'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Department ID is required.'
      );

    // validate name
    } elseif (validate(@$data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Menu name is required.'
      );

    // validate amount
    } elseif (validate(@$data['amount'], 'numeric')) {
      $result = array(
        'ok'  => false,
        'msg' => 'Enter a valid menu amount.'
      );
    } else {

      // check if existing
      $existingConditions = array();
      $existingConditions['code LIKE']  = $data['code'];
      $existingConditions['departmentId'] = $data['departmentId'];
      $existingConditions['visible'] = true;

      if (isset($data['id']))
        $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Menu already exists.'
        );
      // .check if existing
      } else {

        // save data
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Menu has been saved.'
          );
        }
      }
    }

    return $result;
  }

  public function visible($id = null, $value = true){
    $result = false; $this->id = $id;
    if ($this->save(array('visible'=>$value))){ $result = true; } else { $result = false; }
    return $result;
  }

}
