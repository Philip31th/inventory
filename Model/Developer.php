<?php
class Developer extends AppModel {

  public $useTable = 'users';
  public $recursive = -1;
  public $actsAs = array('Containable');

  public $hasMany = array(
    'Task' => array(
      'foreignKey' => 'assignedTo'
    )
  );

}
