<?php
class EmployeePerformance extends AppModel {

  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'Employee'  =>  array(
      'foreignKey' => 'employeeId'
    )
  );
   
}