<?php 
class Supplier extends AppModel {
  
  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'Department' => array(
      'foreignKey' => 'departmentId'
    )
  );

  public $hasMany = array(
    'SupplierItem' => array(
      'foreignKey' => 'supplierId'
    ),
    'SupplierPayment'  =>  array(
      'foreignKey'  =>  'supplierId'
    )
  );

     public function validSave($data) {
    $result = array();

    // transform data
    $data['name']        = properCase(@$data['name']);

    // validate name
    if (validate($data['name'])) {
      $result = array(
        'ok'  => false,
        'msg' => 'Supplier name is required.'
      );

    } else {
      $existingConditions = array();
      $existingConditions['name LIKE'] = $data['name'];
      $existingConditions['visible']   = true;

      if (isset($data['id']))
      $existingConditions['id !='] = $data['id'];

      $existing = $this->existing($existingConditions);

      if ($existing) {
        $result = array(
          'ok'  => false,
          'msg' => 'Supplier already exists.'
        );
      } else {
        if ($this->save($data)) {
          $result = array(
            'ok'  => true,
            'msg' => 'Supplier has been saved.'
          );
        }
      }

    }
    return $result;
  }

}
