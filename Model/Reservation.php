<?php 
class Reservation extends AppModel {

	public $recursive = -1;
	public $actsAs = array('Containable');
	
	public $hasMany = array(
		'ReservationRoom' => array(
      'foreignKey' => 'reservationId'
    )
	);

  public $belongsTo = array(
    'Company' => array(
      'foreignKey' => 'companyId'
    )
  ); 
  
    public function getGuestId($id=null) {
      $datas = $this->find('first', array(
      'contain' => array(
        'ReservationRoom' => array(
          'Folio' => array(
            'Guest'
          )
        ),
      ),
      'conditions' => array(
        'Reservation.id' => $id 
      )
      ));

      // transform
      foreach ($datas['ReservationRoom'] as $data) {
        $gid = $data['Folio']['Guest']['id'];
      }

      return $gid;
    }  

}
