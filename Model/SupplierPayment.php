<?php 
class SupplierPayment extends AppModel {
  
  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'TransactionPayment' => array(
      'foreignKey' => 'paymentId'
    ),
    'Supplier' => array(
      'foreignKey' => 'supplierId'
    )
  );

}
