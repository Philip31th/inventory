<?php
class VehicleType extends AppModel {

  public $actsAs = array('Containable');
  
  public $hasMany = array(
    'Guest' =>  array(
      'foreignKey'  =>  'vehicleTypeId'  
    )
  );  
}