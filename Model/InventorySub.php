<?php 
class InventorySub extends AppModel {

  public $recursive = -1;
  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Inventory' => array(
      'foreignKey' => 'inventoryId'
    )
  );

}
