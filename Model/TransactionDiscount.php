<?php 
class TransactionDiscount extends AppModel {
    
  public $actsAs = array('Containable');
  
  public $belongsTo = array(
    'Transaction' => array(
      'foreignKey' => 'transactionId'
    ),
    'Discount' => array(
      'foreignKey' => 'discountId'
    )
  );

}
