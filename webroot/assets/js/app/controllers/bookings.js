// service
app.factory("Booking", function($resource) {
	return $resource( api + "bookings/:id", { id: '@id', search: '@search' }, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' },
		search: { method: 'GET' },
		createFolio: { method: 'POST' }
	});
});
app.factory("Reservation", function($resource) {
  return $resource( api + "reservations/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});
app.factory("ReservationRoom", function($resource) {
  return $resource( api + "reservation-rooms/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});


// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/bookings', {
    templateUrl: base + 'bookings',
    controller: 'BookingController',
  })
  .when('/bookings/view/:id', {
    templateUrl: base + 'bookings/view',
    controller: 'BookingViewController',
  })
  .when('/bookings/add', {
    templateUrl: base + 'bookings/add',
    controller: 'BookingAddController',
  })
  .when('/bookings/edit/:id', {
    templateUrl: base + 'bookings/edit',
    controller: 'BookingEditController',
  })
  .when('/bookings/book/:date', {
    templateUrl: base + 'bookings/book',
    controller: 'BookingBookController',
  });
});


// controller
app.controller('BookingController', function($scope, Booking){
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({format: 'mm/dd/yyyy', autoclose: true}).datepicker('setDate', $scope.strSearch );
	Booking.query(function(data) {
		$scope.datas = data.result;
	});
});
app.controller('BookingAddController', function($scope, Booking, AvailableRoom){
  // start room selection
  $scope.selectedRooms = [];
  $scope.selectRoom = function(){
    if($scope.data.Reservation.arrival != '' && $scope.data.Reservation.departure != ''){
      // start list available rooms
      AvailableRoom.query({ arrival:$scope.data.Reservation.arrival, departure:$scope.data.Reservation.departure }, function(e){
        if(e.response){
          $scope.availableRooms = e.result;
          $('#select-room-modal').modal('show');
        }
      });
      // end list available rooms
    }else{
      bootbox.alert('Please select arrival and departure date.');
    }
  }
  $scope.addRemoveRoom = function(room){
    if(room.selected){
      $scope.selectedRooms.push(room);
    }else{
      $scope.selectedRooms.splice($scope.selectedRooms.indexOf(room));
    }
    console.log(room.selected);
  }
  $scope.clearSelected = function(){
    if($scope.data.Reservation.arrival != '' && $scope.data.Reservation.departure != ''){
      $scope.data.Reservation.nights = ((Date.parse($scope.data.Reservation.departure).getTime() - Date.parse($scope.data.Reservation.arrival).getTime())/86400000);
    }
    $scope.selectedRooms = [];
  }
  
  $scope.removeSelectedRoom = function(room){
    $scope.selectedRooms.splice($scope.selectedRooms.indexOf(room), 1);
  }
  // end room selection
  
  
  // start save reservation
  $scope.save = function(){
    $scope.data.ReservationRoom = $scope.selectedRooms;
    valid = $("#form").validationEngine('validate');
    if(valid){
      if($scope.selectedRooms.length <= 0){
        bootbox.alert('Please select the rooms you want to book.');
      }else{
        Booking.save($scope.data, function(e){
          if(e.response){
            $.gritter.add({title:'Successful!', text:'New booking has been saved.'});
            window.location = '#bookings';
          }
        });
      }
    }
  }
  // end save reservation
  

  $scope.changeTitle = function(data){
    if(data == 'Mr.'){
      $scope.data.Reservation.gender = 'Male';
    }else if(data == 'Ms.' || data == 'Mrs.'){
      $scope.data.Reservation.gender = 'Female';
    }
  }
});
app.controller('BookingViewController', function($scope, $routeParams, Booking, Folio, Customer, Reservation, AvailableRoom){
	$scope.id = $routeParams.id;
  Customer.query(function(e){
    $scope.customers = e.result;
  });
  $scope.load = function(){
    Booking.get({id:$scope.id}, function(e){
      $scope.data = e.result;
      $scope.data.Reservation.nights = ((Date.parse($scope.data.Reservation.departure).getTime() - Date.parse($scope.data.Reservation.arrival).getTime())/86400000);
      $('#arrival-date').datepicker("setDate", $scope.data.Reservation.arrival);
      $('#departure-date').datepicker("setDate", $scope.data.Reservation.departure);
      
    });
  }
  $scope.load();
//$scope.data.ReservationRoom = {};
  // select available room
  $scope.selectedRooms = [];
 
  $scope.selectRoom = function(){
    if($scope.data.Reservation.arrival != '' && $scope.data.Reservation.departure != ''){
      // start list available rooms
      AvailableRoom.query({ arrival:$scope.data.Reservation.arrival, departure:$scope.data.Reservation.departure }, function(e){
        if(e.response){
          $scope.availableRooms = e.result;
          $('#select-room-modal').modal('show');
        }
      });
      // end list available rooms
    }else{
      bootbox.alert('Please select arrival and departure date.');
    }
  }
  // .select available room
	//add room
	$scope.data.ReservationRoom = {};
	 $scope.addRemoveRoom = function(room){
    if(room.selected){
      $scope.resroom.push(room);
			
    }else{
      $scope.selectedRooms.splice($scope.selectedRooms.indexOf(room));
    }
    console.log(room.selected);
  }
//.addroom

	$scope.checkIn = function(room){
	  
		$scope.temp = {
			room_name: room.Room.name,
			room_type: room.Room.RoomType.name
		}
		$scope.newfolio = {
			Customer: {
				last_name: $scope.data.Reservation.last_name,
				first_name: $scope.data.Reservation.first_name,
				middle_name: $scope.data.Reservation.middle_name,
				gender: $scope.data.Reservation.gender,
				nationality: $scope.data.Reservation.nationality,
				contact_number: $scope.data.Reservation.mobile
			},
			Folio: {
				arrival: $scope.data.Reservation.arrival,
				departure: $scope.data.Reservation.departure,
				nights: $scope.data.Reservation.nights,
				room_rate: room.Room.RoomType.rate,
				room_id: room.Room.id,
				reservation_room_id: room.id,
				adults: room.Room.Accomodation.number,
				children: 0
			}
			
		}
		$('#check-in-modal').modal('show');
	}
	
	$scope.createFolio = function(){
		valid = $("#newfolio").validationEngine('validate');
		if(valid){
		  $scope.newfolio.Folio.departure = Date.parse($scope.newfolio.Folio.departure).add(-1).day().toString('yyyy-MM-dd');
		 
		
			Folio.save($scope.newfolio, function(e){
				$('#check-in-modal').modal('hide');
				$scope.load();
				$.gritter.add({ title: 'Successful!', text: 'Reservation successfuly checked in.' });
				$('#attachment').val('');
				Customer.query(function(e){
					$scope.customers = e.result;
				});
			});	
		}
	}
	
	$scope.viewFolio = function(e){
		$('#view-folio-modal').modal('show');
		Folio.get({id:'reservation_room', reservation_room_id:e}, function(f){
			$scope.viewfolio = f.result;
		});
	}
	
	$scope.remove = function(room){
		bootbox.confirm('Are you sure you want to remove this room?' ,function(e){
			if(e){
				ReservationRoom.remove({id:room.id}, function(f){
					if(f.response){
						$.gritter.add({ title: 'Successful!', text: f.message });
						$scope.data.ReservationRoom.splice($scope.data.ReservationRoom.indexOf(room), 1);
					}
				});
			}
		});
	}
	
	$scope.deleteReservation = function(room){
    bootbox.confirm('Are you sure you want to remove this reservation?', function(e){
      if(e){
        Reservation.remove({id:room.id}, function(f){
          if(f.response){
            $.gritter.add({ title: 'Successful!', text: f.message });
            window.location = '#bookings';
          }
        });
      }
    });
	}
	
	$scope.removeReservation = function(){
		bootbox.confirm('Are you sure you want to remove this reservation?' ,function(e){
			
		});
	}
	
	$scope.selectCustomer = function(id){
		if(id != null){
			$('#attachement-row').hide();
			Customer.get({id:id}, function(e){
				$scope.newfolio.Customer = e.result.Customer;
			});
		} else {
			$('#attachement-row').show();
		}
	}
	
  $scope.changeTitle = function(data){
    if(data == 'Mr.'){
      $scope.data.Reservation.gender = 'Male';
    }else if(data == 'Ms.' || data == 'Mrs.'){
      $scope.data.Reservation.gender = 'Female';
    }
  }
  
  //$scope.update = function(){
 //   console.log('update');
  //}
  
  $scope.updatebooking = function(){
    valid = $("#form").validationEngine('validate');
    if(valid){
    Booking.update({id:$scope.id}.ReservationRoom,function(e){
      if(e.response){
        $.gritter.add({ title: 'Successful!', text: e.message });
        window.location = '#/bookings';
      }
    });
    }
  }
  
  //discount
  
   $scope.bool = [{value: true, name: 'FIX AMOUNT'}, {value: false, name: 'PERCENTAGE'}]
   
// discount
});
app.controller('BookingEditController', function($scope, $routeParams, Booking){

});
app.controller('BookingBookController', function($scope, $timeout, $routeParams, Booking, Reservation, AvailableRoomType){
  modalMaxHeight();
  
  if ($routeParams.date == 'new') {
    $scope.arrivalDate = Date.parse('today').toString('MM/dd/yyyy');
    $scope.departureDate = Date.parse('today').addDays(1).toString('MM/dd/yyyy');
    
  } else {
    $scope.arrivalDate = Date.parse($routeParams.date).toString('MM/dd/yyyy');
    $scope.departureDate = Date.parse($routeParams.date).addDays(1).toString('MM/dd/yyyy');
  }

  $('#arrival-date').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    startDate: Date.today().toString('MM/dd/yyyy'),
  }).on('changeDate', function(e){
    $timeout(function(){
      aUnix = e.date.getTime();
      dUnix = Date.parse($scope.departureDate).getTime();
      nDate = Date.parse(e.date.toString('MM/dd/yyyy')).add(1).day().toString('MM/dd/yyyy');
      if(aUnix >= dUnix){
        $scope.departureDate = nDate;
        $("#departure-date").datepicker('setDate', nDate);
      }
      $("#departure-date").datepicker('setStartDate', nDate);
    });
  });
  $('#departure-date').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    startDate: Date.today().toString('MM/dd/yyyy')
  }).on('changeDate', function(e){
   //$("#arrival-date").datepicker('setEndDate', e.date);
  });
  

  $("#arrival-date").datepicker('setDate', $scope.arrivalDate);
  $("#departure-date").datepicker('setDate', $scope.departureDate);
  
  $('#form').validationEngine('attach');

  $scope.checkAvailable = function(){
    if($scope.arrivalDate != '' && $scope.departureDate != ''){
      // start list available rooms
      AvailableRoomType.query({ arrival:$scope.arrivalDate, departure:$scope.departureDate }, function(e){
        if(e.response){
          $scope.availableRooms = e.result;
        }
      });
      // end list available rooms
    }else{
      alert('Please select arrival and departure date.');
    }
  }
  
  $scope.checkAvailable();
  
  $scope.bookNow = function(data){
    $scope.modalRoomType = data.RoomType.name;
    $scope.newReservation = {
      Reservation:{
        arrival: $scope.arrivalDate,
        departure: $scope.departureDate,
        reservation_type: 'Confirm Booking'
      },
      ReservationRoom:[{
        id:data.room_id,
        arrival: $scope.arrivalDate,
        departure: $scope.departureDate
      }]
    }
    $('#book-now-modal').modal('show');
  }
  
  $scope.save = function(){
    valid = $("#form").validationEngine('validate');
    if(valid){
      Reservation.save($scope.newReservation, function(e){
        if(e.response){
          $('#book-now-modal').modal('hide');
          setTimeout(function(){
            window.location = '#bookings';
          }, 500);
        }
      });
    }

  }
});