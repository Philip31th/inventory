// service
app.factory("Charge", function($resource) {
  return $resource( api + "charges/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});




// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/charges', {
    templateUrl: base + 'charges',
    controller: 'ChargesController',
  })
  .when('/charges/add', {
    templateUrl: base + '/charges/add',
    controller: 'AddChargesController',
  })
  .when('/charges/edit/:id', {
    templateUrl: base + '/charges/edit',
    controller: 'EditChargesController',
  });
});

// controller
app.controller('ChargesController', function($scope, Charge, CheckLevel){
  
  CheckLevel.query({ id : userId }, function (e) {
    if (e.ok) $scope.highLevel = e.data; 
  });
  
  $scope.search = function(strSearch){
    Charge.search({search:strSearch}, function(data) {
      $scope.data = data.result;
    });
  };
  
 Charge.query(function(data) {
		$scope.data = data.result;
	});
	
	$scope.remove = function(data){
		console.log($scope.data);
		msg = confirm('Are you sure you want to delete?');
		if(msg){
			Charge.remove({ id:data.id }, function(e){
				if(e.response){
					$.gritter.add({ title: 'Successful!', text: e.message });
					$scope.data.splice( $scope.data.indexOf(data), 1 );
				}
			});
			
		} 
	};
});

app.controller('AddChargesController', function($scope,Charge){
	$scope.save = function(){
	console.log($scope.data);
	valid = $("#form").validationEngine('validate');
		
		Charge.save($scope.data, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#/charges';
			}
		});
	}
});


app.controller('EditChargesController', function($scope,$routeParams,Charge){
		$scope.id = $routeParams.id;
	Charge.get({id:$scope.id}, function(e){
		$scope.data = e.result;
	});
	$scope.save = function(){
		valid = $("#form").validationEngine('validate');
		
		if(valid){
		Charge.update({id:$scope.id}, $scope.data, function(e){
			if(e.response){
				$.gritter.add({ title: 'Successful!', text: e.message });
				window.location = '#/charges';
			}
		});
	  }
	}
});
