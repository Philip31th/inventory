// service
app.factory("Folio", function($resource) {
	return $resource( api + "folios/:id", { id: '@id', search: '@search' }, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' },
		search: { method: 'GET' },
		save: {
      method: 'POST',
      transformRequest: function(e){
        var formdata = new FormData();
				data = JSON.stringify(e);
				formdata.append('data', data);
				formdata.append('attachment', document.getElementById('attachment').files[0]);
				return formdata;
			},
      headers: {'Content-Type': undefined, enctype:'multipart/form-data'}
    }
	});
});
app.factory("Arrival", function($resource) {
  return $resource( api + "folios/arrival", {}, {
    query: { method: 'GET', isArray: false }
  });
});

app.factory("Departure", function($resource) {
  return $resource( api + "folios/departure", {}, {
    query: { method: 'GET', isArray: false }
  });
});


// route
app.config(function($routeProvider) {
  $routeProvider
  .when('/guests', {
    templateUrl: base + 'folios',
    controller: 'FolioController',
  })
  .when('/guests/view/:id', {
    templateUrl: base + 'folios/view',
    controller: 'FolioViewController',
  })
  .when('/guests/incoming', {
    templateUrl: base + 'folios/arrival',
    controller: 'FolioArrivalController',
  })
	.when('/guests/outgoing', {
    templateUrl: base + 'folios/departure',
    controller: 'FolioDepartureController',
  })
  .when('/guests/allguestlist', {
    templateUrl: base + 'folios/allguestlist',
    controller: 'FolioGuestListController',
  })
  .when('/guests/allguestlist', {
    templateUrl: base + 'folios/allguestlist',
    controller: 'FolioGuestListController',
  });
});


// controller
app.controller('FolioController', function($scope, Folio){
  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({ format: 'mm/dd/yyyy', autoclose: true }).datepicker('setDate', $scope.strSearch );

	$scope.datas = {};
	$scope.paginator = {};
	$scope.showPage = 5;
	$scope.startPage = 1;
	$scope.endPage = null;
	
	$scope.load = function(){
    $scope.datas = Folio.query({ date:$scope.strSearch }, function(data) {
      $scope.datas = data.result;
      $scope.paginator = data.paginator;
      $scope.endPage = $scope.paginator.pageCount < $scope.showPage? $scope.paginator.pageCount:$scope.showPage;
      $scope.showPage = $scope.paginator.pageCount < $scope.showPage? $scope.paginator.pageCount:$scope.showPage;
    });
	}
	$scope.load();


	$scope.page = function(page){
		if(page >= 1 && page <= $scope.paginator.pageCount){
			Room.search({page:page}, function(data) {
				$scope.datas = data.result;
			});
			$scope.paginator.page = page;
			if(page <= Math.round($scope.showPage/2)) $scope.startPage = 1;
			else if(page > $scope.paginator.pageCount-Math.round($scope.showPage/2)) $scope.startPage = $scope.paginator.pageCount-($scope.showPage-1);
			else $scope.startPage = page-Math.floor($scope.showPage/2);
			if($scope.startPage + ($scope.showPage-1) > $scope.paginator.pageCount){
				$scope.startPage = $scope.paginator.pageCount - ($scope.showPage-1);
				$scope.endPage = $scope.paginator.pageCount;
			} else $scope.endPage = $scope.startPage + ($scope.showPage-1);
			$('.pagination-page').removeClass('active');
			$('li[data-page='+ page +']').addClass('active');
		}
	};
});
app.controller('FolioAddController', function($scope, Folio){

});
app.controller('FolioViewController', function($scope, $routeParams, Folio){

});
app.controller('FolioEditController', function($scope, $routeParams){

});
app.controller('FolioArrivalController', function($scope, Arrival){
  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({ format: 'mm/dd/yyyy', autoclose: true, startDate: $scope.today }).datepicker('setDate', $scope.strSearch );
	
	$scope.load = function(){
		Arrival.query({arrival:$scope.strSearch}, function(e){
			$scope.datas = e.result;
		});
	}
	$scope.load();
});
app.controller('FolioDepartureController', function($scope, Departure){
  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({ format: 'mm/dd/yyyy', autoclose: true, startDate: $scope.today }).datepicker('setDate', $scope.strSearch );
  
	$scope.load = function(){
		Departure.query({departure:$scope.strSearch}, function(e){
			$scope.datas = e.result;
		});
	}	
	$scope.load();

});


