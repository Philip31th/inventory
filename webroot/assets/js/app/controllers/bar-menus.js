// service  shivaji sir
app.factory("BarMenus", function($resource) {
	return $resource( api + "barmenus/:id", { id: '@id', search: '@search' }, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' },
		search: { method: 'GET' },
		save: {
      method: 'POST',
      transformRequest: function(e){
        var formdata = new FormData();
				data = JSON.stringify(e);
				formdata.append('data', data);
				formdata.append('attachment', document.getElementById('attachment').files[0]);
				return formdata;
			},
      headers: {'Content-Type':undefined, enctype:'multipart/form-data'}
    }
	});
});
app.factory("inventory", function($resource) {
  return $resource( api + "BarMenus/inventory", {}, {
    query: { method: 'GET', isArray: false }
  });
});

app.factory("suppliers", function($resource) {
  return $resource( api + "BarMenus/suppliers", {}, {
    query: { method: 'GET', isArray: false }
  });
});

// route: template 
app.config(function($routeProvider) {
  $routeProvider
  .when('/BarMenus', {
    templateUrl:  'BarMenus',
    controller: 'BarMenusController',
  })
  .when('/BarMenus/inventory', {
    templateUrl:  'BarMenus/inventory',
    controller: 'BarMenusController',
  })
  .when('/BarMenus/suppliers', {
    templateUrl:  'BarMenus/suppliers/index',
    controller: 'BarMenusController',
  })
  .when('/BarMenus/suppliers-item', {
    templateUrl:  'BarMenus/suppliers-item',
    controller: 'BarMenusController',
  });
});

// controller
app.controller('BarMenusController', function($scope, Folio){
	//$scope.strSearch = Date.parse('today').toString('MM-dd-yyyy');
	//$scope.strSearch = Date.today().toString('yyyy-MM-dd');
	$scope.datas = {};
	$scope.paginator = {};
	$scope.showPage = 5;
	$scope.startPage = 1;
	$scope.endPage = null;
	$scope.datas = Folio.query(function(data) {
		$scope.datas = data.result;
		$scope.paginator = data.paginator;
		$scope.endPage = $scope.paginator.pageCount < $scope.showPage? $scope.paginator.pageCount:$scope.showPage;
		$scope.showPage = $scope.paginator.pageCount < $scope.showPage? $scope.paginator.pageCount:$scope.showPage;
	});
	$scope.search = function(strSearch){
		Folio.search({search:strSearch}, function(data) {
			$scope.datas = data.result;
		});
	};
	$scope.page = function(page){
		if(page >= 1 && page <= $scope.paginator.pageCount){
			Room.search({page:page}, function(data) {
				$scope.datas = data.result;
			});
			$scope.paginator.page = page;
			if(page <= Math.round($scope.showPage/2)) $scope.startPage = 1;
			else if(page > $scope.paginator.pageCount-Math.round($scope.showPage/2)) $scope.startPage = $scope.paginator.pageCount-($scope.showPage-1);
			else $scope.startPage = page-Math.floor($scope.showPage/2);
			if($scope.startPage + ($scope.showPage-1) > $scope.paginator.pageCount){
				$scope.startPage = $scope.paginator.pageCount - ($scope.showPage-1);
				$scope.endPage = $scope.paginator.pageCount;
			} else $scope.endPage = $scope.startPage + ($scope.showPage-1);
			$('.pagination-page').removeClass('active');
			$('li[data-page='+ page +']').addClass('active');
		}
	};
});
app.controller('BarMenusController', function($scope, BarMenus){

});
app.controller('BarMenusController', function($scope, $stateParams, BarMenus){

});
app.controller('BarMenusController', function($scope, $stateParams){

});
app.controller('BarMenusController', function($scope, suppliers){
	$scope.load = function(){
		Arrival.query(function(e){
			$scope.datas = e.result;
		});
	}
	$scope.load();
});
app.controller('FolioDepartureController', function($scope, inventory){
	$scope.load = function(){
		Departure.query(function(e){
			$scope.datas = e.result;
		});
	}	
	$scope.load();

});