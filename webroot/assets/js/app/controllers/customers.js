// service
app.factory("Customer", function($resource) {
	return $resource( api + "customers/:id", { id: '@id', search: '@search' }, {
		query: { method: 'GET', isArray: false },
		update: { method: 'PUT' },
		search: { method: 'GET' }
	});
});
