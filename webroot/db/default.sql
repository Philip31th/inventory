SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Table structure for table `accomodations`
--

CREATE TABLE IF NOT EXISTS `accomodations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `accomodations`
--

INSERT INTO `accomodations` (`id`, `name`, `number`, `visible`, `created`, `modified`) VALUES
(1, 'Single', 1, 1, NULL, '2015-05-06 23:50:00'),
(2, 'Double', 2, 1, NULL, NULL),
(3, 'Triple', 3, 1, NULL, NULL),
(4, 'Quadruple', 4, 1, NULL, NULL),
(5, 'Sextuple', 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE IF NOT EXISTS `businesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `code`, `name`, `description`, `visible`, `created`, `modified`) VALUES
(1, 'hotel', 'Hotel', 'Hotel', 1, NULL, NULL),
(2, 'restaurant', 'Restaurant', 'Restaurant', 1, NULL, NULL),
(3, 'bar', 'Bar', 'Bar', 1, NULL, NULL),
(4, 'travelers-shop', 'Travelers Shop', 'Travelers Shop', 1, NULL, NULL),
(5, 'salon', 'Salon', 'Salon', 1, NULL, NULL),
(6, 'business-center', 'Business Center', 'Business Center', 1, NULL, NULL),
(7, 'events', 'Events', 'Events', 1, NULL, NULL),
(8, 'laundry', 'Laundry', 'Laundry', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `business_sources`
--

CREATE TABLE IF NOT EXISTS `business_sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `business_sources`
--

INSERT INTO `business_sources` (`id`, `code`, `name`, `visible`, `created`, `modified`) VALUES
(1, 'WKN', 'Walk In', 1, NULL, NULL),
(2, 'TVA', 'Travel Agency', 1, NULL, NULL),
(3, 'IND', 'Individual', 1, NULL, NULL),
(4, 'CMP', 'Complimentary', 1, NULL, NULL),
(5, 'CRP', 'Corporate', 1, NULL, NULL),
(6, 'GVC', 'Gift Voucher', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `charges`
--

CREATE TABLE IF NOT EXISTS `charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `charges`
--

INSERT INTO `charges` (`id`, `code`, `name`, `amount`, `visible`, `created`, `modified`) VALUES
(1, 'test', 'test', '100', 1, '2015-04-30 19:53:33', '2015-04-30 19:53:33'),
(2, 'tesad', '123', '100', 1, '2015-04-30 19:54:48', '2015-04-30 19:54:48'),
(3, 'test', '100', '90', 1, '2015-04-30 19:55:12', '2015-04-30 19:55:12');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contact_person_title` varchar(255) DEFAULT NULL,
  `contact_person_name` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `attachment` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `code`, `title`, `last_name`, `first_name`, `middle_name`, `gender`, `nationality`, `address`, `company_id`, `email`, `contact_number`, `attachment`, `visible`, `created`, `modified`) VALUES
(1, NULL, NULL, 'Linwood', 'Rose', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06_big.jpg', 1, '2015-05-07 04:01:07', '2015-05-07 04:01:07'),
(2, NULL, NULL, 'Dexter', 'Shaquille', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1-870x450.jpg', 1, '2015-05-07 04:02:03', '2015-05-07 04:02:03'),
(3, NULL, NULL, 'Folio', 'Customer', NULL, 'Male', 'Filipino', NULL, NULL, NULL, '123456', '11088539_4898490035609_4224321880118889355_n.jpg', 1, '2015-05-12 19:03:12', '2015-05-12 19:03:12'),
(4, NULL, NULL, 'my last', 'my first', NULL, 'Male', 'Filipino', NULL, NULL, NULL, '13216546', '11088539_4898490035609_4224321880118889355_n.jpg', 1, '2015-05-13 17:55:21', '2015-05-13 17:55:21'),
(5, NULL, NULL, 'Wilsonse', 'Anciro', NULL, 'Male', 'Filipino', NULL, NULL, NULL, '13456', 'http://localhost/ehrms-project/demo-api/img/thumbnails/100x100/11088539_4898490035609_4224321880118889355_n.jpg', 1, '2015-05-13 20:24:57', '2015-05-13 20:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `code`, `name`, `description`, `visible`, `created`, `modified`) VALUES
(1, 'restaurant', 'Restaurant', 'Restodsogvkn', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `fixed` tinyint(1) DEFAULT '0',
  `value` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `name`, `fixed`, `value`, `visible`, `created`, `modified`) VALUES
(1, 'test', 0, 10, 0, '2015-04-29 20:03:14', '2015-04-29 20:03:17'),
(2, 'Senior', 0, 50, 1, '2015-05-14 01:04:23', '2015-05-14 01:04:23');

-- --------------------------------------------------------

--
-- Table structure for table `folios`
--

CREATE TABLE IF NOT EXISTS `folios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `arrival` datetime DEFAULT NULL,
  `departure` datetime DEFAULT NULL,
  `nights` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `room_rate` varchar(20) DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `exta_bed` int(11) DEFAULT NULL,
  `payment_mode_id` int(11) DEFAULT NULL,
  `business_source_id` int(11) DEFAULT NULL,
  `special_request` varchar(255) DEFAULT NULL,
  `reservation_room_id` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `closed` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `data` varchar(50000) DEFAULT '{}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `folios`
--

INSERT INTO `folios` (`id`, `code`, `customer_id`, `arrival`, `departure`, `nights`, `room_id`, `room_rate`, `adults`, `children`, `exta_bed`, `payment_mode_id`, `business_source_id`, `special_request`, `reservation_room_id`, `paid`, `closed`, `visible`, `created`, `modified`, `data`) VALUES
(1, '0001', 1, '2015-05-07 00:00:00', '2015-05-07 00:00:00', 1, 2, '1200', 1, 0, NULL, NULL, NULL, NULL, 1, 1, 1, 1, '2015-05-07 04:01:07', '2015-05-07 10:30:22', '{}'),
(2, '0002', 2, '2015-05-08 00:00:00', '2015-05-09 00:00:00', 2, 2, '1200', 1, 0, NULL, NULL, NULL, 'Babychair', 2, 0, 0, 1, '2015-05-07 04:02:04', '2015-05-07 04:02:04', '{}'),
(3, '0003', 3, '2015-05-15 00:00:00', '2015-05-15 00:00:00', 1, 6, '2100', 2, 0, NULL, NULL, NULL, NULL, 5, 0, 0, 1, '2015-05-12 19:03:12', '2015-05-12 19:03:12', '{}'),
(4, '0004', 2, '2015-05-15 00:00:00', '2015-05-21 00:00:00', 7, 37, '1200', 1, 0, NULL, NULL, NULL, NULL, 6, 0, 0, 1, '2015-05-12 21:52:44', '2015-05-12 21:52:44', '{}'),
(5, '0005', 2, '2015-05-22 00:00:00', '2015-06-25 00:00:00', 35, 36, '1200', 1, 0, NULL, NULL, NULL, NULL, 7, 0, 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26', '{}'),
(6, '0006', 4, '2015-05-15 00:00:00', '2015-05-14 00:00:00', 0, 2, '1200', 1, 0, NULL, 1, 1, NULL, 8, 0, 0, 1, '2015-05-13 17:55:21', '2015-05-13 17:55:21', '{}'),
(7, '0007', 5, '2015-05-15 00:00:00', '2015-05-21 00:00:00', 7, 36, '1200', 1, 0, NULL, NULL, NULL, NULL, 4, 0, 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58', '{}'),
(8, '0008', 5, '2015-05-27 00:00:00', '2015-05-28 00:00:00', 2, 4, '2100', 2, 0, NULL, NULL, NULL, NULL, 9, 0, 0, 1, '2015-05-13 21:22:18', '2015-05-13 21:22:18', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `folio_discounts`
--

CREATE TABLE IF NOT EXISTS `folio_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) DEFAULT NULL,
  `folio_transaction_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `folio_payments`
--

CREATE TABLE IF NOT EXISTS `folio_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `folio_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `cash_tender` varchar(255) DEFAULT NULL,
  `change` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `card_number` varchar(255) DEFAULT NULL,
  `cheque_number` varchar(255) DEFAULT NULL,
  `expiration_year` varchar(255) DEFAULT NULL,
  `expiration_month` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
<<<<<<< HEAD
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;
=======
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;
>>>>>>> 7acb0cfb8c06106e084697072eb8d5075fec5861

--
-- Dumping data for table `folio_payments`
--

INSERT INTO `folio_payments` (`id`, `code`, `folio_id`, `transaction_id`, `amount`, `cash_tender`, `change`, `date`, `type`, `account_name`, `account_number`, `card_number`, `cheque_number`, `expiration_year`, `expiration_month`, `bank_name`, `visible`, `created`, `modified`) VALUES
(1, '001', 1, 1, '1200', NULL, NULL, '2015-05-07 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-07 04:01:07', '2015-05-07 04:01:07'),
(2, '002', 2, 2, '1000', NULL, NULL, '2015-05-07 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-07 04:02:04', '2015-05-07 04:02:04'),
(3, '010225', 3, 3, '300', NULL, NULL, '2015-05-12 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-12 19:03:12', '2015-05-12 19:03:12'),
(4, '1100', 4, 4, '1000', NULL, NULL, '2015-05-12 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(5, '111', 5, 5, '11', NULL, NULL, '2015-05-12 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(6, '1313', 6, 6, '200', NULL, NULL, '2015-05-13 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-13 17:55:22', '2015-05-13 17:55:22'),
(7, '12366565', 7, 7, '200', NULL, NULL, '2015-05-13 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-13 20:24:59', '2015-05-13 20:24:59'),
(8, '0000', 8, 8, '200', NULL, NULL, '2015-05-13 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-13 21:22:19', '2015-05-13 21:22:19'),
(9, NULL, 8, 8, '40001', NULL, NULL, '2015-05-15 00:00:00', 'CASH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-05-15 05:25:18', '2015-05-15 05:25:18');

-- --------------------------------------------------------

--
-- Table structure for table `folio_transactions`
--

CREATE TABLE IF NOT EXISTS `folio_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `folio_transactions`
--

INSERT INTO `folio_transactions` (`id`, `folio_id`, `transaction_id`, `visible`, `created`, `modified`) VALUES
(1, 1, 1, 1, '2015-05-07 04:01:08', '2015-05-07 04:01:08'),
(2, 2, 2, 1, '2015-05-07 04:02:04', '2015-05-07 04:02:04'),
(3, 3, 3, 1, '2015-05-12 19:03:12', '2015-05-12 19:03:12'),
(4, 4, 4, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(5, 5, 5, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(6, 6, 6, 1, '2015-05-13 17:55:22', '2015-05-13 17:55:22'),
(7, 7, 7, 1, '2015-05-13 20:24:59', '2015-05-13 20:24:59'),
(8, 8, 8, 1, '2015-05-13 21:22:19', '2015-05-13 21:22:19');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
<<<<<<< HEAD
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
=======
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `department_id`, `code`, `name`, `description`, `visible`, `created`, `modified`) VALUES
(1, 1, '100', 'Cake', 'Vanilla Cake', 1, '2015-05-25 16:05:20', NULL);
>>>>>>> 7acb0cfb8c06106e084697072eb8d5075fec5861

-- --------------------------------------------------------

--
-- Table structure for table `inventory_subs`
--

CREATE TABLE IF NOT EXISTS `inventory_subs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Ins, 0=Outs',
  `quantity` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `inventory_subs`
--

INSERT INTO `inventory_subs` (`id`, `inventory_id`, `type`, `quantity`, `visible`, `created`, `modified`) VALUES
(1, 1, 1, 15, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 0, 8, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

CREATE TABLE IF NOT EXISTS `payment_modes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `code`, `name`, `visible`, `created`, `modified`) VALUES
(1, 'CA', 'Cash', 1, NULL, NULL),
(2, 'SB', 'Send Bill', 1, NULL, NULL),
(3, 'CC', 'Credit Card', 1, NULL, NULL),
(4, 'DC', 'Debit Card', 1, NULL, NULL),
(5, 'CQ', 'Cheque', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `booking_type` varchar(255) DEFAULT NULL,
  `rooms` int(11) DEFAULT NULL,
  `arrival` datetime DEFAULT NULL,
  `departure` datetime DEFAULT NULL,
  `nights` int(11) DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `identity` varchar(255) DEFAULT NULL,
  `identity_id_number` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `vip_status` varchar(255) DEFAULT NULL,
  `rate_type` varchar(255) DEFAULT NULL,
  `bill_to` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `payment_mode` varchar(255) DEFAULT NULL,
  `released` datetime DEFAULT NULL,
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
<<<<<<< HEAD
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;
=======
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;
>>>>>>> 7acb0cfb8c06106e084697072eb8d5075fec5861

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `code`, `title`, `last_name`, `first_name`, `middle_name`, `address`, `booking_type`, `rooms`, `arrival`, `departure`, `nights`, `adults`, `children`, `email`, `mobile`, `phone`, `fax`, `identity`, `identity_id_number`, `nationality`, `gender`, `vip_status`, `rate_type`, `bill_to`, `company_id`, `payment_mode`, `released`, `closed`, `visible`, `created`, `modified`) VALUES
(1, NULL, NULL, 'Linwood', 'Rose', NULL, NULL, 'Reservation', NULL, '2015-05-07 00:00:00', '2015-05-07 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2015-05-07 03:36:08', '2015-05-07 10:30:23'),
(2, NULL, 'Mr.', 'Dexter', 'Shaquille', NULL, 'Los Angeles', 'Walk In', NULL, '2015-05-08 00:00:00', '2015-05-11 00:00:00', 3, NULL, NULL, 'Shaquile_23@live.com', '+52555255636', '0888-36625', NULL, 'Drivers License', '501693247', 'American', 'Male', NULL, NULL, 'Company', NULL, 'Cash', '2015-05-02 00:00:00', 0, 1, '2015-05-07 03:36:52', '2015-05-07 03:36:52'),
(3, NULL, 'Mr.', 'Bailey', 'Chandler', NULL, 'California', 'Reservation', NULL, '2015-05-07 00:00:00', '2015-05-16 00:00:00', 9, NULL, NULL, 'Bailey@ymail.com', '03663256698225', NULL, NULL, 'Passport', '01226233656527931', 'Filipino', 'Male', NULL, NULL, 'Guess', NULL, 'Cash', '2015-05-21 00:00:00', 0, 1, '2015-05-07 08:56:54', '2015-05-07 08:56:54'),
(4, NULL, 'Mr.', 'Wilson', 'Anciro', NULL, 'Olongapo', 'Reservation', NULL, '2015-05-15 00:00:00', '2015-05-28 00:00:00', 13, NULL, NULL, 'konekred@edncsolutions.com', '13456', '651654', '132165465', 'Drivers License', '1321654564', 'Filipino', 'Male', NULL, NULL, 'Company', NULL, 'Cash', '2015-05-12 00:00:00', 0, 1, '2015-05-12 16:37:00', '2015-05-12 16:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `reservation_rooms`
--

CREATE TABLE IF NOT EXISTS `reservation_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `arrival` datetime DEFAULT NULL,
  `departure` datetime DEFAULT NULL,
  `rate` decimal(11,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'pending',
  `checked_in` tinyint(1) DEFAULT '0',
  `checked_in_date` datetime DEFAULT NULL,
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `reservation_rooms`
--

INSERT INTO `reservation_rooms` (`id`, `reservation_id`, `room_id`, `arrival`, `departure`, `rate`, `status`, `checked_in`, `checked_in_date`, `closed`, `visible`, `created`, `modified`) VALUES
(1, 1, 2, '2015-05-07 00:00:00', '2015-05-07 00:00:00', NULL, 'checked out', 1, '2015-05-07 00:00:00', 1, 1, '2015-05-07 03:36:08', '2015-05-07 10:30:22'),
(2, 2, 2, '2015-05-08 00:00:00', '2015-05-09 00:00:00', NULL, 'checked in', 1, '2015-05-07 00:00:00', 0, 1, '2015-05-07 03:36:52', '2015-05-07 04:02:04'),
(3, 3, 24, '2015-05-07 00:00:00', '2015-05-07 00:00:00', NULL, 'pending', 0, NULL, 0, 1, '2015-05-07 08:56:54', '2015-05-07 08:56:54'),
(4, 4, 36, '2015-05-15 00:00:00', '2015-05-21 00:00:00', NULL, 'checked in', 1, '2015-05-13 00:00:00', 0, 1, '2015-05-12 16:37:00', '2015-05-13 20:24:58'),
(5, 5, 6, '2015-05-15 00:00:00', '2015-05-15 00:00:00', NULL, 'checked in', 1, '2015-05-12 00:00:00', 0, 1, '2015-05-12 19:00:31', '2015-05-12 19:03:12'),
(6, 6, 37, '2015-05-15 00:00:00', '2015-05-21 00:00:00', NULL, 'checked in', 1, NULL, 0, 1, '2015-05-12 21:52:16', '2015-05-12 21:52:44'),
(7, 7, 36, '2015-05-22 00:00:00', '2015-06-25 00:00:00', NULL, 'checked in', 1, NULL, 0, 1, '2015-05-12 22:25:09', '2015-05-12 22:33:26'),
(8, 9, 2, '2015-05-15 00:00:00', '2015-05-14 00:00:00', NULL, 'checked in', 1, '2015-05-13 00:00:00', 0, 1, '2015-05-12 22:42:00', '2015-05-13 17:55:21'),
(9, 10, 4, '2015-05-27 00:00:00', '2015-05-28 00:00:00', NULL, 'checked in', 1, NULL, 0, 1, '2015-05-13 21:20:44', '2015-05-13 21:22:18');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index` int(11) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `accomodation_id` int(11) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `synced` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `index`, `code`, `name`, `room_type_id`, `accomodation_id`, `description`, `status`, `visible`, `synced`, `created`, `modified`) VALUES
(1, 1, '201', '201', 3, 4, NULL, NULL, 1, 0, NULL, '2015-02-05 14:08:51'),
(2, 2, '202', '202', 1, 1, NULL, NULL, 1, 0, NULL, '2015-02-05 14:13:00'),
(3, 3, '203', '203', 3, 4, NULL, NULL, 1, 0, NULL, '2015-02-05 14:17:50'),
(4, 4, '204', '204', 4, 2, NULL, NULL, 1, 0, NULL, '2015-02-05 14:18:02'),
(5, 5, '205', '205', 3, 4, NULL, NULL, 1, 0, NULL, '2015-02-05 14:18:48'),
(6, 6, '206', '206', 4, 2, NULL, NULL, 1, 0, NULL, '2015-02-05 14:19:07'),
(7, 7, '207', '207', 3, 4, NULL, NULL, 1, 0, NULL, '2015-02-05 14:19:15'),
(8, 8, '208', '208', 4, 2, NULL, NULL, 1, 0, NULL, '2015-02-05 14:19:22'),
(9, 9, '209', '209', 2, 3, NULL, NULL, 1, 0, NULL, '2015-02-05 14:19:30'),
(10, 10, '210', '210', 4, 2, NULL, NULL, 1, 0, NULL, '2015-02-05 14:18:30'),
(11, 11, '211', '211', 2, 3, NULL, NULL, 1, 0, NULL, '2015-02-05 14:18:10'),
(12, NULL, '212A', '212A', 4, 2, NULL, NULL, 1, 0, '2015-02-05 14:24:32', '2015-02-05 14:24:32'),
(13, NULL, '212B', '212B', 2, 3, NULL, NULL, 1, 0, '2015-02-17 13:00:42', '2015-02-17 13:00:42'),
(14, NULL, '214', '214', 5, 2, NULL, NULL, 1, 0, '2015-02-17 13:01:51', '2015-02-17 13:01:51'),
(15, NULL, '215', '215', 2, 3, NULL, NULL, 1, 0, '2015-02-17 13:02:17', '2015-02-17 13:02:17'),
(16, NULL, '216', '216', 5, 2, NULL, NULL, 1, 0, '2015-02-17 13:02:56', '2015-02-17 13:02:56'),
(17, NULL, '217', '217', 4, 2, NULL, NULL, 1, 0, '2015-02-17 13:03:21', '2015-02-17 13:03:21'),
(18, NULL, '218', '218', 5, 2, NULL, NULL, 1, 0, '2015-02-17 13:03:38', '2015-02-17 13:03:38'),
(19, NULL, '219', '219', 4, 2, 'EXTENSION BUILDING', NULL, 1, 0, '2015-02-17 13:03:58', '2015-02-17 13:03:58'),
(20, NULL, '220', '220', 4, 2, NULL, NULL, 1, 0, '2015-02-17 13:04:25', '2015-02-17 13:04:25'),
(21, NULL, '301', '301', 6, 3, NULL, NULL, 1, 0, '2015-02-17 13:05:04', '2015-02-17 13:05:04'),
(24, NULL, '302', '302', 1, 1, NULL, NULL, 1, 0, '2015-02-17 14:08:59', '2015-02-17 14:08:59'),
(25, NULL, '303', '303', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:10:11', '2015-02-17 14:10:11'),
(26, NULL, '304', '304', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:10:30', '2015-02-17 14:10:30'),
(27, NULL, '305', '305', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:10:47', '2015-02-17 14:10:47'),
(28, NULL, '306', '306', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:11:00', '2015-02-17 14:11:00'),
(29, NULL, '307', '307', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:11:16', '2015-02-17 14:11:16'),
(30, NULL, '308', '308', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:11:29', '2015-02-17 14:11:29'),
(31, NULL, '309', '309', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:11:44', '2015-02-17 14:11:44'),
(32, NULL, '310', '310', 7, 2, NULL, NULL, 1, 0, '2015-02-17 14:11:56', '2015-02-17 14:11:56'),
(33, NULL, '311', '311', 1, 1, NULL, NULL, 1, 0, '2015-02-17 14:13:44', '2015-02-17 14:13:44'),
(34, NULL, '312A', '312A', 1, 1, NULL, NULL, 1, 0, '2015-02-17 14:15:17', '2015-02-17 14:15:17'),
(35, NULL, '312B', '312B', 4, 2, NULL, NULL, 1, 0, '2015-02-17 14:15:33', '2015-02-17 14:15:33'),
(36, NULL, '401', '401', 1, 1, NULL, NULL, 1, 0, '2015-02-17 14:15:47', '2015-02-17 14:15:47'),
(37, NULL, '402', '402', 1, 1, NULL, NULL, 1, 0, '2015-02-17 14:16:07', '2015-02-17 14:16:07'),
(38, NULL, '403', '403', 4, 2, NULL, NULL, 1, 0, '2015-02-17 14:16:19', '2015-02-17 14:16:19'),
(39, NULL, '101', '101', 8, 5, NULL, NULL, 1, 0, '2015-03-31 11:46:38', '2015-03-31 11:46:38'),
(40, NULL, '102', '102', 8, 5, NULL, NULL, 1, 0, '2015-03-31 11:46:49', '2015-03-31 11:46:49'),
(44, NULL, '500', '500', 1, 3, NULL, NULL, 1, 0, '2015-04-26 14:44:08', '2015-04-26 14:44:08'),
(45, NULL, '123', '123', 3, 4, NULL, NULL, 1, 0, '2015-05-14 02:24:51', '2015-05-14 02:24:51');

-- --------------------------------------------------------

--
-- Table structure for table `room_features`
--

CREATE TABLE IF NOT EXISTS `room_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `synced` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

CREATE TABLE IF NOT EXISTS `room_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  `accomodation_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `synced` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `room_types`
--

INSERT INTO `room_types` (`id`, `code`, `name`, `description`, `rate`, `accomodation_id`, `visible`, `synced`, `created`, `modified`) VALUES
(1, 'standard', 'Standard', NULL, '1200', 1, 1, 0, NULL, '2015-04-29 15:04:34'),
(2, 'deluxe_a', 'Deluxe A', NULL, '3100', 3, 1, 0, NULL, NULL),
(3, 'deluxe_b', 'Deluxe B', NULL, '3500', 4, 1, 0, NULL, NULL),
(4, 'superior', 'Superior', NULL, '2100', 2, 1, 0, NULL, NULL),
(5, 'super_twin', 'Super Twin', NULL, '2300', 2, 1, 0, NULL, NULL),
(6, 'presidential_suite', 'Presidential Suite', NULL, '6500', 1, 1, 0, NULL, NULL),
(7, 'executive_deluxe', 'Executive Deluxe', NULL, '2500', 2, 1, 0, NULL, '2015-05-14 16:30:08'),
(8, 'villa', 'Villa', NULL, '00', 4, 1, 0, NULL, '2015-05-14 16:30:14'),
(9, 'Deluxe', 'Deluxe', NULL, '2000', NULL, 0, 0, '2015-04-27 14:47:07', '2015-04-27 14:47:13');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `code`, `name`, `value`, `visible`, `created`, `modified`) VALUES
(1, 'company_name', 'Company Name', '', 1, NULL, '2014-11-14 04:58:14'),
(2, 'address', 'Address', 'Tarlac City', 1, NULL, '2014-10-23 04:21:38'),
(3, 'logo', 'Logo', NULL, 0, NULL, NULL),
(4, 'email', 'Email', NULL, 1, NULL, NULL),
(5, 'telephone', 'Telephone', '+63 00 000 0000', 1, NULL, '2014-10-23 04:21:53'),
(6, 'fax', 'Fax', NULL, 1, NULL, NULL),
(7, 'chairman', 'Chairman', NULL, 1, NULL, NULL),
(8, 'general_manager', 'General Manager', NULL, 1, NULL, NULL),
(11, 'ehrms', 'eHRMS', 'eHRMS - L-SQUARE', 1, NULL, '2014-11-29 05:59:21'),
(12, 'system_title', 'System Title', 'HOTEL AND RESTAURANT MANAGEMENT SYSTEM', 1, NULL, '2014-11-25 01:47:13'),
(13, 'active_year', 'Active Year', '2014', 1, NULL, '2014-12-04 06:35:26');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `particulars` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `paid` tinyint(1) DEFAULT '0',
  `discount_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = percentage, 1 = fixed amount',
  `discount` decimal(11,2) DEFAULT '0.00',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `code`, `title`, `business_id`, `particulars`, `date`, `paid`, `discount_type`, `discount`, `visible`, `created`, `modified`) VALUES
(1, '000001', NULL, 1, 'Room accomodation charges.', '2015-05-07 00:00:00', 0, 1, '500.00', 1, '2015-05-07 04:01:07', '2015-05-07 08:05:10'),
(2, '000002', NULL, 1, 'Room accomodation charges.', '2015-05-07 00:00:00', 0, 1, '0.00', 1, '2015-05-07 04:02:04', '2015-05-07 04:02:04'),
(3, '000003', NULL, 1, 'Room accomodation charges.', '2015-05-12 00:00:00', 0, 1, '500.00', 1, '2015-05-12 19:03:12', '2015-05-12 23:30:51'),
(4, '000004', NULL, 1, 'Room accomodation charges.', '2015-05-12 00:00:00', 0, 1, '0.00', 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(5, '000005', NULL, 1, 'Room accomodation charges.', '2015-05-12 00:00:00', 0, 0, '50.00', 1, '2015-05-12 22:33:26', '2015-05-12 22:40:07'),
(6, '000006', NULL, 1, 'Room accomodation charges.', '2015-05-13 00:00:00', 0, 1, '0.00', 1, '2015-05-13 17:55:22', '2015-05-13 17:55:22'),
(7, '000007', NULL, 1, 'Room accomodation charges.', '2015-05-13 00:00:00', 0, 1, '0.00', 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(8, '000008', NULL, 1, 'Room accomodation charges.', '2015-05-13 00:00:00', 0, 1, '0.00', 1, '2015-05-13 21:22:18', '2015-05-13 21:22:18');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_subs`
--

CREATE TABLE IF NOT EXISTS `transaction_subs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `particulars` varchar(255) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `paid` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `transaction_subs`
--

INSERT INTO `transaction_subs` (`id`, `transaction_id`, `particulars`, `amount`, `paid`, `visible`, `created`, `modified`) VALUES
(1, 1, 'Room accomodation charge ( 05/07/2015 )', '1200', 0, 1, '2015-05-07 04:01:07', '2015-05-07 04:01:07'),
(2, 2, 'Room accomodation charge ( 05/08/2015 )', '1200', 0, 1, '2015-05-07 04:02:04', '2015-05-07 04:02:04'),
(3, 2, 'Room accomodation charge ( 05/09/2015 )', '1200', 0, 1, '2015-05-07 04:02:04', '2015-05-07 04:02:04'),
(4, 3, 'Room accomodation charge ( 05/15/2015 )', '2100', 0, 1, '2015-05-12 19:03:12', '2015-05-12 19:03:12'),
(5, 4, 'Room accomodation charge ( 05/15/2015 )', '1200', 0, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(6, 4, 'Room accomodation charge ( 05/16/2015 )', '1200', 0, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(7, 4, 'Room accomodation charge ( 05/17/2015 )', '1200', 0, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(8, 4, 'Room accomodation charge ( 05/18/2015 )', '1200', 0, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(9, 4, 'Room accomodation charge ( 05/19/2015 )', '1200', 0, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(10, 4, 'Room accomodation charge ( 05/20/2015 )', '1200', 0, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(11, 4, 'Room accomodation charge ( 05/21/2015 )', '1200', 0, 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(12, 5, 'Room accomodation charge ( 05/22/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(13, 5, 'Room accomodation charge ( 05/23/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(14, 5, 'Room accomodation charge ( 05/24/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(15, 5, 'Room accomodation charge ( 05/25/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(16, 5, 'Room accomodation charge ( 05/26/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(17, 5, 'Room accomodation charge ( 05/27/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(18, 5, 'Room accomodation charge ( 05/28/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(19, 5, 'Room accomodation charge ( 05/29/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(20, 5, 'Room accomodation charge ( 05/30/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(21, 5, 'Room accomodation charge ( 05/31/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(22, 5, 'Room accomodation charge ( 06/01/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(23, 5, 'Room accomodation charge ( 06/02/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(24, 5, 'Room accomodation charge ( 06/03/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(25, 5, 'Room accomodation charge ( 06/04/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(26, 5, 'Room accomodation charge ( 06/05/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(27, 5, 'Room accomodation charge ( 06/06/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(28, 5, 'Room accomodation charge ( 06/07/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(29, 5, 'Room accomodation charge ( 06/08/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(30, 5, 'Room accomodation charge ( 06/09/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(31, 5, 'Room accomodation charge ( 06/10/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(32, 5, 'Room accomodation charge ( 06/11/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(33, 5, 'Room accomodation charge ( 06/12/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(34, 5, 'Room accomodation charge ( 06/13/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(35, 5, 'Room accomodation charge ( 06/14/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(36, 5, 'Room accomodation charge ( 06/15/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(37, 5, 'Room accomodation charge ( 06/16/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(38, 5, 'Room accomodation charge ( 06/17/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(39, 5, 'Room accomodation charge ( 06/18/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(40, 5, 'Room accomodation charge ( 06/19/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(41, 5, 'Room accomodation charge ( 06/20/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(42, 5, 'Room accomodation charge ( 06/21/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(43, 5, 'Room accomodation charge ( 06/22/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(44, 5, 'Room accomodation charge ( 06/23/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(45, 5, 'Room accomodation charge ( 06/24/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(46, 5, 'Room accomodation charge ( 06/25/2015 )', '1200', 0, 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(47, NULL, NULL, NULL, 0, 1, '2015-05-13 17:55:22', '2015-05-13 17:55:22'),
(48, 7, 'Room accomodation charge ( 05/15/2015 )', '1200', 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(49, 7, 'Room accomodation charge ( 05/16/2015 )', '1200', 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(50, 7, 'Room accomodation charge ( 05/17/2015 )', '1200', 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(51, 7, 'Room accomodation charge ( 05/18/2015 )', '1200', 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(52, 7, 'Room accomodation charge ( 05/19/2015 )', '1200', 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(53, 7, 'Room accomodation charge ( 05/20/2015 )', '1200', 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(54, 7, 'Room accomodation charge ( 05/21/2015 )', '1200', 0, 1, '2015-05-13 20:24:58', '2015-05-13 20:24:58'),
(55, 8, 'Room accomodation charge ( 05/27/2015 )', '2100', 0, 1, '2015-05-13 21:22:19', '2015-05-13 21:22:19'),
(56, 8, 'Room accomodation charge ( 05/28/2015 )', '2100', 0, 1, '2015-05-13 21:22:19', '2015-05-13 21:22:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `high_level` tinyint(1) DEFAULT '0',
  `activated` tinyint(1) DEFAULT '0',
  `image` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
<<<<<<< HEAD
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;
=======
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;
>>>>>>> 7acb0cfb8c06106e084697072eb8d5075fec5861

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `last_name`, `first_name`, `middle_name`, `role`, `high_level`, `activated`, `image`, `visible`, `created`, `modified`) VALUES
(1, 'red', '8daad8c7e7bf1d3da90e93471a11c25d371d3356', NULL, NULL, NULL, 'superuser', 1, 1, NULL, 1, '2014-10-13 00:00:00', '2015-01-13 13:18:23'),
<<<<<<< HEAD
(3, 'demo@frontoffice', 'ef44c7044f064fef450e433c0eb95a570adc15f8', NULL, NULL, NULL, 'frontoffice', 0, 1, NULL, 1, NULL, '2015-05-15 04:59:02'),
(4, 'demo@resto', 'ef44c7044f064fef450e433c0eb95a570adc15f8', NULL, NULL, NULL, 'resto', 1, 1, NULL, 1, NULL, '2015-01-13 13:18:23'),
(5, 'demo@bar', 'ef44c7044f064fef450e433c0eb95a570adc15f8', NULL, NULL, NULL, 'bar', 1, 1, NULL, 1, NULL, '2015-05-04 21:11:32'),
=======
(3, 'demo@frontoffice', 'ef44c7044f064fef450e433c0eb95a570adc15f8', NULL, NULL, NULL, 'frontoffice', 0, 1, NULL, 1, NULL, '2015-05-25 15:57:21'),
(4, 'demo@resto', 'ef44c7044f064fef450e433c0eb95a570adc15f8', NULL, NULL, NULL, 'resto', 1, 1, NULL, 1, NULL, '2015-05-26 08:12:48'),
(5, 'demo@bar', 'ef44c7044f064fef450e433c0eb95a570adc15f8', NULL, NULL, NULL, 'bar', 1, 1, NULL, 1, NULL, '2015-05-26 09:23:01'),
>>>>>>> 7acb0cfb8c06106e084697072eb8d5075fec5861
(6, 'demo@admin', 'ef44c7044f064fef450e433c0eb95a570adc15f8', NULL, NULL, NULL, 'superuser', 1, 1, NULL, 1, '2014-10-13 00:00:00', '2015-01-13 13:18:23');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
