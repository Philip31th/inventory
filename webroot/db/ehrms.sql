-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2015 at 04:22 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ehrms`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomodations`
--

CREATE TABLE IF NOT EXISTS `accomodations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `accomodations`
--

INSERT INTO `accomodations` (`id`, `code`, `name`, `number`, `visible`, `created`, `modified`) VALUES
(1, 'single', 'Single', 1, 1, '2015-05-06 23:50:00', '2015-05-06 23:50:00'),
(2, 'double', 'Double', 2, 1, '2015-05-06 23:50:00', '2015-05-06 23:50:00'),
(3, 'triple', 'Triple', 3, 1, '2015-05-06 23:50:00', '2015-05-06 23:50:00'),
(4, 'quadruple', 'Quadruple', 4, 1, '2015-05-06 23:50:00', '2015-05-06 23:50:00'),
(5, 'sextuple', 'Sextuple', 5, 1, '2015-05-06 23:50:00', '2015-05-06 23:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE IF NOT EXISTS `businesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `departmentId` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `code`, `name`, `description`, `departmentId`, `visible`, `created`, `modified`) VALUES
(1, 'hotel', 'Hotel', 'Hotel', NULL, 1, NULL, NULL),
(2, 'restaurant', 'Restaurant', 'Restaurant', NULL, 1, NULL, NULL),
(3, 'bar', 'Bar', 'Bar', NULL, 1, NULL, '2015-06-02 09:37:24'),
(4, 'travelers-shop', 'Travelers Shop', 'Travelers Shop', NULL, 1, NULL, NULL),
(5, 'salon', 'Salon', 'Salon', NULL, 1, NULL, NULL),
(6, 'business-center', 'Business Center', 'Business Center', NULL, 1, NULL, NULL),
(7, 'events', 'Events', 'Events', NULL, 1, NULL, NULL),
(8, 'laundry', 'Laundry', 'Laundry', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `business_services`
--

CREATE TABLE IF NOT EXISTS `business_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `type` varchar(50) DEFAULT 'fixed' COMMENT 'fixed, percent',
  `value` decimal(11,2) DEFAULT NULL,
  `businessId` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `business_services`
--

INSERT INTO `business_services` (`id`, `code`, `name`, `description`, `type`, `value`, `businessId`, `visible`, `created`, `modified`) VALUES
(1, 'service-1', 'Service 1', 'service 1', 'fixed', '100.00', 3, 0, '2015-06-02 12:02:11', '2015-06-02 12:10:31'),
(2, 'hello-world', 'Hello World', 'hello world', 'fixed', '600.00', 3, 0, '2015-06-02 12:03:09', '2015-06-02 12:11:31'),
(3, 'service-2', 'Service 2', 'test', 'fixed', '100.00', 3, 0, '2015-06-02 12:04:07', '2015-06-02 12:12:00'),
(4, 'service-3', 'Service 3', 'test', 'fixed', '300.00', 3, 0, '2015-06-02 12:04:15', '2015-06-02 12:11:57'),
(5, 'service-10', 'Service 10', 'service 1', 'fixed', '250.00', 3, 0, '2015-06-02 12:12:41', '2015-06-02 12:48:13'),
(6, 'service-2', 'Service 2', 'test', 'fixed', '500.00', 3, 0, '2015-06-02 12:37:13', '2015-06-02 12:37:34'),
(7, 'service-1', 'Service 1', 'service 1', 'fixed', '100.00', 3, 1, '2015-06-02 12:47:51', '2015-06-02 12:47:51'),
(8, 'laba-laba', 'Laba Laba', 'laba laba', 'fixed', '900.00', 8, 1, '2015-06-05 13:36:31', '2015-06-05 13:36:31');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contact_person_title` varchar(255) DEFAULT NULL,
  `contact_person_name` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `code`, `name`, `description`, `visible`, `created`, `modified`) VALUES
(1, 'restaurant', 'Restaurant', 'Restodsogvkn', 1, NULL, NULL),
(2, 'bar', 'Bar', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL COMMENT 'fixed, percent',
  `value` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `name`, `type`, `value`, `visible`, `created`, `modified`) VALUES
(2, 'Senior', 'percent', 20, 1, '2015-05-14 01:04:23', '2015-05-14 01:04:23'),
(3, 'test', 'percent', 2, 0, '2015-06-03 20:32:27', '2015-06-03 20:37:47'),
(4, 'test', 'percent', 2, 0, '2015-06-03 20:32:33', '2015-06-03 20:37:42'),
(5, 'test', 'percent', 10, 0, '2015-06-03 20:36:33', '2015-06-03 20:37:39'),
(6, 'test', 'fixed', 1, 0, '2015-06-03 20:36:45', '2015-06-03 20:37:37'),
(7, 'qwe', 'fixed', 1, 0, '2015-06-03 20:37:29', '2015-06-03 20:37:50'),
(8, 'test', 'fixed', 2, 0, '2015-06-03 20:58:33', '2015-06-03 20:58:41'),
(9, 'tes', 'percent', 1, 0, '2015-06-03 21:04:34', '2015-06-03 21:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeNumber` varchar(100) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `departmentId` int(11) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `image` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `employeeNumber`, `lastName`, `firstName`, `middleName`, `departmentId`, `position`, `image`, `visible`, `created`, `modified`) VALUES
(1, NULL, 'test', 'tesad', 'asd', 1, 'qwe', NULL, 1, '2015-06-03 13:27:19', '2015-06-03 13:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `folios`
--

CREATE TABLE IF NOT EXISTS `folios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `guestId` int(11) DEFAULT NULL,
  `arrival` datetime DEFAULT NULL,
  `departure` datetime DEFAULT NULL,
  `nights` int(11) DEFAULT NULL,
  `rate` decimal(11,2) DEFAULT '0.00',
  `adults` int(11) DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `exta_bed` int(11) DEFAULT NULL,
  `bookingSource` varchar(255) DEFAULT NULL,
  `sendBillTo` varchar(255) DEFAULT NULL,
  `specialRequest` varchar(5000) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `roomId` int(11) DEFAULT NULL,
  `reservationRoomId` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `closed` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `folios`
--

INSERT INTO `folios` (`id`, `code`, `guestId`, `arrival`, `departure`, `nights`, `rate`, `adults`, `children`, `exta_bed`, `bookingSource`, `sendBillTo`, `specialRequest`, `remarks`, `roomId`, `reservationRoomId`, `paid`, `closed`, `visible`, `created`, `modified`) VALUES
(1, '00001', 1, '2015-06-09 00:00:00', '2015-06-09 00:00:00', 1, '2300.00', 2, 0, NULL, 'reservation', NULL, 'request', 'sample remarks', 6, 1, 0, 0, 1, '2015-06-09 14:30:41', '2015-06-09 14:30:41'),
(2, '00002', 2, '2015-06-27 00:00:00', '2015-12-28 00:00:00', NULL, '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 53, NULL, 0, 0, 1, '2015-06-27 09:40:45', '2015-06-27 09:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `folio_payments`
--

CREATE TABLE IF NOT EXISTS `folio_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `folioId` int(11) DEFAULT NULL,
  `transactionId` int(11) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `folio_payments`
--

INSERT INTO `folio_payments` (`id`, `code`, `folioId`, `transactionId`, `amount`, `date`, `type`, `visible`, `created`, `modified`) VALUES
(1, '001', 1, 1, '1200', '2015-05-07 00:00:00', 'CASH', 1, '2015-05-07 04:01:07', '2015-05-07 04:01:07'),
(2, '002', 1, 2, '1000', '2015-05-07 00:00:00', 'CASH', 1, '2015-05-07 04:02:04', '2015-05-07 04:02:04'),
(3, '010225', 3, 3, '300', '2015-05-12 00:00:00', 'CASH', 1, '2015-05-12 19:03:12', '2015-05-12 19:03:12'),
(4, '1100', 4, 4, '1000', '2015-05-12 00:00:00', 'CASH', 1, '2015-05-12 21:52:45', '2015-05-12 21:52:45'),
(5, '111', 5, 5, '11', '2015-05-12 00:00:00', 'CASH', 1, '2015-05-12 22:33:26', '2015-05-12 22:33:26'),
(6, '1313', 6, 6, '200', '2015-05-13 00:00:00', 'CASH', 1, '2015-05-13 17:55:22', '2015-05-13 17:55:22'),
(7, '12366565', 7, 7, '200', '2015-05-13 00:00:00', 'CASH', 1, '2015-05-13 20:24:59', '2015-05-13 20:24:59'),
(8, '0000', 8, 8, '200', '2015-05-13 00:00:00', 'CASH', 1, '2015-05-13 21:22:19', '2015-05-13 21:22:19');

-- --------------------------------------------------------

--
-- Table structure for table `folio_transactions`
--

CREATE TABLE IF NOT EXISTS `folio_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folioId` int(11) DEFAULT NULL,
  `transactionId` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `folio_transactions`
--

INSERT INTO `folio_transactions` (`id`, `folioId`, `transactionId`, `visible`, `created`, `modified`) VALUES
(1, 1, 1, 1, '2015-06-09 14:30:42', '2015-06-09 14:30:42'),
(2, 1, 2, 1, '2015-06-25 16:46:05', '2015-06-25 16:46:05'),
(3, NULL, NULL, 1, '2015-06-25 19:21:23', '2015-06-25 19:21:23'),
(4, 2, 4, 1, '2015-06-27 09:40:46', '2015-06-27 09:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `guests`
--

CREATE TABLE IF NOT EXISTS `guests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `companyId` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `attachment` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `guests`
--

INSERT INTO `guests` (`id`, `code`, `title`, `lastName`, `firstName`, `middleName`, `gender`, `nationality`, `address`, `companyId`, `email`, `phone`, `mobile`, `attachment`, `visible`, `created`, `modified`) VALUES
(1, '00001', NULL, 'Anciro', 'Wilson', NULL, 'male', 'Filipino', 'Olongapo City', NULL, NULL, NULL, '09091234378', '1.jpg', 1, '2015-06-09 14:30:41', '2015-06-09 14:30:41'),
(2, '00002', NULL, 'baldos', 'veronica', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2015-06-27 09:40:45', '2015-06-27 09:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departmentId` int(11) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `departmentId`, `code`, `name`, `description`, `visible`, `created`, `modified`) VALUES
(1, 1, '100', 'Cake', 'Vanilla Cake', 1, '2015-05-25 16:05:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inventory_subs`
--

CREATE TABLE IF NOT EXISTS `inventory_subs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventoryId` int(11) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Ins, 0=Outs',
  `quantity` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `inventory_subs`
--

INSERT INTO `inventory_subs` (`id`, `inventoryId`, `type`, `quantity`, `visible`, `created`, `modified`) VALUES
(1, 1, 1, 15, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 0, 8, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, NULL, 1, 0, 1, '2015-06-08 08:28:35', '2015-06-08 08:28:35'),
(4, NULL, 1, 0, 1, '2015-06-08 08:28:42', '2015-06-08 08:28:42'),
(5, NULL, 1, 0, 1, '2015-06-08 08:28:44', '2015-06-08 08:28:44'),
(6, NULL, 1, 0, 1, '2015-06-08 08:28:44', '2015-06-08 08:28:44'),
(7, NULL, 1, 0, 1, '2015-06-08 08:29:02', '2015-06-08 08:29:02'),
(8, NULL, 1, 0, 1, '2015-06-08 08:29:23', '2015-06-08 08:29:23'),
(9, NULL, 1, 0, 1, '2015-06-08 08:50:46', '2015-06-08 08:50:46'),
(10, NULL, 1, 0, 1, '2015-06-08 08:50:54', '2015-06-08 08:50:54'),
(11, NULL, 1, 0, 1, '2015-06-08 08:50:55', '2015-06-08 08:50:55'),
(12, NULL, 1, 0, 1, '2015-06-08 14:24:49', '2015-06-08 14:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `amount` decimal(11,2) DEFAULT NULL,
  `departmentId` int(11) DEFAULT NULL,
  `image` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `code`, `name`, `description`, `amount`, `departmentId`, `image`, `visible`, `created`, `modified`) VALUES
(1, 'bar-menu-1', 'Bar Menu 1', '', '250.00', 2, NULL, 1, '2015-06-03 12:44:47', '2015-06-03 12:44:47'),
(2, 'test', 'Test', '123123', '200.00', 1, NULL, 0, '2015-06-04 10:41:59', '2015-06-04 10:42:30'),
(3, 'test', 'Test', 'qwe', '100.00', 1, NULL, 0, '2015-06-04 10:42:40', '2015-06-04 19:26:07'),
(4, 'tests', 'Tests', 'qwe', '100.00', 1, NULL, 0, '2015-06-04 10:42:48', '2015-06-04 11:00:33'),
(5, 'kkk', 'Kkk', 'asdasd', '100.00', 1, NULL, 0, '2015-06-04 10:42:53', '2015-06-04 11:37:51'),
(6, 'asdasd', 'Asdasd', 'xxxx', '260.00', 2, NULL, 0, '2015-06-04 10:43:11', '2015-06-04 12:06:59'),
(7, 'tests', 'Tests', 'qwe', '100.00', 1, NULL, 0, '2015-06-04 11:37:40', '2015-06-04 19:26:09'),
(8, 'resto-menu-1', 'Resto Menu 1', '', '200.00', 1, NULL, 1, '2015-06-04 19:21:35', '2015-06-04 19:21:35'),
(9, 'resto-menu-3', 'Resto Menu 3', 'test', '450.00', 1, NULL, 1, '2015-06-04 19:24:38', '2015-06-04 19:24:38'),
(10, 'resto-menu-2', 'Resto Menu 2', '', '160.00', 1, NULL, 1, '2015-06-04 19:25:57', '2015-06-04 19:25:57'),
(11, 'bar-menu-2', 'Bar Menu 2', '', '240.00', 2, NULL, 1, '2015-06-04 19:31:55', '2015-06-04 19:31:55'),
(12, 'bar-menu-3', 'Bar Menu 3', '', '256.00', 2, NULL, 1, '2015-06-04 19:32:01', '2015-06-04 19:32:01'),
(13, 'bar-menu-4', 'Bar Menu 4', '', '566.00', 2, NULL, 1, '2015-06-04 19:32:54', '2015-06-04 19:32:54'),
(14, 'aaxx', 'Aaxx', '', '233.00', 2, NULL, 1, '2015-06-04 19:33:10', '2015-06-04 19:33:10'),
(15, 'sinigang', 'Sinigang', '', '190.50', 1, NULL, 1, '2015-06-05 13:25:06', '2015-06-05 13:25:06'),
(16, 'adobo-chicken', 'Adobo Chicken', '', '150.00', 1, NULL, 0, '2015-06-08 10:16:16', '2015-06-08 10:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `penalties`
--

CREATE TABLE IF NOT EXISTS `penalties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `value` decimal(11,2) NOT NULL DEFAULT '0.00',
  `type` varchar(50) NOT NULL DEFAULT 'fixed',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `penalties`
--

INSERT INTO `penalties` (`id`, `code`, `name`, `description`, `value`, `type`, `visible`, `created`, `modified`) VALUES
(1, NULL, 'Smoking', NULL, '500.00', 'fixed', 1, NULL, '2015-05-30 16:04:03'),
(2, NULL, NULL, NULL, '0.00', 'fixed', 0, '2015-05-30 15:41:23', '2015-05-30 15:50:06'),
(3, NULL, NULL, NULL, '0.00', 'fixed', 0, '2015-05-30 15:43:05', '2015-05-30 15:50:09'),
(4, NULL, 'test', NULL, '0.00', 'fixed', 0, '2015-05-30 15:43:39', '2015-05-30 15:50:15'),
(5, NULL, 'testasd', NULL, '9.00', 'percent', 0, '2015-05-30 16:04:17', '2015-06-03 21:14:46'),
(6, NULL, 'tasdasdasd', NULL, '2.00', 'fixed', 0, '2015-06-03 21:10:51', '2015-06-03 21:11:06');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `name`, `module`, `controller`, `action`, `description`, `visible`, `created`, `modified`) VALUES
(1, NULL, 'can list room', 'hotel', 'rooms', 'index', NULL, 1, '2015-06-04 22:00:49', '2015-06-04 22:00:49'),
(2, NULL, 'can add room', 'hotel', 'rooms', 'add', NULL, 1, '2015-06-05 15:42:09', '2015-06-05 15:42:09'),
(3, NULL, 'can edit room', 'hotel', 'rooms', 'edit', NULL, 1, '2015-06-05 15:42:47', '2015-06-05 15:42:47'),
(4, NULL, 'can delete room', 'hotel', 'rooms', 'delete', NULL, 1, '2015-06-05 15:43:10', '2015-06-05 15:43:10'),
(5, NULL, 'can show hotel calendars', 'hotel', 'calendar', 'index', NULL, 1, '2015-06-05 15:43:46', '2015-06-05 15:43:46'),
(6, NULL, 'can list room type', 'hotel', 'room-types', 'index', NULL, 1, '2015-06-05 15:44:21', '2015-06-05 15:44:21'),
(7, NULL, 'can add room type', 'hotel', 'room-types', 'add', NULL, 1, '2015-06-05 15:44:42', '2015-06-05 15:44:42'),
(8, NULL, 'can edit room type', 'hotel', 'room-types', 'edit', NULL, 1, '2015-06-05 15:44:57', '2015-06-05 15:44:57'),
(9, NULL, 'can view room', 'hotel', 'rooms', 'view', NULL, 1, '2015-06-05 15:45:09', '2015-06-05 15:45:09'),
(10, NULL, 'can list menu', 'resto', 'menus', 'index', NULL, 1, '2015-06-05 16:02:56', '2015-06-05 16:02:56'),
(11, NULL, 'can delete room type', 'hotel', 'room-types', 'delete', NULL, 1, '2015-06-05 16:04:13', '2015-06-05 16:04:13'),
(12, NULL, 'can add menu', 'resto', 'menus', 'add', NULL, 1, '2015-06-05 16:04:15', '2015-06-05 16:04:15'),
(13, NULL, 'can edit menu', 'resto', 'menus', 'edit', NULL, 1, '2015-06-05 16:04:57', '2015-06-05 16:04:57'),
(14, NULL, 'can view menu', 'resto', 'menus', 'view', NULL, 1, '2015-06-05 16:06:09', '2015-06-05 16:06:09'),
(15, NULL, 'can delete menu', 'resto', 'menus', 'delete', NULL, 1, '2015-06-05 16:07:05', '2015-06-05 16:07:05'),
(16, NULL, 'can list table', 'resto', 'tables', 'index', NULL, 1, '2015-06-05 16:08:13', '2015-06-05 16:08:13'),
(17, NULL, 'can delete table', 'resto', 'tables', 'delete', NULL, 1, '2015-06-05 16:08:32', '2015-06-05 16:08:32'),
(18, NULL, 'can add table', 'resto', 'tables', 'add', NULL, 1, '2015-06-05 16:09:22', '2015-06-05 16:09:22'),
(19, NULL, 'can edit table', 'resto', 'tables', 'edit', NULL, 1, '2015-06-05 16:09:37', '2015-06-05 16:09:37'),
(20, NULL, 'can view table', 'resto', 'tables', 'view', NULL, 1, '2015-06-05 16:09:58', '2015-06-05 16:09:58'),
(21, NULL, 'can list inventory', 'resto', 'inventories', 'index', NULL, 1, '2015-06-05 16:10:50', '2015-06-05 16:10:50'),
(22, NULL, 'can add inventory', 'resto', 'inventories', 'add', NULL, 1, '2015-06-05 16:11:05', '2015-06-05 16:11:05'),
(23, NULL, 'can delete inventory', 'resto', 'inventories', 'delete', NULL, 1, '2015-06-05 16:11:18', '2015-06-05 16:11:18'),
(24, NULL, 'can view inventory', 'resto', 'inventories', 'view', NULL, 1, '2015-06-05 16:11:32', '2015-06-05 16:11:32'),
(25, NULL, 'can list supply', 'resto', 'suppliers', 'index', NULL, 1, '2015-06-05 16:12:05', '2015-06-05 16:12:05'),
(26, NULL, 'can delete supply', 'resto', 'suppliers', 'delete', NULL, 1, '2015-06-05 16:12:15', '2015-06-05 16:12:15'),
(27, NULL, 'can view supply', 'resto', 'suppliers', 'view', NULL, 1, '2015-06-05 16:12:28', '2015-06-05 16:12:28'),
(28, NULL, 'can edit supply', 'resto', 'suppliers', 'edit', NULL, 1, '2015-06-05 16:12:40', '2015-06-05 16:12:40'),
(29, NULL, 'can list bar menu', 'bar', 'menus', 'index', NULL, 1, '2015-06-05 16:13:50', '2015-06-05 16:13:50'),
(30, NULL, 'can list transaction', 'resto', 'transactions', 'index', NULL, 1, '2015-06-05 16:19:42', '2015-06-05 16:19:42'),
(31, NULL, 'can add transaction', 'resto', 'transactions', 'add', NULL, 1, '2015-06-05 16:19:58', '2015-06-05 16:19:58'),
(32, NULL, 'can edit transaction', 'resto', 'transactions', 'edit', NULL, 1, '2015-06-05 16:20:19', '2015-06-05 16:20:19'),
(33, NULL, 'can view transaction', 'resto', 'transactions', 'view', NULL, 1, '2015-06-05 16:20:34', '2015-06-05 16:20:34'),
(34, NULL, 'can add bar menu', 'bar', 'menus', 'add', NULL, 1, '2015-06-05 16:23:18', '2015-06-05 16:23:18'),
(35, NULL, 'can edit bar menu', 'bar', 'menus', 'edit', NULL, 1, '2015-06-05 16:24:38', '2015-06-05 16:24:38'),
(36, NULL, 'can delete bar menu', 'bar', 'menus', 'delete', NULL, 1, '2015-06-05 16:25:27', '2015-06-05 16:25:27'),
(37, NULL, 'can view bar menu', 'bar', 'menus', 'view', NULL, 1, '2015-06-05 16:27:35', '2015-06-05 16:27:35'),
(38, NULL, 'can add bar table', 'bar', 'tables', 'add', NULL, 1, '2015-06-05 16:28:37', '2015-06-05 16:28:37'),
(39, NULL, 'can edit bar table', 'bar', 'tables', 'edit', NULL, 1, '2015-06-05 16:33:15', '2015-06-05 16:33:15'),
(40, NULL, 'can view bar table', 'bar', 'tables', 'view', NULL, 1, '2015-06-05 16:33:31', '2015-06-05 16:33:31'),
(41, NULL, 'can delete bar table', 'bar', 'tables', 'delete', NULL, 1, '2015-06-05 16:33:56', '2015-06-05 16:33:56'),
(42, NULL, 'can list bar inventory', 'bar', 'inventories', 'index', NULL, 1, '2015-06-05 16:35:39', '2015-06-05 16:35:39'),
(43, NULL, 'can add bar inventory', 'bar', 'inventories', 'add', NULL, 1, '2015-06-05 16:37:35', '2015-06-05 16:37:35'),
(44, NULL, 'can edit bar inventory', 'bar', 'inventories', 'edit', NULL, 1, '2015-06-05 16:37:55', '2015-06-05 16:37:55'),
(45, NULL, 'can delete bar inventory', 'bar', 'inventories', 'delete', NULL, 1, '2015-06-05 16:38:15', '2015-06-05 16:38:15'),
(46, NULL, 'can view bar inventory', 'bar', 'inventories', 'view', NULL, 1, '2015-06-05 16:38:42', '2015-06-05 16:38:42'),
(47, NULL, 'can list bar supplier', 'bar', 'suppliers', 'index', NULL, 1, '2015-06-05 16:39:18', '2015-06-05 16:39:18'),
(48, NULL, 'can add bar supplier', 'bar', 'suppliers', 'add', NULL, 1, '2015-06-05 16:39:27', '2015-06-05 16:39:27'),
(49, NULL, 'can edit bar supplier', 'bar', 'suppliers', 'edit', NULL, 1, '2015-06-05 16:39:41', '2015-06-05 16:39:41'),
(50, NULL, 'can delete bar supplier', 'bar', 'suppliers', 'delete', NULL, 1, '2015-06-05 16:39:59', '2015-06-05 16:39:59'),
(51, NULL, 'can view bar supplier', 'bar', 'suppliers', 'view', NULL, 1, '2015-06-05 16:40:29', '2015-06-05 16:40:29'),
(52, NULL, 'can add bar transaction', 'bar', 'transactions', 'add', NULL, 1, '2015-06-05 16:41:09', '2015-06-05 16:41:09'),
(53, NULL, 'can list bar transaction', 'bar', 'transactions', 'index', NULL, 1, '2015-06-05 16:41:39', '2015-06-05 16:41:39'),
(54, NULL, 'can view bar transaction', 'bar', 'transactions', 'view', NULL, 1, '2015-06-05 16:42:07', '2015-06-05 16:42:07'),
(55, NULL, 'can edit bar transaction', 'bar', 'transactions', 'edit', NULL, 1, '2015-06-05 16:42:21', '2015-06-05 16:42:21'),
(56, NULL, 'can delete bar transaction', 'bar', 'transactions', 'delete', NULL, 1, '2015-06-05 16:42:40', '2015-06-05 16:42:40'),
(57, NULL, 'can list index', 'hotel', 'bookings', 'index', NULL, 1, '2015-06-06 10:05:06', '2015-06-06 10:05:06'),
(58, NULL, 'can add booking', 'hotel', 'bookings', 'add', NULL, 1, '2015-06-06 10:05:35', '2015-06-06 10:05:35'),
(59, NULL, 'can edit booking', 'hotel', 'bookings', 'edit', NULL, 1, '2015-06-06 10:05:48', '2015-06-06 10:05:48'),
(60, NULL, 'can view booking', 'hotel', 'bookings', 'view', NULL, 1, '2015-06-06 10:06:05', '2015-06-06 10:06:05'),
(61, NULL, 'can list cashiering', 'hotel', 'cashiering', 'index', NULL, 1, '2015-06-06 10:08:27', '2015-06-06 10:08:27'),
(62, NULL, 'can view cashiering', 'hotel', 'cashiering', 'view', NULL, 1, '2015-06-06 10:08:50', '2015-06-06 10:08:50'),
(63, NULL, 'can list guest', 'hotel', 'guests', 'index', NULL, 1, '2015-06-06 10:10:11', '2015-06-06 10:10:11'),
(64, NULL, 'can list outgoing guest', 'hotel', 'guests', 'outgoing', NULL, 1, '2015-06-06 10:17:10', '2015-06-06 10:17:10'),
(65, NULL, 'can list incoming guest', 'hotel', 'guests', 'incoming', NULL, 1, '2015-06-06 10:54:52', '2015-06-06 10:54:52'),
(66, NULL, 'cal list guest list', 'hotel', 'guests', 'list', NULL, 1, '2015-06-06 10:55:22', '2015-06-06 10:55:22'),
(67, NULL, 'can show hotel transactions', 'hotel', 'transactions', 'index', NULL, 1, '2015-06-06 12:59:30', '2015-06-06 12:59:30'),
(68, NULL, 'can list report', 'hotel', 'reports', 'index', NULL, 1, '2015-06-25 20:48:10', '2015-06-25 20:48:10');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `bookingSource` varchar(100) DEFAULT 'reservation' COMMENT 'reservation, walk in, travel agency',
  `arrival` datetime DEFAULT NULL,
  `departure` datetime DEFAULT NULL,
  `nights` int(11) DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `identity` varchar(255) DEFAULT NULL,
  `identityIdNumber` varchar(255) DEFAULT NULL,
  `billTo` varchar(255) DEFAULT NULL COMMENT 'company, group owner, guest',
  `paymentType` varchar(255) DEFAULT NULL COMMENT 'cash, credit card, cheque, send bill',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `code`, `title`, `lastName`, `firstName`, `middleName`, `gender`, `address`, `email`, `mobile`, `phone`, `nationality`, `bookingSource`, `arrival`, `departure`, `nights`, `adults`, `children`, `fax`, `identity`, `identityIdNumber`, `billTo`, `paymentType`, `closed`, `visible`, `created`, `modified`) VALUES
(1, NULL, NULL, 'Anciro', 'Wilson', NULL, '', 'Olongapo City', NULL, '09091234378', NULL, 'Filipino', 'reservation', '2015-06-09 00:00:00', '2015-06-09 00:00:00', 1, NULL, NULL, NULL, '', NULL, '', NULL, 0, 1, '2015-06-09 14:30:12', '2015-06-09 14:30:12'),
(3, NULL, NULL, 'Anciro', 'Wilson', NULL, NULL, 'Olongapo City', NULL, '09091234378', NULL, 'Filipino', 'reservation', '2015-06-09 00:00:00', '2015-06-09 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-09 14:30:12', '2015-06-27 00:23:56'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-26 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 19:46:47', '2015-06-27 00:24:01'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 19:58:28', '2015-06-27 00:24:04'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:00:36', '2015-06-27 00:25:00'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:29', '2015-06-27 00:24:57'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:31', '2015-06-27 00:24:08'),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:32', '2015-06-27 00:24:55'),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:32', '2015-06-27 00:24:52'),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:33', '2015-06-27 00:24:11'),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:33', '2015-06-27 00:24:14'),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:33', '2015-06-27 00:24:18'),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:34', '2015-06-27 00:24:49'),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:34', '2015-06-27 00:24:46'),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:34', '2015-06-27 00:24:43'),
(17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:34', '2015-06-27 00:24:40'),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:35', '2015-06-27 00:24:36'),
(19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:35', '2015-06-27 00:25:11'),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:35', '2015-06-27 00:24:27'),
(21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:35', '2015-06-27 00:32:35'),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:35', '2015-06-27 00:24:32'),
(23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:36', '2015-06-27 00:25:20'),
(24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'reservation', '0000-00-00 00:00:00', '2015-06-25 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2015-06-25 20:45:36', '2015-06-27 00:25:14'),
(25, NULL, 'Ms.', 'baldos', 'veron', NULL, 'Female', 'okay', NULL, '234', NULL, 'Filipino', 'walk in', '2015-06-27 00:00:00', '2015-07-04 00:00:00', 5, NULL, NULL, NULL, '', NULL, 'guest', 'cash', 0, 1, '2015-06-27 00:32:29', '2015-06-27 00:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `reservation_rooms`
--

CREATE TABLE IF NOT EXISTS `reservation_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservationId` int(11) DEFAULT NULL,
  `roomId` int(11) DEFAULT NULL,
  `arrival` datetime DEFAULT NULL,
  `departure` datetime DEFAULT NULL,
  `checkedIn` tinyint(1) DEFAULT '0',
  `checkedInDate` datetime DEFAULT NULL,
  `checkOutDate` datetime DEFAULT NULL,
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `reservation_rooms`
--

INSERT INTO `reservation_rooms` (`id`, `reservationId`, `roomId`, `arrival`, `departure`, `checkedIn`, `checkedInDate`, `checkOutDate`, `closed`, `visible`, `created`, `modified`) VALUES
(1, 1, 6, '2015-06-09 00:00:00', '2015-06-09 00:00:00', 1, '2015-06-09 00:00:00', NULL, 0, 1, '2015-06-09 14:30:12', '2015-06-09 14:30:41'),
(2, 25, 53, '2015-06-30 00:00:00', '2015-07-04 00:00:00', 0, NULL, NULL, 0, 1, '2015-06-27 00:32:29', '2015-06-27 00:32:29'),
(3, NULL, NULL, NULL, NULL, 1, '2015-06-27 00:00:00', NULL, 0, 1, '2015-06-27 09:40:45', '2015-06-27 09:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index` int(11) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rate` decimal(11,2) NOT NULL DEFAULT '0.00',
  `roomTypeId` int(11) DEFAULT NULL,
  `accomodationId` int(11) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `uncleaned` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `index`, `code`, `name`, `rate`, `roomTypeId`, `accomodationId`, `description`, `uncleaned`, `visible`, `created`, `modified`) VALUES
(5, 5, '205', '205', '3500.00', 3, 4, '', NULL, 1, NULL, '2015-02-05 14:18:48'),
(6, 6, '206', '206', '2300.00', 4, 2, '', NULL, 1, NULL, '2015-02-05 14:19:07'),
(14, NULL, '214', '214', '2100.00', 5, 2, '', NULL, 1, '2015-02-17 13:01:51', '2015-02-17 13:01:51'),
(16, NULL, '216', '216', '3900.00', 5, 2, '', NULL, 1, '2015-02-17 13:02:56', '2015-02-17 13:02:56'),
(53, NULL, '001', '001', '2500.00', 7, 2, '', 0, 1, '2015-06-03 11:51:01', '2015-06-03 11:51:01');

-- --------------------------------------------------------

--
-- Table structure for table `room_features`
--

CREATE TABLE IF NOT EXISTS `room_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `synced` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

CREATE TABLE IF NOT EXISTS `room_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `rate` decimal(11,2) DEFAULT '0.00',
  `accomodationId` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `synced` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `room_types`
--

INSERT INTO `room_types` (`id`, `code`, `name`, `description`, `rate`, `accomodationId`, `visible`, `synced`, `created`, `modified`) VALUES
(1, 'standard', 'Standard', NULL, '1200.00', 1, 1, 0, NULL, '2015-04-29 15:04:34'),
(2, 'deluxe_a', 'Deluxe A', NULL, '3100.00', 3, 1, 0, NULL, NULL),
(3, 'deluxe_b', 'Deluxe B', NULL, '3500.00', 4, 1, 0, NULL, NULL),
(4, 'superior', 'Superior', NULL, '2100.00', 2, 1, 0, NULL, NULL),
(5, 'super_twin', 'Super Twin', NULL, '2300.00', 2, 1, 0, NULL, NULL),
(6, 'presidential_suite', 'Presidential Suite', NULL, '6500.00', 1, 1, 0, NULL, NULL),
(7, 'executive_deluxe', 'Executive Deluxe', NULL, '2500.00', 2, 1, 0, NULL, '2015-05-14 16:30:08'),
(8, 'villa', 'Villa', NULL, '0.00', 4, 1, 0, NULL, '2015-05-14 16:30:14'),
(10, 'xxx', 'Xxx', NULL, '2500.00', 2, 0, 0, '2015-06-06 19:37:47', '2015-06-06 21:00:14');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `code`, `name`, `value`, `visible`, `created`, `modified`) VALUES
(1, 'company-name', 'Company Name', 'L-Square', 1, NULL, '2014-11-14 04:58:14'),
(2, 'address', 'Address', 'Tarlac City, Tarlac', 1, NULL, '2014-10-23 04:21:38'),
(3, 'logo', 'Logo', NULL, 1, NULL, NULL),
(4, 'email', 'Email', NULL, 1, NULL, NULL),
(5, 'telephone', 'Telephone', '+63 00 000 0000', 1, NULL, '2014-10-23 04:21:53'),
(6, 'fax', 'Fax', NULL, 1, NULL, NULL),
(7, 'chairman', 'Chairman', NULL, 1, NULL, NULL),
(8, 'general-manager', 'General Manager', NULL, 1, NULL, NULL),
(11, 'ehrms', 'Ehrms', 'L-SQUARE', 1, NULL, '2014-11-29 05:59:21'),
(12, 'system-title', 'System Title', 'MANAGEMENT SYSTEM', 1, NULL, '2014-11-25 01:47:13'),
(14, 'qwe', 'Qwe', 'asd', 0, '2015-06-03 19:03:34', '2015-06-03 19:06:41'),
(15, 'aasd', 'Aasd', 'asd', 0, '2015-06-03 19:04:09', '2015-06-03 19:06:32'),
(16, 'aasds', 'Aasds', 'asdasd', 0, '2015-06-03 19:04:17', '2015-06-03 19:06:37'),
(17, 'test', 'Test', 'data', 0, '2015-06-03 20:12:39', '2015-06-03 20:12:46'),
(18, 'admin-system-title', 'Admin System Title', 'ADMINISTRATIVE MANAGEMENT', 1, '2015-06-04 21:11:06', '2015-06-04 21:11:06'),
(19, 'front-office-system-', 'Front Office System Title', 'FRONT OFFICE MANAGEMENT', 1, '2015-06-04 21:12:53', '2015-06-04 21:12:53'),
(20, 'restaurant-system-ti', 'Restaurant System Title', 'RESTAURANT MANAGEMENT', 1, '2015-06-04 21:13:43', '2015-06-04 21:13:43'),
(21, 'bar-system-title', 'Bar System Title', 'BAR MANAGEMENT', 1, '2015-06-04 21:14:16', '2015-06-04 21:14:16'),
(22, 'hr-system-title', 'Hr System Title', 'H.R MANAGEMENT', 1, '2015-06-04 21:14:50', '2015-06-04 21:14:50'),
(23, 'booking-sources', 'Booking Sources', 'reservation,walk in,travel agency', 1, '2015-06-07 00:13:59', '2015-06-07 00:13:59'),
(25, 'payment-types', 'Payment Types', 'cash,cheque,credit card,send bill', 1, '2015-06-07 00:29:16', '2015-06-07 00:29:16');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contactNumber` varchar(55) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `departmentId` int(11) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `contactNumber`, `address`, `departmentId`, `visible`, `modified`, `created`) VALUES
(1, 'Resto Supplier 1', '09223655632', 'Balintawak', 1, 1, '2015-06-04 20:13:29', '2015-06-04 20:13:29'),
(5, 'Resto Supplier 2', NULL, NULL, 1, 1, '2015-06-04 20:38:25', '2015-06-04 20:38:25'),
(6, 'Resto Supplier 3', NULL, NULL, 1, 1, '2015-06-04 20:38:31', '2015-06-04 20:38:31'),
(7, 'Resto Supplier 4', NULL, NULL, 1, 1, '2015-06-04 20:38:53', '2015-06-04 20:38:53'),
(8, 'xxx', NULL, NULL, 1, 0, '2015-06-04 20:39:48', '2015-06-04 20:39:19'),
(9, 'Xxx', NULL, NULL, 1, 0, '2015-06-04 20:43:15', '2015-06-04 20:42:54'),
(10, 'Bar Supplier 1', NULL, NULL, 2, 1, '2015-06-04 20:55:35', '2015-06-04 20:55:35'),
(11, 'Bar Supplier 2', NULL, NULL, 2, 1, '2015-06-04 20:56:53', '2015-06-04 20:56:53'),
(12, 'Bar Supplier 3', NULL, NULL, 2, 1, '2015-06-04 20:57:01', '2015-06-04 20:57:01'),
(13, 'Bar Supplier 4', NULL, NULL, 2, 1, '2015-06-04 21:02:39', '2015-06-04 21:02:39'),
(14, 'Xxxx', NULL, NULL, 2, 0, '2015-06-04 21:02:55', '2015-06-04 21:02:52'),
(15, 'Supplier 6', '0', NULL, 1, 0, '2015-06-08 08:49:28', '2015-06-08 08:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_items`
--

CREATE TABLE IF NOT EXISTS `supplier_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `supplierId` int(11) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE IF NOT EXISTS `tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(2500) DEFAULT NULL,
  `departmentId` int(11) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`id`, `code`, `name`, `description`, `departmentId`, `visible`, `created`, `modified`) VALUES
(1, 'resto-table-7', 'Resto Table 7', '', 1, 1, NULL, '2015-06-04 19:28:41'),
(2, 'resto-table-2', 'Resto Table 2', '', 1, 1, '2015-06-04 19:28:52', '2015-06-04 19:28:52'),
(3, 'resto-table-3', 'Resto Table 3', '', 1, 1, '2015-06-04 19:29:06', '2015-06-04 19:29:06'),
(4, 'resto-table-4', 'Resto Table 4', '', 1, 1, '2015-06-04 19:29:11', '2015-06-04 19:29:11'),
(5, 'resto-table-5', 'Resto Table 5', '', 1, 0, '2015-06-04 19:29:18', '2015-06-08 10:17:01'),
(6, 'bar-table-1', 'Bar Table 1', '', 2, 1, '2015-06-04 19:35:35', '2015-06-04 19:35:35'),
(7, 'bar-table-2', 'Bar Table 2', '', 2, 1, '2015-06-04 19:35:40', '2015-06-04 19:35:40'),
(8, 'bar-table-3', 'Bar Table 3', '', 2, 1, '2015-06-04 19:35:45', '2015-06-04 19:35:45'),
(9, 'table', 'Table', '', 2, 0, '2015-06-08 08:27:40', '2015-06-08 08:27:54'),
(10, 'a', 'A', '', 1, 1, '2015-06-08 10:17:08', '2015-06-08 10:17:08'),
(11, 'resto-table-1', 'Resto Table 1', '', 1, 1, '2015-06-08 10:17:37', '2015-06-08 10:17:37'),
(12, 'resto-table-6', 'Resto Table 6', '', 1, 1, '2015-06-08 10:17:51', '2015-06-08 10:17:51'),
(13, 'resto-table-111', 'Resto Table 111', '', 1, 0, '2015-06-08 10:19:56', '2015-06-08 10:20:02'),
(14, 'resto-table-12', 'Resto Table 12', '', 1, 1, '2015-06-08 10:21:11', '2015-06-08 10:21:11'),
(15, 'resto-table-10', 'Resto Table 10', '', 1, 1, '2015-06-08 10:21:40', '2015-06-08 10:21:40'),
(16, 'resto-table-13', 'Resto Table 13', '', 1, 1, '2015-06-08 10:34:55', '2015-06-08 10:34:55'),
(17, 'resto-table-16', 'Resto Table 16', '', 1, 1, '2015-06-08 10:36:16', '2015-06-08 10:36:16'),
(18, 'resto-table-14', 'Resto Table 14', '', 1, 1, '2015-06-08 10:36:31', '2015-06-08 10:36:31'),
(19, 'resto-table-15', 'Resto Table 15', '', 1, 1, '2015-06-08 10:38:09', '2015-06-08 10:38:09'),
(20, 'resto-table-11', 'Resto Table 11', '', 1, 1, '2015-06-08 10:39:50', '2015-06-08 10:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'new',
  `createdBy` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `businessId` int(11) DEFAULT NULL,
  `particulars` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `code`, `title`, `businessId`, `particulars`, `date`, `visible`, `created`, `modified`) VALUES
(1, '000001', 'hotel charges', 1, 'room accomodation charges', '2015-06-09 00:00:00', 1, '2015-06-09 14:30:42', '2015-06-09 14:30:42'),
(2, '0002', 'Other charges.', 1, 'Hotel other charges.', '2015-06-25 00:00:00', 1, '2015-06-25 16:46:05', '2015-06-25 16:46:05'),
(3, '0003', 'Other charges.', NULL, 'Hotel other charges.', '2015-06-25 00:00:00', 1, '2015-06-25 19:21:23', '2015-06-25 19:21:23'),
(4, '000004', 'hotel charges', 1, 'room accomodation charges', '2015-06-27 00:00:00', 1, '2015-06-27 09:40:46', '2015-06-27 09:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_discounts`
--

CREATE TABLE IF NOT EXISTS `transaction_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionId` int(11) DEFAULT NULL,
  `value` decimal(11,2) DEFAULT '0.00',
  `type` varchar(20) DEFAULT NULL COMMENT 'fixed, percent',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_payments`
--

CREATE TABLE IF NOT EXISTS `transaction_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionId` int(11) DEFAULT NULL,
  `orNumber` varchar(50) DEFAULT NULL,
  `amount` decimal(11,2) DEFAULT '0.00',
  `change` decimal(11,2) NOT NULL DEFAULT '0.00',
  `paymentType` varchar(255) DEFAULT NULL COMMENT 'cash, cheque, credit card',
  `accountName` varchar(255) DEFAULT NULL,
  `cardNumber` varchar(255) DEFAULT NULL,
  `chequeNumber` varchar(255) DEFAULT NULL,
  `expirationYear` varchar(255) DEFAULT NULL,
  `expirationMonth` varchar(255) DEFAULT NULL,
  `bankName` varchar(255) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_subs`
--

CREATE TABLE IF NOT EXISTS `transaction_subs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionId` int(11) DEFAULT NULL,
  `particulars` varchar(255) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `paid` tinyint(1) DEFAULT '0',
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `transaction_subs`
--

INSERT INTO `transaction_subs` (`id`, `transactionId`, `particulars`, `amount`, `paid`, `visible`, `created`, `modified`) VALUES
(1, 1, 'room #206 accomodation charge', '2300.00', 0, 1, '2015-06-09 14:30:42', '2015-06-09 14:30:42'),
(2, 2, 'hotel other charges', '2000.00', 0, 1, '2015-06-09 14:30:42', '2015-06-09 14:30:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `employeeId` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `developer` tinyint(1) DEFAULT '0',
  `highLevel` tinyint(1) NOT NULL DEFAULT '0',
  `activated` tinyint(1) DEFAULT '0',
  `image` varchar(2500) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `lastName`, `firstName`, `middleName`, `employeeId`, `role`, `developer`, `highLevel`, `activated`, `image`, `visible`, `created`, `modified`) VALUES
(1, 'red', '3a71f32a8f3e0e9cb59f34b8dce5ba1a35cfec94', 'Anciro', 'Wilson', NULL, NULL, 'superuser', 1, 0, 1, NULL, 1, '2014-10-13 00:00:00', '2015-06-27 09:30:51'),
(3, 'frontoffice', '5d00e235af81508fddf417255dcf511586738d41', 'Administrator', 'Front Office', NULL, NULL, 'hotel', 0, 0, 1, NULL, 1, NULL, '2015-06-27 00:16:34'),
(4, 'resto', '5d00e235af81508fddf417255dcf511586738d41', 'Administrator', 'Restaurant', NULL, NULL, 'restaurant', 0, 0, 1, NULL, 1, NULL, '2015-06-05 16:30:24'),
(5, 'bar', '5d00e235af81508fddf417255dcf511586738d41', 'Administrator', 'Bar', NULL, NULL, 'bar', 0, 0, 1, NULL, 1, NULL, '2015-06-27 00:16:22'),
(6, 'admin', '5d00e235af81508fddf417255dcf511586738d41', '-', '- Administrator', NULL, NULL, 'admin', 0, 0, 1, NULL, 1, '2014-10-13 00:00:00', '2015-06-26 14:18:14'),
(12, 'philip', '0cdac71c9d6d41786b98bd2804329d2be0554ecd', 'Ordiz', 'Philip', NULL, NULL, 'admin', 1, 0, 1, NULL, 1, '2015-06-03 18:19:29', '2015-06-06 19:40:01'),
(13, 'veron', '0cdac71c9d6d41786b98bd2804329d2be0554ecd', 'Baldos', 'Veronica', NULL, NULL, 'admin', 1, 0, 1, NULL, 1, '2015-06-03 18:19:53', '2015-06-27 09:30:51'),
(14, 'emil', '7ebcb5ec53f06daf126c4043f593e783fd520116', 'Lacson', 'Emiliano', NULL, NULL, 'admin', 1, 0, 1, NULL, 1, '2015-06-06 09:45:16', '2015-06-06 10:23:39'),
(15, 'Nickz', 'd76ff09dc5277bae07e06d568995d4ebd4c8420f', 'gfvmh', 'sgfgn', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2015-06-25 16:54:44', '2015-06-25 16:55:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE IF NOT EXISTS `user_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `action` varchar(500) DEFAULT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=239 ;

--
-- Dumping data for table `user_logs`
--

INSERT INTO `user_logs` (`id`, `userId`, `action`, `description`, `visible`, `created`, `modified`) VALUES
(1, 13, 'action', 'sample', 1, '2015-06-25 05:33:00', NULL),
(2, 15, 'action', 'sample', 1, '2015-06-25 05:33:00', NULL),
(3, 13, 'Log In', 'From __ department', 1, '2015-06-25 18:19:35', '2015-06-25 18:19:35'),
(4, 13, 'Log Out', 'From __ department', 1, '2015-06-25 18:22:46', '2015-06-25 18:22:46'),
(5, 13, 'Log In', 'From __ department', 1, '2015-06-25 18:22:55', '2015-06-25 18:22:55'),
(6, 13, 'Log In', 'From __ department', 1, '2015-06-25 18:23:05', '2015-06-25 18:23:05'),
(7, 13, 'Log Out', 'logged out', 1, '2015-06-25 18:25:25', '2015-06-25 18:25:25'),
(8, 13, 'Log In', 'From __ department', 1, '2015-06-25 18:25:32', '2015-06-25 18:25:32'),
(9, 13, 'Print', 'user logs', 1, '2015-06-25 18:49:48', '2015-06-25 18:49:48'),
(10, 13, 'Print', 'user logs', 1, '2015-06-25 18:50:23', '2015-06-25 18:50:23'),
(11, 13, 'Print', 'statement of account', 1, '2015-06-25 18:55:37', '2015-06-25 18:55:37'),
(12, 13, 'Print', 'user logs', 1, '2015-06-25 18:56:42', '2015-06-25 18:56:42'),
(13, 13, 'Print', 'user logs', 1, '2015-06-25 18:59:59', '2015-06-25 18:59:59'),
(14, 13, 'Print', 'user logs', 1, '2015-06-25 19:10:04', '2015-06-25 19:10:04'),
(15, 13, 'Log Out', 'logged out', 1, '2015-06-25 19:13:32', '2015-06-25 19:13:32'),
(16, 1, 'Log In', 'logged in', 1, '2015-06-25 19:13:40', '2015-06-25 19:13:40'),
(17, 1, 'Print', 'user logs', 1, '2015-06-25 19:14:19', '2015-06-25 19:14:19'),
(18, 1, 'Print', 'user logs', 1, '2015-06-25 19:15:39', '2015-06-25 19:15:39'),
(19, 1, 'Print', 'user logs', 1, '2015-06-25 19:15:50', '2015-06-25 19:15:50'),
(20, 1, 'Print', 'user logs', 1, '2015-06-25 19:16:11', '2015-06-25 19:16:11'),
(21, 1, 'Log Out', 'logged out', 1, '2015-06-25 19:45:29', '2015-06-25 19:45:29'),
(22, 13, 'Log In', 'logged in', 1, '2015-06-25 19:45:39', '2015-06-25 19:45:39'),
(23, 13, 'Log Out', 'logged out', 1, '2015-06-25 20:44:14', '2015-06-25 20:44:14'),
(24, 3, 'Log In', 'logged in', 1, '2015-06-25 20:44:36', '2015-06-25 20:44:36'),
(25, 3, 'Log Out', 'logged out', 1, '2015-06-25 20:46:23', '2015-06-25 20:46:23'),
(26, 13, 'Log In', 'logged in', 1, '2015-06-25 20:46:31', '2015-06-25 20:46:31'),
(27, 13, 'Log Out', 'logged out', 1, '2015-06-25 20:48:13', '2015-06-25 20:48:13'),
(28, 3, 'Log In', 'logged in', 1, '2015-06-25 20:48:17', '2015-06-25 20:48:17'),
(29, 13, 'Log In', 'logged in', 1, '2015-06-25 20:52:59', '2015-06-25 20:52:59'),
(30, 3, 'Log Out', 'logged out', 1, '2015-06-25 21:01:54', '2015-06-25 21:01:54'),
(31, 5, 'Log In', 'logged in', 1, '2015-06-25 21:02:14', '2015-06-25 21:02:14'),
(32, 5, 'Log In', 'logged in', 1, '2015-06-26 07:19:42', '2015-06-26 07:19:42'),
(33, 5, 'Log Out', 'logged out', 1, '2015-06-26 07:26:38', '2015-06-26 07:26:38'),
(34, 13, 'Log In', 'logged in', 1, '2015-06-26 07:26:47', '2015-06-26 07:26:47'),
(35, 13, 'Print', 'statement of account', 1, '2015-06-26 11:15:58', '2015-06-26 11:15:58'),
(36, 13, 'Print', 'statement of account', 1, '2015-06-26 11:18:55', '2015-06-26 11:18:55'),
(37, 13, 'Log In', 'logged in', 1, '2015-06-26 11:31:03', '2015-06-26 11:31:03'),
(38, 13, 'Print', 'statement of account', 1, '2015-06-26 11:31:48', '2015-06-26 11:31:48'),
(39, 13, 'Print', 'statement of account', 1, '2015-06-26 11:36:22', '2015-06-26 11:36:22'),
(40, 13, 'Print', 'statement of account', 1, '2015-06-26 11:37:22', '2015-06-26 11:37:22'),
(41, 13, 'Print', 'statement of account', 1, '2015-06-26 11:38:59', '2015-06-26 11:38:59'),
(42, 13, 'Print', 'statement of account', 1, '2015-06-26 11:39:12', '2015-06-26 11:39:12'),
(43, 13, 'Print', 'statement of account', 1, '2015-06-26 11:42:37', '2015-06-26 11:42:37'),
(44, 13, 'Print', 'statement of account', 1, '2015-06-26 11:56:01', '2015-06-26 11:56:01'),
(45, 13, 'Print', 'statement of account', 1, '2015-06-26 11:56:55', '2015-06-26 11:56:55'),
(46, 13, 'Print', 'statement of account', 1, '2015-06-26 11:57:00', '2015-06-26 11:57:00'),
(47, 13, 'Print', 'statement of account', 1, '2015-06-26 11:57:26', '2015-06-26 11:57:26'),
(48, 13, 'Print', 'statement of account', 1, '2015-06-26 11:59:42', '2015-06-26 11:59:42'),
(49, 13, 'Print', 'statement of account', 1, '2015-06-26 12:00:14', '2015-06-26 12:00:14'),
(50, 13, 'Print', 'statement of account', 1, '2015-06-26 12:04:19', '2015-06-26 12:04:19'),
(51, 13, 'Print', 'statement of account', 1, '2015-06-26 12:04:22', '2015-06-26 12:04:22'),
(52, 13, 'Print', 'statement of account', 1, '2015-06-26 12:07:19', '2015-06-26 12:07:19'),
(53, 13, 'Print', 'statement of account', 1, '2015-06-26 12:08:14', '2015-06-26 12:08:14'),
(54, 13, 'Print', 'statement of account', 1, '2015-06-26 12:14:10', '2015-06-26 12:14:10'),
(55, 13, 'Print', 'statement of account', 1, '2015-06-26 12:15:03', '2015-06-26 12:15:03'),
(56, 13, 'Print', 'statement of account', 1, '2015-06-26 12:15:15', '2015-06-26 12:15:15'),
(57, 13, 'Print', 'statement of account', 1, '2015-06-26 12:56:06', '2015-06-26 12:56:06'),
(58, 13, 'Print', 'statement of account', 1, '2015-06-26 12:56:36', '2015-06-26 12:56:36'),
(59, 13, 'Print', 'statement of account', 1, '2015-06-26 12:59:31', '2015-06-26 12:59:31'),
(60, 13, 'Print', 'statement of account', 1, '2015-06-26 12:59:56', '2015-06-26 12:59:56'),
(61, 13, 'Print', 'statement of account', 1, '2015-06-26 13:00:53', '2015-06-26 13:00:53'),
(62, 13, 'Print', 'statement of account', 1, '2015-06-26 13:01:10', '2015-06-26 13:01:10'),
(63, 13, 'Print', 'statement of account', 1, '2015-06-26 13:03:04', '2015-06-26 13:03:04'),
(64, 13, 'Print', 'statement of account', 1, '2015-06-26 13:06:04', '2015-06-26 13:06:04'),
(65, 13, 'Print', 'statement of account', 1, '2015-06-26 13:06:19', '2015-06-26 13:06:19'),
(66, 13, 'Print', 'statement of account', 1, '2015-06-26 13:06:45', '2015-06-26 13:06:45'),
(67, 13, 'Print', 'statement of account', 1, '2015-06-26 13:07:24', '2015-06-26 13:07:24'),
(68, 13, 'Print', 'statement of account', 1, '2015-06-26 13:07:50', '2015-06-26 13:07:50'),
(69, 13, 'Print', 'statement of account', 1, '2015-06-26 13:08:45', '2015-06-26 13:08:45'),
(70, 13, 'Print', 'statement of account', 1, '2015-06-26 13:09:25', '2015-06-26 13:09:25'),
(71, 13, 'Print', 'statement of account', 1, '2015-06-26 13:09:59', '2015-06-26 13:09:59'),
(72, 13, 'Print', 'statement of account', 1, '2015-06-26 13:10:11', '2015-06-26 13:10:11'),
(73, 13, 'Print', 'statement of account', 1, '2015-06-26 13:10:21', '2015-06-26 13:10:21'),
(74, 13, 'Print', 'statement of account', 1, '2015-06-26 13:12:10', '2015-06-26 13:12:10'),
(75, 13, 'Print', 'statement of account', 1, '2015-06-26 13:25:44', '2015-06-26 13:25:44'),
(76, 13, 'Print', 'statement of account', 1, '2015-06-26 13:27:51', '2015-06-26 13:27:51'),
(77, 13, 'Print', 'statement of account', 1, '2015-06-26 13:28:22', '2015-06-26 13:28:22'),
(78, 13, 'Log In', 'logged in', 1, '2015-06-26 13:39:19', '2015-06-26 13:39:19'),
(79, 13, 'Print', 'statement of account', 1, '2015-06-26 13:40:56', '2015-06-26 13:40:56'),
(80, 13, 'Print', 'statement of account', 1, '2015-06-26 13:41:27', '2015-06-26 13:41:27'),
(81, 13, 'Print', 'statement of account', 1, '2015-06-26 13:41:31', '2015-06-26 13:41:31'),
(82, 13, 'Print', 'statement of account', 1, '2015-06-26 13:42:11', '2015-06-26 13:42:11'),
(83, 13, 'Print', 'statement of account', 1, '2015-06-26 13:42:14', '2015-06-26 13:42:14'),
(84, 13, 'Print', 'statement of account', 1, '2015-06-26 13:42:31', '2015-06-26 13:42:31'),
(85, 13, 'Print', 'statement of account', 1, '2015-06-26 13:42:35', '2015-06-26 13:42:35'),
(86, 13, 'Print', 'statement of account', 1, '2015-06-26 13:43:24', '2015-06-26 13:43:24'),
(87, 13, 'Print', 'statement of account', 1, '2015-06-26 13:43:27', '2015-06-26 13:43:27'),
(88, 13, 'Print', 'statement of account', 1, '2015-06-26 13:43:45', '2015-06-26 13:43:45'),
(89, 13, 'Print', 'statement of account', 1, '2015-06-26 13:43:50', '2015-06-26 13:43:50'),
(90, 13, 'Print', 'statement of account', 1, '2015-06-26 13:44:47', '2015-06-26 13:44:47'),
(91, 13, 'Print', 'statement of account', 1, '2015-06-26 13:44:50', '2015-06-26 13:44:50'),
(92, 13, 'Print', 'statement of account', 1, '2015-06-26 14:04:11', '2015-06-26 14:04:11'),
(93, 13, 'Print', 'statement of account', 1, '2015-06-26 14:04:26', '2015-06-26 14:04:26'),
(94, 13, 'Print', 'statement of account', 1, '2015-06-26 14:04:29', '2015-06-26 14:04:29'),
(95, 6, 'Log In', 'logged in', 1, '2015-06-26 14:18:15', '2015-06-26 14:18:15'),
(96, 6, 'Log Out', 'logged out', 1, '2015-06-26 14:18:27', '2015-06-26 14:18:27'),
(97, 13, 'Log In', 'logged in', 1, '2015-06-26 14:18:35', '2015-06-26 14:18:35'),
(98, 13, 'Print', 'statement of account', 1, '2015-06-26 14:18:53', '2015-06-26 14:18:53'),
(99, 13, 'Print', 'statement of account', 1, '2015-06-26 14:19:50', '2015-06-26 14:19:50'),
(100, 13, 'Print', 'statement of account', 1, '2015-06-26 14:20:48', '2015-06-26 14:20:48'),
(101, 13, 'Print', 'statement of account', 1, '2015-06-26 14:20:56', '2015-06-26 14:20:56'),
(102, 13, 'Print', 'statement of account', 1, '2015-06-26 14:22:29', '2015-06-26 14:22:29'),
(103, 13, 'Print', 'statement of account', 1, '2015-06-26 14:22:33', '2015-06-26 14:22:33'),
(104, 13, 'Print', 'statement of account', 1, '2015-06-26 14:23:22', '2015-06-26 14:23:22'),
(105, 13, 'Print', 'statement of account', 1, '2015-06-26 14:23:25', '2015-06-26 14:23:25'),
(106, 13, 'Log In', 'logged in', 1, '2015-06-26 14:34:15', '2015-06-26 14:34:15'),
(107, 13, 'Print', 'statement of account', 1, '2015-06-26 14:34:43', '2015-06-26 14:34:43'),
(108, 13, 'Print', 'statement of account', 1, '2015-06-26 14:35:24', '2015-06-26 14:35:24'),
(109, 13, 'Print', 'statement of account', 1, '2015-06-26 14:35:38', '2015-06-26 14:35:38'),
(110, 13, 'Print', 'statement of account', 1, '2015-06-26 14:36:09', '2015-06-26 14:36:09'),
(111, 13, 'Print', 'statement of account', 1, '2015-06-26 14:37:15', '2015-06-26 14:37:15'),
(112, 13, 'Print', 'statement of account', 1, '2015-06-26 14:37:48', '2015-06-26 14:37:48'),
(113, 13, 'Print', 'statement of account', 1, '2015-06-26 14:39:17', '2015-06-26 14:39:17'),
(114, 13, 'Print', 'statement of account', 1, '2015-06-26 14:40:43', '2015-06-26 14:40:43'),
(115, 13, 'Print', 'statement of account', 1, '2015-06-26 14:41:17', '2015-06-26 14:41:17'),
(116, 13, 'Print', 'statement of account', 1, '2015-06-26 14:52:17', '2015-06-26 14:52:17'),
(117, 13, 'Print', 'statement of account', 1, '2015-06-26 14:52:48', '2015-06-26 14:52:48'),
(118, 13, 'Print', 'statement of account', 1, '2015-06-26 14:53:35', '2015-06-26 14:53:35'),
(119, 13, 'Print', 'statement of account', 1, '2015-06-26 14:54:22', '2015-06-26 14:54:22'),
(120, 13, 'Print', 'statement of account', 1, '2015-06-26 14:54:50', '2015-06-26 14:54:50'),
(121, 13, 'Print', 'statement of account', 1, '2015-06-26 14:56:08', '2015-06-26 14:56:08'),
(122, 13, 'Print', 'statement of account', 1, '2015-06-26 14:56:33', '2015-06-26 14:56:33'),
(123, 13, 'Print', 'statement of account', 1, '2015-06-26 14:57:14', '2015-06-26 14:57:14'),
(124, 13, 'Print', 'statement of account', 1, '2015-06-26 14:57:31', '2015-06-26 14:57:31'),
(125, 13, 'Print', 'statement of account', 1, '2015-06-26 14:58:55', '2015-06-26 14:58:55'),
(126, 13, 'Print', 'statement of account', 1, '2015-06-26 14:59:25', '2015-06-26 14:59:25'),
(127, 13, 'Print', 'statement of account', 1, '2015-06-26 15:00:20', '2015-06-26 15:00:20'),
(128, 13, 'Print', 'statement of account', 1, '2015-06-26 15:00:29', '2015-06-26 15:00:29'),
(129, 13, 'Print', 'statement of account', 1, '2015-06-26 15:04:51', '2015-06-26 15:04:51'),
(130, 13, 'Print', 'statement of account', 1, '2015-06-26 15:04:54', '2015-06-26 15:04:54'),
(131, 13, 'Print', 'statement of account', 1, '2015-06-26 15:05:18', '2015-06-26 15:05:18'),
(132, 13, 'Print', 'statement of account', 1, '2015-06-26 15:05:22', '2015-06-26 15:05:22'),
(133, 13, 'Print', 'statement of account', 1, '2015-06-26 15:06:12', '2015-06-26 15:06:12'),
(134, 13, 'Print', 'statement of account', 1, '2015-06-26 15:06:15', '2015-06-26 15:06:15'),
(135, 13, 'Print', 'statement of account', 1, '2015-06-26 15:06:28', '2015-06-26 15:06:28'),
(136, 13, 'Print', 'statement of account', 1, '2015-06-26 15:06:31', '2015-06-26 15:06:31'),
(137, 13, 'Print', 'statement of account', 1, '2015-06-26 15:06:49', '2015-06-26 15:06:49'),
(138, 13, 'Print', 'statement of account', 1, '2015-06-26 15:06:52', '2015-06-26 15:06:52'),
(139, 13, 'Print', 'statement of account', 1, '2015-06-26 15:08:12', '2015-06-26 15:08:12'),
(140, 13, 'Print', 'statement of account', 1, '2015-06-26 15:08:15', '2015-06-26 15:08:15'),
(141, 13, 'Print', 'statement of account', 1, '2015-06-26 15:09:11', '2015-06-26 15:09:11'),
(142, 13, 'Print', 'statement of account', 1, '2015-06-26 15:09:14', '2015-06-26 15:09:14'),
(143, 13, 'Print', 'statement of account', 1, '2015-06-26 15:09:52', '2015-06-26 15:09:52'),
(144, 13, 'Print', 'statement of account', 1, '2015-06-26 15:09:55', '2015-06-26 15:09:55'),
(145, 13, 'Print', 'statement of account', 1, '2015-06-26 15:13:49', '2015-06-26 15:13:49'),
(146, 13, 'Print', 'statement of account', 1, '2015-06-26 15:13:52', '2015-06-26 15:13:52'),
(147, 13, 'Print', 'statement of account', 1, '2015-06-26 15:16:31', '2015-06-26 15:16:31'),
(148, 13, 'Print', 'statement of account', 1, '2015-06-26 15:16:41', '2015-06-26 15:16:41'),
(149, 13, 'Print', 'statement of account', 1, '2015-06-26 15:16:56', '2015-06-26 15:16:56'),
(150, 13, 'Print', 'statement of account', 1, '2015-06-26 15:17:57', '2015-06-26 15:17:57'),
(151, 13, 'Print', 'statement of account', 1, '2015-06-26 15:18:08', '2015-06-26 15:18:08'),
(152, 13, 'Print', 'statement of account', 1, '2015-06-26 15:18:27', '2015-06-26 15:18:27'),
(153, 13, 'Print', 'statement of account', 1, '2015-06-26 15:18:56', '2015-06-26 15:18:56'),
(154, 13, 'Print', 'statement of account', 1, '2015-06-26 15:18:59', '2015-06-26 15:18:59'),
(155, 13, 'Print', 'statement of account', 1, '2015-06-26 15:19:18', '2015-06-26 15:19:18'),
(156, 13, 'Print', 'statement of account', 1, '2015-06-26 15:19:21', '2015-06-26 15:19:21'),
(157, 13, 'Print', 'statement of account', 1, '2015-06-26 15:19:56', '2015-06-26 15:19:56'),
(158, 13, 'Print', 'statement of account', 1, '2015-06-26 15:19:59', '2015-06-26 15:19:59'),
(159, 13, 'Print', 'statement of account', 1, '2015-06-26 15:20:13', '2015-06-26 15:20:13'),
(160, 13, 'Print', 'statement of account', 1, '2015-06-26 15:20:16', '2015-06-26 15:20:16'),
(161, 13, 'Print', 'statement of account', 1, '2015-06-26 15:20:47', '2015-06-26 15:20:47'),
(162, 13, 'Print', 'statement of account', 1, '2015-06-26 15:20:51', '2015-06-26 15:20:51'),
(163, 13, 'Print', 'statement of account', 1, '2015-06-26 15:21:21', '2015-06-26 15:21:21'),
(164, 13, 'Print', 'statement of account', 1, '2015-06-26 15:21:25', '2015-06-26 15:21:25'),
(165, 13, 'Print', 'statement of account', 1, '2015-06-26 15:22:29', '2015-06-26 15:22:29'),
(166, 13, 'Print', 'statement of account', 1, '2015-06-26 15:22:32', '2015-06-26 15:22:32'),
(167, 13, 'Print', 'statement of account', 1, '2015-06-26 15:23:23', '2015-06-26 15:23:23'),
(168, 13, 'Print', 'statement of account', 1, '2015-06-26 15:23:26', '2015-06-26 15:23:26'),
(169, 13, 'Print', 'statement of account', 1, '2015-06-26 15:23:51', '2015-06-26 15:23:51'),
(170, 13, 'Print', 'statement of account', 1, '2015-06-26 15:23:54', '2015-06-26 15:23:54'),
(171, 13, 'Print', 'statement of account', 1, '2015-06-26 15:28:05', '2015-06-26 15:28:05'),
(172, 13, 'Print', 'statement of account', 1, '2015-06-26 15:31:07', '2015-06-26 15:31:07'),
(173, 13, 'Print', 'statement of account', 1, '2015-06-26 15:31:10', '2015-06-26 15:31:10'),
(174, 13, 'Print', 'statement of account', 1, '2015-06-26 15:31:35', '2015-06-26 15:31:35'),
(175, 13, 'Print', 'statement of account', 1, '2015-06-26 15:33:52', '2015-06-26 15:33:52'),
(176, 13, 'Print', 'statement of account', 1, '2015-06-26 15:34:00', '2015-06-26 15:34:00'),
(177, 13, 'Print', 'statement of account', 1, '2015-06-26 15:34:17', '2015-06-26 15:34:17'),
(178, 13, 'Print', 'statement of account', 1, '2015-06-26 15:34:21', '2015-06-26 15:34:21'),
(179, 13, 'Print', 'statement of account', 1, '2015-06-26 15:35:10', '2015-06-26 15:35:10'),
(180, 13, 'Print', 'statement of account', 1, '2015-06-26 15:35:13', '2015-06-26 15:35:13'),
(181, 13, 'Print', 'statement of account', 1, '2015-06-26 15:35:26', '2015-06-26 15:35:26'),
(182, 13, 'Print', 'statement of account', 1, '2015-06-26 15:35:29', '2015-06-26 15:35:29'),
(183, 13, 'Print', 'statement of account', 1, '2015-06-26 15:35:42', '2015-06-26 15:35:42'),
(184, 13, 'Print', 'statement of account', 1, '2015-06-26 15:35:46', '2015-06-26 15:35:46'),
(185, 13, 'Print', 'statement of account', 1, '2015-06-26 15:39:50', '2015-06-26 15:39:50'),
(186, 13, 'Print', 'statement of account', 1, '2015-06-26 15:39:53', '2015-06-26 15:39:53'),
(187, 13, 'Print', 'statement of account', 1, '2015-06-26 15:41:00', '2015-06-26 15:41:00'),
(188, 13, 'Print', 'statement of account', 1, '2015-06-26 15:41:03', '2015-06-26 15:41:03'),
(189, 13, 'Print', 'statement of account', 1, '2015-06-26 15:42:18', '2015-06-26 15:42:18'),
(190, 13, 'Print', 'statement of account', 1, '2015-06-26 15:42:21', '2015-06-26 15:42:21'),
(191, 13, 'Print', 'statement of account', 1, '2015-06-26 15:42:46', '2015-06-26 15:42:45'),
(192, 13, 'Print', 'statement of account', 1, '2015-06-26 15:42:48', '2015-06-26 15:42:48'),
(193, 13, 'Print', 'statement of account', 1, '2015-06-26 15:43:28', '2015-06-26 15:43:28'),
(194, 13, 'Print', 'statement of account', 1, '2015-06-26 15:43:31', '2015-06-26 15:43:31'),
(195, 13, 'Print', 'statement of account', 1, '2015-06-26 15:46:56', '2015-06-26 15:46:56'),
(196, 13, 'Print', 'statement of account', 1, '2015-06-26 15:46:59', '2015-06-26 15:46:59'),
(197, 13, 'Print', 'statement of account', 1, '2015-06-26 15:50:02', '2015-06-26 15:50:02'),
(198, 13, 'Print', 'statement of account', 1, '2015-06-26 15:50:58', '2015-06-26 15:50:58'),
(199, 13, 'Print', 'statement of account', 1, '2015-06-26 15:51:08', '2015-06-26 15:51:08'),
(200, 13, 'Print', 'statement of account', 1, '2015-06-26 15:51:11', '2015-06-26 15:51:11'),
(201, 13, 'Print', 'statement of account', 1, '2015-06-26 15:51:20', '2015-06-26 15:51:20'),
(202, 13, 'Print', 'statement of account', 1, '2015-06-26 15:51:24', '2015-06-26 15:51:24'),
(203, 13, 'Print', 'statement of account', 1, '2015-06-26 15:52:13', '2015-06-26 15:52:13'),
(204, 13, 'Print', 'statement of account', 1, '2015-06-26 15:52:15', '2015-06-26 15:52:15'),
(205, 13, 'Print', 'statement of account', 1, '2015-06-26 15:53:40', '2015-06-26 15:53:40'),
(206, 13, 'Print', 'statement of account', 1, '2015-06-26 15:53:44', '2015-06-26 15:53:44'),
(207, 13, 'Print', 'statement of account', 1, '2015-06-26 15:54:48', '2015-06-26 15:54:48'),
(208, 13, 'Print', 'statement of account', 1, '2015-06-26 15:54:51', '2015-06-26 15:54:51'),
(209, 13, 'Print', 'statement of account', 1, '2015-06-26 15:55:11', '2015-06-26 15:55:11'),
(210, 13, 'Print', 'statement of account', 1, '2015-06-26 15:55:13', '2015-06-26 15:55:13'),
(211, 13, 'Print', 'statement of account', 1, '2015-06-26 15:55:42', '2015-06-26 15:55:42'),
(212, 13, 'Print', 'statement of account', 1, '2015-06-26 15:55:46', '2015-06-26 15:55:46'),
(213, 13, 'Print', 'statement of account', 1, '2015-06-26 15:56:05', '2015-06-26 15:56:05'),
(214, 13, 'Print', 'statement of account', 1, '2015-06-26 15:56:08', '2015-06-26 15:56:08'),
(215, 13, 'Print', 'statement of account', 1, '2015-06-26 15:56:33', '2015-06-26 15:56:33'),
(216, 13, 'Print', 'statement of account', 1, '2015-06-26 15:57:23', '2015-06-26 15:57:23'),
(217, 13, 'Print', 'statement of account', 1, '2015-06-26 16:35:34', '2015-06-26 16:35:34'),
(218, 13, 'Print', 'statement of account', 1, '2015-06-26 16:36:20', '2015-06-26 16:36:20'),
(219, 13, 'Print', 'statement of account', 1, '2015-06-26 16:36:45', '2015-06-26 16:36:45'),
(220, 13, 'Print', 'statement of account', 1, '2015-06-26 16:38:23', '2015-06-26 16:38:23'),
(221, 13, 'Print', 'statement of account', 1, '2015-06-26 16:40:13', '2015-06-26 16:40:13'),
(222, 13, 'Print', 'statement of account', 1, '2015-06-26 16:40:27', '2015-06-26 16:40:27'),
(223, 13, 'Print', 'statement of account', 1, '2015-06-26 16:42:13', '2015-06-26 16:42:13'),
(224, 13, 'Print', 'statement of account', 1, '2015-06-26 16:42:44', '2015-06-26 16:42:44'),
(225, 13, 'Print', 'statement of account', 1, '2015-06-26 16:43:28', '2015-06-26 16:43:28'),
(226, 13, 'Print', 'statement of account', 1, '2015-06-26 16:52:43', '2015-06-26 16:52:43'),
(227, 13, 'Print', 'statement of account', 1, '2015-06-26 16:53:06', '2015-06-26 16:53:06'),
(228, 13, 'Print', 'statement of account', 1, '2015-06-26 16:54:40', '2015-06-26 16:54:40'),
(229, 13, 'Print', 'statement of account', 1, '2015-06-26 16:55:23', '2015-06-26 16:55:23'),
(230, 13, 'Print', 'statement of account', 1, '2015-06-26 16:57:27', '2015-06-26 16:57:27'),
(231, 5, 'Log In', 'logged in', 1, '2015-06-27 00:16:22', '2015-06-27 00:16:22'),
(232, 5, 'Log Out', 'logged out', 1, '2015-06-27 00:16:29', '2015-06-27 00:16:29'),
(233, 3, 'Log In', 'logged in', 1, '2015-06-27 00:16:34', '2015-06-27 00:16:34'),
(234, 3, 'Log Out', 'logged out', 1, '2015-06-27 01:13:21', '2015-06-27 01:13:21'),
(235, 13, 'Log In', 'logged in', 1, '2015-06-27 01:13:32', '2015-06-27 01:13:32'),
(236, 13, 'Log In', 'logged in', 1, '2015-06-27 02:15:06', '2015-06-27 02:15:06'),
(237, 13, 'Log Out', 'logged out', 1, '2015-06-27 09:30:48', '2015-06-27 09:30:48'),
(238, 13, 'Log In', 'logged in', 1, '2015-06-27 09:30:51', '2015-06-27 09:30:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE IF NOT EXISTS `user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `permissionId` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `user_permissions`
--

INSERT INTO `user_permissions` (`id`, `userId`, `permissionId`, `created`, `modified`) VALUES
(1, 3, 1, '2015-06-06 09:54:06', '2015-06-06 09:54:06'),
(5, 3, 5, '2015-06-06 09:54:06', '2015-06-06 09:54:06'),
(6, 3, 6, '2015-06-06 09:54:06', '2015-06-06 09:54:06'),
(9, 3, 9, '2015-06-06 09:54:06', '2015-06-06 09:54:06'),
(10, 3, 11, '2015-06-06 09:54:06', '2015-06-06 09:54:06'),
(11, 3, 57, '2015-06-06 10:07:08', '2015-06-06 10:07:08'),
(12, 3, 58, '2015-06-06 10:07:08', '2015-06-06 10:07:08'),
(13, 3, 59, '2015-06-06 10:07:08', '2015-06-06 10:07:08'),
(14, 3, 60, '2015-06-06 10:07:08', '2015-06-06 10:07:08'),
(15, 14, 1, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(16, 14, 2, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(17, 14, 3, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(18, 14, 4, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(19, 14, 5, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(20, 14, 6, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(21, 14, 7, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(22, 14, 8, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(23, 14, 9, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(24, 14, 10, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(25, 14, 11, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(26, 14, 12, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(27, 14, 13, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(28, 14, 14, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(29, 14, 15, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(30, 14, 16, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(31, 14, 17, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(32, 14, 18, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(33, 14, 19, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(34, 14, 20, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(35, 14, 21, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(36, 14, 22, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(37, 14, 23, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(38, 14, 24, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(39, 14, 25, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(40, 14, 26, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(41, 14, 27, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(42, 14, 28, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(43, 14, 29, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(44, 14, 30, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(45, 14, 31, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(46, 14, 32, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(47, 14, 33, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(48, 14, 34, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(49, 14, 35, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(50, 14, 36, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(51, 14, 37, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(52, 14, 38, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(53, 14, 39, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(54, 14, 40, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(55, 14, 41, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(56, 14, 42, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(57, 14, 43, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(58, 14, 44, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(59, 14, 45, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(60, 14, 46, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(61, 14, 47, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(62, 14, 48, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(63, 14, 49, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(64, 14, 50, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(65, 14, 51, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(66, 14, 52, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(67, 14, 53, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(68, 14, 54, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(69, 14, 55, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(70, 14, 56, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(71, 14, 57, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(72, 14, 58, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(73, 14, 59, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(74, 14, 60, '2015-06-06 10:08:04', '2015-06-06 10:08:04'),
(75, 3, 64, '2015-06-06 10:17:57', '2015-06-06 10:17:57'),
(76, 14, 61, '2015-06-06 10:18:05', '2015-06-06 10:18:05'),
(77, 14, 62, '2015-06-06 10:18:05', '2015-06-06 10:18:05'),
(78, 14, 63, '2015-06-06 10:18:05', '2015-06-06 10:18:05'),
(79, 14, 64, '2015-06-06 10:18:05', '2015-06-06 10:18:05'),
(80, 3, 2, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(81, 3, 3, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(82, 3, 4, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(83, 3, 7, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(84, 3, 8, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(85, 3, 61, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(86, 3, 62, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(87, 3, 63, '2015-06-06 10:20:02', '2015-06-06 10:20:02'),
(88, 3, 65, '2015-06-06 10:55:57', '2015-06-06 10:55:57'),
(89, 3, 66, '2015-06-06 10:55:57', '2015-06-06 10:55:57'),
(90, 3, 67, '2015-06-06 12:59:54', '2015-06-06 12:59:54');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
