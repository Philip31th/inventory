<?php if ($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
  <?php 
  
    $host     = $_POST['host'];
    $user     = $_POST['user'];
    $password = $_POST['password'];
    $db = $_POST['db'];
    $cake = $_POST['cake'];
    
    $databasePath = '../Config/database.php';
    $database = file($databasePath);
    
    $database[5] = "\t\t'host'     => '$host',\n";
    $database[6] = "\t\t'login'    => '$user',\n";
    $database[7] = "\t\t'password' => '$password',\n";
    $database[8] = "\t\t'database' => '$db',\n";
    
    $database = implode('', $database);
    
    $databaseFile = fopen($databasePath, "w");
    $saveDatabase = fwrite($databaseFile, $database);
    fclose($databaseFile);

    if ($saveDatabase)
      echo 'Database has been setup<br>';
      

    $cakePath = 'index.php';
    $index = file($cakePath);
    
    $index[5] = "define('CAKE_CORE_INCLUDE_PATH', '$cake');\n";
    
    $index = implode('', $index);
    
    $cakeFile = fopen($cakePath, "w");
    $saveCake = fwrite($cakeFile, $index);
    fclose($cakeFile);
    
    if ($saveCake)
      echo 'Cake path has been setup<br>';

      
  ?>
<?php else: ?>
  <form method="POST">
    <input type="text" name="host" placeholder="Database Host">
    <br>
    <input type="text" name="user" placeholder="Database Username">
    <br>
    <input type="text" name="password" placeholder="Database Password">
    <br>
    <input type="text" name="db" placeholder="Database Name">
    <br>
    <input type="text" name="cake" placeholder="Cake Path">
    <br>
    <button>SAVE</button>
  </form>
<?php endif ?>