app.config(function($routeProvider) {
  $routeProvider
  .when('/bar/transactions', {
    templateUrl: tmp + 'bar__transactions__index',
    controller: 'BarTransactionController',
  })
  .when('/bar/transactions/add', {
    templateUrl: tmp + 'bar__transactions__add',
    controller: 'BarTransactionAddController',
  })
  .when('/bar/transactions/view/:id', {
    templateUrl: tmp + 'bar__transactions__view',
    controller: 'BarTransactionViewController',
  })
  .when('/bar/transactions/edit/:id', {
    templateUrl: tmp + 'bar__transactions__edit',
    controller: 'BarTransactionEditController',
  })
   .when('/bar/reports/receivables', {
    templateUrl: tmp + 'bar__transactions__receivables',
    controller: 'BarReceivablesController',
  })
  .when('/bar/reports/receivables/view/:id', {
    templateUrl: tmp + 'bar__transactions__receivables_view',
    controller: 'BarReceivablesViewController',
  });
});