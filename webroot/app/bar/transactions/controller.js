app.controller('BarTransactionController', function($scope, BarTransaction){

  // load inventory
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    BarTransaction.query(options, function(e) {
      if (e.ok) {
        $scope.transactions = e.data;
  
        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code : 'bar' });

  // remove transaction
   $scope.remove = function(transaction) {
    bootbox.confirm('Are you sure you want to delete these ' + transaction.code + ' Transactions?', function(b) {
      if (b) {
        BarTransaction.remove({ id: transaction.id }, function(e) {
          if(e.ok){
            $.gritter.add({ 
              title: 'Successful!', 
              text: e.message 
            });

            $scope.load({
              code:   'bar',
              page:   $scope.paginator.page,
              search: $scope.searchTxt
            });
             window.location = '#/bar/transactions';
          }  else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }  
        });
      }
    });
  }

  // remove transaction
  $scope.removeItem = function(item) {
    bootbox.confirm('Are you sure you want to delete this ' + item.particulars + ' Business?', function(b) {
      if (b) {
        TransactionSub.remove({ id: item.id }, function(e) {
          if (e.ok) {
            $scope.load();
          }           
        });
       
      }
    });
  }

});


app.controller('BarTransactionAddController', function($scope, BarTransaction, TransactionSub, Select){

  // folios selection
  Select.get({ code: 'folios' }, function(e) {
    $scope.folios = e.data;
  });

  $('#form').validationEngine('attach');
  $('.datepicker').datepicker({format:'mm/dd/yyyy', autoclose:true});

  $scope.save = function(){
    valid = $("#form").validationEngine('validate');
    if(valid){

      $scope.data.Transaction.businessId = 3;

      BarTransaction.save($scope.data, function(e){
        if(e.ok){
          $.gritter.add({ title: 'Successful!', text: e.message });
          window.location = '#/bar/transactions';
        }
      });
    }
  }
  
  // add transaction item
  $scope.data = {
    TransactionSub: []
  }
  subs = [];

  $scope.totalBills = function() {
    total = 0;
    for(count = 0; count < $scope.data.TransactionSub.length; count++) {
      total += $scope.data.TransactionSub[count].amount;
    }
    
    return total;
  }
    

  $scope.addItem = function() {
    $scope.title = 'ADD NEW';
    $('#transaction-item-modal').modal('show');
  }

  $scope.saveItem = function(item) {
    $scope.data.TransactionSub.push(item);  
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }  
  
  var key;

  $scope.editItem = function(item) {
    $scope.title = 'EDIT';
    key = $scope.data.TransactionSub.indexOf(item);
    $scope.item = {
      id:          item.id,
      particulars: item.particulars,
      amount:      item.amount
    }
    $('#transaction-item-modal').modal('show');
  }

  $scope.updateItem = function(item) {
    $scope.data.TransactionSub[key] = {
      id:           item.id,
      particulars:  item.particulars,
      amount:       item.amount
    }
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }

  $scope.removeItem = function(index,item) {
    bootbox.confirm('Are you sure you want to delete this transaction items?', function(b) {
      if (b) {
        $scope.data.TransactionSub.splice(index,1); 
        TransactionSub.remove({ id: item.id });
      }
    });
  }
});


app.controller('BarTransactionViewController', function($scope, $routeParams, BarTransaction){
  $scope.transactionId = $routeParams.id;

  // load transactions
  $scope.load = function() {
    BarTransaction.get({ id: $scope.transactionId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();
  
    // remove inventory
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete these ' + data.Business.name + ' Transactions?', function(b) {
      if (b) {
        BarTransaction.remove({ id: data.Transaction.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

          }
          window.location = '#/bar/transactions';
        });
      }
    });
  }
});


app.controller('BarTransactionEditController', function($scope, $routeParams, BarTransaction, TransactionSub, Select){
$scope.transactionId = $routeParams.id;
  
  // get transactions
  $scope.load = function() {
    BarTransaction.get({ id: $scope.transactionId }, function(e){
      $scope.data = e.data;
    }); 
  }
  $scope.load();
  
  // update transaction
  $scope.save = function(){
    BarTransaction.update({ id: $scope.transactionId }, $scope.data, function(e){
      if(e.ok){
        $.gritter.add({ title: 'Successful!', text: e.message });
        window.location = '#/bar/transactions';
      }
    });
  }
  
 $scope.data = {
    TransactionSub: []
  }  

  $scope.totalBills = function() {
    total = 0;
    for(count = 0; count < $scope.data.TransactionSub.length; count++) {
      total += $scope.data.TransactionSub[count].amount;
    }
    
    return total;
  }
    

  $scope.addItem = function() {
    $scope.title = 'ADD NEW';
    $('#transaction-item-modal').modal('show');
  }

  $scope.saveItem = function(item) {
    $scope.data.TransactionSub.push(item);  
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }  
  
  var key;

  $scope.editItem = function(item) {
    $scope.title = 'EDIT';
    key = $scope.data.TransactionSub.indexOf(item);
    $scope.item = {
      id:          item.id,
      particulars: item.particulars,
      amount:      item.amount
    }
    $('#transaction-item-modal').modal('show');
  }

  $scope.updateItem = function(item) {
    $scope.data.TransactionSub[key] = {
      id:           item.id,
      particulars:  item.particulars,
      amount:       item.amount
    }
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }

  $scope.removeItem = function(index,item) {
    bootbox.confirm('Are you sure you want to delete this transaction items?', function(b) {
      if (b) {
        $scope.data.TransactionSub.splice(index,1); 
        TransactionSub.remove({ id: item.id });
      }
    });
  }
});

app.controller('BarReceivablesController', function($scope, $routeParams, BarReceivable){

  // load inventory
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    BarReceivable.query(options, function(e) {
      if (e.ok) {
        $scope.receivables = e.data;
        $scope.total = e.totalReceivables;
  
        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code : 'bar' });
});


app.controller('BarReceivablesViewController', function($scope, $routeParams, BarReceivable){
  $scope.transactionId = $routeParams.id;

  // load inventory
  $scope.load = function() {
    BarReceivable.query({ id: $scope.transactionId }, function(e) {
      if (e.ok) {
        $scope.receivables = e.data;
        $scope.total = e.totalReceivables;
      }
    });
  }
  $scope.load();

});
