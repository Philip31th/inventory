app.factory("BarTransaction", function($resource) {
  return $resource( api + "transactions/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("BarReceivable", function($resource) {
  return $resource( api + "transactions/receivables/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});