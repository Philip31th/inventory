app.controller('BarReportCashieringController', function($scope, BarReportCashiering) {

 $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.searchTxt = Date.today().toString('yyyy-MM-dd');

  $('#search').datepicker({ 
    format: 'yyyy-mm-dd', 
    autoclose: true
  });

  $("#search").datepicker('setDate', $scope.searchTxt);

   $scope.dates = {
    // startTime: new Date(),
    // endTime: new Date()
  };
  
  $scope.open = {
    startTime: false,
    endTime: false
  };
  
  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
  };

  $scope.dateOptions = {
    showWeeks: false,
    startingDay: 1
  };
  
  $scope.timeOptions = {
    readonlyInput: true,
    showMeridian: true
  };
  
  $scope.openCalendar = function(e, date) {
      e.preventDefault();
      e.stopPropagation();

      $scope.open[date] = true;
  };
  // $scope.searchTxt = Date.parse('today').toString('yyyy-MM-dd');
  $scope.datas = {};

  // load employees
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    BarReportCashiering.query(options, function(data) {
      if (data.ok) {
        $scope.datas   = data.result;
        $scope.summary = data.summary;
        $scope.non_guest = data.non_guest;
      }
    });
  }
  $scope.load({date: $scope.searchTxt, business:3 });

  $scope.searchTime = function() {
    $scope.load({date: $scope.searchTxt, startTime: $scope.dates.startTime, endTime: $scope.dates.endTime , business: 3 });
  }

  // print
  $scope.print = function(){

    if($scope.dates.startTime ==null || $scope.dates.endTime==null ){
      shiftTime = '' ;
    } else {
       shiftTime = '/'+ $scope.dates.startTime.getTime() + '/' + $scope.dates.endTime.getTime();
    }
    printTable(base + 'print/bar_resto_cashiering_report/' + $scope.searchTxt + shiftTime + '/business:3');
  }


});
