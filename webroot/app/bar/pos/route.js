app.config(function($routeProvider) {
  $routeProvider
  .when('/bar/pos', {
    templateUrl: tmp + 'bar__pos__index',
    controller: 'BarPosController',
  })
  .when('/bar/pos/table/view/:id', {
    templateUrl: tmp + 'bar__pos__table_view',
    controller: 'BarPosViewController',
  })
  .when('/bar/pos/transactions/add/:id', {
    templateUrl: tmp + 'bar__pos__table_transact',
    controller: 'BarPosAddTransactionsController',
  });
});