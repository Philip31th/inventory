app.controller('BarPosController', function($scope, BarPos,FolioTransaction) {

  // load bar menus
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    BarPos.query(options, function(e) {
      if (e.ok) {
        $scope.tables = e.table;
      }
    });
  }
  $scope.load({ code: 'bar' });

  // remove table transaction 
  $scope.cleanTable = function(tableTransactionId,folio,folioTransactionId) {
    bootbox.confirm('Are you sure you want to clean the table?', function(b) {
      if(b) {
        BarPos.remove({ id: tableTransactionId }, function(e){
          if(e.ok) {
              if (folio==true) {
                $scope.FolioTransaction = {};
                $scope.FolioTransaction = {
                  'id': folioTransactionId            
                }
                FolioTransaction.save($scope.FolioTransaction,function(e){ 
                    if(e.ok){
                      $.gritter.add({
                        title: 'Successful!',
                        text: e.msg,
                      });
                    }
                    $scope.load({ code: 'bar' });
                  });
                } else {
                  $.gritter.add({
                    title: 'Successful!',
                    text: e.msg,
                  });
                  $scope.load({ code: 'bar' });
                }
            }   
        });
      }
    });
  }

});

app.controller('BarPosViewController', function($scope, $routeParams, BarPos, Select, TransactionPayment, TransactionSub, BarMenu, FolioTransaction,TableTransaction) {
  $scope.tableId = $routeParams.id;


  // save/update transaction
  $scope.save = function(){
    if ($scope.paymentStatus()=='PAID') {
      $scope.data.TransactionPaid = true;
    } else {
      $scope.data.TransactionPaid = false;
    }

    $scope.transactions = {
      'TransactionPaid':    $scope.data.TransactionPaid,
      'TableTransactionId': $scope.data.tableTransactionId,  
      'TempTransactionSub': $scope.data.TempTransactionSub,
      'TransactionPayment': $scope.data.TransactionPayment,
      'FolioCode':          $scope.data.Folio.code,
      'Folio':              $scope.data.Folio             
    }

      BarPos.update({ id: $scope.data.transactionId }, $scope.transactions, function(e){
        if(e.ok){
          $.gritter.add({ title: 'Successful!', text: e.message });
          $scope.load();
          window.location = '#/bar/pos';
        }
      });
  }



  // load table transactions
  $scope.load = function() {
    BarPos.get({ id: $scope.tableId }, function(e) {
      $scope.data = e.table;
      $scope.totalPaid = e.total;
      $scope.total = e.total;
      $scope.totalPayments  = e.totalPayments;
      $scope.totalChange    = e.totalChange;
      $scope.folioCode      = e.folioCode;
      $scope.folio         = e.folios;
      // $scope.
    });
    $('#pos_form').hide();
    $('#addOrder').show();
  }
  $scope.load();

    // load folio 
  Select.get({ code: 'pos_folios' },function(e) {
    $scope.folios = e.data;
  }); 

  $scope.paymentStatus = function() {
    status = '';
    if(($scope.totalPayments != 0) && ($scope.total != 0)) {
      if($scope.totalPayments >= $scope.total) {
        status = 'PAID';
       
        return status;
      } else {
      
      }
    } else {
      }  
  }

  $scope.checkFolio = function() {
   if($scope.folio=='true') {
      $('#cleanTable').show();
    }  
  }

  $scope.addOrder = function() {
    $("#form").validationEngine('attach');

    $('#pos_form').slideToggle();
    $('#addOrder').hide();

    // load menus
    Select.get({ code: 'menus', departmentId: 2 },function(e) {
      $scope.menus = e.data;
    });

  }
  // orders
  $scope.getMenuData = function(menuId) {
      BarMenu.get({ id: menuId }, function(e){
        if(e.ok) {
          $scope.menu = e.data;
        } 
      });

    if ($('#menuId').val()=='') {
      $scope.quantity = '';
      $('#addOrderBt').addClass('disabled');
    } else {
       $scope.quantity = 1;
      $('#addOrderBt').removeClass('disabled');
    }
  }

  $scope.data = {
    TransactionSub: [],
    TransactionPayment: [],
    TempTransactionSub: []
  }

  subs = [];
  payments = [];

  Array.prototype.sum = function (amt) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][amt];
    }

    return total;
  } 

  // transaction subs only
  $scope.saveOrder = function() {
    valid = $("#form").validationEngine('validate');
    
    if(valid){
      if($scope.quantity!='') {
        subs = {
          particulars:      $scope.menu.Menu.name,
          totalQuantity:    $scope.quantity,
          amount:           $scope.menu.Menu.amount,
          totalAmount:      ($scope.menu.Menu.amount)*($scope.quantity)
        };  

        // $scope.data.TransactionSub.push(subs);
        // $scope.data.TempTransactionSub = $scope.data.TransactionSub;
        if ($scope.paymentStatus()=='PAID') {
          $scope.data.TransactionPaid = true;
        } else {
          $scope.data.TransactionPaid = false;
        }

        // 
        $scope.transactions = {
          'TransactionPaid':    $scope.data.TransactionPaid,
          'TableTransactionId': $scope.data.tableTransactionId,
          'TransactionSub':        subs  
        }

        BarPos.update({ id: $scope.data.transactionId }, $scope.transactions , function(e){
          if(e.ok){
            $scope.load();

            $scope.paymentStatus();
            }
        });

        $scope.total = $scope.data.TransactionSub.sum('totalAmount');
        console.log($scope.total);

        $scope.menuId = '';
        $scope.quantity = '';
        $scope.menu.Menu.amount = '';
      } 
       $('#addOrder').show();
    }  
  }

  $scope.saveFolioTransactions = function() {

    $scope.foliotransactions = {
      'FolioCode':          $scope.data.Folio.code, 
      'transactionId':      $scope.data.transactionId,
      'FolioTransactionId': $scope.data.Folio.folioTransactionId
    }

    FolioTransaction.save($scope.foliotransactions, function(e){
      $scope.load();
    });
  }

  $('#closeOrderBt').on('click',function(){
    $scope.menuId = '';
    $scope.quantity = '';
    $('#pos_form').slideToggle(function(){
      $('#addOrder').show();
    });
    
  });

  $scope.removeOrder = function(index,item) {
    bootbox.confirm('Are you sure you want to delete this orders?', function(b) {
      if (b) {
        TransactionSub.remove({ id: item.id }, function(e){
          if(e.ok) {
            $scope.load();
          }
        });        
      }
    });
  }

  // PAYMENT SECTION
   $scope.checkOR = function(orNumber,business) {
    // if(orNumber.length >= 3) {
    if(orNumber!='') {  
      $scope.checking = true;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
      TransactionPayment.query({ search: orNumber , businessId: business },function(e){
        if (e.data) {
          $scope.checking = false;
          $scope.notexisted = false;
          $scope.orNumberExisting = true;
        } else {
          $scope.checking = false;
          $scope.notexisted = true;
          $scope.orNumberExisting = false;
        }
      });
    } else {
      $scope.checking = false;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
    }
  }

  $scope.paymentType = function(paymentType) {
    $scope.checking = false;
    $scope.notexisted = false;
    // $scope.orNumberExisting = false;
  }

  // add payment
  $scope.addPayment = function(sub) {

    $('#form').validationEngine('attach');

    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
    $scope.payment = {};
    
    // load payment-types 
    Select.get({ code: 'payment-types', pos: true },function(e) {
      $scope.paymentTypes = e.data;
    });

    $scope.amountToPay = sub.totalAmount - sub.totalPayments;
    $scope.title = 'ADD';
    // $scope.pos = true;
    $scope.sub_id = sub.id;
    $scope.pos = true;
    $scope.business = 'bar';

    $('#add-payment-modal').modal('show');

  }

  // save payment
  $scope.savePayment = function(pay) {


    valid = $("#form").validationEngine('validate');

    if (valid) {

      if($('#change').val()==null) {
        changee = 0;
      } else {
        changee = parseFloat($('#change').val());
      }
        payments = {
          transactionId    : $scope.data.transactionId,
          tableTransactionId: $scope.data.tableTransactionId,
          orNumber         : pay.orNumber,
          amount           : pay.amount,
          change           : changee,
          paymentType      : pay.paymentType,
          accountName      : pay.accountName,
          cardNumber       : pay.cardNumber,
          chequeNumber     : pay.chequeNumber,
          expirationYear   : pay.expirationYear,
          expirationMonth  : pay.expirationMonth,
          bankName         : pay.bankName,
          date             : Date.today(),
          subId           : $scope.sub_id
        }

      if (pay.amount==0 || pay.amount=='') {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must have value to save'
        });
        $('#savePayment').attr('disabled', false);
      }  else if ($scope.orNumberExisting) {
          $.gritter.add({
            title: 'Warning!',
            text: 'OR Number already existed.'
          });     
           $('#savePayment').attr('disabled', false);
      } else if (pay.paymentType!='cash' && pay.amount > $scope.amountToPay) {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must not exceed to transaction balance'
        });
         $('#savePayment').attr('disabled', false);  
      } else {
        $('#savePayment').attr('disabled', true);
        TransactionPayment.save( { TransactionPayment: payments } , function(e) {
          console.log(e);
          if(e.ok) {
              $.gritter.add({
                title: 'Successful!',
                text: e.msg
              });
              $('#add-payment-modal').modal('hide');
              $scope.payment = {};
              $scope.load();
              setTimeout(function(){
                if($scope.paymentStatus()=='PAID'){
                    paymentStatus = true;
                  } else {
                    paymentStatus = false;
                  }

                  foliotransactions = { 
                    'FolioTransactionId': $scope.data.Folio.folioTransactionId,
                    'paymentStatus':      paymentStatus,
                    'FolioCode':          $scope.data.Folio.code        
                  }

                  FolioTransaction.save(foliotransactions, function(e){
                     
                  });          
              }, 300);
               
            $scope.totalPayments = $scope.data.TransactionPayment.sum('amount');
            $scope.totalChange   = $scope.data.TransactionPayment.sum('change');

            $('#savePayment').attr('disabled', false);
          }
        }); 
      }
    }  
  }

   // amount
  $('#amount').keyup(function(){
    $scope.change();
  });

  $scope.changes = 0;

  $scope.change = function(amount,amountToPay) {
    if(amount!=null) {
      if(amount>amountToPay) {
        $scope.changes += amount-amountToPay;
        return amount-amountToPay;
      } else{
        return 0;
      }
    } else {
      return 0;
    }
  }

  $scope.removePayment = function(index,payment) {
    bootbox.confirm('Are you sure you want to delete this payment OR NUMBER ' + payment.orNumber + '?', function(b) {
      if (b) {
          // $scope.data.TransactionPayment.splice(index,1); 
          TransactionPayment.remove({ id: payment.id },function(e){
            if(e.ok) {
              $scope.load();    
            } 
          });   
      }
    });
  }

  // clean table transaction 
  $scope.cleanTable = function(tableTransactionId) {
      bootbox.confirm('Are you sure you want to clean the table?', function(b) {
      if(b) {
        BarPos.remove({ id: tableTransactionId }, function(e){
          if(e.ok) {
            FolioTransaction.save($scope.data,function(e){ 
              if(e.ok){
                $.gritter.add({
                  title: 'Successful!',
                  text: e.msg,
                });
              }
              window.location = '#/bar/pos'; 
            });
            }
        });
      }
    });
  }
});


app.controller('BarPosAddTransactionsController', function($scope, BarPos, $routeParams, Select, BarMenu) {
  $scope.tableId = $routeParams.id;

   // load folio 
  Select.get({ code: 'pos_folios' },function(e) {
    $scope.folios = e.data;
  });
  
  
  $('#form').validationEngine('attach');

  $scope.save = function() {
    
      $scope.data.tableId = $scope.tableId; 
      $scope.data.Transaction = {
          businessId:    3,
          particulars:   'bar orders',
          title:         'bar orders'
        }
      
      BarPos.save($scope.data, function(e){
        if(e.ok){
          $.gritter.add({ title: 'Successful!', text: e.message });
          window.location = '#/bar/pos/table/view/' + $scope.tableId;
        }
      });
  }

  $scope.data = {
    TransactionSub: []  
  };

  subs = [];

  // load menus
  Select.get({ code: 'menus', departmentId: 2 },function(e) {
    $scope.menus = e.data;
  });

  // orders
  $scope.getMenuData = function(menuId) {
      BarMenu.get({ id: menuId }, function(e){
        if(e.ok) {
          $scope.menu = e.data;

          if ($('#menuId').val()!='') {
            $scope.quantity = 1;
            $('#addOrderBt').removeClass('disabled');          
          } else {
            $scope.quantity = null;
            $('#addOrderBt').addClass('disabled');
          } 
        } 
      });
  }
  // transaction subs only// add order
  $scope.saveOrder = function() {
    valid = $("#form").validationEngine('validate');

    if(valid){
      if($scope.quantity!=null) {

        subs = {
          particulars: $scope.menu.Menu.name,
          quantity:    $scope.quantity,
          amount:      $scope.menu.Menu.amount,
          totalAmount: ($scope.menu.Menu.amount*$scope.quantity)
        };  

        $scope.data.TransactionSub.push(subs);

        $scope.total = $scope.data.TransactionSub.sum('totalAmount');
        console.log($scope.total);

        $scope.menuId = '';
        $scope.quantity = null;
        $scope.menu.Menu.amount = null;
      } 

       $('#addOrderBt').addClass('disabled');
    }  
  }

  Array.prototype.sum = function (amt) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][amt];
    }

    return total;
  } 

  $scope.removeOrder = function(index) {
    $scope.data.TransactionSub.splice(index,1); 
    $scope.total = $scope.data.TransactionSub.sum('totalAmount');
    console.log($scope.total);
  }

});
