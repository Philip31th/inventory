app.factory("BarPos", function($resource) {
  return $resource( api + "pos/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("FolioTransaction", function($resource) {
  return $resource( api + "folio-transactions/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("TableTransaction", function($resource) {
  return $resource( api + "table-transactions/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});