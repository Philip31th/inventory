app.config(function($routeProvider) {
  $routeProvider
  .when('/bar/suppliers', {
    templateUrl: tmp + 'bar__suppliers__index',
    controller: 'BarSuppliersController',
  })
  .when('/bar/supplier/add', {
  templateUrl: tmp + 'bar__suppliers__add',
  controller: 'BarSuppliersAddController',
  })
  .when('/bar/supplier/edit/:id', {
  templateUrl: tmp + 'bar__suppliers__edit',
  controller: 'BarSuppliersEditController',
  })
  .when('/bar/supplier/view/:id', {
  templateUrl: tmp + 'bar__suppliers__view',
  controller: 'BarSuppliersViewController',
  })
  .when('/bar/reports/suppliers', {
  templateUrl: tmp + 'bar__suppliers__report',
  controller: 'BarSuppliersReportController',
  }) 
  .when('/bar/reports/supplier/view/:id', {
  templateUrl: tmp + 'bar__suppliers__report_view',
  controller: 'BarSuppliersReportViewController',
  });
});