app.controller('BarSuppliersController', function($scope, BarSupplier) {

  // load suppliers
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    BarSupplier.query(options, function(e) {
      if (e.ok) {
        $scope.suppliers = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code: 'bar' });

  // remove supplier
  $scope.remove = function(supplier) {
    bootbox.confirm('Are you sure you want to delete ' + supplier.name + '?', function(b) {
      if (b) {
        BarSupplier.remove({ id: supplier.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            $scope.load({
              code:   'bar',
              page:   $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
        });
      }
    });
  }
});


app.controller('BarSuppliersAddController', function ($scope, BarSupplier) {
  $('#form').validationEngine('attach');

  // save setting
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    
    if (valid) {
      $scope.data.Supplier.departmentId = 'bar';
      BarSupplier.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });
          window.location = '#/bar/suppliers';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:   e.msg
          });
        }
      });
    }
  }

  // add transaction item
  $scope.data = {
    SupplierItem: []
  }
  items = [];

  $scope.totalSuppliesAmount = function() {
    total = 0;
    for(count = 0; count < $scope.data.SupplierItem.length; count++) {
      total += ($scope.data.SupplierItem[count].price*$scope.data.SupplierItem[count].quantity);
    }
    
    return total;
  }
    
  $scope.addItem = function() {
    $('#supplier_form').validationEngine('attach');

    $scope.supplieritem = {};
    $scope.title = 'ADD NEW';
    $('#add-supplier-item-modal').modal('show');
  }

    $scope.saveItem = function(item) {
      // search if name already exist
      // if ($scope.data.SupplierItem.length > 0) {
      //   for(count = 0; count < $scope.data.SupplierItem.length; count++) {
      //     if($scope.data.SupplierItem[count].name==item.name) {
      //       alert('already exist');
      //     } else {

      valid = $("#supplier_form").validationEngine('validate');
      
      if (valid) {
        if (item.price <= 0 ) {
           $.gritter.add({
            title: 'Warning!',
            text: 'Price must be greater than 0.'
          });
        } else if (item.quantity <=0 ) {
          $.gritter.add({
            title: 'Warning!',
            text: 'Quantity must be 1 or greater.'
          });
        } else {
          $scope.data.SupplierItem.push(item);  
          $('#add-supplier-item-modal').modal('hide');
          $scope.supplieritem = {};
        }
        
      }  
      
      //     }
      //   }
      // }
    } 

  // remove item
  $scope.removeItem = function(index,item) {
    // bootbox.confirm('Are you sure you want to delete this item ' + item.name, function(b) {
    //   if (b) {
        $scope.data.SupplierItem.splice(index,1);     
        $scope.supplieritem = {};
    //   }
    // });
  }   
});


app.controller('BarSuppliersEditController', function ($scope, $routeParams, BarSupplier, BarSupplierItem) {
  $('#form').validationEngine('attach');
  $scope.supplierId = $routeParams.id;

  // load supplier
  $scope.load = function() {
    BarSupplier.get({ id: $scope.supplierId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update supplier
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      BarSupplier.update({ id: $scope.supplierId }, $scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/bar/suppliers';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
  
  // supplier-items
  $scope.data = {
    SupplierItem: []
  }
  items = [];


  $scope.totalSuppliesAmount = function() {
    total = 0;
    for(count = 0; count < $scope.data.SupplierItem.length; count++) {
      total += ($scope.data.SupplierItem[count].price*$scope.data.SupplierItem[count].quantity);
    }
    
    return total;
  }

  $scope.addItem = function() {
    $('#supplier_form').validationEngine('attach');
    // date = new Date();
    $scope.supplieritem = {};
    $scope.title = 'ADD NEW';
    // $scope.supplieritem = {created: date.toString('yyyy-MM-dd hh:mm a')};
    $('#add-supplier-item-modal').modal('show');
  }

    $scope.saveItem = function(item) {
      // search if name already exist
      // if ($scope.data.SupplierItem.length > 0) {
      //   for(count = 0; count < $scope.data.SupplierItem.length; count++) {
      //     if($scope.data.SupplierItem[count].name==item.name) {
      //       alert('already exist');
      //     } else {

      valid = $("#supplier_form").validationEngine('validate');
      
      if (valid) {
        if (item.price <= 0 ) {
           $.gritter.add({
            title: 'Warning!',
            text: 'Price must be greater than 0.'
          });
        } else if (item.quantity <=0 ) {
          $.gritter.add({
            title: 'Warning!',
            text: 'Quantity must be 1 or greater.'
          });
        } else {

          items = {
              supplierId:   $scope.supplierId, 
              name:         item.name, 
              description:  item.description,
              price:        item.price,
              quantity:     item.quantity
            }

          $('#saveSupplierItem').attr('disabled',true);
          BarSupplierItem.save({SupplierItem: items }, function(e){
            if(e.ok) {
              $('#add-supplier-item-modal').modal('hide');
              $scope.supplieritem = {} 
              $scope.load();
              $('#saveSupplierItem').attr('disabled',false);
            }
          });
        }
        
      }  
      
      //     }
      //   }
      // }
    }  

  // edit item
  $scope.editItem = function(item) {
    $scope.supplieritem = item;

    $('#supplier_form').validationEngine('attach');
    $scope.title = 'EDIT';
    $('#add-supplier-item-modal').modal('show');
  }

  // update item
  $scope.updateItem = function(item) {
    valid = $('#supplier_form').validationEngine('validate');

    if(valid) {

      items = {
        id:           item.id,
        supplierId:   $scope.supplierId, 
        name:         item.name, 
        description:  item.description,
        price:        item.price,
        quantity:     item.quantity
      }

      $('#saveSupplierItem').attr('disabled',true);

      BarSupplierItem.save({SupplierItem: items }, function(e){
      if(e.ok) {
        $('#add-supplier-item-modal').modal('hide');
        $scope.supplieritem = {} 
        $scope.load();
        $('#saveSupplierItem').attr('disabled',false);
      }
    });
    }
  }

  // remove item
  $scope.removeItem = function(item) {
    bootbox.confirm('Are you sure you want to delete this item ' + item.name, function(b) {
      if (b) {
        BarSupplierItem.remove({ id: item.id }, function(e) {
          if (e.ok) {
            $scope.load();
          }           
        });
       
      }
    });
  }
});


app.controller('BarSuppliersViewController', function ($scope, $routeParams, BarSupplier) {
  $scope.supplierId = $routeParams.id;

  // load supplier
  $scope.load = function() {
    BarSupplier.get({ id: $scope.supplierId }, function(e) {
      $scope.data = e.data;
      $scope.totals = e.totals;
    });
  }
  $scope.load();

  // remove supplier
  $scope.remove = function(supplier) {
    bootbox.confirm('Are you sure you want to delete ' + supplier.name + '?', function(b) {
      if (b) {
        BarSupplier.remove({ id: supplier.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });
            window.location = '#/bar/suppliers';
          }
        });
      }
    });
  }

  $scope.data = {
    SupplierItem: []
  }

  $scope.totalSuppliesAmount = function() {
    total = 0;
    for(count = 0; count < $scope.data.SupplierItem.length; count++) {
      total += ($scope.data.SupplierItem[count].price*$scope.data.SupplierItem[count].quantity);
    }
    
    return total;
  }
});


app.controller('BarSuppliersReportController', function($scope, BarSupplier, Select, TransactionPayment) {

  // load suppliers
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    BarSupplier.query(options, function(e) {
      if (e.ok) {
        $scope.suppliers = e.payables;
        $scope.totalPayables = e.totalPayables;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code: 'bar' });

  // close button
  $('#close').on('click',function(){
    $scope.searchByName = '';
    document.getElementById("filter").selectedIndex = "0";
  });

   // amount
  $('#amount').keyup(function(){
      $scope.change();
  });
  

  $scope.checkOR = function(orNumber,business) {
    // if(orNumber.length >= 3) {
    if(orNumber!='') {  
      $scope.checking = true;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
      TransactionPayment.query({ search: orNumber , businessId: business },function(e){
        if (e.data) {
          $scope.checking = false;
          $scope.notexisted = false;
          $scope.orNumberExisting = true;
        } else {
          $scope.checking = false;
          $scope.notexisted = true;
          $scope.orNumberExisting = false;
        }
      });
    } else {
      $scope.checking = false;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
    }
  }

  $scope.paymentType = function(paymentType) {
    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
  }

  $scope.change = function(amount,amountToPay) {
    if(amount!=null) {
      if(amount>amountToPay) {
        return amount-amountToPay;
      } else{
        return 0;
      }
    } else {
      return 0;
    }
  }

  // PAYMENTS
  $scope.addPayment = function(supplier) {
    $('#form').validationEngine('attach');

    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
    $scope.payment = {};

    // payment types selection
    Select.get({ code: 'payment-types', supplierPayment: true }, function(e) {
      $scope.paymentTypes = e.data;
    });

    $scope.amountToPay = supplier.totalPayables;
    $scope.title = 'ADD';
    $scope.supplierId = supplier.id;
    $scope.business = 'bar';

    $('#add-payment-modal').modal('show');   
  }

  $scope.savePayment = function(payment) {

    valid = $("#form").validationEngine('validate');

    if (valid){
     
      if($('#change').val()==null || $('#change').val()=='') {
        changee = 0;
      } else {
        changee = parseFloat($('#change').val());
      }

      payment = {
        supplierId       : $scope.supplierId,
        transactionId    : null,
        orNumber         : payment.orNumber,
        amount           : payment.amount,
        change           : changee,
        paymentType      : payment.paymentType,
        accountName      : payment.accountName,
        cardNumber       : payment.cardNumber,
        chequeNumber     : payment.chequeNumber,
        expirationYear   : payment.expirationYear,
        expirationMonth  : payment.expirationMonth,
        bankName         : payment.bankName,
        date             : Date.today()
      }

      if (payment.amount==0 || payment.amount=='') {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must have value to save'
        });$('#savePayment').attr('disabled', false);
      } else if ($scope.orNumberExisting) {
        $.gritter.add({
          title: 'Warning!',
          text: 'OR Number already existed.'
        });   $('#savePayment').attr('disabled', false);
      } else if (payment.paymentType!='cash' && payment.amount > $scope.amountToPay) {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must not exceed to transaction balance'
        });$('#savePayment').attr('disabled', false);
      } else {
        $('#savePayment').attr('disabled', true);
        TransactionPayment.save({ TransactionPayment: payment }, function(e) {
          console.log(e);
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });

            $scope.load({ code: 'bar' });
            $('#add-payment-modal').modal('hide'); 
            $scope.payment = {};
            $('#savePayment').attr('disabled', false);
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }  
    }  
  }  

  $scope.viewSuppliesPaymentHistory = function() {  
    document.getElementById("filter").selectedIndex = "0";
    $scope.filter = '';
    $scope.searchByName = '';
    $scope.searchByDate = Date.today().toString('yyyy-MM-dd');

    $('#searchByDate').datepicker({ 
      format: 'yyyy-mm-dd', 
      autoclose: true
    });

    $('#searchDate2').datepicker({ 
      format: 'yyyy-mm-dd', 
      autoclose: true
    });

    $("#searchByDate").datepicker('setDate', $scope.searchByDate);

    $scope.load2({ code: 'bar' , searchDate: $scope.searchByDate });
    $('#bar-supplies-payment-history-modal').modal('show');
  }

  $scope.load2 = function(options){
    options = typeof options !== 'undefined' ?  options : {};
     BarSupplier.get(options,function(e){
        if(e.ok) {
          $scope.payments = e.supplies_payments;

          // paginator
          $scope.paginator  = e.paginator;
          $scope.pages = paginator($scope.paginator, 5);
        }
    });
  };

  $scope.filterPayment = function(filterBy,date) {
    if (filterBy=='date') {
      $scope.date1(date);
      $scope.load2({ code: 'bar' , searchDate: date });
    } else if (filterBy==''||filterBy==null) {
      $scope.load2({ code: 'bar' });
    } else if (filterBy=='both') {
      $scope.date2(date);
      $scope.load2({ code: 'bar' , searchDate: date });
    }
  }

  $scope.date1 = function(date) {
    $('#searchByDate').datepicker({ 
      format: 'yyyy-mm-dd', 
      autoclose: true
    });

    $("#searchByDate").datepicker('setDate', date);
  }

  $scope.date2 = function(date) {
    $('#searchDate2').datepicker({ 
      format: 'yyyy-mm-dd', 
      autoclose: true
    });
  }

  $scope.filters = function() {
    if ($scope.filter=='' || $scope.filter==null) {
      $scope.load2({ code: 'bar' , searchDate: $scope.searchByDate });
    } 
    if($scope.filter == 'supplier') {
      $scope.load2({ code: 'bar', searchName: $scope.searchByName2 });
    }
    if($scope.filter == 'both') {
      $scope.load2({ code: 'bar', searchDate: $scope.searchDate2, searchName: $scope.searchByName2 });
    }
  }


});  

app.controller('BarSuppliersReportViewController', function($scope, $routeParams, BarSupplier, TransactionPayment, Select) {
  $scope.supplierId = $routeParams.id;

  // load supplier
  $scope.load = function() {
    BarSupplier.get({ id: $scope.supplierId }, function(e) {
      $scope.payments = e.payments;
      $scope.supplier = e.data;
      $scope.totals = e.totals;
    });
  }
  $scope.load();

  // remove transaction payment
  $scope.removePayment = function(payment) {
    bootbox.confirm('Are you sure you want to delete  this payment?', function(b) {
      if (b) {
        $('#removePayment').attr('disabled', true);
        TransactionPayment.remove({ id: payment.id }, function(e) {
          if (e.ok) {
            $scope.load();
          }
        });
      }
    });
  }

  // close button
  $('#close').on('click',function(){
    $scope.payment = {};
  });

  // amount
  $('#amount').keyup(function(){
      $scope.change();
  });
  
  $scope.change = function(amount,amountToPay) {
    if(amount!=null) {
      if(amount>amountToPay) {
        return amount-amountToPay;
      } else{
        return 0;
      }
    } else {
      return 0;
    }
  }
  $scope.checkOR = function(orNumber,business) {
    // if(orNumber.length >= 3) {
    if(orNumber!='') {  
      $scope.checking = true;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
      TransactionPayment.query({ search: orNumber , businessId: business },function(e){
        if (e.data) {
          $scope.checking = false;
          $scope.notexisted = false;
          $scope.orNumberExisting = true;
        } else {
          $scope.checking = false;
          $scope.notexisted = true;
          $scope.orNumberExisting = false;
        }
      });
    } else {
      $scope.checking = false;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
    }
  }

  $scope.paymentType = function(paymentType) {
    $scope.checking = false;
    $scope.notexisted = false;
    // $scope.orNumberExisting = false;
  }

  // PAYMENTS
  $scope.addPayment = function() {
    $('#form').validationEngine('attach');

    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
    $scope.payment = {};


    // payment types selection
    Select.get({ code: 'payment-types', supplierPayment: true }, function(e) {
      $scope.paymentTypes = e.data;
    });

    $scope.amountToPay = $scope.totals.totalAmount - $scope.totals.totalPayments;
    $scope.title = 'ADD';
    $scope.supplierId = $scope.supplier.Supplier.id;
    $scope.business = 'bar';
    $('#add-payment-modal').modal('show');   
  }

  $scope.savePayment = function(payment) {

    valid = $("#form").validationEngine('validate');

    if (valid){
      $('#savePayment').attr('disabled', true);
      if($('#change').val()==null || $('#change').val()=='') {
        changee = 0;
      } else {
        changee = parseFloat($('#change').val());
      }

      payment = {
        supplierId       : $scope.supplierId,
        transactionId    : null,
        orNumber         : payment.orNumber,
        amount           : payment.amount,
        change           : changee,
        paymentType      : payment.paymentType,
        accountName      : payment.accountName,
        cardNumber       : payment.cardNumber,
        chequeNumber     : payment.chequeNumber,
        expirationYear   : payment.expirationYear,
        expirationMonth  : payment.expirationMonth,
        bankName         : payment.bankName,
        date             : Date.today()
      }

      if (payment.amount==0 || payment.amount=='') {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must have value to save'
        });
         $('#savePayment').attr('disabled', false);
      }  else if ($scope.orNumberExisting) {
        $.gritter.add({
          title: 'Warning!',
          text: 'OR Number already existed.'
        });     
         $('#savePayment').attr('disabled', false);
      } else if (payment.paymentType!='cash' && payment.amount > $scope.amountToPay) {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must not exceed to transaction balance'
        });
         $('#savePayment').attr('disabled', false);
      } else {
         $('#savePayment').attr('disabled', true);
        TransactionPayment.save({ TransactionPayment: payment }, function(e) {
          console.log(e);
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });

            $scope.load();

            $('#add-payment-modal').modal('hide'); 
            $scope.payment = {};
             $('#savePayment').attr('disabled', false);
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }  
    }  
  } 
});
