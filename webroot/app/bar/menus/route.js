app.config(function($routeProvider) {
  $routeProvider
  .when('/bar/menus', {
    templateUrl: tmp + 'bar__menus__index',
    controller: 'BarMenuController',
  })
  .when('/bar/menu/view/:id', {
    templateUrl: tmp + 'bar__menus__view',
    controller: 'BarMenuViewController',
  })
  .when('/bar/menu/add', {
    templateUrl: tmp + 'bar__menus__add',
    controller: 'BarMenuAddController',
  })
  .when('/bar/menu/edit/:id', {
    templateUrl: tmp + 'bar__menus__edit',
    controller: 'BarMenuEditController',
  });
});