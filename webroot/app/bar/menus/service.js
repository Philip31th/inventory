app.factory("BarMenu", function($resource) {
  return $resource( api + "menus/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});