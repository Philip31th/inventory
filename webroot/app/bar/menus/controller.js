app.controller('BarMenuController', function($scope, BarMenu) {

  // load bar menus
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    BarMenu.query(options, function(e) {
      if (e.ok) {
        $scope.menus = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code: 'bar' });

  // remove bar menu
  $scope.remove = function(menu) {
    bootbox.confirm('Are you sure you want to delete ' + menu.name + '?', function(b) {
      if (b) {
        BarMenu.remove({ id: menu.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            $scope.load({
              code:   'bar',
              page:   $scope.paginator.page,
              search: $scope.searchTxt
            });
          } else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }
        });
      }
    });
  }
});

app.controller('BarMenuViewController', function($scope, $routeParams, BarMenu) {
  $scope.menuId = $routeParams.id;

// load resto menu
  $scope.load = function() {
    BarMenu.get({ id: $scope.menuId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

// remove resto menu  
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete ' + data.name + '?', function(c) {
      if(c){
        BarMenu.remove({ id: data.id }, function(e) {
          if(e.ok){
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });
            window.location = '#/bar/menus';
          }
        });
      }
    });
  }

});

app.controller('BarMenuAddController', function($scope, BarMenu) {
  $('#form').validationEngine('attach');

  // save bar menu
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    
    if (valid) {
      $scope.data.Menu.departmentId = 'bar';
      BarMenu.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });
          window.location = '#/bar/menus';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:   e.msg
          });
        }
      });
    }
  }
});

app.controller('BarMenuEditController', function($scope, $routeParams, BarMenu) {
  $('#form').validationEngine('attach');
  $scope.barMenuId = $routeParams.id;

  // load bar menu
  $scope.load = function() {
    BarMenu.get({ id: $scope.barMenuId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  

  // update bar menu
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      $scope.data.Menu.departmentId = 'bar';
      BarMenu.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/bar/menus';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});