app.factory("BarInventory", function($resource) {
  return $resource( api + "inventories/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("BarInventoryConsumptions", function($resource) {
  return $resource( api + "inventories/consumptions", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});