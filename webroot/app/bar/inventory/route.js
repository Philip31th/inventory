app.config(function($routeProvider) {
  $routeProvider
  .when('/bar/inventories', {
    templateUrl: tmp + 'bar__inventory__index',
    controller: 'BarInventoryController',
  })
  .when('/bar/inventory/add', {
    templateUrl: tmp + 'bar__inventory__add',
    controller: 'BarInventoryAddController',
  })
  .when('/bar/inventory/edit/:id', {
    templateUrl: tmp + 'bar__inventory__edit',
    controller: 'BarInventoryEditController',
  })
  .when('/bar/inventory/view/:id', {
    templateUrl: tmp + 'bar__inventory__view',
    controller: 'BarInventoryViewController',
  })
  .when('/bar/inventory/daily-consumption', {
    templateUrl: tmp + 'bar__inventory__daily_consumption',
    controller: 'BarDailyConsumptionController',
  });
});