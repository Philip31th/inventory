app.config(function($routeProvider) {
  $routeProvider
  .when('/bar/tables', {
    templateUrl: tmp + 'bar__tables__index',
    controller: 'BarTableController',
  })
  .when('/bar/table/add', {
  templateUrl: tmp + 'bar__tables__add',
  controller: 'BarTableAddController',
  })
  .when('/bar/table/edit/:id', {
  templateUrl: tmp + 'bar__tables__edit',
  controller: 'BarTableEditController',
  })
  .when('/bar/table/view/:id', {
  templateUrl: tmp + 'bar__tables__view',
  controller: 'BarTableViewController',
  });
});