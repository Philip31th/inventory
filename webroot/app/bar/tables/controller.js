app.controller('BarTableController', function($scope, BarTable) {

 // load bar table
 $scope.load = function(options) {
  options = typeof options !== 'undefined' ? options : {};
  BarTable.query(options,function(e){
    if (e.ok) {
      // get data
      $scope.tables    = e.data;

      // paginator
      $scope.paginator = e.paginator;
      $scope.pages     = paginator($scope.paginator,5);
    };
  });
 }

 // load function
$scope.load({ code : 'bar' });

$scope.remove = function(table) {
   bootbox.confirm('Are you sure you want to delete ' + table.name + '?', function(b) {
     if (b) {
       BarTable.remove({ id: table.id }, function(e) {
         if (e.ok) {
           $.gritter.add({
            title: 'Successful!',
             text:   e.msg,
           });

           $scope.load({
            code:   'bar',
            page:   $scope.paginator.page,
            search: $scope.searchTxt
          });
         } else {
          $.gritter.add({
             title: 'Warning!',
             text:   e.msg,
           });
        }
      });
     }
   });
  }

});


app.controller('BarTableAddController', function($scope, BarTable) {
$('#form').validationEngine('attach');
  // save bar menu
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    
    if (valid) {
      $scope.data.Table.departmentId = 'bar';
      BarTable.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });
         window.location = '#/bar/tables';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:   e.msg
          });
        }
      });
    }
  }
  
});


app.controller('BarTableEditController', function($scope, $routeParams, BarTable) {
  // Parameter
    $scope.id = $routeParams.id;
  // load bar table view
    BarTable.get({id:$scope.id}, function(e){
      $scope.data = e.result;
    });
  // update bar table
    $scope.update = function(){
    valid = $("#form").validationEngine('validate');
    if(valid){
    $scope.data.Table.departmentId = 'bar';
    BarTable.update({id:$scope.id}, $scope.data, function(e){
      if(e.ok){
        $.gritter.add({ 
          title: 'Successful!',
          text :  e.msg });
        window.location = '#/bar/tables';
      } else {
        $.gritter.add({ 
          title: 'Warning!',
          text :  e.msg });
      }
    });
    }
  }

});


app.controller('BarTableViewController', function($scope, $routeParams, BarTable) {
// Parameters
  $scope.id = $routeParams.id;
// load Bar table view
  BarTable.get({id:$scope.id}, function(e){
    $scope.data = e.result; 
  });
// remove table  
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete bar name ' + data.name + '?', function(c) {
      if(c){
        BarTable.remove({ id: data.id }, function(e) {
          if(e.ok){
            $.gritter.add({
              title: 'Successful!',
              text :  e.msg
            });
            window.location = '#/bar/tables';
          }
        });
      }
    });
  }

});