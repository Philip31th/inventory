app.factory("BarTable", function($resource) {
  return $resource( api + 'tables/:id', {id:'@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});