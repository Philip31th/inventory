app.directive('icheck', function($timeout, $parse) {
  return {
    require: 'ngModel',
    link: function($scope, element, $attrs, ngModel) {
      return $timeout(function() {
        var value;
        value = $attrs['value'];
        $scope.$watch($attrs['ngModel'], function(newValue){
          $(element).iCheck('update');
        });
        return $(element).iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue'
        }).on('ifChanged', function(event) {
          if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
            $scope.$apply(function() {
                return ngModel.$setViewValue(event.target.checked);
            });
          }
          if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
            return $scope.$apply(function() {
                return ngModel.$setViewValue(value);
            });
          }
        });
      });
    }
  };
});

app.directive('amount', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attr, ngModel) {
      element.on('keypress', function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (
          (charCode != 46 || $(element).val().indexOf('.') != -1) &&  // “.” CHECK DOT, AND ONLY ONE.
          (charCode  < 48 || charCode > 57)
        ) return false;
        return true;
      });

      element.on('blur', function(evt) {
        if (ngModel.$viewValue == '' || isNaN(ngModel.$viewValue))
          result = 0;
        else
          result = parseFloat(ngModel.$viewValue);
        return scope.$apply(function() {
          $(element).val(result);
          return ngModel.$setViewValue(result);
        });
      });
    }
  };
});

app.directive('number', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attr, ngModel) {
      element.on('keypress', function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ((charCode  < 48 || charCode > 57))
          return false;
        return true;
      });

      element.on('blur', function(evt) {
        if (ngModel.$viewValue == '' || isNaN(ngModel.$viewValue))
          result = 0;
        else
          result = parseInt(ngModel.$viewValue);
        return scope.$apply(function() {
          $(element).val(result);
          return ngModel.$setViewValue(result);
        });
      });
    }
  };
});

app.directive('date', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attr, ngModel) {
      format = typeof format !== 'undefined' ?  format : 'mm/dd/yyyy';
      $(element).datepicker({
        format:    format,
        autoclose: true
      });
    }
  };
});

// app.directive('autoCompleteDirective',function($http){
//   return {
//     restrict : 'A',
//     scope : {
//       url : '@'
//     },
//     link: function(scope, elm, attrs) {
//       elm.autocomplete({
//         source : function(request, response) {
//           $http.get({ method: 'json', url: scope.url, params: { q:request.term } }).success(function(data){
//             response(data);
//             // $scope.coys = response;
//           })
//         }
//       })
//     }
//   };
// });

