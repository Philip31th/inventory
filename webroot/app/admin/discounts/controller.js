app.controller('DiscountController', function($scope, Discount) {

  // load settings
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Discount.query(options, function(e) {
      if (e.ok) {
        $scope.discounts = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove settings
  $scope.remove = function(discount) {
    bootbox.confirm('Are you sure you want to delete ' + discount.name + '?', function(b) {
      if (b) {
        Discount.remove({ id: discount.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            $scope.load({
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
        });
      }
    });
  }
});


app.controller('DiscountAddController', function($scope, Discount) {
  $('#form').validationEngine('attach');

  // save discount
  $scope.save = function(){
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Discount.save($scope.data, function(e) {
        if(e.ok){
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });
          window.location = '#/discounts';
        }
      });
    }
  };
});


app.controller('DiscountEditController', function($scope, $routeParams, Discount) {
  $('#form').validationEngine('attach');
  $scope.discountId = $routeParams.id;

  // load discount
  $scope.load = function() {
   Discount.get({ id: $scope.discountId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update discount
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Discount.update({ id: $scope.discountId }, $scope.data, function(e) {
        if(e.ok){
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });

          window.location = '#/discounts';
        }
      });
    }
  };
});


app.controller('DiscountViewController', function($scope, $routeParams, Discount) {
  $('#form').validationEngine('attach');
  $scope.discountId = $routeParams.id;

  // load discount
  $scope.load = function() {
   Discount.get({ id: $scope.discountId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove discount
  $scope.remove = function(discount) {
    bootbox.confirm('Are you sure you want to delete ' + discount.name + '?', function(b) {
      if (b) {
        Discount.remove({ id: discount.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            window.location = '#/discounts';
          }
        });
      }
    });
  }
});