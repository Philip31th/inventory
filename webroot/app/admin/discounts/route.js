app.config(function($routeProvider) {
  $routeProvider
  .when('/discounts', {
    templateUrl: tmp + 'admin__discounts__index',
    controller: 'DiscountController',
  })
  .when('/discount/view/:id', {
    templateUrl: tmp + 'admin__discounts__view',
    controller: 'DiscountViewController',
  })
  .when('/discount/add', {
    templateUrl: tmp + 'admin__discounts__add',
    controller: 'DiscountAddController',
  })
  .when('/discount/edit/:id', {
    templateUrl: tmp + 'admin__discounts__edit',
    controller: 'DiscountEditController',
  });
});