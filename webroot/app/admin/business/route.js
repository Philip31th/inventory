app.config(function($routeProvider) {
  $routeProvider
  .when('/business', {
    templateUrl: tmp + 'admin__business__index',
    controller: 'BusinessController',
  })
  .when('/business/view/:id', {
    templateUrl: tmp + 'admin__business__view',
    controller: 'BusinessViewController',
  })
  .when('/business/edit/:id', {
    templateUrl: tmp + 'admin__business__edit',
    controller: 'BusinessEditController',
  });
});