app.controller('BusinessController', function($scope, Business) {

  // load business
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Business.query(options, function(e) {
      if (e.ok) {
        $scope.businesses = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove business
  $scope.remove = function(type) {
    bootbox.confirm('Are you sure you want to delete ' + type.name + '?', function(b) {
      if (b) {
        Service.remove({ id: type.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: 'Membership type has been removed.',
            });

            $scope.load({
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
        });
      }
    });
  }
});

app.controller('BusinessViewController', function($scope, $routeParams, Business, BusinessService) {
  $scope.businessId = $routeParams.id;

  // load business
  $scope.load = function() {
    Business.get({ id: $scope.businessId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // add service
  $('#add-service-form').validationEngine('attach');
  $scope.addService = function() {
    $scope.service = {
      BusinessService: {
        businessId: $scope.businessId
      }
    }
    $('#add-service-modal').modal('show');
  }
  $scope.saveService = function() {
    BusinessService.save($scope.service, function(e) {
      if (e.ok) {
        $.gritter.add({
          title: 'Successful!',
          text: e.msg
        });

        $scope.load();
        $('#add-service-modal').modal('hide');
      } else {
        $.gritter.add({
          title: 'Warning!',
          text: e.msg
        });
      }
    });
  }

  // edit service
  $('#edit-service-form').validationEngine('attach');
  $scope.editService = function(service) {
    BusinessService.get({ id: service.id }, function(e) {
      if (e.ok)
        $scope.eservice = e.data;
    });

    $('#edit-service-modal').modal('show');
  }
  $scope.updateService = function() {
    BusinessService.update({ id: $scope.eservice.BusinessService.id }, $scope.eservice, function(e) {
      if (e.ok) {
        $.gritter.add({
          title: 'Successful!',
          text: e.msg
        });

        $scope.load();
        $('#edit-service-modal').modal('hide');
      } else {
        $.gritter.add({
          title: 'Warning!',
          text: e.msg
        });
      }
    });
  }

  // remove service
  $scope.removeService = function(service) {
    bootbox.confirm('Are you sure you want delete ' + service.name + '?', function(c) {
      if (c) {
        BusinessService.remove({ id: service.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            }); 
            $scope.load();
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }
    });
  }
});

app.controller('BusinessEditController', function($scope, $routeParams, Business) {
  $('#form').validationEngine('attach');
  $scope.businessId = $routeParams.id;

  // load business
  $scope.load = function() {
    Business.get({ id: $scope.businessId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update business
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      Business.update({ id: $scope.businessId }, $scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/business';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});