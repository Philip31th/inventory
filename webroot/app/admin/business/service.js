app.factory("Business", function($resource) {
  return $resource( api + "businesses/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("BusinessService", function($resource) {
  return $resource( api + "business-services/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});