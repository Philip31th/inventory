app.config(function($routeProvider) {
  $routeProvider
  .when('/logs', {
    templateUrl: tmp + 'admin__logs__index',
    controller: 'LogController',
  })
  .when('/log/view/:id', {
    templateUrl: tmp + 'admin__logs__view',
    controller: 'LogViewController',
  });
});