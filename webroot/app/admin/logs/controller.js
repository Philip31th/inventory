app.controller('LogController', function($scope, Log) {

  // load settings
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Log.query(options, function(e) {
      if (e.ok) {
        $scope.logs = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  $scope.print = function(){
    $scope.load();
    printTable(base + 'print/logs');
  }

});