app.factory("Room", function($resource) {
  return $resource( api + "rooms/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
  });
});

app.factory("Occupied", function($resource) {
  return $resource( api + "rooms/occupied", {}, {
    query: { method: 'GET', isArray: false }
  });
});
