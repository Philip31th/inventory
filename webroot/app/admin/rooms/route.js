app.config(function($routeProvider) {
  $routeProvider
  .when('/hotel/rooms', {
    templateUrl: tmp + 'admin__rooms__index',
    controller: 'RoomController',
  })
  .when('/hotel/room/add', {
    templateUrl: tmp + 'admin__rooms__add',
    controller: 'RoomAddController',
  })
  .when('/hotel/room/edit/:id', {
    templateUrl: tmp + 'admin__rooms__edit',
    controller: 'RoomEditController',
  })
  .when('/hotel/room/view/:id', {
    templateUrl: tmp + 'admin__rooms__view',
    controller: 'RoomViewController',
  })
    .when('/hotel/rooms/occupied', {
    templateUrl: tmp + 'admin__rooms__occupied',
    controller: 'OccupiedController',
  });
});