app.controller('RoomController', function($scope, Room) {
  $scope.searchTxt = Date.parse('today').toString('MM/dd/yyyy');

  // load rooms
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Room.query(options, function(e) {
      if (e.ok) {
        $scope.rooms = e.data;
      }
    });
  }
  $scope.load();

  // remove room  
  $scope.remove = function(room) {
    bootbox.confirm('Are you sure you want to delete room #' + room.name + '?', function(c) {
      if (c) {
        Room.remove({ id: room.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });

            $scope.load();
          }
        });
      }
    });
  }
});


app.controller('RoomAddController', function($scope, Room, Select) {
  $('#form').validationEngine('attach');

  // room type selection
  Select.get({ code: 'room-types' }, function(e) {
    $scope.roomTypes = e.data;
  });
  
  // save room
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Room.save($scope.data, function(e){
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });

          window.location = '#/hotel/rooms';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:   e.msg
          });
        }
      });
    } else {
      $.gritter.add({
        title: 'Warning!',
        text: 'Please fill up required fields.'
      });
    }
  }
});

app.controller('RoomEditController', function($scope, $routeParams, Room, Select) {
  $scope.roomId = $routeParams.id;

  // load room
  $scope.load = function() {
    Room.get({ id: $scope.roomId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // room type selection
  Select.get({ code: 'room-types' }, function(e) {
    $scope.roomTypes = e.data;
  });

  // update room
  $scope.update = function() {
    Room.update({ id: $scope.roomId }, $scope.data, function(e) {
      if (e.ok) {
        $.gritter.add({
          title: 'Successful!',
          text:   e.msg
        });

        window.location = '#/hotel/rooms'; 
      } else {
        $.gritter.add({
          title: 'Warning!',
          text:   e.msg
        });
      }
    });
  }
});

app.controller('RoomViewController', function($scope, $routeParams, Room) {
  $scope.roomId = $routeParams.id;

  // load room
  $scope.load = function() {
    Room.get({ id: $scope.roomId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove room  
  $scope.remove = function(room) {
    bootbox.confirm('Are you sure you want to delete room #' + room.name + '?', function(c) {
      if (c) {
        Room.remove({ id: room.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });

            window.location = '#/hotel/rooms';
          }
        });
      }
    });
  }
});

app.controller('OccupiedController', function($scope, $timeout, Occupied) {
  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({ format: 'mm/dd/yyyy', autoclose: true}).datepicker('setDate', $scope.strSearch );
  
  $scope.load = function() {
    Occupied.query({date:$scope.strSearch}, function(e){
      $scope.occupieds = e.occupied;
      $scope.outs = e.out;
    });
  } 
  $scope.load();
});
