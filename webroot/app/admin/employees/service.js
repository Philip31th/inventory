app.factory("Employee", function($resource) {
  return $resource( api + "employees/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    save: {
      method: 'POST',
      headers: { 'Content-Type': undefined, enctype: 'multipart/form-data' },
      transformRequest: function(data) {
        // transform data
        var formData = new FormData();
        formData.append('data', JSON.stringify(data));

        // attach file
        employeeImage = document.getElementById('employeeImage');
        if (employeeImage != null && employeeImage.files.length > 0)
          formData.append('attachment', employeeImage.files[0]);

        return formData;
      }
    }
  });
});

app.factory("EmployeeLeave", function($resource) {
  return $resource( api + "employee-leaves/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});
