app.controller('EmployeeController', function($scope, Employee) {

  // load employees
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Employee.query(options, function(e) {
      if (e.ok) {
        $scope.employees = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove employee
  $scope.remove = function(employee) {
    bootbox.confirm('Are you sure you want to delete ' + employee.name + '?', function(b) {
      if (b) {
        Employee.remove({ id: employee.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

            $scope.load({
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
        });
      }
    });
  }
});

app.controller('EmployeeViewController', function($scope, $routeParams, Employee, EmployeePerformance, Select, EmployeeLeave) {
  modalMaxHeight();
  
  $scope.employeeId = $routeParams.id;

  // load business
  $scope.load = function() {
    Employee.get({ id: $scope.employeeId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();
  
  $scope.viewRemarks = function() {
     Employee.get({ id: $scope.employeeId }, function(e) {
          $scope.vperformances = e.data;
        }); 
    $('#employee-performances-modal').modal('show');
  }

  $scope.addRemarks = function() {
    $("#form").validationEngine('attach');

    $scope.title = "ADD";
    $('#employee-performances-edit-modal').modal('show');
  }
  
  $scope.editRemarks = function(employeeperformances) {
    $scope.title = "EDIT";
    $scope.vdata = {
      vremarks:    employeeperformances.remarks,
      vid:         employeeperformances.id,
      vemployeeid: employeeperformances.employeeId
    }
    
    $('#employee-performances-edit-modal').modal('show');
   }   
  // save remarks

  $scope.saveRemarks = function(){
    valid = $("#form").validationEngine('validate');
    
    if (valid) {
      
      $scope.data = {
        employeeId: $scope.employeeId,
        remarks:    $scope.vdata.vremarks
      }
      
      EmployeePerformance.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
           $('#employee-performances-edit-modal').modal('hide');
           $scope.viewRemarks();
           $scope.load();
            $scope.vdata = {}
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
    
    
  }
  
  $scope.updateremarks = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      EmployeePerformance.save($scope.vdata, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
            $('#employee-performances-edit-modal').modal('hide');
            $scope.viewRemarks();
            $scope.vdata = {}
         }else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
 
  // remove employee
  $scope.remove = function(employee) {
    bootbox.confirm('Are you sure you want to delete ' + employee.firstName + '?', function(b) {
      if (b) {
        Employee.remove({ id: employeeperformance.id }, function(e) {
          if (e.ok) {
            
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });
          }
          window.location = "#/employees";
        });
      }
    });
  }
  // remove employee
  $scope.removeperformance = function(performance) {
    bootbox.confirm('Are you sure you want to delete ' + performance.remarks + '?', function(b) {
      if (b) {
        EmployeePerformance.remove({ id: performance.id },function(e){
          if(e.ok) {
            $scope.viewRemarks();
          }
        });
      }
    });
  }

  $scope.viewLeaves = function() {
    Employee.get({ id: $scope.employeeId }, function(e) {
      $scope.vleave = e.data;
    }); 
    $('#view-leaves-modal').modal('show');
  }

  $scope.addLeave = function(leave) {
    $scope.title="ADD";
  
    $scope.aleave = {
      employeeId: $scope.employeeId
    }
    
    startDate = '';
    endDate = '';
  
    $("#start").datepicker('setDate', startDate);
    $("#end").datepicker('setDate', endDate);
    
    $('#add-leaves-modal').modal('show');
  }
 
  $('#start').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      startDate: Date.today().toString('MM/dd/yyyy')
    });
    
    $('#end').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true,
      startDate: Date.today().toString('MM/dd/yyyy')
    });
    
  // save Leave
  $scope.saveLeave = function() {
    
    EmployeeLeave.save($scope.aleave,function(e){
      if(e.ok) {
        $('#add-leaves-modal').modal('hide');
        $scope.viewLeaves();
        $scope.aleave = {}
      }
    });
  }
  
   $scope.editLeave = function(leave) {
    $scope.title = "EDIT";
    $scope.aleave = {
      id:           leave.id,
      startDate:    leave.start_date,
      endDate:      leave.end_date,
      leaveType:    leave.leaveType,
      purpose:      leave.purpose
    }
    
    startDate = Date.parse(leave.start_date).toString('MM/dd/yyyy');
    endDate = Date.parse(leave.end_date).toString('MM/dd/yyyy');
  
    $("#start").datepicker('setDate', startDate);
    $("#end").datepicker('setDate', endDate);
  
    $('#add-leaves-modal').modal('show');
   }   
   
   $scope.updateLeave = function() {
      valid = $("#form").validationEngine('validate');
      if (valid) {
        EmployeeLeave.save($scope.aleave, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });
              $('#add-leaves-modal').modal('hide');
              $scope.viewLeaves();
              $scope.aleave = {}
           }else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }
   }
   
   $scope.removeLeave = function(leave) {
      bootbox.confirm('Are you sure you want to delete this ?', function(b) {
      if (b) {
        EmployeeLeave.remove({ id: leave.id },function(e){
          if(e.ok) {
            $scope.viewLeaves();
          }
        });
      }
    });
   }
});

app.controller('EmployeeAddController', function($scope, Employee, Select) {
  $('#form').validationEngine('attach');

  // department selection
  Select.get({ code: 'departments' }, function(e) {
    $scope.departments = e.data;
  });

  // save employee
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      Employee.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/employees';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
  
  $('#dateHired').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true
    });

});

app.controller('EmployeeEditController', function($scope, $routeParams, Employee, Select) {
  $('#form').validationEngine('attach');
  $scope.employeeId = $routeParams.id;

  // load employee
  $scope.load = function() {
    Employee.get({ id: $scope.employeeId }, function(e) {
      $scope.data = e.data;
    });
    // department selection
    Select.get({ code: 'departments' }, function(e) {
      $scope.departments = e.data;
    });
  }
  $scope.load();

  // update employee
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      Employee.update({ id: $scope.employeeId } ,$scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/employees';
        }else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});