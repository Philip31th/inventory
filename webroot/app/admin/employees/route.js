app.config(function($routeProvider) {
  $routeProvider
  .when('/employees', {
    templateUrl: tmp + 'admin__employees__index',
    controller: 'EmployeeController',
  })
  .when('/employee/view/:id', {
    templateUrl: tmp + 'admin__employees__view',
    controller: 'EmployeeViewController',
  })
  .when('/employee/add', {
    templateUrl: tmp + 'admin__employees__add',
    controller: 'EmployeeAddController',
  })
  .when('/employee/edit/:id', {
    templateUrl: tmp + 'admin__employees__edit',
    controller: 'EmployeeEditController',
  });
});