app.factory("DailyTimeRecord", function($resource) {
  return $resource( api + "daily-time-records/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
  });
});