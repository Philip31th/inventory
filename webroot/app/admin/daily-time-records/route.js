app.config(function($routeProvider) {
  $routeProvider
  .when('/daily-time-records', {
    templateUrl: tmp + 'admin__daily_time_records__index',
    controller: 'DailyTimeRecordController',
  })
  .when('/daily-time-records/add', {
    templateUrl: tmp + 'admin__daily_time_records__add',
    controller: 'DailyTimeRecordAddController',
  })
  .when('/daily-time-records/edit/:id', {
    templateUrl: tmp + 'admin__daily_time_records__edit',
    controller: 'DailyTimeRecordEditController',
  })
  .when('/daily-time-records/view/:id', {
    templateUrl: tmp + 'admin__daily_time_records__view',
    controller: 'DailyTimeRecordViewController',
  });
});