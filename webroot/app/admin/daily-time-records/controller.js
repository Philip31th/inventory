app.controller('DailyTimeRecordController', function($scope, DailyTimeRecord) {

  // load 
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    DailyTimeRecord.query(options, function(e) {
      if (e.ok) {
        $scope.datas = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove dtr
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete ' + data.name + ' DTR?', function(b) {
      if (b) {
        DailyTimeRecord.remove({ id: data.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Succesful!',
              text: e.msg,
            });
            $scope.load();
          }
        });
      }
    });
  }
  
});


app.controller('DailyTimeRecordAddController', function($scope, DailyTimeRecord, Select) {

  // get employees
  Select.get({code: 'employees'},function(e){
    $scope.employees = e.data;
  }); 

  $('#form').validationEngine('attach');

  // save employee
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      DailyTimeRecord.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/daily-time-records';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }

  
  $scope.open = {
    timeIn: false,
    timeOut: false
  };
  
  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
  };

  $scope.dateOptions = {
    showWeeks: false,
    startingDay: 1
  };
  
  $scope.timeOptions = {
    readonlyInput: true,
    showMeridian: false
  };
  
  $scope.openCalendar = function(e, date) {
      e.preventDefault();
      e.stopPropagation();

      $scope.open[date] = true;
  };
  
});


app.controller('DailyTimeRecordEditController', function($scope, $routeParams, DailyTimeRecord, Select) {
  $('#form').validationEngine('attach');
  $scope.dtrId = $routeParams.id;
  
  // get employees
  Select.get({code: 'employees'},function(e){
    $scope.employees = e.data;
  }); 

  // load Daily Time Record
  $scope.load = function() {
    DailyTimeRecord.get({ id: $scope.dtrId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update Daily Time Record
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      DailyTimeRecord.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/daily-time-records';
        }else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }

});


app.controller('DailyTimeRecordViewController', function($scope, $routeParams, DailyTimeRecord) {
 $scope.id = $routeParams.id;
 
 // load settings
  $scope.load = function() {
    DailyTimeRecord.get({ id: $scope.id }, function(e){
      $scope.data = e.data;
    });
  }
  $scope.load();
  
  // remove dtr
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete ' + data.Employee.name + ' DTR ?', function(b) {
      if (b) {
        DailyTimeRecord.remove({ id: data.DailyTimeRecord.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Succesful!',
              text: e.msg,
            });
            window.location = '#/daily-time-records';
          }
        });
      }
    });
  }
  
  

});