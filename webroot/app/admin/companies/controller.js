app.controller('CompanyController', function($scope, Company) {

  // load settings
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Company.query(options, function(e) {
      if (e.ok) {
        $scope.companies = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ company: true });

  // remove company
  $scope.remove = function(company) {
    bootbox.confirm('Are you sure you want to delete ' + company.name + '?', function(c) {
      if (c) {
        Company.remove({ id: company.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:  e.msg,
            });
            $scope.load();
          }
        });
      }
    });
  }
});


app.controller('CompanyAddController', function($scope, Company) {
  $('#form').validationEngine('attach');

  // save company data
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Company.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:  e.msg,
          });
          window.location = '#/companies';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:  e.msg,
          });
        }
      });
    }
  }

});


app.controller('CompanyEditController', function($scope, $routeParams, Company) {
  $('#form').validationEngine('attach');
  $scope.companyId = $routeParams.id;

  // load company data
  $scope.load = function() {
    Company.get({ id: $scope.companyId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update company data
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Company.update({ id: $scope.companyId }, $scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:  e.msg,
          });
          window.location = '#/companies';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:  e.msg,
          });
        }
      });
    }
  }
});


app.controller('CompanyViewController', function($scope, $routeParams, Company) {
  $('#form').validationEngine('attach');
  $scope.companyId = $routeParams.id;

  // load company
  $scope.load = function() {
   Company.get({ id: $scope.companyId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove company
  $scope.remove = function(company) {
    bootbox.confirm('Are you sure you want to delete ' + company.name + '?', function(b) {
      if (b) {
        Company.remove({ id: company.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            window.location = '#/companies';
          }
        });
      }
    });
  }
});