app.config(function($routeProvider) {
  $routeProvider
  .when('/companies', {
    templateUrl: tmp + 'admin__companies__index',
    controller: 'CompanyController',
  })
  .when('/company/view/:id', {
    templateUrl: tmp + 'admin__companies__view',
    controller: 'CompanyViewController',
  })
  .when('/company/add', {
    templateUrl: tmp + 'admin__companies__add',
    controller: 'CompanyAddController',
  })
  .when('/company/edit/:id', {
    templateUrl: tmp + 'admin__companies__edit',
    controller: 'CompanyEditController',
  });
});