app.factory("Log", function($resource) {
  return $resource( api + "user-logs/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});