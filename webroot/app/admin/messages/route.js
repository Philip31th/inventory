app.config(function($routeProvider) {
  $routeProvider
  .when('/messages', {
    templateUrl: tmp + 'admin__messages__index',
    controller: 'MessagesIndexController',
  })
  .when('/messages/new', {
    templateUrl: tmp + 'admin__messages_add',
    controller: 'MessagesAddController',
  });
});