app.controller('UserController', function($scope, User) {

  // load users
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    User.query(options, function(e) {
      if (e.ok) {
        $scope.users = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove user  
  $scope.remove = function(user) {
    bootbox.confirm('Are you sure you want to delete room #' + user.username + '?', function(c) {
      if(c){
        User.remove({ id: user.id }, function(e) {
          if(e.ok){
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });
            $scope.load();
          }
        });
      }
    });
  }
});


app.controller('UserAddController', function ($scope, User, Select) {
  $('#form').validationEngine('attach');
  $scope.bool = [{ id: true, value: 'Yes' }, { id: false, value: 'No' }];

  // employee selection
  Select.get({ code: 'employees' }, function(e) {
    $scope.employees = e.data;
  });

  // save user
  $scope.save = function () {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      if ($scope.data.User.password != $scope.confirmPassword) {
        $.gritter.add({
          title: 'Warning!',
          text:  'Password does not match.',
        });
      } else {
        User.save($scope.data, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });
            window.location = '#/users';
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }
    }
  }
});


app.controller('UserEditController', function ($scope, $routeParams, User, UserPermission, Select) {
  modalMaxHeight();
  $('#form').validationEngine('attach');
  $scope.userId = $routeParams.id;
  $scope.bool = [{ id: true, value: 'Yes' }, { id: false, value: 'No' }];

  // employee selection
  Select.get({ code: 'employees' }, function(e) {
    $scope.employees = e.data;
  });

  // load user
  $scope.load = function() {
    User.get({ id: $scope.userId }, function(e) {
      $scope.data = e.data;
      $scope.data.User.password = '';
      $scope.data.Confirm = {
        password: ''
      }
    });
  }
  $scope.load();

  // add permission
  $scope.addPermission = function() {
    $('#add-permission-modal').modal('show');
  }
  $scope.savePermission = function() {
    permissions = [];
    for (i in $scope.data.PermissionSelection) {
      if ($scope.data.PermissionSelection[i].selected) {
        permissions.push({
          userId:       $scope.userId,
          permissionId: $scope.data.PermissionSelection[i].id
        });
      }
    }
    if (permissions.length <= 0) {
      $.gritter.add({
        title: 'Warning!',
        text: 'Please select permission to save.',
      });
    } else {
      UserPermission.save({ UserPermission: permissions }, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });

          $scope.load();
          $('#add-permission-modal').modal('hide');
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }

  // update user
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      if ($scope.data.User.password != $scope.data.Confirm.password) {
        $.gritter.add({
          title: 'Warning!',
          text: 'Password does not match'
        });
      } else {
        User.update({ id: $scope.userId }, $scope.data, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });
            window.location = '#/users';
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }
    } else {
      $.gritter.add({
        title: 'Warning!',
        text: e.msg
      });
    }
  }

  // remove user
  $scope.remove = function (permission) {
    bootbox.confirm('Are you sure you want to delete ' + permission.name + '?', function(c) {
      if (c) {
        UserPermission.remove({ id:permission.id }, function(e){
          if(e.ok){
            $.gritter.add({ title: 'Successful!', text: e.msg });
            $scope.load();
          }
        }); 
      }
    }); 
  };
});


app.controller('UserViewController', function($scope, $routeParams, User) {
  $scope.userId = $routeParams.id;

  // load user
  $scope.load = function() {
    User.get({ id: $scope.userId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove room  
  $scope.remove = function(user) {
    bootbox.confirm('Are you sure you want to delete room #' + user.username + '?', function(c) {
      if(User){
        Room.remove({ id: user.id }, function(e) {
          if(e.ok){
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });
            window.location = '#/users';
          }
        });
      }
    });
  }
});