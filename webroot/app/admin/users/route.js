app.config(function($routeProvider) {
  $routeProvider
  .when('/users', {
    templateUrl: tmp + 'admin__users__index',
    controller: 'UserController',
  })
  .when('/user/view/:id', {
    templateUrl: tmp + 'admin__users__view',
    controller: 'UserViewController',
  })
  .when('/user/add', {
    templateUrl: tmp + 'admin__users__add',
    controller: 'UserAddController',
  })
  .when('/user/edit/:id', {
    templateUrl: tmp + 'admin__users__edit',
    controller: 'UserEditController',
  });
});