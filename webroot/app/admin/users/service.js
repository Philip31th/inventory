app.factory("User", function($resource) {
  return $resource( api + 'users/:id', {id:'@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("UserPermission", function($resource) {
  return $resource( api + 'user-permissions/:id', {id:'@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});