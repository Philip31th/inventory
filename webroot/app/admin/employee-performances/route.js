app.config(function($routeProvider) {
  $routeProvider
  .when('/admin/employee-performances', {
    templateUrl: tmp + 'admin__employee_performances__index',
    controller: 'EmployeePerformanceController',
  })
  .when('/admin/employee-performances/add', {
    templateUrl: tmp + 'admin__employee_performances__add',
    controller: 'EmployeePerformanceAddController',
  })
  .when('/admin/employee-performances/edit/:id', {
    templateUrl: tmp + 'admin__employee_performances__edit',
    controller: 'EmployeePerformanceEditController',
  })
  .when('/admin/employee-performances/view/:id', {
    templateUrl: tmp + 'admin__employee_performances__view',
    controller: 'EmployeePerformanceViewController',
  });
});