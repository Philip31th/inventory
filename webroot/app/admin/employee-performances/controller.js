app.controller('EmployeePerformanceController', function($scope, EmployeePerformance) {

  // load users
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    EmployeePerformance.query(options, function(e) {
      if (e.ok) {
        $scope.vehicles = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();
  
  // remove settings
  $scope.remove = function(employeeperformance) {
    bootbox.confirm('Are you sure you want to delete ' + employeeperformance.name + '?', function(b) {
      if (b) {
        EmployeePerformance.remove({ id: employeeperformance.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

            $scope.load({
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
        });
      }
    });
  }
});

app.controller('EmployeePerformanceAddController', function ($scope, EmployeePerformance, Select) {
  $('#form').validationEngine('attach');
  $scope.bool = [{ id: true, value: 'Yes' }, { id: false, value: 'No' }];

   // save user
  $scope.save = function () {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      if ($scope.data.EmployeePerformance.password != $scope.confirmPassword) {
        $.gritter.add({
          title: 'Warning!',
          text:  'Password does not match.',
        });
      } else {
        EmployeePerformance.save($scope.data, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });
            window.location = '#employee-performances';
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }
    }
  }
});


app.controller('EmployeePerformanceEditController', function ($scope, $routeParams, EmployeePerformance) {
   // parameter
  $scope.id = $routeParams.id;
  // load project
  $scope.load = function() {
    EmployeePerformance.get({ id: $scope.id }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  
   // update project
  $scope.update = function() {
   valid = $("#form").validationEngine('validate');
   if (valid) {
    EmployeePerformance.update({ id:$scope.id },$scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title : 'Successful!',
            text  :  e.msg
          });
          window.location = '#employee-performances';
        } else {
          $.gritter.add({
            title : 'Warning!',
            text  :  e.msg
          });
        }
      });
    }
  }
;


  // remove user
  $scope.remove = function (permission) {
    bootbox.confirm('Are you sure you want to delete ' + permission.name + '?', function(c) {
      if (c) {
        EmployeePerformance.remove({ id:permission.id }, function(e){
          if(e.ok){
            $.gritter.add({ title: 'Successful!', text: e.msg });
            $scope.load();
          }
        }); 
      }
    }); 
  };
});


app.controller('EmployeePerformanceViewController', function($scope, $routeParams, EmployeePerformance) {
  $scope.id = $routeParams.id;

  // load user
  $scope.load = function() {
    EmployeePerformance.get({ id: $scope.id }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();
  
  // remove settings
  $scope.remove = function(employeeperformance) {
    bootbox.confirm('Are you sure you want to delete ' + employeeperformance.name + '?', function(b) {
      if (b) {
        EmployeePerformance.remove({ id: employeeperformance.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });
            window.location = '#employee-performances';
          }
        });
      }
    });
  }
});