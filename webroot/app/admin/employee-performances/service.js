app.factory("EmployeePerformance", function($resource) {
  return $resource( api + "employee-performances/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

