app.config(function($routeProvider) {
  $routeProvider
  .when('/hotel/room-types', {
    templateUrl: tmp + 'admin__room_types__index',
    controller: 'RoomTypeController',
  })
  .when('/hotel/room-types/add', {
    templateUrl: tmp + 'admin__room_types__add',
    controller: 'RoomTypeAddController',
  })
  .when('/hotel/room-types/edit/:id', {
    templateUrl: tmp + 'admin__room_types__edit',
    controller: 'RoomTypeEditController',
  })
  .when('/hotel/room-types/view/:id', {
    templateUrl: tmp + 'admin__room_types__view',
    controller: 'RoomTypeViewController',
  });
});