app.controller('RoomTypeController', function($scope, RoomType) {
  
  // load room-types
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    RoomType.query(options, function(e) {
      if (e.ok) {
        $scope.roomTypes = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();
    
  // remove room type
  $scope.remove = function(type) {
    bootbox.confirm('Are you sure you want to delete #' + type.name, function(c) {
      if (c) {
        RoomType.remove({ id: type.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });

            $scope.load();
          }
        });
      }
    });
  };
});


app.controller('RoomTypeAddController', function($scope, RoomType, Select) {
  $('#form').validationEngine('attach');

  // room accomodation selection
  Select.get({ code: 'accomodations' }, function(e) {
    $scope.accomodations = e.data;
  });

  // save room type
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      RoomType.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg,
          });

          window.location = '#/hotel/room-types';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:   e.msg,
          });
        }
      });
    } 
  }
});


app.controller('RoomTypeEditController', function($scope, $routeParams, RoomType, Select) {
  $scope.roomTypeId = $routeParams.id;

  // room accomodation selection
  Select.get({ code: 'accomodations' }, function(e) {
    $scope.accomodations = e.data;
  });

  // load room type
  RoomType.get({ id: $scope.roomTypeId }, function(e) {
    $scope.data = e.data;
  });

  // update room type
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      RoomType.update({ id: $scope.roomTypeId }, $scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg,
          });

          window.location = '#/hotel/room-types';
        } else {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg,
          });
        }
      });
    }
  }
});


app.controller('RoomTypeViewController', function($scope, $routeParams, RoomType, Room) {
  $scope.roomTypeId = $routeParams.id;

  // load room type
  $scope.load = function() {
    RoomType.get({ id: $scope.roomTypeId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  
  // remove room  
  $scope.remove = function(aroomtype) {
    bootbox.confirm('Are you sure you want to delete room #' + aroomtype.name + '?', function(c) {
      if (c) {
        RoomType.remove({ id: aroomtype.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            window.location = '#/hotel/room-types';
          }
        });
      }
    });
  }

  // remove room  
  $scope.remove = function(room) {
    bootbox.confirm('Are you sure you want to delete room #' + room.name + '?', function(c) {
      if (c) {
        Room.remove({ id: room.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });

            $scope.load();
          }
        });
      }
    });
  }
});                                                                                                                                       