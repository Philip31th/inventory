app.factory("RoomType", function($resource) {
  return $resource( api + "room-types/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});
app.factory("AvailableRoomType", function($resource) {
  return $resource( api + "room_types/available", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});
