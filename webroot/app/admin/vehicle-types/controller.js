app.controller('VehicleTypeController', function($scope, VehicleType) {

  // load users
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    VehicleType.query(options, function(e) {
      if (e.ok) {
        $scope.vehicles = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();
  
  // remove settings
  $scope.remove = function(vehicletype) {
    bootbox.confirm('Are you sure you want to delete ' + vehicletype.name + '?', function(b) {
      if (b) {
        VehicleType.remove({ id: vehicletype.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

            $scope.load({
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
        });
      }
    });
  }
});

app.controller('VehicleTypeAddController', function ($scope, VehicleType, Select) {
  $('#form').validationEngine('attach');
  $scope.bool = [{ id: true, value: 'Yes' }, { id: false, value: 'No' }];

   // save user
  $scope.save = function () {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      if ($scope.data.VehicleType.password != $scope.confirmPassword) {
        $.gritter.add({
          title: 'Warning!',
          text:  'Password does not match.',
        });
      } else {
        VehicleType.save($scope.data, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });
            window.location = '#/vehicle-types';
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }
    }
  }
});


app.controller('VehicleTypeEditController', function ($scope, $routeParams, VehicleType) {
   // parameter
  $scope.id = $routeParams.id;
  // load project
  $scope.load = function() {
    VehicleType.get({ id: $scope.id }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  
   // update project
  $scope.update = function() {
   valid = $("#form").validationEngine('validate');
   if (valid) {
    VehicleType.update({ id:$scope.id },$scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title : 'Successful!',
            text  :  e.msg
          });
          window.location = '#/vehicle-types';
        } else {
          $.gritter.add({
            title : 'Warning!',
            text  :  e.msg
          });
        }
      });
    }
  }
;


  // remove user
  $scope.remove = function (permission) {
    bootbox.confirm('Are you sure you want to delete ' + permission.name + '?', function(c) {
      if (c) {
        VehicleType.remove({ id:permission.id }, function(e){
          if(e.ok){
            $.gritter.add({ title: 'Successful!', text: e.msg });
            $scope.load();
          }
        }); 
      }
    }); 
  };
});


app.controller('VehicleTypeViewController', function($scope, $routeParams, VehicleType) {
  $scope.id = $routeParams.id;

  // load user
  $scope.load = function() {
    VehicleType.get({ id: $scope.id }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();
  
  // remove settings
  $scope.remove = function(vehicletype) {
    bootbox.confirm('Are you sure you want to delete ' + vehicletype.name + '?', function(b) {
      if (b) {
        VehicleType.remove({ id: vehicletype.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });
            window.location = '#vehicle-types';
          }
        });
      }
    });
  }
});