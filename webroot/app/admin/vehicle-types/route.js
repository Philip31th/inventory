app.config(function($routeProvider) {
  $routeProvider
  .when('/vehicle-types', {
    templateUrl: tmp + 'admin__vehicle_types__index',
    controller: 'VehicleTypeController',
  })
  .when('/vehicle-types/view/:id', {
    templateUrl: tmp + 'admin__vehicle_types__view',
    controller: 'VehicleTypeViewController',
  })
  .when('/vehicle-types/add', {
    templateUrl: tmp + 'admin__vehicle_types__add',
    controller: 'VehicleTypeAddController',
  })
  .when('/vehicle-types/edit/:id', {
    templateUrl: tmp + 'admin__vehicle_types__edit',
    controller: 'VehicleTypeEditController',
  });
});