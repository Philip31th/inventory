app.factory("VehicleType", function($resource) {
  return $resource( api + 'vehicle-types/:id', {id:'@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

