app.config(function($routeProvider) {
  $routeProvider
  .when('/hotel/room-accomodations', {
    templateUrl: tmp + 'admin__room_accomodations__index',
    controller: 'RoomAccomodationController',
  })
  .when('/hotel/room-accomodation/view/:id', {
    templateUrl: tmp + 'admin__room_accomodations__view',
    controller: 'RoomAccomodationViewController',
  })
  .when('/hotel/room-accomodation/add', {
    templateUrl: tmp + 'admin__room_accomodations__add',
    controller: 'RoomAccomodationAddController',
  })
  .when('/hotel/room-accomodation/edit/:id', {
    templateUrl: tmp + 'admin__room_accomodations__edit',
    controller: 'RoomAccomodationEditController',
  });
});