app.controller('RoomAccomodationController', function($scope, Accomodation) {

  // load room accomodations
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Accomodation.query(options, function(e) {
      if (e.ok) {
        $scope.accomodations = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove accomodation
  $scope.remove = function(accomodation) {
    bootbox.confirm('Are you sure you want to delete #' + accomodation.name + '?', function(c) {
      if (c) {
        Accomodation.remove({ id: accomodation.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });

            $scope.load();
          }
        });
      }
    });
  };
});


app.controller('RoomAccomodationAddController', function($scope, Accomodation) {
  $('#form').validationEngine('attach');

  // save accomodation
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Accomodation.save($scope.data, function(e){
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });

          window.location = '#/hotel/room-accomodations';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:   e.msg,
          });
        }
      });
    } else {
      $.gritter.add({
        title: 'Warning!',
        text: 'Please fill up required fields.'
      });
    }
  }
});


app.controller('RoomAccomodationEditController', function($scope, $routeParams, Accomodation) {
  $scope.accomodationId = $routeParams.id;

  // load accomodation
  $scope.load = function() {
    Accomodation.get({ id: $scope.accomodationId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update accomodation
  $scope.update = function() {
    Accomodation.update({ id: $scope.accomodationId }, $scope.data, function(e) {
      if (e.ok) {
        $.gritter.add({
          title: 'Successful!',
          text:   e.msg
        });
        
        window.location = '#/hotel/room-accomodations'; 
      } else {
        $.gritter.add({
          title: 'Warning!',
          text:   e.msg
        });
      }
    });
  }
});


app.controller('RoomAccomodationViewController', function($scope, $routeParams, Accomodation) {
  $scope.roomId = $routeParams.id;

  // load accomodation
  $scope.load = function() {
    Accomodation.get({ id: $scope.roomId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove accomodation
  $scope.remove = function(accomodation) {
    bootbox.confirm('Are you sure you want to delete #' + accomodation.name + '?', function(c) {
      if (c) {
        Accomodation.remove({ id: accomodation.id }, function(e) {
          if(e.ok){
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg
            });

            window.location = '#/hotel/room-accomodations';
          }
        });
      }
    });
  };
});