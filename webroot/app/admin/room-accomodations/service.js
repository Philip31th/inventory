app.factory("Accomodation", function($resource) {
  return $resource( api + "accomodations/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});
