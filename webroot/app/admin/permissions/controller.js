app.controller('PermissionController', function($scope, Permission) {

  // load permissions
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Permission.query(options, function(e) {
      if (e.ok) {
        $scope.permissions = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove permission
  $scope.remove = function(permission) {
    bootbox.confirm('Are you sure you want to delete ' + permission.name + '?', function(b) {
      if (b) {
        Permission.remove({ id: type.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });

            $scope.load({
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }
    });
  }
});

app.controller('PermissionAddController', function($scope, Permission) {
  $('#form').validationEngine('attach');

  // save permission
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      Permission.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/permissions';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});

app.controller('PermissionEditController', function($scope, $routeParams, Permission) {
  $('#form').validationEngine('attach');
  $scope.permissionId = $routeParams.id;

  // load permission
  $scope.load = function() {
    Permission.get({ id: $scope.permissionId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update permission
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      Permission.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/permissions';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});

