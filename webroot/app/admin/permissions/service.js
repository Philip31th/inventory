app.factory("Permission", function($resource) {
  return $resource( api + 'permissions/:id', {id:'@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});