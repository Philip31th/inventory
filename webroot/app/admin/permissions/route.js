app.config(function($routeProvider) {
  $routeProvider
  .when('/permissions', {
    templateUrl: tmp + 'admin__permissions__index',
    controller: 'PermissionController',
  })
  .when('/permission/view/:id', {
    templateUrl: tmp + 'admin__permissions__view',
    controller: 'PermissionViewController',
  })
  .when('/permission/add', {
    templateUrl: tmp + 'admin__permissions__add',
    controller: 'PermissionAddController',
  })
  .when('/permission/edit/:id', {
    templateUrl: tmp + 'admin__permissions__edit',
    controller: 'PermissionEditController',
  });
});