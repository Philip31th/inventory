app.factory("Penalty", function($resource) {
  return $resource( api + "penalties/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});