app.config(function($routeProvider) {
  $routeProvider
  .when('/penalties', {
    templateUrl: tmp + 'admin__penalties__index',
    controller: 'PenaltyController',
  })
  .when('/penalty/view/:id', {
    templateUrl: tmp + 'admin__penalties__view',
    controller: 'PenaltyViewController',
  })
  .when('/penalty/add', {
    templateUrl: tmp + 'admin__penalties__add',
    controller: 'PenaltyAddController',
  })
  .when('/penalty/edit/:id', {
    templateUrl: tmp + 'admin__penalties__edit',
    controller: 'PenaltyEditController',
  });
});