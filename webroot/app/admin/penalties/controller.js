app.controller('PenaltyController', function($scope, Penalty) {

  // load settings
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Penalty.query(options, function(e) {
      if (e.ok) {
        $scope.penalties = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

  // remove penalty
  $scope.remove = function(penalty) {
    bootbox.confirm('Are you sure you want to delete ' + penalty.name + '?', function(c) {
      if (c) {
        Penalty.remove({ id: penalty.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:  e.msg,
            });
            $scope.load();
          }
        });
      }
    });
  }
});


app.controller('PenaltyAddController', function($scope, Penalty) {
  $('#form').validationEngine('attach');

  // save penalty data
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Penalty.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:  e.msg,
          });
          window.location = '#/penalties';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:  e.msg,
          });
        }
      });
    }
  }

});


app.controller('PenaltyEditController', function($scope, $routeParams, Penalty) {
  $('#form').validationEngine('attach');
  $scope.penaltyId = $routeParams.id;

  // load penalty data
  $scope.load = function() {
    Penalty.get({ id: $scope.penaltyId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update penalty data
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');

    if (valid) {
      Penalty.update({ id: $scope.penaltyId }, $scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:  e.msg,
          });
          window.location = '#/penalties';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:  e.msg,
          });
        }
      });
    }
  }
});


app.controller('PenaltyViewController', function($scope, $routeParams, Penalty) {
  $('#form').validationEngine('attach');
  $scope.penaltyId = $routeParams.id;

  // load penalty
  $scope.load = function() {
   Penalty.get({ id: $scope.penaltyId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove penalty
  $scope.remove = function(penalty) {
    bootbox.confirm('Are you sure you want to delete ' + penalty.name + '?', function(b) {
      if (b) {
        Penalty.remove({ id: penalty.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            window.location = '#/penalties';
          }
        });
      }
    });
  }
});