var app = angular.module('ehrms', ['ngRoute', 'ngResource', 'chieffancypants.loadingBar', 'selectize','ui.bootstrap','ui.bootstrap.datetimepicker']);

app.config(function($routeProvider) {
  $routeProvider
  .otherwise({
    redirectTo: '/dashboard'
  });
});
