app.config(function($routeProvider) {
  $routeProvider
  .when('/resto/menus', {
    templateUrl: tmp + 'resto__menus__index',
    controller: 'RestoMenuController',
  })
  .when('/resto/menu/view/:id', {
    templateUrl: tmp + 'resto__menus__view',
    controller: 'RestoMenuViewController',
  })
  .when('/resto/menu/add', {
    templateUrl: tmp + 'resto__menus__add',
    controller: 'RestoMenuAddController',
  })
  .when('/resto/menu/edit/:id', {
    templateUrl: tmp + 'resto__menus__edit',
    controller: 'RestoMenuEditController',
  });
});