app.controller('RestoMenuController', function($scope, RestoMenu) {

  // load resto menus
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    RestoMenu.query(options, function(e) {
      if (e.ok) {
        $scope.menus = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code: 'restaurant' });

  // remove resto menu
  $scope.remove = function(menu) {
    bootbox.confirm('Are you sure you want to delete ' + menu.name + '?', function(b) {
      if (b) {
        RestoMenu.remove({ id: menu.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            $scope.load({
              code:   'restaurant',
              page:   $scope.paginator.page,
              search: $scope.searchTxt
            });
          } else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }
        });
      }
    });
  }
});


app.controller('RestoMenuViewController', function($scope, $routeParams, RestoMenu) {
$scope.menuId = $routeParams.id;

// load resto menu
  $scope.load = function() {
    RestoMenu.get({ id: $scope.menuId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

// remove room  
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete restaurant name ' + data.name + '?', function(c) {
      if(c){
        RestoMenu.remove({ id: data.id }, function(e) {
          if(e.ok){
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });
            window.location = '#/resto/menus';
          }
        });
      }
    });
  }

});


app.controller('RestoMenuAddController', function($scope, RestoMenu) {
  $('#form').validationEngine('attach');

  // save bar menu
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      $scope.data.Menu.departmentId = 'restaurant';
      RestoMenu.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/resto/menus';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});

app.controller('RestoMenuEditController', function($scope, $routeParams, RestoMenu) {
  $('#form').validationEngine('attach');
  $scope.menuId = $routeParams.id;

  // load resto menu
  $scope.load = function() {
    RestoMenu.get({ id: $scope.menuId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update resto menu
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      $scope.data.Menu.departmentId = 'restaurant';
      RestoMenu.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/resto/menus';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }
});