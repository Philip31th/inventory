app.controller('RestoInventoryController', function($scope, RestoInventory) {
  // load inventory
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    RestoInventory.query(options, function(e) {
      if (e.ok) {
        $scope.items = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code : 'restaurant' });
    
  // remove inventory
  $scope.remove = function(item) {
    bootbox.confirm('Are you sure you want to delete ' + item.name + '?', function(b) {
      if (b) {
        RestoInventory.remove({ id: item.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

            $scope.load({
              code : 'restaurant',
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
          }
          window.location = '#resto/inventories';
        });
      }
    });
  }
  
 });


app.controller('RestoInventoryAddController', function($scope, RestoInventory) {
  $('#form').validationEngine('attach');

  // bool type
  $scope.bool = [{value: true, name: 'IN'}, {value: false, name: 'OUT'}]

  // save inventory
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      $scope.data.Inventory.departmentId = 'restaurant';
      
      RestoInventory.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title : 'Successful!',
            text  :  e.msg
          });
          window.location = '#/resto/inventories';
        } else {
          $.gritter.add({
            title : 'Warning!',
            text  :  e.msg
          });
        }
      });
    }
  }
});


app.controller('RestoInventoryEditController', function ($scope, $routeParams, RestoInventory, InventorySub) {
 $scope.inventoryId = $routeParams.id;

  // load
  $scope.load = function() {
    RestoInventory.get({ id: $scope.inventoryId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // update employee
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if (valid) {
      RestoInventory.update({ id: $scope.inventoryId },$scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg
          });
          window.location = '#/resto/inventories';
        }else {
          $.gritter.add({
            title: 'Warning!',
            text: e.msg
          });
        }
      });
    }
  }

  // inventory items
  $scope.data = {
    InventorySub: []
  }

  subs = [];
  subs2 = [];

  // add inventory item
  $scope.addItem = function() {
    $scope.modalTitle = 'ADD';
    $scope.inventorysub = { 
      type:         1, 
      name:         $scope.data.Inventory.name,
      averageCost:  $scope.data.Inventory.averageCost
    }
    $('#inventory-item-modal').modal('show');
  }

  $scope.editItem = function(item) {
    $scope.modalTitle = 'EDIT';
    cost = '';
    if(item.type==true) {
      cost = item.cost_n;
    }
    
    $scope.inventorysub = {
      id:           item.id,
      cost:         cost,
      quantity:     item.quantity,
      name:         $scope.data.Inventory.name,
      averageCost:  $scope.data.Inventory.averageCost
    }
    $('#inventory-item-modal').modal('show');
  }

  $scope.saveItem = function() {
      subs.push({
        inventoryId:  $scope.inventoryId,
        type:         $scope.inventorysub.type,
        quantity:     $scope.inventorysub.quantity,
        cost:         $scope.inventorysub.cost
      }); 

    InventorySub.save({ InventorySub: subs }, function(e){
      if(e.ok) {
        $('#inventory-item-modal').modal('hide');
        $scope.load();
        $scope.inventorysub = {}; 
        subs = [];
      }
    });
  }

  $scope.updateItem = function() {
    cost = null;
    if($scope.inventorysub.cost!='') {
      cost = $scope.inventorysub.cost;
    }
    
    subs.push({
      id:          $scope.inventorysub.id,
      inventoryId: $scope.inventoryId,
      type:        $scope.inventorysub.type,
      quantity:    $scope.inventorysub.quantity,
      cost:        cost
    });

    InventorySub.save({ InventorySub: subs }, function(e){
      if(e.ok) {
        $('#inventory-item-modal').modal('hide');
        $scope.load();
        $scope.inventorysub = {}; 
        subs = [];
      }
    });
  }

  // remove subs
  $scope.remove = function(item) {
    bootbox.confirm('Are you sure you want to delete this item?', function(b) {
      if (b) {
        InventorySub.remove({ id: item.id }, function(e) {
          if (e.ok) {
            $scope.load();
          }           
        });
       
      }
    });
  }

});


app.controller('RestoInventoryViewController', function($scope, $routeParams, RestoInventory) {
  $scope.inventoryId = $routeParams.id;

  // load inventory
  $scope.load = function() {
    RestoInventory.get({ id: $scope.inventoryId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

  // remove inventory
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete ' + data.name + '?', function(b) {
      if (b) {
        RestoInventory.remove({ id: data.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });
          }
          window.location = '#/resto/inventories';
        });
      }
    });
  }
});

app.controller('RestoDailyConsumptionController', function($scope, RestoInventory,InventorySub, RestoInventoryConsumptions) {
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    RestoInventory.query(options,function(e) {
      if (e.ok) {
        $scope.items = e.data;
      }
    });
  }
   $scope.load({ code : 'restaurant' });
  
  // inventory items
  $scope.data = {
    InventorySub: []
  }
  
  subs = []
  
  $scope.saveDailyConsumption = function() {
    
   angular.forEach($scope.items, function(item, e) {
     if(item.delivered!=0) {
        subs.push({
          type:         0,
          inventoryId:  item.id,
          cost:         null,
          quantity:     item.delivered
        });
     }
    });
    
   InventorySub.save({ InventorySub: subs }, function(e){
      if(e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text: e.msg,
          });
        window.location = '#/resto/inventories';
        subs = [];
      }
    });
  }
  
  $('#searchConsumption').datepicker({
      format: 'mm/dd/yyyy',
      autoclose: true
    });
    
  $scope.viewDailyConsumption = function() {
    $('#searchConsumption').datepicker('setDate','');
    $('#view-daily-consumptions').modal('show');
  }
  
  $scope.searchConsumption = function() {

    RestoInventoryConsumptions.get({date: $scope.consumedDate, code : 'restaurant' }, function(e){
      if(e.ok) {
        $scope.consumed = e.data;
      }
    });
  }
  
});  