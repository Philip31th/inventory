app.config(function($routeProvider) {
  $routeProvider
  .when('/resto/inventories', {
    templateUrl: tmp + 'resto__inventory__index',
    controller: 'RestoInventoryController',
  })
  .when('/resto/inventory/add', {
    templateUrl: tmp + 'resto__inventory__add',
    controller: 'RestoInventoryAddController',
  })
  .when('/resto/inventory/edit/:id', {
    templateUrl: tmp + 'resto__inventory__edit',
    controller: 'RestoInventoryEditController',
  })
  .when('/resto/inventory/view/:id', {
    templateUrl: tmp + 'resto__inventory__view',
    controller: 'RestoInventoryViewController',
  })
  .when('/resto/inventory/daily-consumption', {
    templateUrl: tmp + 'resto__inventory__daily_consumption',
    controller: 'RestoDailyConsumptionController',
  });
});