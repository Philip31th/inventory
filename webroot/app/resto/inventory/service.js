app.factory("RestoInventory", function($resource) {
  return $resource( api + "inventories/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("RestoInventoryConsumptions", function($resource) {
  return $resource( api + "inventories/consumptions", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("InventorySub", function($resource) {
  return $resource( api + "inventory-subs/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});