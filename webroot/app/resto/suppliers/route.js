app.config(function($routeProvider) {
  $routeProvider
  .when('/resto/suppliers', {
    templateUrl: tmp + 'resto__suppliers__index',
    controller: 'RestoSuppliersController',
  })
  .when('/resto/supplier/add', {
    templateUrl: tmp + 'resto__suppliers__add',
    controller: 'RestoSuppliersAddController',
  })
  .when('/resto/supplier/edit/:id', {
    templateUrl: tmp + 'resto__suppliers__edit',
    controller: 'RestoSuppliersEditController',
  })
  .when('/resto/supplier/view/:id', {
    templateUrl: tmp + 'resto__suppliers__view',
    controller: 'RestoSuppliersViewController',
  })
  .when('/resto/reports/suppliers', {
  templateUrl: tmp + 'resto__suppliers__report',
  controller: 'RestoSuppliersReportController',
  }) 
  .when('/resto/reports/supplier/view/:id', {
  templateUrl: tmp + 'resto__suppliers__report_view',
  controller: 'RestoSuppliersReportViewController',
  });
});