app.factory('RestoSupplier', function($resource) {
  return $resource( api + 'suppliers/:id', {id:'@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("RestoSupplierItem", function($resource) {
  return $resource( api + 'supplier-items/:id', {id:'@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});