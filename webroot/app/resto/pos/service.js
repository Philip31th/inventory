app.factory("RestoPos", function($resource) {
  return $resource( api + "pos/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});