app.config(function($routeProvider) {
  $routeProvider
  .when('/resto/pos', {
    templateUrl: tmp + 'resto__pos__index',
    controller: 'RestoPosController',
  })
  .when('/resto/pos/table/view/:id', {
    templateUrl: tmp + 'resto__pos__table_view',
    controller: 'RestoPosViewController',
  })
  .when('/resto/pos/transactions/add/:id', {
    templateUrl: tmp + 'resto__pos__table_transact',
    controller: 'RestoPosAddTransactionsController',
  });
});