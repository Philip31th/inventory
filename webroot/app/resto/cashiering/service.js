app.factory("RestoReportCashiering", function($resource) {
  return $resource( api + "bar-resto-cashiering-report/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});
