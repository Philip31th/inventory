app.controller('TableController', function($scope, RestoTable) {
 
 // load resto table
  $scope.load  = function (options) {
    options = typeof options !== 'undefined' ? options : {};
    RestoTable.query(options,function(e) {
    if (e.ok) {
      // get table data
      $scope.tables = e.data;
     
      //paginator
      $scope.paginator = e.paginator;
      $scope.pages     = paginator($scope.paginator,5);
    };
  });
}
  $scope.load({ code : 'restaurant' });

  $scope.remove = function(table) {
    bootbox.confirm('Are you sure you want to delete ' + table.name + '?', function(b) {
      if (b) {
        RestoTable.remove({ id: table.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            $scope.load({
              code:   'restaurant',
              page:   $scope.paginator.page,
              search: $scope.searchTxt
            });
          } else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }
        });
      }
    });
  }
});


app.controller('TableAddController', function($scope, RestoTable) {
  $('#form').validationEngine('attach');
  // save bar menu
  $scope.save = function() {
    valid = $("#form").validationEngine('validate');
    
    if (valid) {
      $scope.data.Table.departmentId = 'restaurant';
      RestoTable.save($scope.data, function(e) {
        if (e.ok) {
          $.gritter.add({
            title: 'Successful!',
            text:   e.msg
          });
          window.location = '#/resto/tables';
        } else {
          $.gritter.add({
            title: 'Warning!',
            text:   e.msg
          });
        }
      });
    }
  }
});


app.controller('TableEditController', function($scope, $routeParams, RestoTable) {
  // parameters
  $scope.id = $routeParams.id;

  // load view 
  RestoTable.get({id:$scope.id}, function(e){
    $scope.data = e.result;
  });

  // update table
  $scope.update = function() {
    valid = $("#form").validationEngine('validate');
    if(valid){
      $scope.data.Table.departmentId = 'restaurant';
      RestoTable.update({id:$scope.id}, $scope.data, function(e){
      if(e.ok){
        $.gritter.add({ title: 'Successful!', text: e.msg });
        window.location = '#/resto/tables';
      }
    });
   }
  }

});


app.controller('TableViewController', function($scope, $routeParams, RestoTable) {
 // parameters
  $scope.id = $routeParams.id;

  // load view 
  RestoTable.get({id:$scope.id}, function(e){
    $scope.data = e.result;
  });

  // remove table  
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete restaurant name ' + data.name + '?', function(c) {
      if(c){
        RestoTable.remove({ id: data.id }, function(e) {
          if(e.ok){
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });
            window.location = '#/resto/tables';
          }
        });
      }
    });
  }

});