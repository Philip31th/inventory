app.config(function($routeProvider) {
  $routeProvider
  .when('/resto/tables', {
    templateUrl: tmp + 'resto__tables__index',
    controller: 'TableController',
  })
  .when('/resto/table/add', {
    templateUrl: tmp + 'resto__tables__add',
    controller: 'TableAddController',
  })
  .when('/resto/table/edit/:id', {
   templateUrl: tmp + 'resto__tables__edit',
    controller: 'TableEditController',
  })
  .when('/resto/table/view/:id', {
   templateUrl: tmp + 'resto__tables__view',
   controller: 'TableViewController',
  });
});