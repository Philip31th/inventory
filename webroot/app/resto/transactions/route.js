app.config(function($routeProvider) {
  $routeProvider
  .when('/resto/transactions', {
    templateUrl: tmp + 'resto__transactions__index',
    controller: 'RestoTransactionController',
  })
  .when('/resto/transactions/add', {
    templateUrl: tmp + 'resto__transactions__add',
    controller: 'RestoTransactionAddController',
  })
  .when('/resto/transactions/view/:id', {
    templateUrl: tmp + 'resto__transactions__view',
    controller: 'RestoTransactionViewController',
  })
  .when('/resto/transactions/edit/:id', {
    templateUrl: tmp + 'resto__transactions__edit',
    controller: 'RestoTransactionEditController',
  })
  .when('/resto/reports/receivables', {
    templateUrl: tmp + 'resto__transactions__receivables',
    controller: 'RestoReceivablesController',
  })
  .when('/resto/reports/receivables/view/:id', {
    templateUrl: tmp + 'resto__transactions__receivables_view',
    controller: 'RestoReceivablesViewController',
  });
});