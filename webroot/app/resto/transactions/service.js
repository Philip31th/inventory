app.factory("RestoTransaction", function($resource) {
  return $resource( api + "transactions/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("RestoReceivable", function($resource) {
  return $resource( api + "transactions/receivables/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});
