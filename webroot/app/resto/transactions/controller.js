app.controller('RestoTransactionController', function($scope, RestoTransaction){

  // load inventory
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    RestoTransaction.query(options, function(e) {
      if (e.ok) {
        $scope.transactions = e.data;
  
        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code : 'restaurant' });

  // remove transaction
  $scope.remove = function(transaction) {
    bootbox.confirm('Are you sure you want to delete this ' + transaction.business + ' Business?', function(b) {
      if (b) {
        RestoTransaction.remove({ id: transaction.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

            $scope.load({
              code: 'restaurant',
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
             window.location = '#/resto/transactions';
          } else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }
          
        });
       
      }
    });
  }
});


app.controller('RestoTransactionAddController', function($scope, RestoTransaction, TransactionSub, Select){

  // folios selection
  Select.get({ code: 'folios' }, function(e) {
    $scope.folios = e.data;
  });

  $('#form').validationEngine('attach');
  $('.datepicker').datepicker({format:'mm/dd/yyyy', autoclose:true});

  $scope.save = function(){
    valid = $("#form").validationEngine('validate');
    if(valid){

      $scope.data.Transaction.businessId = 2;

      RestoTransaction.save($scope.data, function(e){
        if(e.ok){
          $.gritter.add({ title: 'Successful!', text: e.message });
          window.location = '#/resto/transactions';
        }
      });
    }
  }
  
 // add transaction item
  $scope.data = {
    TransactionSub: []
  }
  subs = [];

  $scope.totalBills = function() {
    total = 0;
    for(count = 0; count < $scope.data.TransactionSub.length; count++) {
      total += $scope.data.TransactionSub[count].amount;
    }
    
    return total;
  }
    

  $scope.addItem = function() {
    $scope.title = 'ADD NEW';
    $('#transaction-item-modal').modal('show');
  }

  $scope.saveItem = function(item) {
    $scope.data.TransactionSub.push(item);  
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }  
  
  var key;

  $scope.editItem = function(item) {
    $scope.title = 'EDIT';
    key = $scope.data.TransactionSub.indexOf(item);
    $scope.item = {
      id:          item.id,
      particulars: item.particulars,
      amount:      item.amount
    }
    $('#transaction-item-modal').modal('show');
  }

  $scope.updateItem = function(item) {
    $scope.data.TransactionSub[key] = {
      id:           item.id,
      particulars:  item.particulars,
      amount:       item.amount
    }
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }

  $scope.removeItem = function(index,item) {
    bootbox.confirm('Are you sure you want to delete this transaction items?', function(b) {
      if (b) {
        $scope.data.TransactionSub.splice(index,1); 
        TransactionSub.remove({ id: item.id });
      }
    });
  }
});


app.controller('RestoTransactionEditController', function($scope, $routeParams, RestoTransaction, TransactionSub, Select){
  $scope.transactionId = $routeParams.id;
  
  // get transactions
  $scope.load = function() {
    RestoTransaction.get({ id: $scope.transactionId }, function(e){
      $scope.data = e.data;
    }); 
  }
  $scope.load();
  
  // update transaction
  $scope.save = function(){
    RestoTransaction.update({ id: $scope.transactionId }, $scope.data, function(e){
      if(e.ok){
        $.gritter.add({ title: 'Successful!', text: e.message });
        window.location = '#/resto/transactions';
      }
    });
  }
  
  $scope.data = {
    TransactionSub: []
  }  

  $scope.totalBills = function() {
    total = 0;
    for(count = 0; count < $scope.data.TransactionSub.length; count++) {
      total += $scope.data.TransactionSub[count].amount;
    }
    
    return total;
  }
    

  $scope.addItem = function() {
    $scope.title = 'ADD NEW';
    $('#transaction-item-modal').modal('show');
  }

  $scope.saveItem = function(item) {
    $scope.data.TransactionSub.push(item);  
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }  
  
  var key;

  $scope.editItem = function(item) {
    $scope.title = 'EDIT';
    key = $scope.data.TransactionSub.indexOf(item);
    $scope.item = {
      id:          item.id,
      particulars: item.particulars,
      amount:      item.amount
    }
    $('#transaction-item-modal').modal('show');
  }

  $scope.updateItem = function(item) {
    $scope.data.TransactionSub[key] = {
      id:           item.id,
      particulars:  item.particulars,
      amount:       item.amount
    }
    $('#transaction-item-modal').modal('hide');
    $scope.item = {}
  }

  $scope.removeItem = function(index,item) {
    bootbox.confirm('Are you sure you want to delete this transaction items?', function(b) {
      if (b) {
        $scope.data.TransactionSub.splice(index,1); 
        TransactionSub.remove({ id: item.id });
      }
    });
  }
});

app.controller('RestoTransactionViewController', function($scope, $routeParams, RestoTransaction){
  $scope.transactionId = $routeParams.id;

  // load transactions
  $scope.load = function() {
    RestoTransaction.get({ id: $scope.transactionId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();
  
    // remove inventory
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete these ' + data.Business.name + ' Transactions?', function(b) {
      if (b) {
        RestoTransaction.remove({ id: data.Transaction.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

          }
          window.location = '#/resto/transactions';
        });
      }
    });
  }
});

app.controller('RestoReceivablesController', function($scope, $routeParams, RestoReceivable){

  // load inventory
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    RestoReceivable.query(options, function(e) {
      if (e.ok) {
        $scope.receivables = e.data;
        $scope.total = e.totalReceivables;
  
        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code : 'restaurant' });
});


app.controller('RestoReceivablesViewController', function($scope, $routeParams, RestoReceivable){
  $scope.transactionId = $routeParams.id;

  // load inventory
  $scope.load = function() {
    RestoReceivable.query({ id: $scope.transactionId }, function(e) {
      if (e.ok) {
        $scope.receivables = e.data;
        $scope.total = e.totalReceivables;
      }
    });
  }
  $scope.load();

});