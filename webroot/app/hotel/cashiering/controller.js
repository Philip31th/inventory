app.controller('CashieringController', function($scope, Cashiering) {
  // load cashiering
  $scope.load = function(options) { 
    options = typeof options !== 'undefined' ?  options : {};
    Cashiering.query(options, function(e) {
      if (e.ok) {
        $scope.cashierings = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }  
    });
  }
  $scope.load(); 
});


app.controller('CashieringViewController', function($scope, $routeParams, Cashiering, FolioSub, ReservationCharges, TransactionPayment, BusinessService, Transaction, Select , TransactionDiscount, GuestHistory) {
  modalMaxHeight();
  $scope.id = $routeParams.id;

  // load
  $scope.load = function() {
    Cashiering.get({ id: $scope.id }, function(e) {
      $scope.data = e.data;
      $scope.transactions = e.transactions;      
    });
  }
  $scope.load();


  // addDiscount
  $scope.addDiscount = function(data) {
    Select.get({ code: 'discounts' }, function(e) {
      $scope.discountSelection = e.data;
    });
    
    $scope.transactionId = data.id;
    $('#add-discount-modal').modal('show');
  }
  
  // editCharge
  $scope.editCharge = function(data) {
    $('#edit-charge-modal').modal('show');
  }
  
  // editDiscount
  $scope.removeDiscount = function(data) {
    bootbox.confirm('Are you sure you want to remove this discount?', function(b) {
      console.log(b);
      if(b){
        TransactionDiscount.remove({ id: data.id },
          function(e){
            if(e.ok){
              $.gritter.add({title:'Successful!', text:e.msg});
              $('#view-transaction-modal').modal('hide');
              $scope.load();
            }
        });
      }
    });  
  }
  
  $scope.saveDiscount = function() {
    discounts = {
      TransactionDiscount: [],
    };
    
    angular.forEach($scope.discountSelection, function(discount, k) {
      if (discount.selected) {
        discounts.TransactionDiscount.push({
          transactionId: $scope.transactionId,
          discountId:    discount.id,
          value:         discount.value,
          type:          discount.type,
        });
      }
    });
    
    TransactionDiscount.save(discounts, function(e) {
      if (e.ok) {
        $.gritter.add({
          title: 'Successful!', 
          text:   e.msg,
        });

        $scope.transactionId = null;
        $('#add-discount-modal').modal('hide');       
        $('#view-transaction-modal').modal('hide');
        $scope.load();
      }
    });
  }

  // add payment item
  $scope.payment = {
    TransactionPayment: []
  }
  payment = [];  

  // addPayment
  $scope.addPayment = function(data) {
    $('#form').validationEngine('attach');

    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
    $scope.payment = {};

    // payment types selection
    Select.get({ code: 'payment-types'}, function(e) {
      $scope.paymentTypes = e.data;
    });

    $scope.amountToPay = data.totalBalance;
    $scope.transactionId = data.id;
    $scope.title = 'ADD';
    $scope.payment.folioCode = data.folioCode;
    $scope.business = data.business;
    // 
    $('#add-payment-modal').modal('show');   
  }

  // amount
  $('#amount').keyup(function(){
    // if ($('#paymentType').val()=='3') {
    //   alert();
    // } else {
      $scope.change();
    // }
  });
  
  $scope.checkOR = function(orNumber,business) {

    // if(orNumber.length >= 3) {
    if(orNumber!='') {  
      $scope.checking = true;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
      $('#savePayment').attr('disabled', true);
      TransactionPayment.query({ search: orNumber , businessId:  business },function(e){
        if (e.data) {
          $scope.checking = false;
          $scope.notexisted = false;
          $scope.orNumberExisting = true;
        } else {
          $scope.checking = false;
          $scope.notexisted = true;
          $scope.orNumberExisting = false;
        }
        if (e.ok) {$('#savePayment').attr('disabled', false);}
      });
    } else {
      $scope.checking = false;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
    }
  }

  $scope.paymentType = function(paymentType) {
    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
  }

  $scope.change = function(amount,amountToPay) {
    if(amount!=null) {
      if(amount>amountToPay) {
        return amount-amountToPay;
      } else{
        return 0;
      }
    } else {
      return 0;
    }
  }

  // view payment
  $scope.viewTransactions = function(data) {
    $scope.viewPaymentId = data.id;
    $scope.totalBalance = data.totalBalance;

    Transaction.get({ id: data.id }, function(e) {
      $scope.vTransaction = e.data;
    });

    // view payment
    $('#view-transaction-modal').modal('show');
  }

  // remove transaction charges
  $scope.removeCharge = function(transaction) {
    if (transaction.totalAmount != (transaction.totalBalance + transaction.totalDiscount ) ) {
      $.gritter.add({
        title: 'Warning!',
        text: 'You can not delete this if it already have payment recorded.'
      });
    } else {
      bootbox.confirm('Are you sure you want to delete  this '+ transaction.particulars +' ?', function(b) {
        if (b) {
          Transaction.remove({ id: transaction.id }, function(e) {
            if (e.ok) {
              $scope.load();
            }
          });
        }
      });
    }
  }

  $scope.savePayment = function(payment) {

    valid = $("#form").validationEngine('validate');

    if (valid){
      if($('#change').val()==null || $('#change').val()=='') {
        changee = 0;
      } else {
        changee = parseFloat($('#change').val());
      }

      payment = {
        transactionId    : $scope.transactionId,
        orNumber         : payment.orNumber,
        amount           : payment.amount,
        change           : changee,
        paymentType      : payment.paymentType,
        accountName      : payment.accountName,
        cardNumber       : payment.cardNumber,
        chequeNumber     : payment.chequeNumber,
        expirationYear   : payment.expirationYear,
        expirationMonth  : payment.expirationMonth,
        bankName         : payment.bankName,
        date             : Date.today(),
        folioCode        : $scope.payment.folioCode
      }

      if (payment.amount==0 || payment.amount=='') {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must have value to save.'
        });
         $('#savePayment').attr('disabled', false);
      } else if ($scope.orNumberExisting) {
        $.gritter.add({
          title: 'Warning!',
          text: 'OR Number already existed.'
        }); 
         $('#savePayment').attr('disabled', false);
      } else if (payment.paymentType!='cash' && payment.amount > $scope.amountToPay) {
        $.gritter.add({
          title: 'Warning!',
          text: 'Amount must not exceed to transaction balance.'
        }); 
         $('#savePayment').attr('disabled', false);
      } else if (payment.paymentType=='send bill' && $scope.data.company == null) {
          $.gritter.add({
            title: 'Warning!',
            text: 'You must have register a company details to send a bill' 
          }); 
         $('#savePayment').attr('disabled', false);   
      } else {
        $('#savePayment').attr('disabled', true);
        TransactionPayment.save({ TransactionPayment: payment }, function(e) {
          console.log(e);
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg
            });

            $scope.load();
            $('#add-payment-modal').modal('hide'); 
            $scope.payment = {};
            $('#savePayment').attr('disabled', false);
          } else {
            $.gritter.add({
              title: 'Warning!',
              text: e.msg
            });
          }
        });
      }  
    }  
  }      


  // checkout
  $scope.checkOut = function() {
    bootbox.confirm('Are you sure you want to checkout this transaction', function(b) {
      console.log(b);
      if(b){
        Cashiering.update({id:$scope.id},
          {
            folio_id: $scope.data.id,
            reservation_id: $scope.data.reservation_id,
            reservation_room_id: $scope.data.reservation_room_id
          },
          function(e){
            if(e.ok){
              $.gritter.add({title:'Successful!', text:'Booking has been checked out.'});
              window.location = "#/hotel/cashiering";
            }
        });
      }
    });
  }
  // .checkout
  
  // add charges
  $scope.addCharges = function() {
   $scope.transactionDate = Date.today().toString('MM/dd/yyyy');

     // business services selection
    Select.get({ code: 'business-services' }, function(e) {
      $scope.businessServices = e.data;
    });
  
  // penalties selection
  Select.get({ code: 'penalties' }, function(e){
    $scope.penalties = e.data;
  });

    $('#add-charges-modal').modal('show');
    $scope.atransaction = {};
  }

  $scope.saveCharges = function() {
    if ($scope.chargeType == 'service') {
      businessList = [];
      angular.forEach($scope.businessServices, function(service, e) {
        if (service.selected) {
          if (businessList.contains(service.business)) {
            
            $scope.atransaction.TransactionSub.push({
              particulars: service.name,
              amount:      service.value,
              quantity:    service.quantity,
            });
            
          } else {
            businessList.push(service.business);
            
            $scope.atransaction= {
              Transaction: {
                businessId:  service.businessId,
                particulars: service.business.toLowerCase() + ' services',
                title:       service.business.toLowerCase() + ' services',
                date:        $scope.transactionDate,
              },
              TransactionSub: []
            };
            
            $scope.atransaction.TransactionSub.push({
              particulars: service.name,
              amount:      service.value,
              quantity:    service.quantity,
            });

          }
        }
      });
      
    } else {
      $scope.atransaction = {
        // Penalty: {
          Transaction: {
            businessId:  1,
            particulars: 'hotel penalties',
            title:       'hotel penalties',
            date:        $scope.transactionDate,
          },
          TransactionSub: []
        // }
      }
      
      angular.forEach($scope.penalties, function(penalty, e) {
        if (penalty.selected) {
          $scope.atransaction.TransactionSub.push({
            particulars: penalty.name,
            amount:      penalty.value,
            quantity:    1,
          });
        }
      });
    
    }
     
     Transaction.save({ folioid: $scope.id },$scope.atransaction , function(e) {
      console.log(e);
      if (e.ok) {
        $.gritter.add({
          title : 'Successful!',
          text  : e.msg
        });
        $('#add-charges-modal').modal('hide');
        $scope.load();
      }
    });
    
    
  }
  // .add charges
  
  // print
  $scope.print = function(){
    $scope.load();
    printTable(base + 'print/guestfoliosoa/' + $routeParams.id);
  }

  // remove transaction payment
  $scope.removePayment = function(payment) {
    bootbox.confirm('Are you sure you want to delete  this payment?', function(b) {
      if (b) {
        TransactionPayment.remove({ id: payment.id }, function(e) {
          if (e.ok) {
            $scope.load();
          }
        });
      }
    });
  }
  // folio
  $scope.efoliosub = {
    FolioSub: []
  }

  // transfer bills
  $scope.transferBill = function() {
    // folios selection
    Select.get({ code: 'folio',excludeId: $scope.id }, function(e) {
      $scope.folio = e.data;
    });

    $('#transfer-bill-modal').modal('show');
  }

  $scope.saveTransferBills = function() {
    FolioSub.save({ 
      FolioSub: {
        folioId : $scope.id,
        parentId: $scope.parentId,
      }
    }, function(e) {
      if (e.ok) {
        $.gritter.add({
          title: 'Successful!',
          text: e.msg
        });

         $('#transfer-bill-modal').modal('hide');  
         setTimeout(function(){
            window.location = "#/hotel/cashiering";
          }, 500);
      }
    });
  }

  // guest history
  $scope.viewHistory = function(guestId) {
    GuestHistory.get({id: guestId }, function(e){
      $scope.history = e.data;
      $scope.received = e.received;
    });
    $('#view-history-modal').modal('show');
  }

  $scope.viewTransactionsHistory = function(guestId,folioId) {
    GuestHistory.get({id: guestId , folioId:  folioId}, function(e){
      $scope.historyTransactions = e.data;
      
    });
     $('#view-transaction-history-modal').modal('show');
  }

});


app.controller('CashieringReportController', function($scope, HotelReportCashiering) {

  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.searchTxt = Date.today().toString('yyyy-MM-dd');

  $('#search').datepicker({ 
    format: 'yyyy-mm-dd', 
    autoclose: true
  });

  $("#search").datepicker('setDate', $scope.searchTxt);

   $scope.dates = {
    // startTime: new Date(),
    // endTime: new Date()
  };
  
  $scope.open = {
    startTime: false,
    endTime: false
  };
  
  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
  };

  $scope.dateOptions = {
    showWeeks: false,
    startingDay: 1
  };
  
  $scope.timeOptions = {
    readonlyInput: true,
    showMeridian: true
  };
  
  $scope.openCalendar = function(e, date) {
      e.preventDefault();
      e.stopPropagation();

      $scope.open[date] = true;
  };
  // $scope.searchTxt = Date.parse('today').toString('yyyy-MM-dd');
  $scope.datas = {};

  // load employees
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    HotelReportCashiering.query(options, function(data) {
      if (data.ok) {
        $scope.datas   = data.result;
        $scope.summary = data.summary;
      }
    });
  }
  $scope.load({date: $scope.searchTxt, business: 1 });

  $scope.searchTime = function() {
    $scope.load({date: $scope.searchTxt, startTime: $scope.dates.startTime, endTime: $scope.dates.endTime, business: 1});
  }

  // print
  $scope.print = function(){
    d8 = Date.today();

    if($scope.dates.startTime ==null || $scope.dates.endTime==null ){
      shiftTime = '' ;
    } else {
       shiftTime = '/'+ $scope.dates.startTime.getTime() + '/' + $scope.dates.endTime.getTime();
    }
    printTable(base + 'print/cashiering_report/' + $scope.searchTxt + shiftTime + '/business:1');
  }

});


app.controller('CashieringStatementOfAccountController', function($scope, $routeParams, Cashiering ,StatementOfAccount){
  $scope.id = $routeParams.id ;
  
  $scope.load = function(){
    StatementOfAccount.query({ id: $scope.id }, function(e){
      $scope.data = e.data;
    });
  }
  
  $scope.load();

  $scope.print = function(){
    $scope.load();
    printTable(base + 'print/statementofaccount/' + $routeParams.id);
  }
});


app.controller('CashieringReportViewController', function($scope, $routeParams, ReportCashiering) {
  // $scope.id = $routeParams.id ;
  // $scope.today = Date.today().toString('MM/dd/yyyy');
  // $scope.strSearch = Date.parse('today').toString('yyyy-MM-dd');

  // $scope.load = function(){
  //   ReportCashiering.query({id:$scope.id}, function(e){
  //     $scope.data = e.result;
  //   });
  // }
  
  // $scope.load();


  // // print
  // $scope.print = function(){
  //   $scope.load();
  //   printTable(base + 'print/cashiering_report_view/' + $scope.strSearch + '/1/' + $scope.id);
  // }

});  