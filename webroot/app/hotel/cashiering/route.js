app.config(function($routeProvider) {
  $routeProvider
  .when('/hotel/cashiering', {
    templateUrl: tmp + 'hotel__cashiering__index',
    controller: 'CashieringController',
  })
  // view
  .when('/hotel/cashiering/view/:id', {
    templateUrl: tmp + 'hotel__cashiering__view',
    controller: 'CashieringViewController',
  })
  // statement of account
  .when('/hotel/cashiering/soa/:id', {
    templateUrl: tmp + 'hotel__cashiering__soa',
    controller: 'CashieringStatementOfAccountController',
  })
  // cashiering report
  .when('/hotel/reports/cashiering', {
    templateUrl: tmp + 'hotel__cashiering__report',
    controller: 'CashieringReportController',
  }) 
  .when('/hotel/report/cashiering/view/:id', {
    templateUrl: tmp + 'hotel__cashiering__report_view',
    controller: 'CashieringReportViewController',
  });
});
