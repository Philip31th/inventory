app.factory("Cashiering", function($resource) {
  return $resource( api + "cashiering/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});


app.factory("ReservationCharges", function($resource) {
  return $resource( api + "reservation-charges/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});


app.factory("TransactionPayment", function($resource) {
  return $resource( api + "transaction-payments/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});


app.factory("HotelReportCashiering", function($resource) {
  return $resource( api + "cashiering/report/:id", {}, {
    query: { method:'GET', isArray:false },
    update: {method: 'PUT' }
  });
});


app.factory("AuditReportCashiering", function($resource) {
  return $resource( api + "cashiering/audit/:id", {},{
    query: { method:'GET', isArray:false },
    update: {method: 'PUT' }
  });
});



app.factory("Transaction", function($resource) {
  return $resource( api + "transactions/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});


app.factory("StatementOfAccount", function($resource) {
  return $resource( api + "cashiering/soa/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});


app.factory("Charge", function($resource) {
  return $resource( api + "charges/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
  });
});


app.factory("FolioSub", function($resource) {
  return $resource( api + "folio-subs/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
  });
});

app.factory("Occupied", function($resource) {
  return $resource( api + "rooms/occupied", {}, {
    query: { method: 'GET', isArray: false }
  });
});

app.factory("GuestHistory", function($resource) {
  return $resource( api + "returned-guest-history/:id", {}, {
    query: { method: 'GET', isArray: false }
  });
});