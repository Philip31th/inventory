app.config(function($routeProvider) {
  $routeProvider
  // night audit
  .when('/hotel/reports/night-audit', {
    templateUrl: tmp + 'hotel__night_audits__index',
    controller: 'NightAuditController',
  });
});
