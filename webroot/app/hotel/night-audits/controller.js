app.controller('NightAuditController', function($scope, NightAudit){
  
  $scope.date = Date.today().toString('MM/dd/yyyy');

  // load data
  $scope.load = function(){
    NightAudit.query({ date: $scope.date },function(e) {
      $scope.datas = e.data;
    });
  }
  $scope.load();

  $scope.print = function(){
    $scope.load();
    printTable(base + 'print/audit/');
  }

});