app.factory("NightAudit", function($resource) {
  return $resource( api + "night-audits/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
  });
});


