app.controller('CalendarController', function($scope, Calendar) {
  modalMaxHeight();

  $scope.startDate = Date.today().toString('MM/dd/yyyy');
  $scope.endDate = Date.parse($scope.startDate).add({ days: 6 }).toString('MM/dd/yyyy');
  $('.search').datepicker({format: 'mm/dd/yyyy', autoclose: true}).datepicker('setDate', $scope.startDate );
  
  

  $scope.load = function(date) {
    Calendar.query({ date: date }, function(data) {
      $scope.datas = data.result;
      $scope.dates = data.dates;
    });
  }
  $scope.load($scope.startDate);


  $scope.next = function() {
    $scope.startDate = Date.parse($scope.startDate).add({ days: 7 }).toString('MM/dd/yyyy');
    $scope.endDate = Date.parse($scope.startDate).add({ days: 6 }).toString('MM/dd/yyyy');
    $('.search').datepicker("setDate", $scope.startDate);
  }

  $scope.prev = function() {
    $scope.startDate = Date.parse($scope.startDate).add({days:-7}).toString('MM/dd/yyyy');
    $scope.endDate = Date.parse($scope.startDate).add({days:6}).toString('MM/dd/yyyy');
    $('.search').datepicker("setDate", $scope.startDate);
  }

  // $scope.cellClick = function(room, date, status) {
  //   if(status=='available'){
  //   $scope.newReservation.Reservation.arrival = Date.parse(date.date).toString('MM/dd/yyyy');
  //   $scope.newReservation.Reservation.departure = Date.parse(date.date).add({days:1}).toString('MM/dd/yyyy');
  //   $('#reservation-modal').modal('show');
  //   }
  // }

   $scope.cellClick = function(room, date, status) {
    if(status=='available'){
      $scope.newReservation.Reservation.arrival = Date.parse(date.date).toString('MM/dd/yyyy');
      $scope.newReservation.Reservation.departure = Date.parse(date.date).add({days:1}).toString('MM/dd/yyyy');
      $('#reservation-modal').modal('show');
    }
  }
});
