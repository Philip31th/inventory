app.config(function($routeProvider) {
  $routeProvider
  .when('/hotel/bookings', {
    templateUrl: tmp + 'hotel__bookings__index',
    controller: 'BookingController',
  })
  .when('/hotel/booking/view/:id', {
    templateUrl: tmp + 'hotel__bookings__view',
    controller: 'BookingViewController',
  })
  .when('/hotel/booking/add', {
    templateUrl: tmp + 'hotel__bookings__add',
    controller: 'BookingAddController',
  })
  .when('/hotel/booking/edit/:id', {
    templateUrl: tmp + 'hotel__bookings__edit',
    controller: 'BookingEditController',
  })
  .when('/hotel/booking/book/:date', {
    templateUrl: tmp + 'hotel__bookings__book',
    controller: 'BookingBookController',
  });
});