app.factory("Booking", function($resource) {
  return $resource( api + "bookings/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
    createFolio: { method: 'POST' }
  });
});

app.factory("Reservation", function($resource) {
  return $resource( api + "reservations/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});

app.factory("ReservationRoom", function($resource) {
  return $resource( api + "reservation-rooms/:id", { id: '@id', search: '@search' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    search: { method: 'GET' },
  });
});

app.factory("Customer", function($resource) {
  return $resource( api + "customers/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});