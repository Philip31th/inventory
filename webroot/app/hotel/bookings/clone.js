app.controller('BookingViewController', function($scope, $routeParams, Booking, Select, TransactionPayment, Folio, ReservationRoom, Reservation, Company) {
  modalMaxHeight();
  $scope.reservationId = $routeParams.id;

  // 
  $scope.deleteReservation = function(Reservation) {
    bootbox.confirm('Are you sure you want to delete ' +  + '?', function(c) {
      if (c) {
        Booking.remove({ id: $scope.reservationId }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            window.location = '#/hotel/bookings';
          }
        });
      }
    });
  }

  $('#form').validationEngine('attach');

  $('.start-date').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    startDate: Date.today().toString('MM/dd/yyyy'),
  }).on('changeDate', function(e) {
    dt = e.date.add({ day: 1}).toString('MM/dd/yyyy');
    $('.departure-date').datepicker('setStartDate', dt);
  });
  
  $('.ens-date').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    startDate: Date.today().toString('MM/dd/yyyy'),
  }).on('changeDate', function(e) {
    dt = e.date.add({ day: -1}).toString('MM/dd/yyyy');
    $('.arrival-date').datepicker('setEndDate', dt);
  });

  // booking source selection
  Select.get({ code: 'booking-sources' }, function(e) {
    $scope.bookingSources = e.data;
  });


  // load reservation
  $scope.load = function() {
    Booking.get({ id: $scope.reservationId }, function(e) {
      $scope.data = e.data;
      // default gender if not null
      // if ($scope.data.Reservation.gender == null) $scope.data.Reservation.gender = '';
      if ($scope.data.Reservation.identity == null) $scope.data.Reservation.identity = '';
      if ($scope.data.Reservation.billTo == null) $scope.data.Reservation.billTo = '';

      $scope.searchedCoy = '';
    });
  }
  $scope.load();

  // select available rooms
  $scope.checkIfSelected = function(id) {
    for(i in $scope.data.ReservationRoom) {
      if ($scope.data.ReservationRoom[i].id == id)
        return true
    }
    return false;
  }
  $scope.selectAvailableRoom = function() {
    Select.get({
      code:      'available-rooms',
      arrival:   $scope.data.Reservation.arrival,
      departure: $scope.data.Reservation.departure,
    }, function(e) {
      $scope.availableRooms = e.data;
    });
    $('#select-room-modal').modal('show');
  }
  
  $scope.addSeletedRooms = function() {
    $scope.reservationRooms = [];
    for (i in $scope.availableRooms) {
      if ($scope.availableRooms[i].selected) {
        $scope.reservationRooms.push({
          reservationId: $scope.data.Reservation.id,
          arrival:       Date.parse($scope.data.Reservation.arrival).toString('yyyy-MM-dd'),
          departure:     Date.parse($scope.data.Reservation.departure).add({ day: -1 }).toString('yyyy-MM-dd'),
          roomId:        $scope.availableRooms[i].id,  
        });
      }
    }

    ReservationRoom.save({ ReservationRoom: $scope.reservationRooms }, function(e) {
      if (e.ok) {
        $.gritter.add({
          title: 'Successful!',
          text:   e.msg,
        });

        $scope.load();
        $('#select-room-modal').modal('hide');
      } else {
        $.gritter.add({
          title: 'Warning!',
          text:   e.msg,
        });
      }
    });
  }
  
  $scope.viewFolio = function(guestId) {
    var d = new Date();
    d.setDate(d.getDate() + 1);

      // guests selection
    Select.get({ code: 'folio-data', id: guestId }, function(e) {
      $scope.viewfolio = e.data;
      var d = new Date(e.data.Folio[0].ReservationRoom.departure);
      $scope.departure = d.setDate(d.getDate()+1);
    });  

    $('#view-folio-modal').modal('show');
  } 

  $scope.checkOR = function(orNumber,business) {

    // if(orNumber.length >= 3) {
    if(orNumber!='') {  
      $scope.checking = true;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
      TransactionPayment.query({ search: orNumber , businessId:  business },function(e){
        if (e.data) {
          $scope.checking = false;
          $scope.notexisted = false;
          $scope.orNumberExisting = true;
        } else {
          $scope.checking = false;
          $scope.notexisted = true;
          $scope.orNumberExisting = false;
        }
      });
    } else {
      $scope.checking = false;
      $scope.notexisted = false;
      $scope.orNumberExisting = false;
    }
  }

  $scope.paymentType = function(paymentType) {
    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
  }

  // check in reservation room
  $scope.checkInReservationRoom = function(room) {
    $scope.checking = false;
    $scope.notexisted = false;
    $scope.orNumberExisting = false;
    $scope.newFolio.TransactionPayment = {};

    // guests selection
    Select.get({ code: 'guests' }, function(e) {
      $scope.guests = e.data;
    });

    // payment types selection
    Select.get({ code: 'payment-types' }, function(e) {
      $scope.paymentTypes = e.data;
    });

    $('#newfolio').validationEngine('attach');

    $scope.newFolio = {
      Room: {
        number: room.Room.name,
      },
      Guest: {
        title:       $scope.data.Reservation.title,
        lastName:    $scope.data.Reservation.lastName,
        firstName:   $scope.data.Reservation.firstName,
        middleName:  $scope.data.Reservation.middleName,
        gender:      $scope.data.Reservation.gender,
        nationality: $scope.data.Reservation.nationality,
        mobile:      $scope.data.Reservation.mobile,
        email:       $scope.data.Reservation.email,
        address:     $scope.data.Reservation.address
      },
      Folio: {
        arrival:           $scope.data.Reservation.arrival,
        departure:         $scope.data.Reservation.departure,
        nights:            $scope.data.Reservation.nights,
        rate:              room.Room.rate,
        bookingSource:     $scope.data.Reservation.bookingSource,
        reservationRoomId: room.id,
        roomId:            room.Room.id,
        adults:            room.Room.Accomodation.number,
        children:          0,
      },
      Company: {
        name:  $scope.data.Company.name,
        id:    $scope.data.Company.id
      },
      Reservation: {
        id:   $scope.data.Reservation.id,
        title: $scope.data.Reservation.title,
      },
      TransactionPayment: {

      }
    };
    $('#submitFolio').attr('disabled', false);    
    $('#check-in-modal').modal('show');
  }    

  // get guest data if returning
  $scope.getGuestData = function() {
    if ($scope.newFolio.Guest.id != null && $scope.newFolio.Guest.id != '') {
      Select.get({ code: 'guest-data', id: $scope.newFolio.Guest.id }, function(e) {
        $scope.newFolio.Guest.lastName    = e.data.Guest.lastName;
        $scope.newFolio.Guest.firstName   = e.data.Guest.firstName;
        $scope.newFolio.Guest.middleName  = e.data.Guest.middleName;
        $scope.newFolio.Guest.gender      = e.data.Guest.gender;
        $scope.newFolio.Guest.nationality = e.data.Guest.nationality;
        $scope.newFolio.Guest.mobile      = e.data.Guest.mobile;
        $scope.newFolio.Guest.email       = e.data.Guest.email;
        $scope.newFolio.Guest.address     = e.data.Guest.address;
        $scope.newFolio.Guest.companyId   = e.data.Guest.companyId;
        $scope.newFolio.Guest.company     = e.data.Company.name;
      });
    }
  }
  
  $scope.data = {ReservationRoom: [], Reservation: []};

  // save folio
  $scope.saveFolio = function() {
    valid = $("#newfolio").validationEngine('validate');

    if (valid) {

      if ($scope.guestType == 'new') {
        $scope.newFolio.Guest.id = null;
      }
      // if merong payment amount
      if ($scope.newFolio.TransactionPayment.amount != undefined || $scope.newFolio.TransactionPayment.amount != null ) {
        if ($scope.newFolio.TransactionPayment.amount > ($scope.newFolio.Folio.rate*$scope.newFolio.Folio.nights)) {
          $scope.newFolio.TransactionPayment.change = ($scope.newFolio.Folio.rate*$scope.newFolio.Folio.nights) - $scope.newFolio.TransactionPayment.amount;
        }  

        // if walang payment type
        if ($scope.newFolio.TransactionPayment.paymentType == null || $scope.newFolio.TransactionPayment.paymentType == undefined ) {
          $.gritter.add({
            title: 'Warning!', 
            text:  'Kindly specify the payment type',
          });
           $('#submitFolio').attr('disabled', false);

        // if walang orNumber    
        } else if ($scope.newFolio.TransactionPayment.orNumber == null || $scope.newFolio.TransactionPayment.orNumber == undefined || $scope.newFolio.TransactionPayment.orNumber == '' ) {
          $.gritter.add({
            title: 'Warning!', 
            text:  'Kindly specify the orNumber',
          });
           $('#submitFolio').attr('disabled', false);
        } else if ($scope.orNumberExisting == true) {
          $.gritter.add({
            title: 'Warning!', 
            text:  'OR Number Already Exist!',
          });
          $('#submitFolio').attr('disabled', false);
        } else {
          $('#submitFolio').attr('disabled', true);

          Folio.save($scope.newFolio, function(e) {
          if (e.ok) {
            $('#submitFolio').attr('disabled', true);
            $('#check-in-modal').modal('hide');
            
             Booking.get({ id: $scope.reservationId }, function(e) {
                $scope.data.ReservationRoom = e.reservation_room;
             });   

            $.gritter.add({
              title: 'Successful!', 
              text:  e.msg,
              });          
            } else {
              $.gritter.add({
                title: 'Warning!',
                text:  e.msg
              });
          }
        });   
        }

      } else {
         // if theres ornumber/payment type
        if ($scope.newFolio.TransactionPayment.amount == undefined || $scope.newFolio.TransactionPayment.amount == 0 || $scope.newFolio.TransactionPayment.amount == null) {
          $scope.newFolio.TransactionPayment = {};
        }else if ($scope.newFolio.TransactionPayment.amount > ($scope.newFolio.Folio.rate*$scope.newFolio.Folio.nights)) {
          $scope.newFolio.TransactionPayment.change = ($scope.newFolio.Folio.rate*$scope.newFolio.Folio.nights) - $scope.newFolio.TransactionPayment.amount;
        }  
        $('#submitFolio').attr('disabled', true);
        
        Folio.save($scope.newFolio, function(e) {
          if (e.ok) {
            $('#submitFolio').attr('disabled', true);
            $('#check-in-modal').modal('hide');
            
            Booking.get({ id: $scope.reservationId }, function(e) {
                $scope.data.ReservationRoom = e.reservation_room;
             }); 

            $.gritter.add({
              title: 'Successful!', 
              text:  e.msg,
              });          
            } else {
              $.gritter.add({
                title: 'Warning!',
                text:  e.msg
              });
          }
        });   
     }   
    } else {
      $.gritter.add({
        title: 'Warning!',
        text:  'Please fill up required fields.',
      });
    }
  }

  // delete reservation room
  $scope.deleteReservationRoom = function(room) {
    bootbox.confirm('Are you sure you want to delete room #' + room.Room.name + '?', function(c) {
      if (c) {
        ReservationRoom.remove({ id: room.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });
            $scope.load();
          } else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }
        });
      }
    });
  }

  // delete Folio
  $scope.deleteFolio = function(folio) {
    bootbox.confirm('Are you sure you want to remove this?', function(c) {
      if (c) {
        Folio.remove({ id: folio.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text:   e.msg,
            });

            $scope.load();
          } else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }
        });
      }
    });
  }

  // update booking
  $scope.updatebooking = function() {
    Booking.update({ id:$scope.reservationId},$scope.data, function(e){
      if (e.ok) {
        $.gritter.add({
          title : 'Successful',
          text  : e.msg 
        });
        window.location = '#/hotel/bookings';
      }
      
    });    
  }

  $scope.changeTitle = function(title,type){
    if(title == 'Mr.'){
        $scope.data.Reservation.gender = 'male';
    } else if(title == 'Ms.' || title == 'Mrs.'){
        $scope.data.Reservation.gender = 'female';
    } else if (title=='male') {
        $scope.data.Reservation.title = 'Mr.';
    } else if (title=='female') {
        $scope.data.Reservation.title = 'Ms.';
    } else if (title!=null && type=='gender') {
        if (title == 'male') {
          $scope.data.Reservation.title = 'Mr.';
        } else if (title=='female') {
          $scope.data.Reservation.title = 'Ms.';
        }
    } else if (title!=null && type=='title') {
        if (title == 'Mr.') {
          $scope.data.Reservation.gender = 'male';
        } else if (title=='Ms.') {
          $scope.data.Reservation.gender = 'female';
        }    
    } else if (title==null && type=='gender' || title==null && type=='title'){
        $scope.data.Reservation.gender = null;
        $scope.data.Reservation.title = null;
    } else {
        $scope.data.Reservation.gender = null;
        $scope.data.Reservation.title = null;
    }
  }
  
  // search company 
  $scope.search = function() {
    $scope.searchItems = [];
    $scope.data = {Company: {name: ''}}

    // auto complete search
    Company.query(function(e){
      $scope.searchItems = e.data;
      $scope.searchItems.sort();
  
      //Change It To lowercase
      $scope.searchItemsSmallLetter = [];
      $scope.l = $scope.searchItems.length;
      for(var i=0; i<$scope.searchItems.length; i++){
        $scope.searchItemsSmallLetter.push($scope.searchItems[i].toLowerCase());
      }
        
      //Function To Call on ng-keydown
      $scope.autoCompleteInput = function(){
        $("#searchFiled_TXT").autocomplete({
          source: function(request, response) {
            $scope.results = $.ui.autocomplete.filter($scope.searchItemsSmallLetter, request.term);
                response($scope.results.splice(0,4));
          },
          select: function( event , ui ) {
            $scope.data.Company.name =  ui.item.value;

            $scope.$apply();
          }
        });
      } 
    });
  } 
});
