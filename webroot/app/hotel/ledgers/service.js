app.factory("Ledger", function($resource) {
  return $resource( api + "ledgers/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
  });
});

app.factory("LedgerView", function($resource) {
  return $resource( api + "ledgers/view/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
  });
});



