app.config(function($routeProvider) {
  $routeProvider
  // city ledger
  .when('/hotel/reports/city-ledger', {
    templateUrl: tmp + 'hotel__ledgers__index',
    controller: 'LedgerController',
  })
   .when('/hotel/reports/city-ledger/view/:company', {
    templateUrl: tmp + 'hotel__ledgers__view',
    controller: 'LedgerViewController',
  });
});
