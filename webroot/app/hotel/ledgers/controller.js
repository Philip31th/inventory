app.controller('LedgerController', function($scope, Ledger){
  
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Ledger.query(options,function(e) {
      if(e.ok) {
        $scope.datas = e.data;
      }
    });
  }
    $scope.load();  
    
  $scope.print = function(){
    $scope.load(); 
    printTable(base + 'print/ledger_all/');
  }
});  
  
  
app.controller('LedgerViewController', function($scope, $routeParams, LedgerView){
  $scope.company = $routeParams.company;

  $scope.load = function() {
    LedgerView.query({ company: $scope.company },function(e) {
      $scope.datas = e.data;
      $scope.total = e.totalAmount;
    });
  }
  
  $scope.load();  
  
   $scope.print = function(){
    $scope.load();
    printTable(base + 'print/ledger_view/' + $scope.company);
  }
});    