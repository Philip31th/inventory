app.factory("Guest", function($resource) {
  return $resource( api + "guests/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});

app.factory("Folio", function($resource) {
  return $resource( api + "folios/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' },
    save: {
      method: 'POST',
      headers: { 'Content-Type': undefined, enctype: 'multipart/form-data' },
      transformRequest: function(data) {
        // transform data
        var formData = new FormData();
        formData.append('data', JSON.stringify(data));

        // attach file
        guestAttachment = document.getElementById('guestAttachment');
        if (guestAttachment != null && guestAttachment.files.length > 0)
          formData.append('attachment', guestAttachment.files[0]);

        return formData;
      }
    }
  });
});

app.factory("Arrival", function($resource) {
  return $resource( api + "folios/arrival", {}, {
    query: { method: 'GET', isArray: false }
  });
});

app.factory("Departure", function($resource) {
  return $resource( api + "folios/departure", {}, {
    query: { method: 'GET', isArray: false }
  });
});

