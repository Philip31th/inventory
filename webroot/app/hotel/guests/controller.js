app.controller('GuestController', function($scope, Guest) {
  $scope.today     = Date.today().toString('MM/dd/yyyy');
  $scope.searchTxt = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({format: 'mm/dd/yyyy', autoclose: true}).datepicker('setDate', $scope.searchTxt );

  // load guest 
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    Guest.query(options, function(e) {
      if (e.ok) {
        $scope.guests = e.data;

        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages      = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load();

});


app.controller('GuestIncomingController', function($scope, Arrival) {
  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({ format: 'mm/dd/yyyy', autoclose: true, startDate: $scope.today }).datepicker('setDate', $scope.strSearch );
  // load incoming guest
  $scope.load = function(){
    Arrival.query({arrival:$scope.strSearch}, function(e){
      $scope.datas = e.result;
    });
  }
  $scope.load();
});


app.controller('GuestOutgoingController', function($scope, Departure) {
  $scope.today = Date.today().toString('MM/dd/yyyy');
  $scope.strSearch = Date.today().toString('MM/dd/yyyy');
  $('.search').datepicker({ format: 'mm/dd/yyyy', autoclose: true, startDate: $scope.today }).datepicker('setDate', $scope.strSearch );
  
  $scope.load = function(){
    Departure.query({departure:$scope.strSearch}, function(e){
      $scope.datas = e.result;
    });
  } 
  $scope.load();

});

app.controller('FolioGuestListViewController', function($scope, $routeParams, Guest) {

  $scope.guestId = $routeParams.id;

  // load user
  $scope.load = function() {
    Guest.get({ id: $scope.guestId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();

});  


