app.config(function($routeProvider) {
  $routeProvider
  .when('/hotel/guests', {
    templateUrl: tmp + 'hotel__guests__index',
    controller: 'GuestController',
  })
  .when('/hotel/guestslists/view/:id', {
    templateUrl: tmp + 'hotel__guests__view',
    controller: 'FolioGuestListViewController',
  })
  .when('/hotel/guestslists/edit/:id', {
    templateUrl: tmp + 'hotel__guests__edit',
    controller: 'FolioGuestListEditController',
  })
  .when('/hotel/guests/incoming', {
    templateUrl: tmp + 'hotel__guests__incoming',
    controller: 'GuestIncomingController',
  })
  .when('/hotel/guests/outgoing', {
    templateUrl: tmp + 'hotel__guests__outgoing',
    controller: 'GuestOutgoingController',
  });
});