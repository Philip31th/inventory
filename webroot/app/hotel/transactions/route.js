app.config(function($routeProvider) {
  $routeProvider
  .when('/hotel/transactions', {
    templateUrl: tmp + 'hotel__transactions__index',
    controller: 'HotelTransactionController',
  })
  .when('/hotel/transactions/add', {
    templateUrl: tmp + 'hotel__transactions__add',
    controller: 'HotelTransactionAddController',
  })
  .when('/hotel/transactions/view/:id', {
    templateUrl: tmp + 'hotel__transactions__view',
    controller: 'HotelTransactionViewController',
  })
  .when('/hotel/transactions/edit/:id', {
    templateUrl: tmp + 'hotel__transactions__edit',
    controller: 'HotelTransactionEditController',
  });
});