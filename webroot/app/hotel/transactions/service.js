app.factory("HotelTransaction", function($resource) {
  return $resource( api + "transactions/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});


app.factory("TransactionSub", function($resource) {
  return $resource( api + "transaction-subs/:id", { id: '@id' }, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});