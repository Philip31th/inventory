app.controller('HotelTransactionController', function($scope, HotelTransaction){

  // load inventory
  $scope.load = function(options) {
    options = typeof options !== 'undefined' ?  options : {};
    HotelTransaction.query(options, function(e) {
      if (e.ok) {
        $scope.transactions = e.data;
  
        // paginator
        $scope.paginator  = e.paginator;
        $scope.pages = paginator($scope.paginator, 5);
      }
    });
  }
  $scope.load({ code : 'hotel' });

  // remove transaction
  $scope.remove = function(transaction) {
    bootbox.confirm('Are you sure you want to delete this ' + transaction.business + ' Business?', function(b) {
      if (b) {
        HotelTransaction.remove({ id: transaction.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

            $scope.load({
              code: 'hotel',
              page: $scope.paginator.page,
              search: $scope.searchTxt
            });
             window.location = '#/hotel/transactions';
          } else {
            $.gritter.add({
              title: 'Warning!',
              text:   e.msg,
            });
          }
          
        });
       
      }
    });
  }
});


app.controller('HotelTransactionAddController', function($scope, HotelTransaction, TransactionSub, Select){

  // folios selection
  Select.get({ code: 'folios' }, function(e) {
    $scope.folios = e.data;
  });

  $('#form').validationEngine('attach');
  $('.datepicker').datepicker({format:'mm/dd/yyyy', autoclose:true});

  $scope.save = function(){
    valid = $("#form").validationEngine('validate');
    if(valid){

      $scope.atransaction.Transaction.businessId = 1;

      HotelTransaction.save($scope.atransaction, function(e){
        if(e.ok){
          $.gritter.add({ title: 'Successful!', text: e.message });
          window.location = '#/hotel/transactions';
        }
      });
    }
  }
  
  // add transaction item
  $scope.atransaction = {
    TransactionSub: []
  }
  subs = [];

  $scope.addItem = function(){
    $('#add-transaction-item-modal').modal('show');
  }

  $scope.saveItem = function(){
    subs = {
        transactionId: $scope.transactionId,
        particulars:   $scope.atransactionsub.particulars, 
        amount:        $scope.atransactionsub.amount
      }

      // save to database
      TransactionSub.save({ TransactionSub: subs }, function(e){
        if(e.ok){
          $('#add-transaction-item-modal').modal('hide');
        } 
      });
  }
  // .add transaction item
});


app.controller('HotelTransactionViewController', function($scope, $routeParams, HotelTransaction){
$scope.transactionId = $routeParams.id;

  // load transactions
  $scope.load = function() {
    HotelTransaction.get({ id: $scope.transactionId }, function(e) {
      $scope.data = e.data;
    });
  }
  $scope.load();
  
    // remove inventory
  $scope.remove = function(data) {
    bootbox.confirm('Are you sure you want to delete these ' + data.Business.name + ' Transactions?', function(b) {
      if (b) {
        HotelTransaction.remove({ id: data.Transaction.id }, function(e) {
          if (e.ok) {
            $.gritter.add({
              title: 'Successful!',
              text: e.msg,
            });

          }
          window.location = '#/hotel/transactions';
        });
      }
    });
  }
});


app.controller('HotelTransactionEditController', function($scope, $routeParams, HotelTransaction, TransactionSub, Select){
$scope.transactionId = $routeParams.id;
  
  // get transactions
  $scope.load = function() {
    HotelTransaction.get({ id: $scope.transactionId }, function(e){
      $scope.data = e.data;
    }); 
  }
  $scope.load();
  
  // update transaction
  $scope.save = function(){
    HotelTransaction.update({ id: $scope.transactionId }, $scope.data, function(e){
      if(e.ok){
        $.gritter.add({ title: 'Successful!', text: e.message });
        window.location = '#/hotel/transactions';
      }
    });
  }
  
 // add/edit transaction item
    $scope.data = {
      TransactionSub: []
    }
    subs = [];

  // ADD ITEM HERE
    $scope.addItem = function(){
      $('#add-transaction-item-modal').modal('show');
    }

    // save transaction item
    $scope.saveItem = function(){   
      subs = {
        transactionId: $scope.transactionId,
        particulars:   $scope.atransactionsub.particulars, 
        amount:        $scope.atransactionsub.amount
      }

      // save to database
      TransactionSub.save({ TransactionSub: subs }, function(e){
        if(e.ok){
          $scope.load();
          $('#add-transaction-item-modal').modal('hide');
        } 
      });

      $scope.atransactionsub = {}  
  }
 

  // EDIT ITEM HERE
    $scope.editItem = function(item){
      TransactionSub.get({ id: item.id }, function(e) {
        $scope.etransactionsub = e.data.TransactionSub;
        $('#edit-transaction-item-modal').modal('show');
      });
    }

    // update item
    $scope.updateItem = function(){
      subs = {
        transactionId: $scope.transactionId,
        particulars:   $scope.etransactionsub.particulars,
        amount:        $scope.etransactionsub.amount
      };
    
      // update to database
      TransactionSub.update({ id: $scope.etransactionsub.id }, { TransactionSub: subs }, function(e){
        $scope.load();
        $('#edit-transaction-item-modal').modal('hide');
      });

      $scope.etransactionsub = {}

    }
    // .edit transaction item
  

  // remove transaction
  $scope.removeItem = function(item) {
    bootbox.confirm('Are you sure you want to delete this ' + item.particulars + ' Business?', function(b) {
      if (b) {
        TransactionSub.remove({ id: item.id }, function(e) {
          if (e.ok) {
            $scope.load();
          }           
        });
       
      }
    });
  }
});
