app.factory("TransactionDiscount", function($resource) {
  return $resource( api + "transaction-discounts/:id", { id: '@id'}, {
    query: { method: 'GET', isArray: false },
    update: { method: 'PUT' }
  });
});
