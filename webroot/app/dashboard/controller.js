app.controller('DashboardController', function($scope, Dashboard, Occupied) {
  $scope.data = {
    availableRooms: '0',
    incomingGuests: '0',
    outgoingGuests: '0',
    occupiedRooms:  '0',
    uncleanedRooms: '0',
    occupancyRate:  '0',
  };
  
  Dashboard.query(function(e){
    $scope.data = e.result;
  });
  
  Occupied.query({date: Date.today().toString('MM/dd/yyyy')}, function(e) {
    $scope.occupieds = e.occupied;
    $scope.outs = e.out;
  });
});




