<!DOCTYPE html>
<html>
<head>
	<title>eHRMS - LOGIN</title>
	<link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/login.css?version=<?php echo time() ?>">
	<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/jquery/jquery.min.js"></script>
</head>
<body>
	<blink></blink>
	<section id="login">
    <header>
      <h1 style="font-size:47px;padding-bottom:10px"><?php echo $this->Application->settings('ehrms') ?></h1>
      <p><?php echo $this->Application->settings('system-title') ?></p>
    </header>
    <div class="clearfix"></div>
  </section>
    
  <!-- Login -->
  <div style="max-width:900px;margin:auto;padding:50px 0px 20px 0px">
  	<div class="col-md-6" style="padding-top:30px">
  		
  		<div class="clearfix"></div>
  	</div>
  	<div class="col-md-6">
      <?php echo $this->Form->create('User', array('url'=>array('controller'=>'main', 'action'=>'login'), 'class'=>'', 'id'=>'', 'inputDefaults'=>array('label'=>false, 'div'=>false, 'class'=>'form-control' ))) ?>
          <h2 class="login-title">SIGN IN</h2>
          <div class="form-group">
          	<?php echo $this->Form->input('username', array('required'=>true, 'placeholder'=>'USERNAME', 'autofocus'=>true, 'class'=>'form-control input-form')) ?>
          </div>
          <div class="form-group">
          	<?php echo $this->Form->input('password', array('required'=>true, 'placeholder'=>'PASSWORD', 'class'=>'form-control input-form')) ?>
          </div>
          
          <button class="btn btn-primary pull-right btn-min">
          	SIGN IN
          </button>
      <?php echo $this->Form->end() ?>
  	</div>
  </div>
    
  <div class="footer">
  	<div class="copyright">COPYRIGHT &copy 2012 | ALL RIGHTS RESERVED</div>
  	<div class="poweredby">POWERED BY: <a href="http://edncsolutions.com.ph">E D & C SOLUTIONS</a></div>
  </div>
</body>
</html>