<!DOCTYPE html>
<html>
<head>
  <title>eHRMS</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/scrollbar/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/angular-loading/loading-bar.css"> 
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
  <script>
    var base = '<?php echo $base ?>';
    var api  = '<?php echo $api  ?>';
    var tmp  = '<?php echo $tmp  ?>';
  </script>
  <script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/jquery/jquery.min.js"></script>
</head>
<body ng-app="ehrms">
  <?php if($this->Session->read('Auth.User.role') == 'superuser'): ?>
    <?php echo $this->element('menus/admin') ?>
  <?php else: ?>
    <?php echo $this->element('menus/'. $this->Session->read('Auth.User.role')) ?>
  <?php endif ?>

	<div class="main">
		<div class="col-md-3 leftside">
			 <?php echo $this->element('menus/user') ?> 
		</div>
		<div class="col-md-9 rightside">
			<div ng-view></div>
		</div>
	</div>
	
  <?php echo $this->element('angularjs') ?>
  <?php echo $this->element('scripts') ?>

	<?php echo $this->fetch('extrajs') ?>
</body>
</html>
