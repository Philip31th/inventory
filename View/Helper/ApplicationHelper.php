<?php
class ApplicationHelper extends AppHelper {

  public $components = array(
    'Session'
  );

  public function settings($code = null) {
    $this->Setting = ClassRegistry::init('Setting');
    $result = null;
    $data = $this->Setting->find('first', array(
      'conditions' => array(
        'code' => $code
      )
    ));

    $result = !empty($data)? $data['Setting']['value'] : null;
    return $result;
  }
  
}
