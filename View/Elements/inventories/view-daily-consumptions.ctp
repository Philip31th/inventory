<!-- add item -->
<div class="modal fade" id="view-daily-consumptions">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">DAILY CONSUMPTIONS RECORDS</h4>
      </div>
      
      <div class="modal-body">          
        <div class="col-md-12">
          <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="consumedDate" ng-change="searchConsumption()" id="searchConsumption" data-date-format="mm/dd/yyyy">
          </div> 
        </div>
        
        <div class="clearfix"></div>
        <hr>
        
        <div class="col-md-12" ng-if="consumed!=''">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr class="bg-info"> 
                <th class="w30px">#</th>
                <th class="uppercase text-center">NAME</th>
                <th class="text-center w120px">DESCRIPTION</th>
                <th class="text-center w120px">CONSUMED</th>
              </tr>
            </thead>
            <tbody>
               <tr ng-repeat="item in consumed"> 
  
                 <td>{{ $index + 1 }}</td>
                 <td class="uppercase text-center">{{ item.name }}</td>
                 <td class="uppercase text-center">{{ item.description }}</td>
                 <td class="text-center uppercase">{{ item.consumed | number:2 }} {{ item.unit }}</td>
               </tr>
            </tbody>
          </table>
        </div>  
      </div>
      
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>

          <button ng-if="modalTitle=='EDIT'" type="button" class="btn btn-primary btn-sm btn-min" ng-click="updateItem()"><i class="fa fa-save"></i> UPDATE</button>

        </div>
      </div>
    </div>
  </div>
</div>  
<!-- .add item -->