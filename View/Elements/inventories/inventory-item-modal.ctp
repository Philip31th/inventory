<!-- add item -->
<div class="modal fade" id="inventory-item-modal">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">{{ modalTitle }} INVENTORY ITEM</h4>
      </div>
      <div class="modal-body">     
        
          <div class="col-md-6">
            <div class="form-group">
              <label>Name:</label> {{ inventorysub.name }}
            </div> 
            <hr/>
          </div>  
          
           <div class="col-md-6">
            <div class="form-group">
              <label>Average Cost:</label> {{ inventorysub.averageCost }}
            </div>  
            <hr/>
          </div>  
          
          <div class="col-md-{{ inventorysub.cost!=''?  6 : 12 }}">
            <div class="form-group">
              <label>Quantity <i class="required">*</i></label>
              <input amount type="text" class="form-control input-sm" ng-model="inventorysub.quantity" data-validation-engine="validate[required]" autofocus>
            </div>
          </div>
          
          <div class="col-md-6" ng-if="inventorysub.cost!=''">
            <div class="form-group">
              <label>Cost <i class="required">*</i></label>
              <input amount type="text" class="form-control input-sm" ng-model="inventorysub.cost" data-validation-engine="validate[required]" autofocus>
            </div>
          </div>
          
          <input type="hidden" ng-model="inventorysub.type">
          <input type="hidden" ng-model="inventorysub.id">

        </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>

          <button ng-if="modalTitle!='EDIT'" type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveItem()"><i class="fa fa-save"></i> SAVE</button>

          <button ng-if="modalTitle=='EDIT'" type="button" class="btn btn-primary btn-sm btn-min" ng-click="updateItem()"><i class="fa fa-save"></i> UPDATE</button>

        </div>
      </div>
    </div>
  </div>
</div>  
<!-- .add item -->