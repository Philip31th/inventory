<!--add transaction sub modal-->
<div class="modal fade" id="add-transaction-item-modal">
<div class="modal-dialog modal-vertical-centered">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">ADD NEW ITEM</h4>
    </div>
    <div class="modal-body">
      <div class="col-md-6">
		<div class="form-group">
			<label>PARTICULARS</label>
			<input type="text" class="form-control input-sm" ng-model="atransactionsub.particulars" autofocus>
		</div>
	</div>
	 <div class="col-md-6">
		<div class="form-group">
			<label>AMOUNT</label>
			<input type="text" class="form-control input-sm float-value" ng-model="atransactionsub.amount">
		</div>
	</div>

    <input type="hidden" ng-model="atransactionsub.id">

    </div>
    <div class="modal-footer">
      <div class="btn-group btn-group-sm pull-right btn-min">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveItem()"><i class="fa fa-save"></i> SAVE ITEM</button>
      </div> 

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->