<!--add transaction sub modal-->
<div class="modal fade" id="transaction-item-modal">
<div class="modal-dialog modal-vertical-centered">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">{{ title  }} ITEM</h4>
    </div>
    <div class="modal-body">
      <div class="col-md-6">
    		<div class="form-group">
    			<label>PARTICULARS</label>
    			<input type="text" class="form-control input-sm" ng-model="item.particulars" autofocus>
    		</div>
    	</div>
    	 <div class="col-md-6">
    		<div class="form-group">
    			<label>AMOUNT</label>
    			<input number type="text" class="form-control input-sm" ng-model="item.amount">
    		</div>
    	</div>
    </div>
    <div class="modal-footer">
      <div class="btn-group btn-group-sm pull-right btn-min">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-if="title=='EDIT'"  ng-click="updateItem(item)"><i class="fa fa-save"></i> UPDATE ITEM</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-if="title=='ADD NEW'"  ng-click="saveItem(item)"><i class="fa fa-save"></i> SAVE ITEM</button>
      </div> 
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->