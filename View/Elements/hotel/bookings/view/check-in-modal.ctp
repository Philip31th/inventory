<div class="modal fade" id="check-in-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">CHECK IN FORM</h4>
      </div>
      <div class="modal-body">
        <form id="newfolio">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Guest Type</label>
                <select class="form-control" ng-init="guestType = 'returning'" ng-model="guestType">
                  <option value="new">New Guest</option>
                  <option value="returning">Returning Guest</option>
                </select>
              </div>
            </div>

            <!-- new guest -->
            <div ng-if="guestType == 'new'">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Last Name <i class="required">*</i></label>
                  <input type="text" class="form-control " ng-model="newFolio.Guest.lastName" data-validation-engine="validate[required]">
                  <input type="hidden" ng-model="newFolio.Reservation.id">
                </div>
              </div>


              <div class="col-md-3">
                <div class="form-group">
                  <label>First Name <i class="required">*</i></label>
                  <input type="text" class="form-control " ng-model="newFolio.Guest.firstName" data-validation-engine="validate[required]">
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Middle Name</label>
                  <input type="text" class="form-control " ng-model="newFolio.Guest.middleName">
                </div>
              </div>
            </div>

            <!-- returning guest -->
            <div ng-if="guestType == 'returning'">
              <div class="col-md-9">
                <div class="form-group">
                  <label>Guest's Name <i class="required">*</i></label>
                  <select class="form-control " ng-model="newFolio.Guest.id" ng-options="opt.id as opt.value for opt in guests"  data-validation-engine="validate[required]" ng-change="getGuestData()">
                    <option value="">Select Guest</option>
                    <!-- for testing purposes -->
                    <option ng-if="noGuest">No previous guest found</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Gender <i class="required">*</i></label>
                <select class="form-control" ng-model="newFolio.Guest.gender" data-validation-engine="validate[required]">
                  <!-- <option value="">Select Gender</option> -->
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </select>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Nationality <i class="required">*</i></label>
                <input type="text" class="form-control " ng-model="newFolio.Guest.nationality" data-validation-engine="validate[required]" ng-init="newFolio.Guest.nationality = 'Filipino'">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Contact # <i class="required">*</i></label>
                <input type="text" ng-if="guestType == 'returning'" class="form-control" ng-model="newFolio.Guest.mobile" data-validation-engine="validate[required]">
                <input type="text" ng-if="guestType == 'new'" class="form-control" ng-model="newFolio.Guest.mobile" data-validation-engine="validate[required]">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" ng-model="newFolio.Guest.email">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Address <i class="required">*</i></label>
                <input type="text" class="form-control " ng-model="newFolio.Guest.address" data-validation-engine="validate[required]">
              </div>
            </div>
            

            <div class="col-md-6" ng-if="guestType == 'new'">
              <div class="form-group">
                <label>Company</label>
                <input type="text" class="form-control " ng-model="newFolio.Company.name">
              </div>
            </div>

            <div class="col-md-6" ng-if="guestType == 'returning'">
              <div class="form-group">
                <label>Company</label>
                <input type="text" class="form-control" ng-model=" newFolio.Guest.company">
              </div>
            </div>

            <div class="clearfix"></div>
            <hr>

            <div class="col-md-4">
              <div class="form-group">
                <label>Vehicle Made</label>
                <input type="text" class="form-control" ng-model="newFolio.Guest.vehicleMade">
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Plate Number</label>
                <input type="text" class="form-control" ng-model="newFolio.Guest.vehiclePlateNo">
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Vehicle Color</label>
                <input type="text" class="form-control" ng-model="newFolio.Guest.vehicleColor">
              </div>
            </div>

            <div class="clearfix"></div>
            <hr>

            <div class="col-md-3">
              <div class="form-group">
                <label>Booking Source <i class="required">*</i></label>
                <select class="form-control" ng-model="newFolio.Folio.bookingSource" ng-options="opt.id as opt.value for opt in bookingSources" data-validation-engine="validate[required]">
                  <option value="">Select Booking Source</option>
                </select>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Arrival <i class="required">*</i></label>
                <input disabled date type="text" class="form-control" ng-model="newFolio.Folio.arrival">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Departure <i class="required">*</i></label>
                <input disabled date type="text" class="form-control" ng-model="newFolio.Folio.departure">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Night(s)</label>
                <input disabled number type="text" class="form-control" ng-model="newFolio.Folio.nights" disabled>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Room Number</label>
                <input type="text" class="form-control" ng-model="newFolio.Room.number" disabled>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Room Rate <i class="required">*</i></label>
                <input disabled amount type="text" class="form-control" ng-model="newFolio.Folio.rate">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Adult(s) <i class="required">*</i></label>
                <input number type="text" class="form-control" ng-model="newFolio.Folio.adults" data-validation-engine="validate[required]">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Children <i class="required">*</i></label>
                <input number type="text" class="form-control" ng-model="newFolio.Folio.children" data-validation-engine="validate[required]">
              </div>
            </div>

            <div class="col-md-9">
              <div class="form-group">
                <label>Special Request</label>
                <input type="text" class="form-control" ng-model="newFolio.Folio.specialRequest">
              </div>
            </div>

            <div class="col-md-3" ng-if="guestType == 'new'">
              <div class="form-group">
                <label>File Attachment <i class="required">*</i></label>
                <span class="btn btn-primary btn-block btn-sm btn-file no-border-radius">
                  Choose File <input id="guestAttachment" class="form-control" type="file" accept="image/gif, image/jpeg, image/png" data-validation-engine="validate[required]">
                </span>
              </div>
            </div>

            <div class="clearfix"></div>
            <hr>

            <div class="col-md-6">
              <div class="form-group">
                <label>Initial Payment</label>
                <input amount type="text" class="form-control" ng-model="newFolio.TransactionPayment.amount" ng-init="newFolio.TransactionPayment.amount=0">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Payment Type</label>
                <select class="form-control" ng-model="newFolio.TransactionPayment.paymentType" ng-options="opt.id as opt.value for opt in paymentTypes">
                  <option value="">Select Payment Type</option>
                </select>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>O.R Number</label> 
                <i class="fa fa-times-circle text-danger italic pull-right ng-hide" ng-show="orNumberExisting&&!notexisted&&!checking"> Already existed!</i> <i class="italic pull-right text-primary" ng-show="checking&&!notexisted&&!orNumberExisting">Checking</i><i class="fa fa-check-circle pull-right italic text-success" ng-show="notexisted&&!checking&&!orNumberExisting"></i>

                <input type="text" class="form-control" ng-model="newFolio.TransactionPayment.orNumber" ng-keyup="checkOR(newFolio.TransactionPayment.orNumber,'Hotel')" id="orNumber">
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Discount Type</label>
                <input type="text" class="form-control" ng-init="newFolio.TransactionDiscount.type = ''" ng-model="newFolio.TransactionDiscount.type">
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Discount Value</label>
                <input amount type="text" class="form-control" ng-init="newFolio.TransactionDiscount.value = 0" ng-model="newFolio.TransactionDiscount.value">
              </div>
            </div>


          </div>
        </form>
      </div>
      <!-- .moda-body -->

      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>
          <button type="button" id="submitFolio" class="btn btn-primary btn-sm btn-min" ng-click="saveFolio()"><i class="fa fa-save"></i> SAVE</button>
        </div> 
      </div>
    </div>
  </div>
</div>