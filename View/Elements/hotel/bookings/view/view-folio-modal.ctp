<!-- view folio -->
<div class="modal fade" id="view-folio-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">VIEW FOLIO</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered vcenter">
          <tbody>
            <tr>
              <td class="w150px">ATTACHMENT</td>
              <td>
                <img src="<?php echo $this->base ?>/uploads/guests/{{ viewfolio.Guest.id + '/' + viewfolio.Guest.attachment }}" height="100">
              </td>
            </tr>
            <tr>
              <td>LAST NAME</td>
              <td class="uppercase">{{ viewfolio.Guest.lastName }}</td>
            </tr>
            <tr>
              <td>FIRST NAME</td>
              <td class="uppercase">{{ viewfolio.Guest.firstName }}</td>
            </tr>
            <tr>
              <td>MIDDLE NAME</td>
              <td class="uppercase">{{ viewfolio.Guest.middleName }}</td>
            </tr>
            <tr>
              <td>GENDER</td>
              <td class="uppercase">{{ viewfolio.Guest.gender }}</td>
            </tr>
            <tr>
              <td>NATIONALITY</td>
              <td>{{ viewfolio.Guest.nationality }}</td>
            </tr>
            <tr>
              <td>CONTACT #</td>
              <td>{{ viewfolio.Guest.mobile }}</td>
            </tr>
            <tr>
              <td>ADDRESS</td>
              <td>{{ viewfolio.Guest.address }}</td>
            </tr>
            <tr>
              <td>EMAIL</td>
              <td>{{ viewfolio.Guest.email }}</td>
            </tr>
            <tr>
              <td>COMPANY</td>
              <td>{{ viewfolio.Company.name }}</td>
            </tr>
            
            <tr>
              <td>ARRIVAL</td>
              <td class="uppercase">{{ viewfolio.Folio[0].ReservationRoom.arrival| dateFormat:'MMMM dd, yyyy' }}</td>
            </tr>
            <tr>
              <td>DEPARTURE</td>
              <td class="uppercase">{{ departure | dateFormat:'MMMM dd, yyyy' }}</td>
            </tr>
            <tr>
              <td>NIGHTS</td>
              <td>{{ viewfolio.Folio[0].nights }}</td>
            </tr>
            <tr>
              <td>RM #</td>
              <td class="uppercase">{{ viewfolio.Folio[0].ReservationRoom.Room.name }} - {{ viewfolio.Folio[0].ReservationRoom.Room.RoomType.name }}</td>
            </tr>
            <tr>
              <td>RM RATE</td>
              <td>{{ viewfolio.Folio[0].ReservationRoom.Room.RoomType.rate }}</td>
            </tr>
            <tr>
              <td>ADULTS</td>
              <td>{{ viewfolio.Folio[0].adults }}</td>
            </tr>
            <tr>
              <td>CHILDREN</td>
              <td>{{ viewfolio.Folio[0].children }}</td>
            </tr>
            <tr>
              <td>PAYMENT MODE</td>
              <td class="uppercase">{{ viewfolio.Folio[0].ReservationRoom.Reservation.paymentType }}</td>
            </tr>
            <tr>
              <td>BOOKING SOURCE</td>
              <td class="uppercase">{{ viewfolio.Folio[0].bookingSource }}</td>
            </tr>
            <tr>
              <td>SPECIAL REQUEST</td>
              <td>{{ viewfolio.Folio[0].specialRequest }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>
      </div>
    </div>
  </div>
</div>
<!-- view folio -->