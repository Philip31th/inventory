<div class="modal fade" id="select-room-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">AVAILABLE ROOMS  <small class="uppercase"><i>({{ data.Reservation.arrival | dateFormat:'MMM-dd-yyyy' }} to {{ data.Reservation.departure | dateFormat:'MMM-dd-yyyy' }})</i></small></h4>
      </div>
      <div class="modal-body">
        <div class="clearfix"></div>
        <hr style="margin-top:0px">
        <table class="table table-bordered table-striped vcenter">
          <thead>
            <tr>
              <th class="w30px"></th>
              <th class="w60px">RM #</th>
              <th>RM TYPE</th>
              <th class="w150px">ACCOMODATION</th>
              <th class="w120px text-right">RATE</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="room in availableRooms" ng-if="checkIfSelected(room.id) == false">
              <td><input icheck type="checkbox" ng-init="room.selected = false" ng-model="room.selected"></td>
              <td>{{ room.code }}</td>
              <td class="uppercase">{{ room.type }}</td>
              <td class="uppercase">{{ room.accomodation }}</td>
              <th class="text-right italic">{{ room.rate | currency:'PHP ' }}</th>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="addSeletedRooms()"><i class="fa fa-check"></i> OK</button>
      </div>
    </div>
  </div>
</div>