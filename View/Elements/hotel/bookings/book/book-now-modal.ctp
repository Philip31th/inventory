<link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/css.css">

<div class="modal fade" id="book-now-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title ">{{ modalRoomType }} ROOM - RESERVATION</h4>
      </div>
      <div class="modal-body">
        <form id="form">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Arrival</label>
              <input type="text" class="form-control input-sm" disabled value="{{ arrivalDate }}">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Departure</label>
              <input type="text" class="form-control input-sm" disabled value="{{ departureDate }}">
            </div>
          </div>
          
            <input type="hidden" ng-model="newReservation.Reservation.nights" value="{{ newReservation.Reservation.nights }}">
            
          <div class="col-md-12">
            <div class="form-group">
              <label>Booking Type <i class="required">*</i></label>
              <select class="form-control input-sm" ng-model="newReservation.Reservation.bookingSource" data-validation-engine="validate[required]">
                <option value=""></option>
                <option value="Reservation">Reservation</option>
                <option value="Walk In">Walk In</option>
                 <option value="Travel Agency(TVA)">Travel Agency(TVA)</option>
              </select>
            </div>
          </div>  

          <div class="col-md-12">
            <div class="form-group">
              <label>Company</label>
              <!-- {{ l }}<span ng-repeat="items in searchItems">{{ items }}</span> -->
              <!-- <input type="text" class="form-control input-sm "> -->
              <input id="searchFiled_TXT" class="input form-control " ng-keydown="autoCompleteInput()" ng-model="newReservation.Company.name"/>
        <!--       <select selectize ng-options="department.name as department.name for department in departments" data-validation-engine="validate[required]" ng-model="data.CustomerLog.department" id="department">
                  <option value=""></option>
                </select> -->
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Last Name <i class="required">*</i></label>
              <input type="text" class="form-control input-sm " ng-model="newReservation.Reservation.lastName" data-validation-engine="validate[required]">
            </div>  
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>First Name <i class="required">*</i></label>
              <input type="text" class="form-control input-sm " ng-model="newReservation.Reservation.firstName" data-validation-engine="validate[required]">
            </div>  
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Contact #</label>
              <input type="text" class="form-control input-sm" ng-model="newReservation.Reservation.mobile">
            </div>
          </div>
          
        </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">      
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CANCEL</button>
          <button type="button" id="saveReservation" class="btn btn-primary btn-sm btn-min" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
        </div>  
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/jquery-ui-autocomplete/jquery-ui.min.js"></script>