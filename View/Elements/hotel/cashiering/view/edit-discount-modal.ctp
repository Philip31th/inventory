<!-- add charges -->
<div class="modal fade" id="add-discount-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">ADD DISCOUNT</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
              <tr>
                <th colspan="4" class="bg-blue text-left">DISCOUNTS</th>
              </tr>
              <tr>
                <th class="w30px"></th>
                <th>NAME</th>
                <th class="w120px">AMOUNT</th>
                <th class="w120px text-left">TYPE</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="item in discountSelection">
                <td><input icheck type="checkbox" ng-init="item.selected = false" ng-model="item.selected"></td>
                <td>{{ item.name }}</td>
                <td class="text-right">{{ item.value | number:2 }}</td>
                <td class="text-left">
                  <span ng-if="item.type == 'fixed'">PESOS</span>
                  <span ng-if="item.type == 'percent'">PERCENT</span>
                </td>
              </tr>
            </tbody>
          </table>    
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal">CLOSE</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveDiscount()">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- .add charges -->
