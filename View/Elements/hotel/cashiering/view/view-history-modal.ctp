<!-- view history -->
<div class="modal fade" id="view-history-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">VIEW HISTORY</h4>
      </div>

      <div class="modal-body">
        <div class="row">  
          <div class="col-md-12">
            <dl class="dl-horizontal dl-bordered">
              <dt>Guest Name:</dt>
              <dd>{{ history.GuestName }}</dd>
            </dl>
          </div>

          <!-- CHARGES -->
          <div class="col-md-12" >
            <table class="table table-bordered table-striped table-hover vcenter center">
              <thead>
                <tr>
                  <th class="bg-info text-left" colspan="8">CHARGES</th>
                </tr>
                <tr>
                  <th class="text-center w30px">#</th>
                  <th class="text-right">FOLIO #</th>
                  <th class="text-right">DATE</th>
                  <th class="text-right">BUSINESS</th>
                  <th class="text-center">PARTICULARS</th>
                  <th class="text-right">AMOUNT PAID</th>
                  <th class="text-right">BALANCE</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="transactions in history.Folio" ng-if="!transactions.paidByParent">
                  <td>{{ $index + 1 }}</td>
                  <td class="text-right">{{ transactions.code }}</td>
                  <td class="text-right">{{ transactions.date }}</td>
                  <td class="text-right">{{ transactions.business }}</td>
                  <td class="text-center">{{ transactions.particulars }}</td>
                  <td class="text-right">{{ transactions.totalPayment | number: 2 }}</td>
                  <td class="text-right">{{ transactions.totalBalance | number: 2 }}</td>
                </tr>
              </tbody>
            </table>
          </div>
             
             
          <!-- RECEIVED CHARGES -->
          <div class="col-md-12">
            <table class="table table-bordered table-striped table-hover vcenter center">
              <thead>
                <tr>
                  <th class="bg-info text-left" colspan="8">RECEIVED CHARGES</th>
                </tr>
                <tr>
                  <th class="text-center w30px">#</th>
                  <th class="text-right">FOLIO #</th>
                  <th class="text-right">SENDER FOLIO #</th>
                  <th class="text-right">DATE</th>
                  <th class="text-right">BUSINESS</th>
                  <th class="text-center">PARTICULARS</th>
                  <th class="text-right w100px">AMOUNT PAID</th>
                  <th class="text-right w100px">BALANCE</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="rtransaction in received">
                  <td class="text-center">{{  $index + 1 }}</td>
                  <td class="text-right">{{ rtransaction.folioId }}</td>
                  <td class="text-right">{{ rtransaction.senderFolio }}</td>  
                  <td class="text-right">{{ rtransaction.date }}</td>
                  <td class="text-right">{{  rtransaction.business }}</td>
                  <td class="text-center">{{ rtransaction.particulars }}</td>
                  <td class="text-right">{{ rtransaction.totalAmount   | number: 2 }}</td>
                  <td class="text-right">{{ rtransaction.totalBalance  | number: 2 }}</td>
                </tr>    
              </tbody> 
            </table>
          </div>
          
          <!-- TRANSFERED CHARGES -->
          <div class="col-md-12">
            <table class="table table-bordered table-striped table-hover vcenter center">
              <thead>
                <tr>
                  <th class="bg-info text-left" colspan="10">TRANSFERED CHARGES</th>
                </tr>
                <tr>
                  <th class="text-center w30px">#</th>
                  <th class="text-right">FOLIO #</th>
                  <th class="text-right">DATE</th>
                  <th class="text-right">BUSINESS</th>
                  <th class="text-center">PARTICULARS</th>
                  <th class="text-right w100px">AMOUNT PAID</th>
                  <th class="text-right w100px">BALANCE</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="transactions in history.Folio" ng-if="transactions.paidByParent">
                  <td>{{ $index + 1 }}</td>
                  <td class="text-right">{{ transactions.parentFolio }}</td>
                  <td class="text-right">{{ transactions.date }}</td>
                  <td class="text-right">{{ transactions.business }}</td>
                  <td class="text-center">{{ transactions.particulars }}</td>
                  <td class="text-right">{{ transactions.totalPayment | number: 2 }}</td>
                  <td class="text-right">{{ transactions.totalBalance | number: 2 }}</td>
                </tr>
              </tbody> 
            </table>
          </div>
              
          <div class="cleafix"></div>
          <hr>

        </div>
      </div>    
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-times"></i> CLOSE</button>
        </div>

        <div class="btn-group btn-group-sm pull-left">
          <!-- <button type="button" class="btn btn-danger btn-min"><i class="fa fa-print"></i> PRINT</button> -->
        </div>
      </div>
    </div>  
  </div>
</div>
<!-- .view history -->
