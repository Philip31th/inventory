<!-- add charges -->
<div class="modal fade" id="add-charges-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">ADD CHARGES</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>DATE</label>
              <input date type="text" class="form-control" ng-model="transactionDate">
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              <label>TYPE</label>
              <select class="form-control" ng-init="chargeType = 'service'" ng-model="chargeType">
                <option value="service">SERVICES</option>
                <option value="penalty">PENALTIES</option>
              </select>      
            </div>
          </div>
        </div>
        
        <div class="clearfix"></div>
        <hr>
        
        <div class="row">
          <!-- services -->
          <div class="col-md-12" ng-if="chargeType == 'service'">
            <table class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th class="w30px"></th>
                  <th>BUSINESS</th>
                  <th>NAME</th>
                  <th class="w120px text-right">VALUE</th>
                  <th>TYPE</th>
                  <th class="text-right">QTY.</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="service in businessServices">
                  <td><input icheck type="checkbox" ng-init="service.selected = false" ng-model="service.selected" ng-change="addRemoveCharge(charge)"></td>
                  <td class="uppercase">{{ service.business }}</td>
                  <td class="uppercase">{{ service.name }}</td>
                  <td class="text-right">{{ service.value | number:2 }}</td>
                  <td>
                    <span ng-if="service.type == 'fixed'">PESOS</span>
                    <span ng-if="service.type == 'percent'">PERCENT</span>
                  </td>
                  <td class="w60px">
                    <input number type="text" class="form-control text-right" ng-init="service.quantity = 1" ng-model="service.quantity">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- services -->
          
          <!-- penalties -->
          <div class="col-md-12" ng-if="chargeType == 'penalty'">
            <table class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th class="w30px"></th>
                  <th>NAME</th>
                  <th class="w120px text-right">VALUE</th>
                  <th>TYPE</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="penalty in penalties">
                  <td><input icheck type="checkbox" ng-init="penalty.selected = false" ng-model="penalty.selected" ng-change="addRemoveCharge(charge)"></td>
                  <td class="uppercase">{{ penalty.name }}</td>
                  <td class="text-right">{{ penalty.value | number:2 }}</td>
                  <td>
                    <span ng-if="penalty.type == 'fixed'">PESOS</span>
                    <span ng-if="penalty.type == 'percent'">PERCENT</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- penalties -->
          
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-times"></i> CLOSE</button>
          <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveCharges()"><i class="fa fa-save"></i> SAVE</button>
        </div>  
      </div>
    </div>
  </div>
</div>
<!-- .add charges -->
