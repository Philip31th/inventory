<!-- add payment -->
<div class="modal fade" id="transfer-bill-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">TRANSFER BILLS</h4>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-md-12">
              <div class="form-group">
                <label>Folio # </label>
                <select class="form-control" ng-model="parentId" ng-options="opt.code as opt.value for opt in folio" data-validation-engine="validate[required]">
                  <option value="">Select Folio</option>
                </select>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-times"></i> CLOSE</button>
          <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveTransferBills()"><i class="fa fa-save"></i> SAVE</button>
        </div>  
      </div>
    </div>
  </div>
</div>
<!-- .add payment -->