<!-- add payment -->
<div class="modal fade" id="add-payment-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">{{ title }} PAYMENT</h4>
      </div>
      <form id="form">
      <div class="modal-body">
        <div class="row">
          <input type="hidden" ng-model="payment.folioCode">
          <div class="col-md-12">
            <div class="form-group">
              <label>Transaction Balance: </label> {{ amountToPay | number:2 }}
            </div>
          </div>

            <div class="col-md-12">
              <div class="form-group">
                <label>Payment Type <i class="required">*</i></label>
                <select class="form-control" id="paymentType" ng-model="payment.TransactionPayment.paymentType" ng-options="opt.id as opt.value for opt in paymentTypes" data-validation-engine="validate[required]" ng-change="paymentType()">
                  <option value="">Select Payment Type</option>
                </select>
              </div>
            </div>

            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType != 'send bill' && payment.TransactionPayment.paymentType != null">
              <div class="form-group">
                <label>O.R # <i class="required">*</i></label> <i class="fa fa-times-circle text-danger italic pull-right ng-hide" ng-show="orNumberExisting&&!notexisted&&!checking"> Already existed!</i> <i class="italic pull-right text-primary" ng-show="checking&&!notexisted&&!orNumberExisting">Checking</i><i class="fa fa-check-circle pull-right italic text-success" ng-show="notexisted&&!checking&&!orNumberExisting"></i>

                <input type="text" class="form-control" ng-model="payment.TransactionPayment.orNumber" ng-keyup="checkOR(payment.TransactionPayment.orNumber,business)" id="orNumber" data-validation-engine="validate[required]">
              </div>
            </div>
            
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType != null ">
              <div class="form-group">
                <label>Amount <i class="required">*</i></label>
                <input amount type="text" class="form-control" id="amount" ng-model="payment.TransactionPayment.amount"  data-validation-engine="validate[required]">
              </div>
            </div>
            
            <!-- credit card -->
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
              <div class="form-group">
                <label>Account Name</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.accountName">
              </div>
            </div>
            
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
              <div class="form-group">
                <label>Card Number</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.cardNumber">
              </div>
            </div>
            
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
              <div class="form-group">
                <label>Expiration Year</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.expirationYear">
              </div>
            </div>
            
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
              <div class="form-group">
                <label>Expiration Month</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.expirationMonth">
              </div>
            </div>
            
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
              <div class="form-group">
                <label>Bank Name</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.bankName">
              </div>
            </div>
            <!-- credit card -->
            
            <!-- cheque -->
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cheque'">
              <div class="form-group">
                <label>Account Name</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.accountName">
              </div>
            </div>
            
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cheque'">
              <div class="form-group">
                <label>Account Number</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.accountNumber">
              </div>
            </div>
            
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cheque'">
              <div class="form-group">
                <label>Cheque Number</label>
                <input type="text" class="form-control" ng-model="payment.TransactionPayment.chequeNumber">
              </div>
            </div>
            <!-- cheque -->
            
            <!-- cash -->
            <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cash'">
              <div class="form-group">
                <label>Change</label>
                <input disabled type="text" class="form-control" id="change" value="{{change(payment.TransactionPayment.amount,amountToPay)}}">
              </div>
            </div>
            <!-- cash -->
          
          </div> <!--  end row -->  
      </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">

          <button type="button" class="btn btn-danger btn-sm btn-min" id="close" data-dismiss="modal"><i class="fa fa-times"></i> CLOSE</button>
          
          <button type="button" class="btn btn-primary btn-sm btn-min {{ payment.TransactionPayment.paymentType == null? 'disabled':''}}" id="savePayment" ng-click="savePayment(payment.TransactionPayment)"><i class="fa fa-save"></i> SAVE</button>

<!--           <button type="button" ng-if="pos&&title=='ADD'" class="btn btn-primary btn-sm btn-min" ng-click="savePayment(payment.TransactionPayment)"><i class="fa fa-save"></i> SAVE</button> -->

        </div>  
      </div>
        </form>
    </div>
  </div>
</div>
<!-- .add payment