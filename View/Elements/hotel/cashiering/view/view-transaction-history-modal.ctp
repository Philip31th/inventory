<!-- view history -->
<div class="modal fade" id="view-transaction-history-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">{{ history.GuestName }} TRANSACTIONS </h4>
      </div>

      <div class="modal-body">
        <div class="row"> 
          <div class="col-md-12">
            <dl class="dl-horizontal dl-bordered">
              <dt>Room:</dt>
              <dd>{{ historyTransactions.Folio.roomType }}</dd>

              <dt>Arrival:</dt>
              <dd>{{ historyTransactions.Folio.arrival }}</dd>

              <dt>Departure:</dt>
              <dd>{{ historyTransactions.Folio.departure }}</dd>
            </dl>
          </div>

        <!-- CHARGES -->
        <div class="col-md-12" >
          <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
              <tr>
                <th class="bg-info text-left" colspan="8">CHARGES</th>
              </tr>
              <tr>
                <th class="text-center w30px">#</th>
                <th>BUSINESS</th>
                <th>PARTICULARS</th>
                <th class="text-right w100px">AMOUNT</th>
                <th class="text-right w100px">DISCOUNT</th>
                <th class="text-right w100px">PAYMENT</th>
                <th class="w30px"></th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="transaction in historyTransactions.Folio.transaction.transactions">
                <td>{{  $index + 1 }}</td>
                <td class="uppercase">{{  transaction.business }}</td>
                <td class="uppercase text-center">{{  transaction.particulars }}</td>
                <th class="text-right">{{ transaction.totalAmount   | number: 2 }}</th>
                <th class="text-right">{{ transaction.totalDiscount | number: 2 }}</th>
                <th class="text-right">{{ transaction.totalPayment  | number: 2 }}</th>
                <td>
                  <div class="btn-group btn-group-xs">
                    <button class="btn btn-success" title="CLICK TO VIEW TRANSACTION" ng-click="viewHistoTranSubs(transaction)"><i class="fa fa-eye"></i></button>
                  </div>
                </td>
              </tr>
            </tbody>
            <tfoot class="bg-info">
              <th colspan="3" class="text-right">TOTAL</th>
              <th class="text-right">{{ historyTransactions.Folio.transaction.grandAmount | number: 2 }}</th>
              <th class="text-right">{{ historyTransactions.Folio.transaction.grandDiscount | number: 2 }}</th>
              <th class="text-right">{{ historyTransactions.Folio.transaction.grandPayment | number: 2 }}</th>
              <td></td>
            </tfoot>
          </table>
        </div>


          <div class="cleafix"></div>
          <hr>

        </div>
      </div>    
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-times"></i> CLOSE</button>
        </div>  
      </div>
    </div>
  </div>
</div>      