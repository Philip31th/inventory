<!-- change discount -->
<div class="modal fade" id="change-discount-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">DISCOUNT</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>DISCOUNT TYPE</label>
              <select class="form-control input-sm" ng-model="ediscount.Transaction.discount_type" ng-options="b.value as b.name for b in bool"></select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>VALUE</label>
              <input class="form-control input-sm" ng-model="ediscount.Transaction.discount">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal">CLOSE</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveDiscount()">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- .change discount -->