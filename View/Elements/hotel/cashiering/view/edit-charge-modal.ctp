<!-- add charges -->
<div class="modal fade" id="add-discount-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">EDIT CHARGE</h4>
      </div>
      <div class="modal-body">
           <div class="col-md-12">
            <div class="form-group">
              <label>Payment Type</label>
              <select class="form-control" ng-model="payment.TransactionPayment.paymentType" ng-options="opt.id as opt.value for opt in paymentTypes">
                <option value="">Select Payment Type</option>
              </select>
            </div>
          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal">CLOSE</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveDiscount()">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- .add charges -->
