<!-- view history -->
<div class="modal fade" id="view-history-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">VIEW HISTORY</h4>
      </div>

      <div class="modal-body">
        <div class="row">  
          <div class="col-md-12">
            <dl class="dl-horizontal dl-bordered">
              <dt>Guest:</dt>
              <dd>{{ history.GuestName }}</dd>
            </dl>
          </div>

          <!-- FOLIOS -->
          <div class="col-md-12" >
            <table class="table table-bordered table-striped table-hover vcenter center">
              <thead>
                <tr>
                  <th class="bg-info text-left" colspan="8">FOLIO HISTORY</th>
                </tr>
                <tr>
                  <th class="text-center w30px">#</th>
                  <th class="text-right">FOLIO #</th>
                  <th class="text-right">ROOM</th>
                  <th class="text-right">ARRIVAL</th>
                  <th class="text-right">DEPARTURE</th>
                  <th class="text-right">NIGHTS</th>
                  <th class="text-right">BOOKING SOURCE</th>
                  <th class="w30px"></th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="folio in history.Folio">
                  <td>{{  $index + 1 }}</td>
                  <td class="text-right">{{ folio.code }}</td>
                  <td class="text-right">{{ folio.roomType }}</td>
                  <td class="text-right">{{ folio.arrival }}</td>
                  <td class="text-right">{{ folio.departure }}</td>
                  <td class="text-right">{{ folio.nights }}</td>
                  <td class="text-right">{{ folio.bookingSource }}</td>
                  <td>
                    <div class="btn-group btn-group-xs">
                       <button class="btn btn-success" title="CLICK TO VIEW TRANSACTIONS HISTORY" ng-click="viewTransactionsHistory(folio.guestId,folio.id)"><i class="fa fa-eye"></i></button>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
             
          <div class="cleafix"></div>
          <hr>

        </div>
      </div>    
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-times"></i> CLOSE</button>
        </div>

        <div class="btn-group btn-group-sm pull-left">
          <!-- <button type="button" class="btn btn-danger btn-min"><i class="fa fa-print"></i> PRINT</button> -->
        </div>
      </div>
    </div>  
  </div>
</div>
<!-- .view history -->
