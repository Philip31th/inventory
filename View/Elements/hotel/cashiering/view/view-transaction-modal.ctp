<!-- view payment -->
<div class="modal fade" id="view-transaction-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">VIEW TRANSACTIONS</h4>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <dl class="dl-horizontal dl-bordered">
              <dt>Folio:</dt>
              <dd>{{ vTransaction.FolioTransaction.Folio.code }}</dd>

              <dt> Guest:</dt>
              <dd>{{ vTransaction.Guest.name  }}</dd>
              
              <dt>Transaction Code:</dt>
              <dd>{{ vTransaction.Transaction.code  }}</dd>
              
              <dt>Particulars:</dt>
              <dd class="propercase">{{ vTransaction.Transaction.particulars  }}</dd>
        
              <dt>Total Charges:</dt>
              <dd>PHP {{ vTransaction.TotalAmount | number: 2 }}</dd>
              
              <dt>Discounts:</dt>
              <dd>PHP {{ vTransaction.TotalDiscount | number: 2 }}</dd>
              
              <dt>Deposit Received:</dt>
              <dd>PHP {{ vTransaction.TotalPayment | number: 2 }}</dd>
              
              <dt>Balance:</dt>
              <dd>PHP {{ totalBalance | number: 2 }}</dd>
              
            </dl>
          </div>
         
          <div class="col-md-12">
            <table class="table table-bordered table-striped table-hover vcenter center">
              <thead>
                <tr>
                  <th colspan="4" class="bg-info text-left">ITEMS</th>
                </tr>
                <tr>
                  <th class="w30px">#</th>
                  <th>PARTICULARS</th>
                  <th class="w120px">AMOUNT</th>
                  <!-- <th class="w60px"></th> -->
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="item in vTransaction.TransactionSub">
                  <td>{{ $index + 1 }}</td>
                  <td class="uppercase text-center">{{ item.particulars }}</td>
                  <td class="text-right">{{ item.amount*item.quantity | number:2 }}</td>
                  <!-- <td>
                    <div class="btn-group btn-group-xs">
                      <button class="btn btn-primary" title="EDIT" ng-click="editCharge();"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                    </div>
                  </td> -->
                </tr>
                </tr>
              </tbody>
              <tfoot class="bg-info">
                <tr>
                  <th colspan="2" class="text-right">TOTAL AMOUNT</th>
                  <th class="text-right">{{ vTransaction.TotalAmount | number:2 }}</th>
                  <!-- <td></td> -->
              </tfoot>
            </table>    
          </div>
          
          <div class="cleafix"></div>
          <hr>
          
          <div class="col-md-12" ng-if="vTransaction.TransactionPayment!=''">
            <table class="table table-bordered table-striped table-hover vcenter center">
              <thead>
                <tr>
                  <th colspan="5" class="bg-info text-left">PAYMENTS</th>
                </tr>
                <tr>
                  <th class="w30px">#</th>
                  <th>O.R #</th>
                  <th>DATE</th>
                  <th class="w120px">AMOUNT</th>
                  <th class="w120px">CHANGE</th>
                  <!-- <th class="w60px"></th> -->
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="item in vTransaction.TransactionPayment">
                  <td>{{ $index + 1 }}</td>
                  <td class="uppercase"> {{ item.orNumber }} </td>
                  <td class="text-right">{{ item.date }}</td>
                  <td class="text-right">{{ item.amount - item.change | number:2 }}</td>
                  <td class="text-right">{{ item.change | number:2 }}</td>
                  <!-- <td>
                    <div class="btn-group btn-group-xs">
                      <button class="btn btn-primary" title="EDIT" ng-click="editPayment();"><i class="fa fa-pencil"></i></button>
                      <button class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                    </div>
                  </td> -->
                </tr>
                </tr>
              </tbody>
              <tfoot class="bg-info">
                <tr>
                  <th colspan="3" class="text-right">TOTAL PAYMENT</th>
                  <th class="text-right">{{ vTransaction.TotalPayment | number:2 }}</th>
                  <th class="text-right">{{ vTransaction.TotalChange | number:2 }}</th>
                  <!-- <td></td> -->
                </tr>
              </tfoot>
            </table>    
          </div>
          
          <div class="cleafix"></div>
          <hr>
          
          <div class="col-md-12" ng-if="vTransaction.TransactionDiscount!=''">
            <table class="table table-bordered table-striped table-hover vcenter center">
              <thead>
                <tr>
                  <th colspan="{{vTransaction.TotalBalance>0? 6 : 5 }}" class="bg-info text-left">DISCOUNTS</th>
                </tr>
                
                <tr>
                  <th class="w30px">#</th>
                  <th>NAME</th>
                  <th>TYPE</th>
                  <th>RATE (%)</th>
                  <th>VALUE</th>
                  <th class="w30px" ng-if="vTransaction.TotalBalance>0"></th>  
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="item in vTransaction.TransactionDiscount">
                  <td>{{ $index + 1 }}</td>
                  <td>{{ item.name }}</td>
                  <td>{{ item.type }}</td>
                  <td>{{ item.rate }}</td>
                  <td>{{ item.amount | number:2 }}</td>
                  <td ng-if="vTransaction.TotalBalance>0">
                    <button class="btn btn-danger btn-xs" title="DELETE" ng-click="removeDiscount(item)"><i class="fa fa-trash"></i></button>
                  </div>
                  </td>
                </tr>
                </tr>
              </tbody>
              <tfoot>
                <tr class="bg-info">
                  <th colspan="{{vTransaction.TotalBalance>0? 4 : 3 }}" class="text-right">TOTAL DISCOUNT</th>
                  <th class="text-center">{{ vTransaction.TotalDiscount | number:2 }}</th>
                  <td></td>
                </tr>
              </tfoot>
            </table>    
          </div>

          <div class="clearfix"></div>
        </div>
      </div>    
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>

          <button type="button" ng-if="!BarRestoOrders&&vTransaction.TotalBalance>0" class="btn btn-primary btn-min" ng-click="addDiscount(vTransaction.Transaction)"><i class="fa fa-plus"></i> ADD DISCOUNT</button>

        </div>
      </div>
    </div>  
  </div>
</div>
<!-- .view payment -->

<?php echo $this->element('hotel/cashiering/view/add-discount-modal') ?>
<?php echo $this->element('hotel/cashiering/view/edit-discount-modal') ?>
<?php echo $this->element('hotel/cashiering/view/edit-charge-modal') ?>
<?php echo $this->element('hotel/cashiering/view/edit-payment-modal') ?>