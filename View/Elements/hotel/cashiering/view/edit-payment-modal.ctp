<!-- add payment -->
<div class="modal fade" id="edit-payment-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">EDIT PAYMENT</h4>
      </div>
      <div class="modal-body">
        <div class="row">

        <input type="hidden" ng-model="payment.TransactionPayment.id">

          <div class="col-md-12">
            <div class="form-group">
              <label>Payment Type</label>
              <select class="form-control" ng-model="payment.TransactionPayment.paymentType" ng-options="opt.id as opt.value for opt in paymentTypes">
                <option value="">Select Payment Type</option>
              </select>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>O.R #</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.orNumber">
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
              <label>Amount</label>
              <input amount type="text" class="form-control" ng-model="payment.TransactionPayment.amount">
            </div>
          </div>
          
          <!-- credit card -->
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
            <div class="form-group">
              <label>Account Name</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.accountName">
            </div>
          </div>
          
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
            <div class="form-group">
              <label>Card Number</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.cardNumber">
            </div>
          </div>
          
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
            <div class="form-group">
              <label>Expiration Year</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.expirationYear">
            </div>
          </div>
          
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
            <div class="form-group">
              <label>Expiration Month</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.expirationMonth">
            </div>
          </div>
          
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'credit card'">
            <div class="form-group">
              <label>Bank Name</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.bankNumber">
            </div>
          </div>
          <!-- credit card -->
          
          <!-- cheque -->
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cheque'">
            <div class="form-group">
              <label>Account Name</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.accountName">
            </div>
          </div>
          
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cheque'">
            <div class="form-group">
              <label>Account Number</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.accountNumber">
            </div>
          </div>
          
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cheque'">
            <div class="form-group">
              <label>Cheque Number</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.chequeNumber">
            </div>
          </div>
          <!-- cheque -->
          
          <!-- cash -->
          <div class="col-md-12" ng-if="payment.TransactionPayment.paymentType == 'cash'">
            <div class="form-group">
              <label>Change</label>
              <input type="text" class="form-control" ng-model="payment.TransactionPayment.change">
            </div>
          </div>
          <!-- cash -->
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal">CLOSE</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="updatePayment()">SAVE</button>
      </div>
    </div>
  </div>
</div>
<!-- .add payment -->