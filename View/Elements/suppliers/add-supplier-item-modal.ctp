<!-- add item -->
<div class="modal fade" id="add-supplier-item-modal">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">{{ title }} SUPPLIER ITEM</h4>
      </div>
      <div class="modal-body">
       <form id="supplier_form">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>NAME <i class="required">*</i></label>
              <input type="text" class="form-control" data-validation-engine="validate[required]" ng-model="supplieritem.name">
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label>PRICE <i class="required">*</i></label>
              <input amount type="text" class="form-control input-sm float-value" ng-model="supplieritem.price" data-validation-engine="validate[required]">
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label>QTY <i class="required">*</i></label>
              <input amount type="text" class="form-control input-sm float-value" ng-model="supplieritem.quantity" data-validation-engine="validate[required]">
            </div>
            </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>DESCRIPTION </label>
              <textarea class="form-control" ng-model="supplieritem.description"></textarea>
            </div>
          </div>
        </div>
       </form> 
      </div>
        <div class="modal-footer">
          <div class="btn-group btn-group-sm pull-right btn-min">
            <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>

            <button type="button" class="btn btn-primary btn-sm btn-min" id="saveSupplierItem" ng-click="saveItem(supplieritem)" ng-if="title=='ADD NEW'"><i class="fa fa-check"></i> OK</button>
            <button type="button" class="btn btn-primary btn-sm btn-min" id="saveSupplierItem" ng-click="updateItem(supplieritem)" ng-if="title=='EDIT'"><i class="fa fa-check"></i> OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .add item -->