<!--add item -->
<div class="modal fade" id="resto-supplies-payment-history-modal">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" id="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">RESTAURANT SUPPLIES PAYMENTS</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-4">
                <select class="form-control" ng-model="filter" id="filter" ng-change="filters()">
                  <option value="">Filter By Date</option>
                  <!-- <option value="date">Date</option> -->
                  <option value="supplier">Supplier Name</option>
                  <option value="both">Both</option>
                </select>
              </div>

              <div class="col-md-8" ng-if="filter=='date' || filter==null || filter==''">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                  <input type="text" class="form-control search" placeholder="SEARCH DATE" ng-model="searchByDate" ng-change="filterPayment('date',searchByDate)" ng-init="date1(searchByDate)" ng-click="date1(searchByDate)" id="searchByDate" data-date-format="mm/dd/yyyy">
                </div> 
              </div>

              <div class="col-md-8" ng-if="filter=='supplier'">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                  <input type="text" class="form-control search" placeholder="SEARCH NAME" ng-model="searchByname" ng-keyup="load2({ code: 'restaurant', searchName: searchByname })" id="paymentDate" data-date-format="mm/dd/yyyy">
                </div> 
              </div>

              <div class="col-md-8" ng-if="filter=='both'">
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                      <input type="text" class="form-control search" placeholder="SEARCH DATE" ng-model="searchDate2" ng-change="filterPayment('both',searchDate2)" ng-init="date2(searchDate2)" ng-click="date2(searchDate2)" id="searchDate2" data-date-format="mm/dd/yyyy">
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                      <input type="text" class="form-control search" placeholder="SEARCH NAME" ng-model="searchByName2" ng-keyup="load2({ code: 'restaurant', searchName: searchByName2 , searchDate: searchDate2 })" id="paymentDate" data-date-format="mm/dd/yyyy">
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="clearfix"></div><hr>

          <div class="col-md-12">
            <table class="table table-bordered table-striped table-hover center">
              <thead>
                <tr>
                  <th>OR # </th>
                  <th>DATETIME</th>
                  <th>SUPPLIER NAME</th>
                  <th>PAYMENT TYPE</th>
                  <th>PAYMENT</th>
                  <th>CHANGE</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="payment in payments">
                  <td class="uppercase">{{ payment.TransactionPayment.orNumber }}</td>
                  <td class="uppercase">{{ payment.TransactionPayment.datetime }}</td>
                  <td class="uppercase">{{ payment.Supplier.name }}</td>
                  <td class="uppercase">{{ payment.TransactionPayment.paymentType }}</td>
                  <th class="uppercase">{{ payment.TransactionPayment.amount | number: 2 }}</th>
                  <th class="uppercase">{{ payment.TransactionPayment.change | number: 2 }}</th>
                </tr>
                <tr ng-if="payments==''">
                  <td colspan="6">No data found.</td>
                </tr>
              </tbody>
              <tfoot>
               <!--  <tr>
                  <th colspan="3" class="text-right">TOTAL</th>
                  <th class="italic">PHP {{ totals.totalPayments | number: 2 }}</th>
                  <th class="italic">PHP {{ totals.totalChange | number: 2 }}</th>
                  <td></td>
                </tr> -->
              </tfoot>
            </table>
          </div>

        </div>  

        <ul class="pagination pull-right">
          <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
          <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'bar',page: page.number, search: searchTxt })">{{ page.number }}</a></li>
          <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
        </ul>
        <div class="clearfix"></div>

      </div>
      <div class="modal-footer">
        <!-- <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>
        </div> -->
      </div>
      </div>
    </div>
  </div>
</div>
<!-- .add item