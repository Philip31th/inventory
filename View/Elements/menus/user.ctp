<div class="user-info">
  <div class="list-group">
    <a href="#" class="list-group-item active">HOME</a>
    <a href="#" class="list-group-item uppercase"><?php echo date('F d, Y') ?></a>
    <a href="#" class="list-group-item"><?php echo $currentUser['User']['firstName'] . ' ' . $currentUser['User']['lastName'] ?></a>
    <a class="list-group-item">
      <div class="user-image">
        <img src="<?php echo $this->base ?>/assets/img/default-user.jpg" class="img-circle img-responsive">
      </div>
    </a>
    <a href="javascript:void(0)" class="list-group-item">Last Login: <?php echo date('F, d Y h:i A', strtotime($this->Session->read('Auth.User.modified'))) ?></a>
  </div>
	<div class="sign-out">
		<div class="col-md-6">
			<a href="<?php echo Router::url(array('controller'=>'main', 'action'=>'lock')) ?>" class="logout">
				<i class="fa fa-lock"></i> LOCK ACCOUNT
			</a>
		</div>
		<div class="col-md-6">
			<a href="<?php echo Router::url(array('controller'=>'main', 'action'=>'logout')) ?>" class="logout">
				<i class="fa fa-sign-out"></i> SIGN OUT
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- <div id="time"><?php echo date('F-d-Y h:i A')?></div> -->
</div>