<div class="header">
  <div class="col-md-3 leftside">
    <div class="header-sub-wrapper">
      <a href="<?php echo Router::url(array('controller'=>'main', 'action'=>'index')) ?>" class="logo-link">
        <div id="logo"><?php echo $this->Application->settings('ehrms') ?></div>
        <div id="title"><?php echo $this->Application->settings('system-title') ?></div> 
      </a>
    </div>
  </div>
  
	<div class="col-md-9 rightside">
		<div class="header-sub-wrapper-2">
			<div class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="system-title"><?php echo $this->Application->settings('front-office-system-title') ?></li>
					</ul>
						
					<ul class="nav navbar-nav navbar-right">
						<?php if (hasAccess('hotel/calendar/index', $currentUser)): ?>
						<li>
							<a href="#/hotel/calendar">
								<div class="menu-icon"><i class="fa fa-calendar"></i></div>
								<div>CALENDAR</div>
							</a>
						</li>
						<?php endif ?>

				
						<li>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><i class="fa fa-building-o"></i></div>
								<div>ROOMS</div>
							</a>
							<ul class="dropdown-menu" role="menu">
							
							 <?php if (hasAccess('hotel/rooms/index', $currentUser)): ?>
								<li><a href="#/hotel/rooms"><i class="fa fa-arrow-circle-right"></i> ROOM MANAGEMENT</a></li>
							 <?php endif ?>

							 <?php if (hasAccess('hotel/room-types/index', $currentUser)): ?>
								<li><a href="#/hotel/room-types"><i class="fa fa-arrow-circle-right"></i> ROOM TYPES</a></li>
							 <?php endif ?>

							 <?php if (hasAccess('hotel/accomodations/index', $currentUser)): ?>
								<li><a href="#/hotel/room-accomodations"><i class="fa fa-arrow-circle-right"></i> ROOM ACCOMODATIONS</a></li>
							 <?php endif ?>

							 <?php if (hasAccess('hotel/rooms/index', $currentUser)): ?>
								<li><a href="#/hotel/rooms/occupied"><i class="fa fa-arrow-circle-right"></i> OCCUPIED ROOMS</a></li>
							 <?php endif ?>

							</ul>
						 </li>
					

						<li>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><i class="fa fa-book"></i></div>
								<div>BOOKINGS</div>
							</a>
							<ul class="dropdown-menu" role="menu">
							<?php if (hasAccess('hotel/bookings/index', $currentUser)): ?>
								<li><a href="#/hotel/bookings"><i class="fa fa-arrow-circle-right"></i> BOOKINGS</a></li>
							<?php endif ?>
							<?php if (hasAccess('hotel/booking/add', $currentUser)): ?>
								<li><a href="#/hotel/booking/add"><i class="fa fa-arrow-circle-right"></i> NEW BOOKING</a></li>
							<?php endif ?>
							</ul>
						</li>

						<li>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><i class="fa fa-male"></i></div>
								<div>GUESTS</div>
							</a>
							<ul class="dropdown-menu" role="menu">
							 <?php if (hasAccess('hotel/guests/index', $currentUser)): ?>
								<li><a href="#/hotel/guests"><i class="fa fa-arrow-circle-right"></i> GUEST LIST</a></li>
							 <?php endif ?>

							 <?php if (hasAccess('hotel/guests/incoming', $currentUser)): ?>
								<li><a href="#/hotel/guests/incoming"><i class="fa fa-arrow-circle-right"></i> INCOMING GUESTS</a></li>
							 <?php endif ?>

							 <?php if (hasAccess('hotel/guests/outgoing', $currentUser)): ?>
								<li><a href="#/hotel/guests/outgoing"><i class="fa fa-arrow-circle-right"></i> OUTGOING GUESTS</a></li>
							 <?php endif ?>
							</ul>
						</li>
						
            <li>
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><i class="fa fa-calculator"></i></div>
								<div>CASHIERING</div>
							</a>
							<ul class="dropdown-menu" role="menu">
								<?php if (hasAccess('hotel/cashiering/index', $currentUser)): ?>
									<li><a href="#/hotel/cashiering"><i class="fa fa-arrow-circle-right"></i> CASHIERING</a></li>
								<?php endif ?>

								<?php if (hasAccess('hotel/transactions/index', $currentUser)): ?>
									<li><a href="#/hotel/transactions"><i class="fa fa-arrow-circle-right"></i> TRANSACTIONS</a></li>
								<?php endif ?>
							</ul>
						</li>

            <li>
							<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><i class="fa fa-bar-chart"></i></div>
								<div>REPORTS</div>
							</a>
							<ul class="dropdown-menu" role="menu">

						 <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
    						<li><a href="#/hotel/reports/cashiering"><i class="fa fa-arrow-circle-right"></i> CASHIERING REPORT </a></li>
    					 <?php endif ?>

    					 <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
    						<li><a href="#/hotel/reports/night-audit"><i class="fa fa-arrow-circle-right"></i> NIGHT AUDIT REPORT </a></li>
    					 <?php endif ?>
    					 
    					 <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
    						<li><a href="#/hotel/reports/city-ledger"><i class="fa fa-arrow-circle-right"></i> CITY LEDGER </a></li>
    					 <?php endif ?>

    					 <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
    						<li><a href="#/hotel/reports/room-blocking"><i class="fa fa-arrow-circle-right"></i> ROOM BLOCKING REPORT </a></li>
    					 <?php endif ?>

    					 <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
    						<li><a href="#/hotel/reports/arrival"><i class="fa fa-arrow-circle-right"></i> ARRIVAL REPORT </a></li>
    					 <?php endif ?>
							</ul>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>