<div class="header">
	<div class="col-md-3 leftside">
		<div class="header-sub-wrapper">
			<a href="<?php echo Router::url(array('controller'=>'main', 'action'=>'resto_invent')) ?>" class="logo-link">
				<div id="logo">eHRMS - DEMO</div>
				<div id="title">Electronic Hotel and Restaurant Management System</div>	
			</a>
		</div>
	</div>

	<div class="col-md-9 rightside">
		<div class="header-sub-wrapper-2">
			<div class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-resto"></span>
						<span class="icon-resto"></span>
						<span class="icon-resto"></span>
					</button>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="system-title">DEMO RESTO</li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
					  <li>
							<a href="#/resto/pos">
								<div class="menu-icon"><i class="fa fa-desktop"></i></div>
								<div>POS</div>
							</a>
						</li>
						<li>
							<a href="#/resto/menus">
								<div class="menu-icon"><i class="fa fa-list"></i></div>
								<div>MENUS</div>
							</a>
						</li>
						<li>
							<a href="#/resto/inventories">
								<div class="menu-icon"><i class="fa fa-list-alt"></i></div>
								<div>INVENTORY</div>
							</a>
						</li>
						<li>
							<a href="#/resto/suppliers">
								<div class="menu-icon"><i class="fa fa-truck"></i></div>
								<div>SUPPLIERS </div>
							</a>
						</li>
						<li>
							<a href="#/resto/tables">
								<div class="menu-icon"><img src="images/table.png" /></div>
								<div>TABLES</div>
							</a>
						</li>
              <li>
               <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
								<div class="menu-icon"><i class="fa fa-money"></i></div>
								<div>CASHIERING</div>
							 </a>
							<ul class="dropdown-menu" role="menu">
								<li><a ui-sref="cashiering"><i class="fa fa-arrow-circle-right"></i> CASHIERING</a></li>
								<li><a href="#/resto/transactions"><i class="fa fa-arrow-circle-right"></i> ALL TRANSACTIONS</a></li>
							</ul>
						</li>
            <li>
							<a ui-sref="reports">
								<div class="menu-icon"><i class="fa fa-file-pdf-o"></i></div>
								<div>REPORTS</div>
							</a>
						</li>
					</ul>

				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>