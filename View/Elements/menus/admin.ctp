<div class="header">
  <div class="col-md-3 leftside">
    <div class="header-sub-wrapper">
      <a href="<?php echo Router::url(array('controller'=>'main', 'action'=>'index')) ?>" class="logo-link">
        <div id="logo"><?php echo $this->Application->settings('ehrms') ?></div>
        <div id="title"><?php echo $this->Application->settings('system-title') ?></div> 
      </a>
    </div>
  </div>

  <div class="col-md-9 rightside">
    <div class="header-sub-wrapper-2">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="system-title"><?php echo $this->Application->settings('admin-system-title') ?></li>
          </ul>
            
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <div class="menu-icon"><i class="fa fa-building-o"></i></div>
                <div>HOTEL</div>
              </a>
              <ul class="dropdown-menu" role="menu">

                <?php if (hasAccess('hotel/rooms/index', $currentUser)): ?>
                  <li><a href="#/hotel/rooms"><i class="fa fa-arrow-circle-right"></i> ROOMS </a></li>
                <?php endif ?>

                <?php if (hasAccess('hotel/room-types/index', $currentUser)): ?>
                  <li><a href="#/hotel/room-types"><i class="fa fa-arrow-circle-right"></i> ROOM TYPES </a></li>
                <?php endif ?>

                <?php if (hasAccess('hotel/room-accomodations/index', $currentUser)): ?>
                  <li><a href="#/hotel/room-accomodations"><i class="fa fa-arrow-circle-right"></i> ACCOMODATIONS </a></li>
                  <li class="divider"></li>
                <?php endif ?>

                <?php if (hasAccess('hotel/calendar/index', $currentUser)): ?>
                  <li><a href="#/hotel/calendar"><i class="fa fa-arrow-circle-right"></i> CALENDAR </a></li>
                <?php endif ?>

                <?php if (hasAccess('hotel/bookings/index', $currentUser)): ?>
                  <li><a href="#/hotel/bookings"><i class="fa fa-arrow-circle-right"></i> BOOKINGS </a></li>
                <?php endif ?>

                <?php if (hasAccess('hotel/guests/index', $currentUser)): ?>
                  <li><a href="#/hotel/guests"><i class="fa fa-arrow-circle-right"></i> GUESTS </a></li>
                  <li class="divider"></li>
                <?php endif ?>
                
                <?php if (hasAccess('hotel/cashiering/index', $currentUser)): ?>
                  <li><a href="#/hotel/cashiering"><i class="fa fa-arrow-circle-right"></i> CASHIERING </a></li>
                  <li class="divider"></li>
                <?php endif ?>

                <!-- <?php if (hasAccess('hotel/transactions/index', $currentUser)): ?> -->
                <!--   <li><a href="#/hotel/transactions"><i class="fa fa-arrow-circle-right"></i> TRANSACTIONS </a></li>
                  <li class="divider"></li> -->
                <!-- <?php endif ?> -->
                
                <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
                  <li><a href="#/hotel/reports/night-audit"><i class="fa fa-arrow-circle-right"></i> NIGHT AUDIT REPORT </a></li>
                <?php endif ?>
                
                <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
                  <li><a href="#/hotel/reports/city-ledger"><i class="fa fa-arrow-circle-right"></i> CITY LEDGER </a></li>
                <?php endif ?>
                
                <?php if (hasAccess('hotel/reports/index', $currentUser)): ?>
                  <li><a href="#/hotel/reports/cashiering"><i class="fa fa-arrow-circle-right"></i> CASHIERING REPORT </a></li>
                <?php endif ?>  
              </ul>
            </li>
          
            <li>
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <div class="menu-icon"><i class="fa fa-cutlery"></i></div>
                <div>RESTO</div>
              </a>
              <ul class="dropdown-menu" role="menu">
                <?php if (hasAccess('resto/menus/index', $currentUser)): ?>
                  <li><a href="#/resto/menus"><i class="fa fa-arrow-circle-right"></i> MENUS </a></li>
                <?php endif ?>

                <?php if (hasAccess('resto/tables/index', $currentUser)): ?>
                  <li><a href="#/resto/tables"><i class="fa fa-arrow-circle-right"></i> TABLES </a></li>
                <?php endif ?>

                <?php if (hasAccess('resto/inventories/index', $currentUser)): ?>
                  <li><a href="#/resto/inventories"><i class="fa fa-arrow-circle-right"></i> INVENTORIES </a></li>
                <?php endif ?>

                <?php if (hasAccess('resto/suppliers/index', $currentUser)): ?>
                  <li><a href="#/resto/suppliers"><i class="fa fa-arrow-circle-right"></i> SUPPLIERS </a></li>
                <?php endif ?>

                <!-- <?php if (hasAccess('resto/transactions/index', $currentUser)): ?> -->
                  <!-- <li><a href="#/resto/transactions"><i class="fa fa-arrow-circle-right"></i> TRANSACTIONS </a></li> -->
                <!-- <?php endif ?> -->
                
                <?php if (hasAccess('resto/pos/index', $currentUser)): ?>
                  <li><a href="#/resto/pos"><i class="fa fa-arrow-circle-right"></i> POS </a></li>
                  <li class="divider"></li>
                <?php endif ?>

                <?php if (hasAccess('resto/reports/index', $currentUser)): ?>
                  <li><a href="#/resto/reports/cashiering"><i class="fa fa-arrow-circle-right"></i> CASHIERING REPORT </a></li>
                <?php endif ?> 

                <?php if (hasAccess('bar/reports/index', $currentUser)): ?>
                  <li><a href="#/resto/reports/suppliers"><i class="fa fa-arrow-circle-right"></i> SUPPLIERS PAYABLES </a></li>
                <?php endif ?>

                <?php if (hasAccess('bar/reports/index', $currentUser)): ?>
                  <li><a href="#/resto/reports/receivables"><i class="fa fa-arrow-circle-right"></i> RECEIVABLES</a></li>
                <?php endif ?>
              </ul>
            </li>

            <li>
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <div class="menu-icon"><i class="fa fa-glass"></i></div>
                <div>BAR</div>
              </a>
              <ul class="dropdown-menu" role="menu">
                <?php if (hasAccess('bar/menus/index', $currentUser)): ?>
                  <li><a href="#/bar/menus"><i class="fa fa-arrow-circle-right"></i> MENUS </a></li>
                <?php endif ?>

                <?php if (hasAccess('bar/tables/index', $currentUser)): ?>
                  <li><a href="#/bar/tables"><i class="fa fa-arrow-circle-right"></i> TABLES </a></li>
                <?php endif ?>

                <?php if (hasAccess('bar/inventories/index', $currentUser)): ?>
                  <li><a href="#/bar/inventories"><i class="fa fa-arrow-circle-right"></i> INVENTORIES </a></li>
                <?php endif ?>

                <?php if (hasAccess('bar/suppliers/index', $currentUser)): ?>
                  <li><a href="#/bar/suppliers"><i class="fa fa-arrow-circle-right"></i> SUPPLIERS </a></li>
                <?php endif ?>

                <!-- <?php if (hasAccess('bar/transactions/index', $currentUser)): ?> -->
                  <!-- <li><a href="#/bar/transactions"><i class="fa fa-arrow-circle-right"></i> TRANSACTIONS </a></li> -->
                <!-- <?php endif ?> -->
                
                <?php if (hasAccess('bar/pos/index', $currentUser)): ?>
                  <li><a href="#/bar/pos"><i class="fa fa-arrow-circle-right"></i> POS </a></li>
                  <li class="divider"></li>
                <?php endif ?>

                <?php if (hasAccess('bar/reports/index', $currentUser)): ?>
                  <li><a href="#/bar/reports/cashiering"><i class="fa fa-arrow-circle-right"></i> CASHIERING REPORT </a></li>
                <?php endif ?>  

                <?php if (hasAccess('bar/reports/index', $currentUser)): ?>
                  <li><a href="#/bar/reports/suppliers"><i class="fa fa-arrow-circle-right"></i> SUPPLIERS PAYABLES </a></li>
                <?php endif ?>

                <?php if (hasAccess('bar/reports/index', $currentUser)): ?>
                  <li><a href="#/bar/reports/receivables"><i class="fa fa-arrow-circle-right"></i> RECEIVABLES</a></li>
                <?php endif ?>

              </ul>
            </li>

            <li>
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <div class="menu-icon"><i class="fa fa-users"></i></div>
                <div>HR</div>
              </a>
              <ul class="dropdown-menu" role="menu">
                <?php if (hasAccess('hr/employees/index', $currentUser)): ?>
                  <li><a href="#/employees"><i class="fa fa-arrow-circle-right"></i> EMPLOYEES </a></li>
                <?php endif ?>

                <?php if (hasAccess('hr/daily-time-records/index', $currentUser)): ?>
                  <li><a href="#/daily-time-records"><i class="fa fa-arrow-circle-right"></i> DAILY TIME RECORD </a></li>
                <?php endif ?>
              </ul>
            </li>
            
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <div class="menu-icon"><i class="fa fa-calculator"></i></div>
                <div>ACCOUNTING</div>
              </a>
              <ul class="dropdown-menu" role="menu">
                <?php if (hasAccess('admin/settings/index', $currentUser)): ?>
                  <li><a href="#/accounting/statement-of-accounts"><i class="fa fa-arrow-circle-right"></i> STATEMENT OF ACCOUNT </a></li>
                <?php endif ?>
              </ul>
            </li>
            
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <div class="menu-icon"><i class="fa fa-cog"></i></div>
                <div>SETTINGS</div>
              </a>
              <ul class="dropdown-menu" role="menu">
                <?php if (hasAccess('admin/business/index', $currentUser)): ?>
                  <li><a href="#/business"><i class="fa fa-arrow-circle-right"></i> BUSINESS </a></li>
                <?php endif ?>

                <?php if (hasAccess('admin/companies/index', $currentUser)): ?>
                  <li><a href="#/companies"><i class="fa fa-arrow-circle-right"></i> COMPANY </a></li>
                <?php endif ?>

                <?php if (hasAccess('admin/users/index', $currentUser)): ?>
                  <li><a href="#/users"><i class="fa fa-arrow-circle-right"></i> USERS </a></li>
                <?php endif ?>

                <?php if ($currentUser['User']['developer']): ?>
                  <li><a href="#/permissions"><i class="fa fa-arrow-circle-right"></i> PERMISSION </a></li>
                <?php endif ?>

                <?php if (hasAccess('admin/penalties/index', $currentUser)): ?>
                  <li><a href="#/penalties"><i class="fa fa-arrow-circle-right"></i> PENALTY </a></li>
                <?php endif ?>

                <?php if (hasAccess('admin/services/index', $currentUser)): ?>
                  <li><a href="#/services"><i class="fa fa-arrow-circle-right"></i> SERVICES </a></li>
                <?php endif ?>
                
                <?php if (hasAccess('admin/discounts/index', $currentUser)): ?>
                  <li><a href="#/discounts"><i class="fa fa-arrow-circle-right"></i> DISCOUNT </a></li>
                <?php endif ?>
                
                <?php if (hasAccess('admin/logs/index', $currentUser)): ?>
                  <li><a href="#/logs"><i class="fa fa-arrow-circle-right"></i> LOGS </a></li>
                <?php endif ?>
                
                
                <li><a href="#/messages"><i class="fa fa-arrow-circle-right"></i> MESSAGES </a></li>
               
                <?php if (hasAccess('admin/settings/index', $currentUser)): ?>
                  <li><a href="#/settings"><i class="fa fa-arrow-circle-right"></i> SYSTEM SETTINGS </a></li>
                <?php endif ?>
              </ul>
            </li>

            <?php if ($currentUser['User']['developer']): ?>
            <li>
              <a href="#/tasks">
                <div class="menu-icon"><i class="fa fa-tasks"></i></div>
                <div>TASKS</div>
              </a>
            </li>
            <?php endif ?>
            
            <!-- LOG OUT -->
             <!-- <li>
              <a href="<?php echo Router::url(array('controller'=>'main', 'action'=>'logout')) ?>">
                <div class="menu-icon"><i class="fa fa-sign-out"></i></div>
                <div>LOGOUT</div>
              </a>
            </li>  -->
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>