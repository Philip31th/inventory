<!-- SCRIPTS UPLOADIFY --> 
<link rel="stylesheet" href="<?php echo $this->base?>/assets/plugins/uploadifive/css/uploadify.css"> 
<script type="text/javascript" src="<?php echo $this->base?>/assets/plugins/uploadifive/jquery.uploadify.min.js"></script>

<script>
$(document).ready(function() {

	setTimeout(function() {
	  jQuery('#file_upload').uploadify({
			'formData' : {
			  'path'					:'<?php echo rtrim($this->base,"/") ?>',
			  'filename'			:'sample'
			},
			'multi'    				: false,
			'buttonClass'			: 'btn-success',
			'buttonText'      : '<i class="fa fa-upload"></i>&emsp;CHOOSE IMAGE',
			'queueSizeLimit' 	: 1,
			'uploadLimit' 		: 5,
			'auto'     				: true,
			'fileSizeLimit' 	: '5MB',
		 	'onFallback' 			: function() {
				alert('Flash was not detected.');
		  },
			'removeCompleted' : true,
			'fileTypeExts' 		: '*.gif; *.jpg; *.jpeg; *.png; *.bitmap;',
			'removeTimeout'   : 2, 
			// 'checkExisting'  	: '<?php echo $this->base?>/assets/plugins/uploadifive/check-exists.php',
			'swf'      				: '<?php echo $this->base?>/assets/plugins/uploadifive/uploadify.swf',
			'uploader' 				: '<?php echo $this->base?>/assets/plugins/uploadifive/uploadify.php',
			'onUploadSuccess' : function(file, data, response) {    
				$('#preview').html("<img class='img-responsive img-user' src='<?php echo $this->base?>/webroot/uploads/" + data + "'>");
				$('#file_upload').uploadify('disable',true);
				$('#delete_img').show();
			},
		});
	});

});   
</script>