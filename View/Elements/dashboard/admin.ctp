<div>
<div class="row">
    <div class="col-md-12">
      
      <div class="row">
        <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="#/hotel/booking/book/new">
              <div class="stats-icon"><i class="fa fa-plus"></i></div>
              <div class="stats-label">ADD RESERVATION</div>
            </a>
          </div>
        </div>
        
        <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="#/hotel/rooms">
              <div class="stats-icon"><span>{{ data.availableRooms }}</span></div>
              <div class="stats-label">AVAILABLE ROOMS</div>
            </a>
          </div>
        </div>
        
        <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="#/hotel/guests/incoming">
              <div class="stats-icon"><span>{{ data.incomingGuests }}</span></div>
              <div class="stats-label">INCOMING GUEST</div>
            </a>
          </div>
        </div>
        
         <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="#/hotel/guests/outgoing">
              <div class="stats-icon"><span>{{ data.outgoingGuests }}</span></div>
              <div class="stats-label">OUTGOING GUEST</div>
            </a>
          </div>
        </div>
        
        <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="#/hotel/rooms/occupied">
              <div class="stats-icon"><span>{{ data.occupiedRooms + data.outgoingGuests }}</span></div>
              <div class="stats-label">IN-HOUSE GUEST</div>
            </a>
          </div>
        </div>

        <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="#/hotel/calendar">
              <div class="stats-icon"><span>{{ data.uncleanedRooms }}</span></div>
              <div class="stats-label">UNCLEANED ROOMS</div>
            </a>
          </div>
        </div>

        <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="javascript:void(0)">
              <div class="stats-icon"><span>{{ data.occupancyRate }}</span></div>
              <div class="stats-label">OCCUPANCY RATE</div>
            </a>
          </div>
        </div>
      </div>
      
      <h3 class="page-title" style="font-weight:100">IN-HOUSE GUEST</h3>
      <div class="row">
        <div class="col-md-3 image" ng-repeat="occupied in occupieds">
          <div class="stats bg-primary">
            <a href="#/hotel/cashiering/view/{{ occupied.id }}">
              <div class="stats-icon"><span class="fa fa-user"></span></div>
              <div class="stats-label uppercase">
                <div>{{ occupied.guest }}</div>
                <div>RM. {{ occupied.room }} - <small>{{ occupied.type }}</small></div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-3 image" ng-repeat="out in outs">
          <div class="stats bg-primary">
            <a href="#/hotel/cashiering/view/{{ out.id }}">
              <div class="stats-icon"><span class="fa fa-user"></span></div>
              <div class="stats-label uppercase">
                <div>{{ out.guest }}</div>
                <div>RM. {{ out.room }} - <small>{{ out.type }}</small></div>
              </div>
            </a>
          </div>
        </div>
      </div>
      
    </div>
  </div>

</div>