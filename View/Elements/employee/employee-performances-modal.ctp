<!-- view performances -->
<div class="modal fade" id="employee-performances-modal">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">EMPLOYEE PERFORMANCES</h4>
      </div>
     
      <div class="modal-body">
        <table class="table table-bordered vcenter">
            
             <thead class="bg-info">
               <tr>
                  <th class="text-center w180px">DATE</th>
                  <th class="text-center">PERFORMANCES</th>
                  <th class="w70px"></th>
               </tr>
             </thead>
             <tbody>
             <tr ng-repeat="performances in vperformances.EmployeePerformance">
                <td class="text-center">{{ performances.date }}</td>
                <td class="text-center">{{ performances.remarks }}</td>
                <td class="text-center">
                  <div class="btn-group btn-group-xs">
      		          <button type="button" ng-click="editRemarks(performances)"  class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></button>
                    <button href="javascript:void(0)" ng-click="removeperformance(performances)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
    		         </div>
                </td>
		         
		         </tbody>
             
             </tr>
        </table>
      </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>
          <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="addRemarks()"><i class="fa fa-plus"></i> ADD PERFORMANCE</button>
        </div> 
      </div>
        </div>
      </div>
    </div>
<!-- view performances -->