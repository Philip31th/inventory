<!-- view performances -->
<div class="modal fade" id="add-leaves-modal">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">{{ title }} EMPLOYEE LEAVE</h4>
      </div>
          <div class="modal-body">
          
         <div class="col-md-12">
            <div class="form-group">
              <label>Leave Type<i class="required">*</i></label>
              <input type="text" class="form-control" ng-model="aleave.leaveType" data-validation-engine="validate[required]">
            </div>
          </div>
            
          <div class="col-md-12">  
            <div class="form-group">
              <label>Start Date<i class="required">*</i></label>
              <input type="text" class="form-control input-sm" id="start" ng-model="aleave.startDate" data-date-format="mm/dd/yyyy" data-validation-engine="validate[required]">
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="form-group">
            <label>End Date<i class="required">*</i></label>
              <input type="text" class="form-control input-sm" id="end" ng-model="aleave.endDate" data-date-format="mm/dd/yyyy" data-validation-engine="validate[required]">
            </div>
          </div>
            
          <div class="col-md-12">
            <div class="form-group">
              <label>Purpose<i class="required">*</i></label>
              <textarea type="text" class="form-control" ng-model="aleave.purpose" data-validation-engine="validate[required]"></textarea>
            </div>
          </div>

          </div>
          <div class="modal-footer">
            <div class="btn-group btn-group-sm pull-right btn-min">
              <button type="button" ng-if="title=='ADD'" class="btn btn-primary btn-sm btn-min" ng-click="saveLeave()"><i class="fa fa-save"></i> SAVE</button>
              
              <button type="button" ng-if="title=='EDIT'" class="btn btn-primary btn-sm btn-min" ng-click="updateLeave()"><i class="fa fa-save"></i> UPDATE</button>
            </div> 
        </div>
      </div>
    </div>
<!-- view performances -->