<!-- view performances -->
<div class="modal fade" id="view-leaves-modal">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">EMPLOYEE LEAVES</h4>
      </div>
     
          <div class="modal-body">
            <table class="table table-bordered vcenter">
                 <thead class="bg-info">
                   <tr>
                       <th>#</th>
                      <th class="text-center">START DATE</th>
                      <th class="text-center">END DATE</th>
                      <th class="text-center">PURPOSE</th>
                      <th class="w70px"></th>
                   </tr>
                 </thead>
                 <tbody>
                 <tr ng-repeat="leave in vleave.EmployeeLeave">
                    <td>{{ $index+1 }}</td>
                    <td class="text-center">{{ leave.startDate }}</td>
                    <td class="text-center">{{ leave.endDate }}</td>
                    <td class="text-center">{{ leave.purpose }}</td>
                    <td class="text-center">
                      <div class="btn-group btn-group-xs">
                        <button type="button" ng-click="editLeave(leave)"  class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></button>
                        <button ng-click="removeLeave(leave)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                     </div>
                    </td>
                 
                 </tbody>
                 
                 </tr>
            </table>  
          </div>
          <div class="modal-footer">
            <div class="btn-group btn-group-sm pull-right btn-min">
              <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-close"></i> CLOSE</button>
              <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="addLeave(leave)"><i class="fa fa-plus"></i> ADD LEAVE</button>
            </div> 
        </div>
      </div>
    </div>
<!-- view performances -->