<!-- view performances -->
<div class="modal fade" id="employee-performances-edit-modal">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">{{ title }} PERFORMANCE</h4>
      </div>
     
      <div class="modal-body">
        <div class="col-md-12">
          <div class="form-group">
            <label>Performance</label>
            <textarea type="text" class="form-control" ng-model="vdata.vremarks"></textarea>
          </div>
        </div>
     </div>   
     
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" ng-if="title=='ADD'" class="btn btn-primary btn-sm btn-min" ng-click="saveRemarks()"><i class="fa fa-save"></i> SAVE REMARKS</button>
          <button type="button" ng-if="title=='EDIT'" class="btn btn-primary btn-sm btn-min" ng-click="updateremarks()"><i class="fa fa-save"></i> UPDATE REMARKS</button>
        </div>
      </div> 
        </div>
      </div>
    </div>
<!-- view performances -->