<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <style>
    body {
      padding: 15px;
    }
  </style>
</head>
<body>
  
  <div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> STATEMENT OF ACCOUNT</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <td class="w120px bggray">FOLIO #</td>
                <td><?php echo $folio['Folio']['code'] ?></td>
                
                <td class="w120px bggray">ROOM #</td>
                <td class="w120px text-right"><?php echo $folio['ReservationRoom']['Room']['name'] ?></td>
              </tr>
  
              <tr>
                <td class="bggray">COMPANY NAME</td>
                <td class="uppercase"><?php echo $folio['Guest']['company'] ?></td>
                
                <td class="bggray">ROOM TYPE</td>
                <td class="uppercase text-right"><?php echo $folio['ReservationRoom']['Room']['RoomType']['name'] ?></td>
              </tr>
  
              <tr>
                <td class="bggray">GUEST'S NAME</td>
                <td class="uppercase"><?php echo $folio['Guest']['firstName'] . ' ' . $folio['Guest']['lastName'] ?></td>
                
                <td class="bggray">ROOM RATE</td>
                <td class="text-right"><?php $folio['ReservationRoom']['Room']['rate'] ?></td>
              </tr>
  
              <tr>
                <td class="bggray" rowspan="2">ADDRESS</td>
                <td rowspan="2" class="uppercase"><?php ?></td>
                
                <td class="bggray">ARRIVAL</td>
                <td class="text-right">{{ data.arrival }}</td>
              </tr>
  
              <tr>
                <td class="bggray">DEPARTURE</td>
                <td class="text-right">{{ data.departure }}</td>
              </tr>
  
              <tr>
                <td class="bggray">CONTACT #</td>
                <td>{{ data.mobile }}</td>
                
                <td class="bggray">SOURCE</td>
                <td class="text-right uppercase">{{ data.bookingSource }}</td>
              </tr>
  
              <tr>
                <td class="bggray">SPECIAL REQUEST</td>
                <td class="uppercase">
                  {{ data.special_request }}
                </td>
                <td class="bggray">STATUS</td>
                <td class="uppercase text-right">
                  <span class="label label-success" ng-if="data.closed == false">CHECKED IN</span>
                  <span class="label label-danger" ng-if="data.closed">CHECKED OUT</span>
                </td>
              </tr>
  
              <tr>
                <td class="text-right" colspan="3">TAXABLE AMOUNT</td>
                <td class="uppercase text-right">{{ data.Transactions.totalAmount - (data.Transactions.totalAmount * 0.12) | number:2  }}</td>
              </tr>
  
              <tr>
                <td class="text-right" colspan="3">TAX RATE</td>
                <td class="uppercase text-right">1.12</td>
              </tr>
  
              <tr>
                <td class="text-right" colspan="3">VAT</td>
                <td class="uppercase text-right">{{ (data.Transactions.totalAmount * 0.12) | number:2   }}</td>
              </tr>
  
              <tr>
                <td class="text-right" colspan="3">TOTAL CHARGES</td>
                <td class="uppercase text-right">{{ data.Transactions.totalAmount | number:2 }}</td>
              </tr>
  
              <tr>
                <td class="text-right" colspan="3">DEPOSITS RECEIVED</td>
                <td class="uppercase text-right">{{ data.Transactions.totalPayment | number:2  }}</td>
              </tr>
  
              <tr>
                <td class="text-right" colspan="3">BALANCE</td>
                <td class="uppercase text-right">{{ data.Transactions.totalBalance | number:2 }}</td>
              </tr>
            </tbody>
          </table>
        </div>
  
        <div class="col-md-12">
          <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
              <tr>
                <th class="bg-blue text-left" colspan="9">CHARGES</th>
              </tr>
              <tr>
                <th class="text-center w30px">#</th>
                <th>BUSINESS</th>
                <th>PARTICULARS</th>
                <th class="text-right w100px">AMOUNT</th>
                <th class="text-right w100px">DISCOUNT</th>
                <th class="text-right w100px">PAYMENT</th>
                <th class="text-right w100px">BALANCE</th>
                <th class="w120px">STATUS</th>
                <th class="w70px"></th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="transaction in data.Transactions.charges">
                <td>{{  $index + 1 }}</td>
                <td class="uppercase">{{  transaction.business }}</td>
                <td class="uppercase">{{  transaction.particulars }}</td>
                <th class="text-right">{{ transaction.totalAmount   | number: 2 }}</th>
                <th class="text-right">{{ transaction.totalDiscount | number: 2 }}</th>
                <th class="text-right">{{ transaction.totalPayment  | number: 2 }}</th>
                <th class="text-right">{{ transaction.totalBalance  | number: 2 }}</th>
                <td>
                  <span class="label label-success" ng-if="transaction.totalBalance <= 0">TRANSACTION PAID</span>
                  <span class="label label-danger" ng-if="transaction.totalBalance > 0">PENDING PAYMENT</span>
                </td>
                <td>
                  <div class="btn-group btn-group-xs">
                    <a class="btn btn-primary nbr" title="CLICK TO ADD PAYMENT" ng-click="addPayment(transaction)"><i class="fa fa-plus"></i></a>
                    <a class="btn btn-success nbr" title="CLICK TO VIEW TRANSACTION"><i class="fa fa-eye"></i></a>
                  </div>
                  
                </td>
              </tr>
            </tbody>
          </table>
        </div>
  
        <div class="col-md-12">
          <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
              <tr>
                <th class="bg-blue text-left" colspan="9">PAYMENTS</th>
              </tr>
              <tr>
                <th class="text-center w30px">#</th>
                <th>OR #</th>
                <th class="text-right w120px">DATE</th>
                <th>PAYMENT TYPE</th>
                <th class="text-right w120px">AMOUNT</th>
                <th class="w70px"></th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="payments in data.Payments">
                <td>{{  $index + 1 }}</td>
                <td class="uppercase">{{  }}</td>
                <td class="text-right uppercase">{{  }}</td>
                <th class="">{{ }}</th>
                <th class="text-right">{{ charge.totalDiscount | number: 2 }}</th>
                <th class="text-right">{{ charge.totalPayment  | number: 2 }}</th>
                <td>
                  <div class="btn-group btn-group-xs">
                    <a class="btn btn-primary nbr"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-danger nbr"><i class="fa fa-pencil"></i></a>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
  
      <div class="clearfix"></div>
      <hr>
  
      <div class="row">
        <div class="col-md-3 pull-right">
          <button class="btn btn-primary btn-sm btn-block" ng-click="checkOut()"><i class="fa fa-check"></i> CHECK OUT</button>
        </div>
        
        <div class="col-md-3 pull-right">
          <button class="btn btn-primary btn-sm btn-block" ng-click="addCharges()"><i class="fa fa-plus"></i> ADD CHARGES</button>
        </div>
      </div>
    </div>
  </div>
  
</body>
</html>