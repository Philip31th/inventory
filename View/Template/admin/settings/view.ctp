<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW SETTINGS</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name:</dt>
          <dd>{{ data.Setting.name }}</dd>
          
          <dt>Value:</dt>
          <dd>{{ data.Setting.value }}</dd>
        </dl>
      </div>
    </div>

    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-danger btn-sm btn-block" ng-click="remove(data.Setting)"><i class="fa fa-trash"></i> DELETE</button>
      </div>

      <div class="col-md-3 pull-right">
        <a href="#/setting/edit/{{ data.Setting.id }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> EDIT</a>
      </div>
    </div>
  </div>
</div>