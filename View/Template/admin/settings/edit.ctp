<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT SETTINGS</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Setting.name" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Value <i class="required">*</i></label>
            <textarea class="form-control" ng-model="data.Setting.value" data-validation-engine="validate[required]"></textarea>
          </div>
        </div>
      </div>
    </form>

    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="update()"><i class="fa fa-save"></i> SAVE</button>
      </div>
    </div>
  </div>
</div>