<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW PERMISSION</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Name <i class="required">*</i></label>
            <input type="text" class="form-control"  value="can" ng-model="data.Permission.name" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Module <i class="required">*</i></label>
            <!-- <input type="text" class="form-control" ng-model="data.Permission.module" data-validation-engine="validate[required]"> -->
             <select class="form-control" ng-model="data.Permission.module" data-validation-engine="validate[required]">
              <option value="hotel">hotel</option>
              <option value="bar">bar</option>
              <option value="resto">resto</option>
             </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Controller <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Permission.controller" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Action <i class="required">*</i></label>
            <!-- <input type="text" class="form-control" ng-model="data.Permission.action" data-validation-engine="validate[required]"> -->
            <select class="form-control" ng-model="data.Permission.action" data-validation-engine="validate[required]">
              <option value="add">add</option>
              <option value="edit">edit</option>
              <option value="index">index</option>
              <option value="delete">delete</option>
             </select>
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Description</label>
            <textarea type="text" class="form-control" ng-model="data.Permission.description"></textarea>
          </div>
        </div>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="row">
        <div class="col-md-3 pull-right">
          <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fs-save"></i> SAVE</button>
        </div>
      </div>
    </form>
  </div>
</div>