<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> PERMISSION MANAGEMENT</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-3">
        <a href="#/permission/add" class="btn btn-primary btn-sm btn-block modal-form"><i class="fa fa-plus"></i> NEW PERMISSION</a>
      </div>

      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td class="w30px">#</td>
              <th class="uppercase w90px">MODULE</th>
              <th class="">NAME</th>
              <th>CONTROLLER</th>
              <th>ACTION</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="permission in permissions">
              <td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
              <td class="uppercase">{{ permission.module }}</td>
              <td class="uppercase">{{ permission.name }}</td>
              <td class="">{{ permission.controller }}</td>
              <td>{{ permission.action }}</td>
              <td>
                <div class="btn-group btn-group-xs">
                  <a href="#/permission/view/{{ permission.id }}" class="btn btn-success no-border-radius"><i class="fa fa-eye"></i></a>
                  <a href="#/permission/edit/{{ permission.id }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                  <a href="javascript:void(0)" class="btn btn-danger no-border-radius" ng-click="remove(permission)"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>