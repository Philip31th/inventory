<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> DAILY TIME RECORDS </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-3">
        <a href="#/daily-time-records/add" class="btn btn-primary btn-sm btn-block modal-form">
          <i class="fa fa-plus"></i> NEW DTR</a>
      </div>

      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>

      <div class="clearfix"></div>
      <hr>

   		<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover center">
					<thead>
    				  <tr>
        				  <th class="text-center w30px">#</th>
                  <th class="text-center">NAME</th>
                  <th class="text-right">TIME IN</th>
                  <th class="text-right">TIME OUT</th>
                  <th class="text-right">TOTAL HOURS</th>
                  <th class="w90px"></th>
					  </tr>
					</thead>
					<tbody>
    					<tr ng-repeat="data in datas">
                  <td class="text-center">{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
                  <td class="uppercase text-left">{{ data.name }}</td>
                  <td class="text-right uppercase italic">{{ data.timeIn }}</td>
                  <td class="text-right uppercase italic">{{ data.timeOut }}</td>
                  <td class="text-right uppercase italic">{{ data.total_hours }}</td>
                  <td class="text-center">
                    <div class="btn-group btn-group-xs">
                      
                      <?php //if(hasAccess('resto/menus/view', $currentUser)): ?>
                        <a href="#/daily-time-records/view/{{ data.id }}" class="btn btn-success" title="VIEW"><i class="fa fa-eye"></i></a>
                      <?php //endif ?>
                      
                      <?php //if(hasAccess('resto/menus/view', $currentUser)): ?>
                       <!--  <a href="#/daily-time-records/edit/{{ data.id }}" class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></a> -->
                      <?php //endif ?>
                      
                      <?php //if(hasAccess('resto/menus/view', $currentUser)): ?>
                        <a href="javascript:void(0)" ng-click="remove(data)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></a>
                      <?php //endif ?>
                    </div>
                  </td>
              </tr>
					</tbody>
				</table>
   		</div>
  	</div> 
    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>