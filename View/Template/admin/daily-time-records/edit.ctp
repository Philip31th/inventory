<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT DAILY TIME RECORD</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Employee<i class="required">*</i></label>
            <select class="form-control" ng-model="data.DailyTimeRecord.employeeId" ng-options="opt.id as opt.name for opt in employees" data-validation-engine="validate[required]">
              <option value="">SELECT EMPLOYEE</option>
            </select>
          </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
              <label>Time In</label>
                <p class="input-group">
                  <input type="text" class="form-control" datetime-picker="dd MMM yyyy HH:mm:ss" ng-model="data.DailyTimeRecord.timeIn" is-open="open.timeIn" />
                  <span class="input-group-btn">
                      <button type="button" class="btn btn-default" ng-click="openCalendar($event, 'timeIn')"><i class="fa fa-calendar"></i></button>
                  </span>
                </p>
                <!-- {{ data.DailyTimeRecord.timeIn}} -->
              <!-- <input type="time" class="form-control" ng-model="data.DailyTimeRecord.timeIn" > -->
            </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Time Out</label>
              <p class="input-group">
                <input type="text" class="form-control" datetime-picker="dd MMM yyyy HH:mm:ss" ng-model="data.DailyTimeRecord.timeOut" is-open="open.timeOut" />
                <span class="input-group-btn">
                    <button type="button" class="btn btn-default" ng-click="openCalendar($event, 'timeOut')"><i class="fa fa-calendar"></i></button>
                </span>
              </p>
                <!-- {{ data.DailyTimeRecord.timeOut}} -->
              <!-- <input type="time" class="form-control" ng-model="data.DailyTimeRecord.timeOut"> -->
          </div>
        </div>
      </div>    
    </form>
    
    <div class="clearfix"></div>
    <hr>
        
    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
      </div>
    </div>
  </div>
</div>

  