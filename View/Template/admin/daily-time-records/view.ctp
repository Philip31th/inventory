<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> DAILY TIME RECORD </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name :</dt>
          <dd>{{ data.DailyTimeRecord.name }}</dd>

          <dt>Time In :</dt>
          <dd>{{ data.DailyTimeRecord.in }}</dd>

          <dt>Time Out:</dt>
          <dd>{{ data.DailyTimeRecord.out }}</dd>

          <dt>Total Hours:</dt>
          <dd>{{ data.DailyTimeRecord.total_hours }}</dd>
          
        </dl>
     

      <div class="clearfix"></div>
      <hr>

      <div class="btn-group btn-group-sm pull-right btn-min">
        <!-- <a href="#/daily-time-records/edit/{{ data.DailyTimeRecord.id }}" class="btn btn-primary btn-min"><i class="fa fa-edit"></i> EDIT</a>     -->  
        <button class="btn btn-danger btn-min" ng-click="remove(data)"><i class="fa fa-trash"></i> DELETE</button>
      </div>  

     </div>  
    </div>
  </div>
</div>
