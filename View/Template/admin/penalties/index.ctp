<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> PENALTIES</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-3">
        <a href="#/penalty/add" class="btn btn-primary btn-sm btn-block modal-form"><i class="fa fa-plus"></i> NEW PENALTY</a>
      </div>

      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered center">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th class="text-left">NAME</th>
              <th class="text-right w120px">VALUE</th>
              <th class="text-left w120px">TYPE</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="penalty in penalties">
              <td>{{ $index + 1 }}</td>
              <td class="uppercase text-left">{{ penalty.name }}</td>
              <th class="text-right uppercase italic">{{ penalty.value | number:2  }}</th>
              <td class="text-left uppercase">
                <span ng-if="penalty.type == 'fixed'">PESOS</span>
                <span ng-if="penalty.type == 'percent'">PERCENT</span>
              </td>
              <td class="text-center">
                <div class="btn-group btn-group-xs">
                 <!--  <a href="#/penalty/view/{{ penalty.id }}" class="btn btn-success"><i class="fa fa-eye"></i></a> -->
                  <a href="#/penalty/edit/{{ penalty.id }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                  <a href="javascript:void(0)" ng-click="remove(penalty)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>
