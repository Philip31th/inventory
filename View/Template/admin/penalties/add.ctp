<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW PENALTY</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Penalty.name" data-validation-engine="validate[required]" >
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Value <i class="required">*</i></label>
            <input amount type="text" class="form-control" ng-model="data.Penalty.value" data-validation-engine="validate[required]">
          </div>
        </div> 
        
        <div class="col-md-6">
          <div class="form-group">
            <label>TYPE <i class="required">*</i></label>
            <select class="form-control" ng-model="data.Penalty.type">
              <option value="">SELECT TYPE</option>
              <option value="fixed">PESOS</option>
              <option value="percent">PERCENT</option>
            </select>
          </div>
        </div> 

        <div class="clearfix"></div>
        <hr>

        <div class="col-md-4 pull-right">
          <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
        </div>
      </div>
    </form> 
  </div>
</div>
