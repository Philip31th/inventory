<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW COMPANY</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name:</dt>
          <dd>{{ data.Company.name }}</dd>

          <dt>Contact Person:</dt>
          <dd>{{ data.Company.contactPerson }}</dd>

          <dt>Email Address:</dt>
          <dd>{{ data.Company.email }}</dd>

          <dt>Mobile #:</dt>
          <dd>{{ data.Company.mobile }}</dd>

          <dt>Phone #:</dt>
          <dd>{{ data.Company.phone }}</dd>

          <dt>Fax #:</dt>
          <dd>{{ data.Company.fax }}</dd>

          <dt>Address:</dt>
          <dd>{{ data.Company.address}}</dd>

          <dt>Date Created:</dt>
          <dd>{{ data.Company.date }}</dd>
        </dl>
      </div>  
    </div>

    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-3 pull-right">
        <a href="#/company/edit/{{ data.Company.id }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-edit"></i> EDIT</a>
      </div>
    </div>
  </div>
</div>
