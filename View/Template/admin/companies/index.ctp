<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> COMPANIES</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-3">
        <a href="#/company/add" class="btn btn-primary btn-sm btn-block modal-form"><i class="fa fa-plus"></i> NEW COMPANY</a>
      </div>

      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ company: true, search: searchTxt })">
        </div>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered center">
          <thead>
            <tr>
              <th class="w30px"></th>
              <th class="text-left">NAME</th>
              <th class="text-left">ADDRESS</th>
              <th class="text-left w200px">DATE CREATED</th>
              <th class="w70px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="coy in companies">
              <td>{{ $index + 1 }}</td>
              <td class="uppercase text-left">{{ coy.Company.name }}</td>
              <td class="text-left uppercase">{{ coy.Company.address }}</td>
              <td class="text-left uppercase">{{ coy.Company.date }}</td>
              <td class="text-center">
                <div class="btn-group btn-group-xs">
                  <a href="#/company/view/{{ coy.Company.id }}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                  <a href="#/company/edit/{{ coy.Company.id }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ company: true , page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>
