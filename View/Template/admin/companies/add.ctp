<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW COMPANY</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-5">
          <div class="form-group">
            <label>Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Company.name" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Title</label>
            <select class="form-control" ng-model="data.Company.contactPersonTitle">
              <option value="Mr.">Mr.</option>
              <option value="Ms.">Ms.</option>
              <option value="Mrs.">Mrs.</option>
            </select>
          </div>
        </div> 

        <div class="col-md-5">
          <div class="form-group">
            <label>Contact Person</label>
            <input type="text" class="form-control" ng-model="data.Company.contactPersonName">
          </div>
        </div> 

        <div class="col-md-3">
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" ng-model="data.Company.email">
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="form-group">
            <label>Mobile</label>
            <input type="text" class="form-control" ng-model="data.Company.mobile">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" ng-model="data.Company.phone">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Fax</label>
            <input type="text" class="form-control" ng-model="data.Company.fax">
          </div>
        </div>
        
        <div class="col-md-12">
          <div class="form-group">
            <label>Address <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Company.address" data-validation-engine="validate[required]">
          </div>
        </div>
      </div>
        

        <div class="clearfix"></div>
        <hr>

        <div class="row">
          <div class="col-md-3 pull-right">
            <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
          </div>
        </div>  
      </div>
    </form> 
  </div>
</div>
