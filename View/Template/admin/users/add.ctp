<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW USER </div>
  <div class="panel-body">
      <div class="row">  
        <div class="col-md-3">
          <div class="form-group">
            <div id="preview">
              <img src="<?php echo $this->base ?>/assets/img/default-user.jpg" class="img-circle img-responsive">
            </div>
            <input type="file" id="file_upload">
          </div>
        </div>

      <div class="col-md-9">
        <div class="col-md-6">
          <div class="form-group">
            <label>Username <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.User.username" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Type <i class="required">*</i></label>
            <select class="form-control" ng-model="data.User.role" data-validation-engine="validate[required]">
              <option value=""></option>
              <option value="admin">ADMIN</option>
              <option value="hotel">HOTEL</option>
              <option value="restaurant">RESTAURANT</option>
              <option value="bar">BAR</option>
            </select>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>First Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.User.firstName" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Last Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.User.lastName" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Password <i class="required">*</i></label>
            <input type="password" class="form-control" ng-model="data.User.password" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Re-type Password <i class="required">*</i></label>
            <input type="password" class="form-control" ng-model="confirmPassword" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Employee</label>
            <select selectize ng-model="data.Room.roomTypeId" ng-options="opt.id as opt.value for opt in employees">
              <option value=""></option>
            </select>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Activated <i class="required">*</i></label>
            <select class="form-control" ng-model="data.User.activated" ng-options="opt.id as opt.value for opt in bool" data-validation-engine="validate[required]">
              <option value=""></option>
            </select>
          </div>
        </div>

        <?php if ($this->Session->read('Auth.User.role') == 'superuser'): ?>
        <div class="col-md-6 pull-left">
          <div class="form-group">
            <label>Developer</label>
            <select class="form-control" ng-model="data.User.developer" ng-options="opt.id as opt.value for opt in bool">
              <option value=""></option>
            </select>
          </div>
        </div>
        <?php endif ?>        

        </div>
    </div>
    <!-- </form> -->

    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
      </div>
    </div>
  </div>
</div>

<?php echo $this->element('users/uploadify') ?>