<div class="panel panel-primary">
  <div class="panel-heading">USER MANAGEMENT</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>

      <div class="col-md-3">
        <a href="#/user/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> NEW USER </a>
      </div>

      <div class="clearfix"></div> 
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th class="w180px">USERNAME</th>
              <th>NAME</th>
              <th class="w150px">ROLE</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="user in users">
              <td class="text-center">{{ $index + 1 }}</td>
              <td class="">{{ user.username }}</td>
              <td class="uppercase">{{ user.name }}</td>
              <td class="uppercase">
                <span ng-if="user.developer">DEVELOPER</span>
                <span ng-if="!user.developer">{{ user.role }}</span>
              </td>
              <td class="text-center">
                <div class="btn-group btn-group-xs">
                  <a href="#/user/view/{{user.id}}" class="btn btn-success nbr"><i class="fa fa-eye"></i></a>
                  <a href="#/user/edit/{{user.id}}" class="btn btn-primary nbr"><i class="fa fa-pencil"></i></a>
                  <button class="btn btn-danger nbr" ng-click="remove(user)"><i class="fa fa-trash"></i></button>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>
