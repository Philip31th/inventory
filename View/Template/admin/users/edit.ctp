<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT USER </div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Username <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.User.username" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Type <i class="required">*</i></label>
            <select class="form-control" ng-model="data.User.role" data-validation-engine="validate[required]">
              <option value=""></option>
              <option value="admin">ADMIN</option>
              <option value="hotel">HOTEL</option>
              <option value="restaurant">RESTAURANT</option>
              <option value="bar">BAR</option>
            </select>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>First Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.User.firstName" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Last Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.User.lastName" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>New Password</label>
            <input type="password" class="form-control" ng-model="data.User.password">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Re-type New Password</label>
            <input type="password" class="form-control" ng-model="data.Confirm.password">
          </div>
        </div>

        <div class="col-md-12">
          <div class="form-group">
            <label>Employee</label>
            <select selectize ng-model="data.Room.roomTypeId" ng-options="opt.id as opt.value for opt in employees">
              <option value=""></option>
            </select>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Activated <i class="required">*</i></label>
            <select class="form-control" ng-model="data.User.activated" ng-options="opt.id as opt.value for opt in bool" data-validation-engine="validate[required]">
              <option value=""></option>
            </select>
          </div>
        </div>

        <?php if ($this->Session->read('Auth.User.role') == 'superuser'): ?>
        <div class="col-md-6 pull-left">
          <div class="form-group">
            <label>Developer</label>
            <select class="form-control" ng-model="data.User.developer" ng-options="opt.id as opt.value for opt in bool">
              <option value=""></option>
            </select>
          </div>
        </div>
        <?php endif ?>

      </div>
    </form>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <th class="bg-blue" colspan="4">PERMISSIONS</th>
          </thead>
          <tbody>
            <tr ng-repeat="permission in data.UserPermission">
              <td class="w30px">{{ $index + 1 }}</td>
              <td class="uppercase w120px">{{ permission.module }}</td>
              <td class="uppercase">{{ permission.name }}</td>
              <td class="w30px">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-danger no-border-radius" ng-click="remove(permission);" ><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="col-md-3">
        <button type="button" class="btn btn-primary btn-xs btn-block" ng-click="addPermission()"><i class="fa fa-plus"></i> ADD PERMISSION</button>
      </div>
    </div>

    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="update()">SAVE</button>
      </div>
    </div>
  </div>
</div>

<!-- add permission -->
<div class="modal fade" id="add-permission-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD PERMISSION</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered center">
          <thead>
            <th class="w30px">#</th>
            <th class="w120px">MODULE</th>
            <th>PERMISSION</th>
          </thead>
          <tbody>
            <tr ng-repeat="optPermission in data.PermissionSelection">
              <td>
                <input icheck type="checkbox" ng-init="optPermission.selected = false" ng-model="optPermission.selected">
              </td>
              <td class="uppercase">{{ optPermission.module }}</td>
              <td class="uppercase text-left">{{ optPermission.name }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal">CANCEL</button>
        <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="savePermission()"> <i class="fa fa-save"></i> SAVE</button>
      </div>
    </div>
  </div>
</div>
<!-- .add permission -->