<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> USER INFORMATION</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-6">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Username:</dt>
          <dd>{{ data.User.username }}</dd>

          <dt>Name:</dt>
          <dd class="uppercase">{{ data.User.ffirstName }} {{ data.User.lastName }}</dd>
        </dl>
      </div>

      <div class="col-md-6">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Type:</dt>
          <dd class="uppercase">{{ data.User.role }}</dd>

          <dt>Activated:</dt>
          <dd>
            <span class="label label-success" ng-if="data.User.activated">Activated</span>
            <span class="label label-danger" ng-if="!data.User.activated">Not Activated</span>
          </dd>
        </dl>
      </div>

      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <th class="bg-blue" colspan="3">PERMISSIONS</th>
          </thead>
          <tbody>
            <tr ng-repeat="permission in data.UserPermission">
              <td class="w30px">{{ $index + 1 }}</td>
              <td class="uppercase w120px">{{ permission.module }}</td>
              <td class="uppercase">{{ permission.name }}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-3 pull-right">
        <button class="btn btn-danger btn-sm btn-block"><i class="fa fa-trash"></i> DELETE</a>
      </div>

      <div class="col-md-3 pull-right">
        <a href="#/user/edit/{{ data.User.id }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> EDIT</a>
      </div>
    </div>
  </div>
</div>
