<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> ROOM TYPES MANAGEMENT</div>
  <div class="panel-body">
  	<div class="row">
			<div class="col-md-4 pull-right">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
					<input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
				</div> 
			</div>

			<div class="col-md-3">
				<a href="#/hotel/room-types/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> NEW ROOM TYPE</a>
			</div>

			<div class="clearfix"></div>
			<hr>

			<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th class="text-center w30px">#</th>
							<th>TYPE</th>
							<th>ACCOMODATION</th>
							<th class="text-right w120px">RATE</th>
							<th class="text-right w30px">ROOMS</th>
							<th class="w90px"></th>
						</tr>
					</thead>
					<tbody>
						<tr class="{{ roomType.visible? '' : 'danger' }}" ng-repeat="roomType in roomTypes">
							<td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
							<td class="uppercase">{{ roomType.name }}</td>
							<td class="uppercase">{{ roomType.accomodation }}</td>
							<th class="text-right italic">{{ roomType.rate | currency:'PHP ' }}</th>
							<td class="text-right">{{ roomType.count }}</td>
							<td class="text-center">
								<div class="btn-group btn-group-xs">
								 <?php if (hasAccess('hotel/room-types/view', $currentUser)): ?>
									<a href="#/hotel/room-types/view/{{roomType.id}}" class="btn btn-success no-border-radius" title="VIEW"><i class="fa fa-eye"></i></a>
								 <?php endif ?>

								 <?php if (hasAccess('hotel/room-types/edit', $currentUser)): ?>
									<a href="#/hotel/room-types/edit/{{roomType.id}}" class="btn btn-primary no-border-radius"><i class="fa fa-pencil"></i></a>
								 <?php endif ?>

								 <?php if (hasAccess('hotel/room-types/delete', $currentUser)): ?>	
									<a class="btn btn-danger no-border-radius" ng-click="remove(roomType)"><i class="fa fa-trash"></i></a>
								 <?php endif ?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
  	</div>

    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>
