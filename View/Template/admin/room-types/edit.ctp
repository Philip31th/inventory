<div class="panel panel-primary">
	<div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT ROOM TYPE</div>
	<div class="panel-body">
		<form id="form">
			<div class="row">
		    <div class="col-md-4">
					<div class="form-group">
						<label>Room Type <i class="required">*</i></label>
						<input type="text" class="form-control" ng-model="data.RoomType.name" data-validation-engine="validate[required]">
					</div>
				</div>

		    <div class="col-md-4">
					<div class="form-group">
						<label>Accomodation <i class="required">*</i></label>
						<select class="form-control" ng-model="data.RoomType.accomodationId" ng-options="opt.id as opt.value for opt in accomodations" data-validation-engine="validate[required]">
							<option value="">Select Accomodation</option>
						</select>
					</div>
				</div>

		    <div class="col-md-4">
					<div class="form-group">
						<label>Rate <i class="required">*</i></label>
						<input amount type="text" class="form-control" ng-model="data.RoomType.rate" data-validation-engine="validate[required]">
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label>Description </label>
						<textarea class="form-control" ng-model="data.RoomType.description"></textarea>
					</div>
				</div>

				<?php if ($currentUser['User']['role'] == 'superuser'): ?>
				<div class="col-md-4">
					<div class="form-group">
						<label>Visible</label>
						<div>
							<input icheck type="checkbox" class="form-control" ng-model="data.RoomType.visible">
						</div>
					</div>
				</div>
				<?php endif ?>

			</div>
		</form>

		<div class="clearfix"></div>
		<hr>

		<div class="row">
			<div class="col-md-4 pull-right">
				<button class="btn btn-primary btn-sm btn-block" ng-click="update()">SAVE</button>
			</div>
		</div>

	</div>
</div>
