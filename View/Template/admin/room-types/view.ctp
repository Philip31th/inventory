<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> ROOM TYPES </div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
  			<dl class="dl-horizontal dl-data dl-bordered">
  				<dt>Room Type:</dt>
  				<dd>{{ data.RoomType.name }}</dd>

          <dt>Room Accomodation:</dt>
          <dd>{{ data.Accomodation.name }}</dd>

  				<dt>Room Rate:</dt>
  				<dd>{{ data.RoomType.rate | currency:'PHP ' }}</dd>

          <dt>Description:</dt>
          <dd>{{ data.RoomType.description }}</dd>

  			</dl>
  		</div>

  		<div class="clearfix"></div>
  		<hr>

      <div class="col-md-3 pull-right">
        <button class="btn btn-danger btn-sm btn-block" ng-click="remove(aroomtype.RoomType)"><i class="fa fa-trash"></i> DELETE</button>
      </div>

      <div class="col-md-3 pull-right">
        <a href="#/hotel/room-types/edit/{{ aroomtype.RoomType.id }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> EDIT</a>
      </div>
      
      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover center">
          <thead>
            <tr>
              <th class="bg-blue text-left" colspan="6">ROOMS</th>
            </tr>
            <tr>
              <th class="text-center w30px">#</th>
              <th class="w90px">RM #</th>
              <th>TYPE</th>
              <th>ACCOMODATION</th>
              <th class="text-right w120px">RM RATE</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="room in data.Room">
              <td class="text-center">{{ $index + 1 }}</td>
              <td class="uppercase">{{ room.name }}</td>
              <td class="uppercase">{{ data.RoomType.name }}</td>
              <td class="uppercase">{{ data.Accomodation.name }}</td>
              <th class="text-right italic">{{ room.rate | currency:'PHP ' }}</th>
              <td class="text-center">
                <div class="btn-group btn-group-xs">
                  <a href="#/hotel/room/view/{{ room.id }}" class="btn btn-success no-border-radius" title="VIEW"><i class="fa fa-eye"></i></a>
                  <a href="#/hotel/room/edit/{{ room.id }}" class="btn btn-primary no-border-radius" title="EDIT"><i class="fa fa-pencil"></i></a>
                  <a class="btn btn-danger no-border-radius" title="DELETE" ng-click="remove(room)"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>




  	</div>
	</div>
</div>
