<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW ROOM ACCOMODATIONS</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Accomodation.name" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Number <i class="required">*</i></label>
            <input number type="text" class="form-control" ng-model="data.Accomodation.number" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="clearfix"></div>
        <hr>

        <div class="col-md-4 pull-right">
          <button class="btn btn-primary btn-sm btn-block" ng-click="save()">SAVE</button>
        </div>
      </div>
    </form>
  </div>
</div>
