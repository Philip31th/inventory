<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> Messages</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>
      
      <div class="col-md-3">
        <a href="#/messages/new" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> New Message</a>
      </div>
    
      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered center">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th class="">FROM</th>
              <th class="">SUBJECT</th>
              <th>MESSAGE</th>
     
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="data in logs">
              <td>{{ $index + 1 }}</td>
              <td class="">{{ data.created }}</td>
              <td class="">{{ data.role }}</td>
              <td class="">{{ data.user }}</td>
   
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>
