<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> ROOM </div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
  			<dl class="dl-horizontal dl-data dl-bordered">
  				<dt>Room Number:</dt>
  				<dd>{{ data.Room.name }}</dd>

  				<dt>Room Type:</dt>
  				<dd>{{ data.RoomType.name }}</dd>

  				<dt>Accomodation:</dt>
  				<dd>{{ data.Accomodation.name }}</dd>

  				<dt>Room Rate:</dt>
  				<dd>{{ data.Room.rate | currency:'PHP ' }}</dd>

  				<dt>Description</dt>
  				<dd>{{ data.Room.description }}</dd>
  			</dl>
  		</div>

  		<div class="clearfix"></div>
  		<hr>

  		<div class="col-md-3 pull-right">
  			<button class="btn btn-danger btn-sm btn-block" ng-click="remove(data.Room)"><i class="fa fa-trash"></i> DELETE</button>
  		</div>

  		<div class="col-md-3 pull-right">
  			<a href="#/hotel/room/edit/{{ data.Room.id }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> EDIT</a>
  		</div>
  	</div>
	</div>
</div>
