
<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW ROOM</div>
  <div class="panel-body">
		<form id="form">
			<div class="row">
		    <div class="col-md-4">
					<div class="form-group">
						<label>Room Number <i class="required">*</i></label>
						<input type="text" class="form-control" ng-model="data.Room.name" data-validation-engine="validate[required]">
					</div>
				</div>

		    <div class="col-md-8">
					<div class="form-group">
						<label>Room Type <i class="required">*</i></label>
						<select class="form-control" ng-model="data.Room.roomTypeId" ng-options="opt.id as opt.value for opt in roomTypes" data-validation-engine="validate[required]">
							<option value=""></option>
						</select>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label>Description</label>
						<textarea class="form-control" ng-model="data.Room.description"></textarea>
					</div>
				</div>
			</div>
		</form>

		<div class="clearfix"></div>
		<hr>

		<div class="row">
			<div class="col-md-4 pull-right">
				<button class="btn btn-primary btn-sm btn-block" ng-click="save()">SAVE</button>
			</div>
		</div>
  </div>
</div>
