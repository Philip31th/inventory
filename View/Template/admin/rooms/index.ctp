<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> ROOM BLOCKINGS <i class="uppercase pull-right">({{ searchTxt | dateFormat:'MMM.dd.yyyy' }})</i></div>
  <div class="panel-body">
 		<div class="row">
			<div class="col-md-4 pull-right">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
					<input date type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-change="load({ date: searchTxt })">
				</div> 
			</div>

			<div class="col-md-3">
				<a href="#/hotel/room/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> NEW ROOM</a>
			</div>

			<div class="clearfix"></div>
			<hr>

			<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover center">
					<thead>
						<tr>
							<th></th>
							<th class="w90px">RM #</th>
							<th>TYPE</th>
							<th>ACCOMODATION</th>
							<th class="w120px">RATE</th>
							<th class="w90px">STATUS</th>
							<th class="w90px"></th>
						</tr>
					</thead>
					<tbody>
						<tr class="{{ room.visible? '' : 'danger' }}" ng-repeat="room in rooms">
							<td>{{ $index + 1 }}</td>
							<td class="uppercase">{{ room.name }}</td>
							<td class="uppercase">{{ room.type }}</td>
							<td class="uppercase">{{ room.accomodation }}</td>
							<th class="text-right italic">{{ room.rate | currency:'PHP ' }}</th>
							<td class="uppercase italic">
								<span class="label label-{{ room.available? 'success' : 'danger' }}">{{ room.status }}</span>
							</td>
							<td>
								<div class="btn-group btn-group-xs">
								  <?php if (hasAccess('hotel/rooms/view', $currentUser)): ?>
								    <a href="#/hotel/room/view/{{ room.id }}" class="btn btn-success" title="VIEW"><i class="fa fa-eye"></i></a>
									<?php endif ?>
									
									<?php if (hasAccess('hotel/rooms/edit', $currentUser)): ?>
                    <a href="#/hotel/room/edit/{{ room.id }}" class="btn btn-primary" title="EDIT"><i class="fa fa-pencil"></i></a>
									<?php endif ?>
									
									<?php if (hasAccess('hotel/rooms/delete', $currentUser)): ?>
                    <a class="btn btn-danger no-border-radius" title="DELETE" ng-click="remove(room)"><i class="fa fa-trash"></i></a>
								  <?php endif ?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
 		</div>
  </div>
</div>
