<div>
	<div class="panel panel-primary">
	    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> OCCUPIED ROOMS <i class="uppercase pull-right">({{ strSearch|dateFormat:'MMM.dd.yyyy' }})</i></div>
	    <div class="panel-body">
  			<div class="col-md-4 pull-right">
  				<div class="input-group">
  					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
  					<input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="strSearch" ng-change="load()">
  				</div> 
  			</div>
  			<div class="clearfix"></div>
  			<hr>
  			<div class="col-md-12">
  				<table class="table table-bordered center">
  					<thead>
  						<tr>
  							<!--<th class="w30px"></th>-->
  							<th class="w60px">RM #</th>
  							<th>RM TYPE</th>
  							<th>GUEST</th>
  							<th>ARRIVAL</th>
                <th>DEPARTURE</th>
                <th>EXTENSION</th>
  						</tr>
  					</thead>
  					<tbody>
  						<tr ng-repeat="occ in occupieds">
  							<!--<td>{{ $index + 1 }}</td>-->
  							<td>{{ occ.room }}</td>
  							<td class="uppercase">{{ occ.type }}</td>
  							<td class="uppercase">{{ occ.guest }}</td>		
  							<td class="uppercase">{{ occ.arrival }}</td>	
                <td class="uppercase">{{ occ.departure }}</td> 
                <td class="uppercase"></td> 
  						</tr>
  						<tr ng-repeat="out in outs">
  							<!--<td>{{ $index + 1 }}</td>-->
  							<td>{{ out.room }}</td>
  							<td class="uppercase">{{ out.type }}</td>
  							<td class="uppercase">{{ out.guest }}</td>		
  							<td class="uppercase">{{ out.arrival }}</td>	
                <td class="uppercase">{{ out.departure }}</td> 
                <td class="uppercase"></td> 
  						</tr>
  					</tbody>
  				</table>
  			</div>
	    </div>
	</div>
</div>
