<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> LOGS</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>

      <div class="col-md-3">
         <button class="btn btn-danger btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i>PRINT</button>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered center">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th class="text-ri'">DATETIME</th>
              <th class="text-right">ROLE</th>
              <th class="text-right">USER</th>
              <th class="text-right">ACTION</th>
              <th class="text-right">DESCRIPTION</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="data in logs">
              <td>{{ $index + 1 }}</td>
              <td class="uppercase text-right">{{ data.created }}</td>
              <td class="uppercase text-right">{{ data.role }}</td>
              <td class="text-right uppercase">{{ data.user }}</td>
              <td class="text-right uppercase">{{ data.action }}</td>
              <td class="text-right uppercase">{{ data.description }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>
