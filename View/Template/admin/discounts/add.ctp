<div class="panel panel-primary">
	<div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW DISCOUNT</div>
	<div class="panel-body">
		<form id="form">
			<div class="row">
		    <div class="col-md-12">
					<div class="form-group">
						<label>Name <i class="required">*</i></label>
						<input type="text" class="form-control" ng-model="data.Discount.name" data-validation-engine="validate[required]" >
					</div>
				</div>	

		    <div class="col-md-6">
					<div class="form-group">
						<label>Value <i class="required">*</i></label>
						<input amount type="text" class="form-control" ng-model="data.Discount.value" data-validation-engine="validate[required]">
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Type <i class="required">*</i></label>
						<select class="form-control" ng-model="data.Discount.type" data-validation-engine="validate[required]">
							<option value=""></option>
							<option value="fixed">PESOS</option>
							<option value="percent">PERCENT</option>
						</select>
					</div>
				</div>
			</div>
		</form>

		<div class="clearfix"></div>
		<hr>

		<div class="row">
			<div class="col-md-3 pull-right">
				<button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
			</div>
		</div>
	</div>
</div>
