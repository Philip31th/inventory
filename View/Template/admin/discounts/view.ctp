<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW DISCOUNT</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name:</dt>
          <dd>{{ data.Discount.name }}</dd>

          <dt>Value:</dt>
          <dd>{{ data.Discount.value | number:2 }}</dd>

          <dt>Type:</dt>
          <dd>
            <span ng-if="data.Discount.type == 'percent'">PERCENT</span>
            <span ng-if="data.Discount.type == 'fixed'">PESOS</span>
          </dd>
        </dl>
      </div>  
    </div>

    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-danger btn-sm btn-block" ng-click="remove(data.Discount)"><i class="fa fa-trash"></i> DELETE</button>
      </div>

      <div class="col-md-3 pull-right">
        <a href="#/discount/edit/{{ data.Discount.id }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> EDIT</a>
      </div>
    </div>
  </div>
</div>
