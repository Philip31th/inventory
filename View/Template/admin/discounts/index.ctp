<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> DISCOUNTS</div>
  <div class="panel-body">
	  <div class="row">
			<div class="col-md-3">
				<a href="#/discount/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> ADD DISCOUNT</a>
			</div>

      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>

			<div class="clearfix"></div>
			<hr>

			<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th class="w30px">#</th>
							<th>NAME</th>
							<th class="text-right w90px">VALUE</th>
							<th class="w120px">TYPE</th>
							<th class="w90px"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="discount in discounts">
							<td class="text-center">{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
							<td class="uppercase">{{ discount.name }}</td>
							<th class="text-right italic">{{ discount.value | number:2 }}</th>
							<td>
								<span ng-if="discount.type == 'percent'">PERCENT</span>
								<span ng-if="discount.type == 'fixed'">PESOS</span>
							</td>
							<td class="text-center">
								<div class="btn-group btn-group-xs">
									<!-- <a href="#/discount/view/{{ discount.id }}" class="btn btn-success"><i class="fa fa-eye"></i></a> -->
									<a href="#/discount/edit/{{ discount.id }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
									<a href="javascript:void(0)" ng-click="remove(discount)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>
    
  </div>
</div>

