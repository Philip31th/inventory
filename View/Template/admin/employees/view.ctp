<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW EMPLOYEE</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
      <br>
        <div class="col-md-3">
          <img src="<?php echo $this->base ?>/uploads/employees/{{ data.Employee.image }}" width="210" height="200" id="user-image" class="user-image img-circle">
        </div>
        
        <div class="col-md-9">
          <div role="tabpanel">
          
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="javascript:void(0)" role="tab" data-toggle="tab" data-target="#personal">PERSONAL INFO</a>
              </li>
              <li role="presentation" class="">
                <a href="javascript:void(0)" role="tab" data-toggle="tab" data-target="#leaves">LEAVES</a>
              </li>
              <li role="presentation" class="">
                <a href="javascript:void(0)" role="tab" data-toggle="tab" data-target="#performance">PERFORMANCES</a>
              </li>
            </ul>

           <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade active in" id="personal">
              <dl class="dl-horizontal dl-data dl-bordered">
                <dt>ID #: </dt>
                <dd>{{ data.Employee.employeeNumber }}</dd>

                <dt>Name: </dt>
                <dd>{{ data.Employee.firstName + ' ' + data.Employee.lastName }}</dd>
      
                <dt>Department: </dt>
                <dd>{{ data.Department.name }}</dd>
      
                <dt>Position: </dt>
                <dd>{{ data.Employee.position }}</dd>

                <dt>Rate: </dt>
                <dd>{{ data.Employee.rate }}</dd>

                <dt>Date Hired: </dt>
                <dd>{{ data.Employee.dateHired }}</dd>

                <dt>Contract Validity: </dt>
                <dd>{{ data.Employee.contractValidity }}</dd>

                <dt>Type: </dt>
                <dd>{{ data.Employee.type }}</dd>
             </dl>
          
                <div class="clearfix"></div>
                <hr>
                <div class="btn-group btn-group-sm pull-right btn-min">
                  <a href="#/employee/edit/{{ data.Employee.id }}" class="btn btn-primary btn-sm btn-min"><i class="fa fa-edit"></i> EDIT</a>
                  <button type="button" class="btn btn-danger btn-sm btn-min" ng-click="remove(data.Employee)"><i class="fa fa-trash"></i> DELETE</button>
                </div> 
              </div>

            <div role="tabpanel" class="tab-pane fade" id="leaves">
              <br>
              <table class="table table-bordered vcenter">
                 <thead class="bg-info">
                   <tr>
                       <th>#</th>
                      <th class="text-center">START DATE</th>
                      <th class="text-center">END DATE</th>
                      <th class="text-center">PURPOSE</th>
                      <th class="w70px"></th>
                   </tr>
                 </thead>
                 <tbody>
                 <tr ng-repeat="leave in vleave.EmployeeLeave">
                    <td>{{ $index+1 }}</td>
                    <td class="text-center">{{ leave.startDate }}</td>
                    <td class="text-center">{{ leave.endDate }}</td>
                    <td class="text-center">{{ leave.purpose }}</td>
                    <td class="text-center">
                      <div class="btn-group btn-group-xs">
                        <button type="button" ng-click="editLeave(leave)"  class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></button>
                        <button ng-click="removeLeave(leave)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                     </div>
                    </td>
                 
                 </tbody>
                 
                 </tr>
            </table>  
            </div>  

            <div role="tabpanel" class="tab-pane fade" id="performance">
              <br>
              <table class="table table-bordered vcenter">
              
               <thead class="bg-info">
                 <tr>
                    <th class="text-center w180px">DATE</th>
                    <th class="text-center">PERFORMANCES</th>
                    <th class="w70px"></th>
                 </tr>
               </thead>
               <tbody>
               <tr ng-repeat="performances in vperformances.EmployeePerformance">
                  <td class="text-center">{{ performances.date }}</td>
                  <td class="text-center">{{ performances.remarks }}</td>
                  <td class="text-center">
                    <div class="btn-group btn-group-xs">
                      <button type="button" ng-click="editRemarks(performances)"  class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></button>
                      <button href="javascript:void(0)" ng-click="removeperformance(performances)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                   </div>
                  </td>
               
               </tbody>
               
               </tr>
               </table>
          </div>   
         </div>   
        </div>
      </div>  
          <!--  -->
        <!--   <div class="btn-group btn-group-sm pull-left btn-min">
            <button type="button" class="btn btn-info btn-sm btn-min" ng-click="viewLeaves()"><i class=""></i> LEAVES</button>
            <button type="button" class="btn btn-success btn-sm btn-min" ng-click="viewRemarks()"><i class=""></i> PERFORMANCES</button> -->
            <!-- // here for absences -->
          </div>
        </div>  
        
    </div>
  </div>
</div>


<?php echo $this->element('employee/employee-performances-modal')?>
<?php echo $this->element('employee/employee-performances-edit-modal')?>
<?php echo $this->element('employee/view-leaves-modal')?>
<?php echo $this->element('employee/add-leaves-modal')?>
















