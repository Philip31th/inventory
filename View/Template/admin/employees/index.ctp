<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EMPLOYEE MANAGEMENT</div>
  <div class="panel-body">
  	<div class="row">
    	<div class="col-md-3">
				<a href="#/employee/add" class="btn btn-primary btn-sm btn-block modal-form"><i class="fa fa-plus"></i> NEW EMPLOYEE</a>
			</div>

      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div>
      </div>

      <div class="clearfix"></div>
   		<hr>

   		<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th class="w30px">#</th>
							<th class="">NAME</th>
							<th class="">DEPARTMENT</th>
              <th class="">POSITION</th>
							<th class="w90px"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="type in employees">
							<td class="text-center">{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
							<td class="uppercase">{{ type.name }}</td>
							<td class="uppercase">{{ type.department }}</td>
              <td class="uppercase">{{ type.position }}</td>
							<td class="text-center">
								<div class="btn-group btn-group-xs">
									<a href="#/employee/view/{{ type.id }}" class="btn btn-success" title="VIEW"><i class="fa fa-eye"></i></a>
									<a href="#/employee/edit/{{ type.id }}" class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></a>
									<a href="javascript:void(0)" ng-click="remove(type)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
   		</div>
  	</div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>