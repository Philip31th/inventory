<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT EMPLOYEE</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-3">
          <img src="<?php echo $this->base ?>/uploads/employees/{{ data.Employee.image }}" width="210" height="200" id="user-image" class="user-image">
          <!-- <ul class="list-group">
            <span class="btn btn-primary btn-block btn-sm btn-file">
              Choose File 
              <input id="employeeImage" onchange="readURL(this)" disabled name="picture" class="form-control" type="file" accept="image/gif, image/jpeg, image/png">
            </span>
          </ul> -->
        </div>
        
        <div class="col-md-9">
          <div class="col-md-12">
            <div class="form-group">
                <label>Last Name <i class="required">*</i></label>
                <input type="text" class="form-control" ng-model="data.Employee.lastName" data-validation-engine="validate[required]">
            </div>
          </div> 
  
          <div class="col-md-12">
            <div class="form-group">
              <label>First Name <i class="required">*</i></label>
              <input type="text" class="form-control" ng-model="data.Employee.firstName" data-validation-engine="validate[required]">
            </div>
          </div>  
  
          <div class="col-md-12">
            <div class="form-group">
              <label>Middle Name</label>
              <input type="text" class="form-control" ng-model="data.Employee.middleName">
            </div>
          </div>
  
          <div class="col-md-12">
            <div class="form-group">
              <label>Department <i class="required">*</i></label>
              <select class="form-control" ng-model="data.Employee.departmentId" ng-options="opt.id as opt.value for opt in departments" data-validation-engine="validate[required]">
                <option value="">SELECT DEPARTMENT</option>
              </select>
            </div>
          </div> 
  
          <div class="col-md-12">
            <div class="form-group">
              <label>Position <i class="required">*</i></label>
              <input type="text" class="form-control" ng-model="data.Employee.position" data-validation-engine="validate[required]">
            </div>
          </div> 
        </div>
        
         
      </div><!-- ./row -->
    </form><!-- /form -->
    
    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-4 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="update()"><i class="fa fa-save"></i> SAVE</button>
        <br/>
      </div>
    </div>

  </div>
</div>


<script>
 function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('.user-image')
            .attr('src', e.target.result)
            .width(215)
            .height(190)
            .css("border-radius"," 5px")
            .css("margin-bottom"," 10px");
      };
      reader.readAsDataURL(input.files[0]);
  }
}
</script>    

