<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW EMPLOYEE</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-2">
							<center>
								<img src="assets/img/user-male.jpg" width="110" height="100" class="user-image photo">
                <ul class="list-group w110px">
                  <span class="btn btn-primary btn-block btn-sm btn-file">
                    Choose File 
                    <input id="employeeImage" onchange="readURL(this)" name="picture" class="form-control" type="file" accept="image/gif, image/jpeg, image/png" data-validation-engine="validate[required]">
                  </span>
                </ul>
							</center>
        </div>

        <div class="col-md-10">
          <div class="col-md-12">
            <div class="form-group">
                <label>Employee # </label>
                <input type="text" class="form-control" ng-model="data.Employee.employeeNumber">
            </div>
          </div> 

          <div class="col-md-4">
            <div class="form-group">
                <label>Last Name <i class="required">*</i></label>
                <input type="text" class="form-control" ng-model="data.Employee.lastName" data-validation-engine="validate[required]">
            </div>
          </div> 
  
          <div class="col-md-4">
            <div class="form-group">
              <label>First Name <i class="required">*</i></label>
              <input type="text" class="form-control" ng-model="data.Employee.firstName" data-validation-engine="validate[required]">
            </div>
          </div>  
  
          <div class="col-md-4">
            <div class="form-group">
              <label>Middle Name</label>
              <input type="text" class="form-control" ng-model="data.Employee.middleName">
            </div>
          </div>
  
          <div class="col-md-4">
            <div class="form-group">
              <label>Department <i class="required">*</i></label>
              <select class="form-control" ng-model="data.Employee.departmentId" ng-options="opt.id as opt.value for opt in departments" data-validation-engine="validate[required]">
                <option value="">SELECT DEPARTMENT</option>
              </select>
            </div>
          </div> 
  
          <div class="col-md-4">
            <div class="form-group">
              <label>Position <i class="required">*</i></label>
              <input type="text" class="form-control" ng-model="data.Employee.position" data-validation-engine="validate[required]">
            </div>
          </div> 

          <div class="col-md-4">
            <div class="form-group">
              <label>Rate</label>
              <input type="text" class="form-control" ng-model="data.Employee.rate">
            </div>
          </div> 

          <div class="col-md-4">
            <div class="form-group">
              <label>Date Hired</label>
              <input type="text" class="form-control" id="dateHired" ng-model="data.Employee.dateHired">
            </div>
          </div> 

          <div class="col-md-4">
            <div class="form-group">
              <label>Contract Validity</label>
              <input type="text" class="form-control" ng-model="data.Employee.contractValidity">
            </div>
          </div> 

          <div class="col-md-4">
            <div class="form-group">
              <label>Type </label>
              <select class="form-control" ng-model="data.Employee.type">
                <option>Regular</option>
                <option>Contractual</option>
                <option>Probationary</option>
                <option>Project-Based</option>
              </select>
            </div>
          </div> 

        </div>
        
      </div><!-- ./row -->
    </form><!-- /form -->
    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
        <br/>
      </div>
    </div>

  </div>
</div>


<script>
 function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('.user-image.photo')
            .attr('src', e.target.result)
            .width(110)
            .height(100)
            .css("border-radius"," 3px")
            .css("margin-bottom"," 3px");

          // $('#employeeImage')
          //   .attr('disabled', true);  
      };
      reader.readAsDataURL(input.files[0]);
  }
}
</script>    

