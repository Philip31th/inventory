<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW BUSINESS</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Business Name</dt>
          <dd>{{ data.Business.name }}</dd>

          <dt>Business Description:</dt>
          <dd>{{ data.Business.description }}</dd>
        </dl>
      </div>

      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <th class="bg-info" colspan="4">BUSINESS SERVICES</th>
          </thead>
          <tbody>
            <tr ng-repeat="service in data.BusinessService">
              <td class="w30px">{{  $index + 1 }}</td>
              <td class="">{{ service.name }}</td>
              <th class="text-right w120px">{{ service.amount }}</th>
              <td class="w70px text-center">
                <div class="btn-group btn-group-xs">
                  <button class="btn btn-primary" ng-click="editService(service)"><i class="fa fa-edit"></i></button>
                  <button class="btn btn-danger" ng-click="removeService(service)"><i class="fa fa-trash"></i></button>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="col-md-3">
        <button class="btn btn-primary btn-xs btn-block" ng-click="addService()"><i class="fa fa-plus"></i> ADD SERVICE</button>
      </div>
    </div>

    <div class="clearfix"></div>
    <hr>

    <div class="btn-group btn-group-sm pull-right btn-min">
      <a href="#/business/edit/{{ data.Business.id }}" class="btn btn-primary btn-sm btn-min"><i class="fa fa-edit"></i> EDIT</a>
      <button class="btn btn-danger btn-sm btn-min" ng-click="remove(data.Business)"><i class="fa fa-trash"></i> DELETE</button>
    </div>

  </div>
</div>

<!-- add service -->
<div class="modal fade" id="add-service-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">ADD SERVICE</h4>
      </div>
      <div class="modal-body">
        <form id="add-service-form">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Name <i class="required">*</i></label>
                <input type="text" class="form-control" ng-model="service.BusinessService.name" data-validation-engine="validate[required]">
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label>Description <i class="required">*</i></label>
                <textarea class="form-control" ng-model="service.BusinessService.description" data-validation-engine="validate[required]"></textarea>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label>Amount <i class="required">*</i></label>
                <input amount type="text" class="form-control" ng-model="service.BusinessService.value" data-validation-engine="validate[required]">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-times"></i> CANCEL</button>
          <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveService()"><i class="fa fa-save"></i> SAVE</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .add service -->

<!-- edit service -->
<div class="modal fade" id="edit-service-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">EDIT SERVICE</h4>
      </div>
      <div class="modal-body">
        <form id="edit-service-form">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Name <i class="required">*</i></label>
                <input type="text" class="form-control" ng-model="eservice.BusinessService.name" data-validation-engine="validate[required]">
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label>Description <i class="required">*</i></label>
                <textarea class="form-control" ng-model="eservice.BusinessService.description" data-validation-engine="validate[required]"></textarea>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label>Amount <i class="required">*</i></label>
                <input amount type="text" class="form-control" ng-model="eservice.BusinessService.value" data-validation-engine="validate[required]">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" class="btn btn-danger btn-sm btn-min" data-dismiss="modal"><i class="fa fa-times"></i> CANCEL</button>
          <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="updateService()"><i class="fa fa-save"></i> SAVE</button>
        </div>  
      </div>
    </div>
  </div>
</div>
<!-- .edit service -->