<link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/css.css">

<div class="panel panel-primary">
    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW BOOKING</div>
    <div class="panel-body" style="padding-top:0px">
    <form id="form">
    <div class="row">
      <h3 class="table-form-title" style="margin:0px 0px 10px 0px">GUEST INFORMATION</h3>
      <div class="col-md-3">
        <div class="form-group">
          <label>Title</label>
          <select class="form-control" ng-model="data.Reservation.title" ng-change="changeTitle(data.Reservation.title,'')" ng-init="changeTitle(data.Reservation.title,'title')">
            <option value="null"></option>
            <option value="Mr.">Mr.</option>
            <option value="Ms.">Ms.</option>
            <option value="Mrs.">Mrs.</option>
          </select>
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Last Name <i class="required">*</i></label>
          <input type="text" class="form-control" ng-model="data.Reservation.lastName" data-validation-engine="validate[required]">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>First Name <i class="required">*</i></label>
          <input type="text" class="form-control" ng-model="data.Reservation.firstName" data-validation-engine="validate[required]">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Middle Name</label>
          <input type="text" class="form-control" ng-model="data.Reservation.middleName">
        </div>
      </div>
      
      <div class="col-md-12">
        <div class="form-group">
          <label>Address <i class="required">*</i></label>
          <input type="text" class="form-control" ng-model="data.Reservation.address" data-validation-engine="validate[required]">
        </div>
      </div>
    </div>
    
    <div class="row">
      <h3 class="table-form-title">STAY INFORMATION</h3>
      <div class="col-md-3">
        <div class="form-group">
          <label>Arrival <i class="required">*</i> {{ data.Reservation.arrival }}</label>
          <input type="text" class="form-control arrival-date" ng-model="data.Reservation.arrival"  data-validation-engine="validate[required]" ng-change="clearSelected()" ng-init="data.Reservation.arrival=''">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Departure <i class="required">*</i></label>
          <input type="text" class="form-control departure-date" ng-model="data.Reservation.departure"  data-validation-engine="validate[required]" ng-change="clearSelected()" ng-init="data.Reservation.departure=''">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Night(s) <i class="required">*</i></label>
          <input type="text" class="form-control" ng-model="data.Reservation.nights" disabled>
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Booking Source <i class="required">*</i></label>
          <select class="form-control" ng-model="data.Reservation.bookingSource" ng-options="opt.id as opt.value for opt in bookingSources" data-validation-engine="validate[required]">
            <option value=""></option>
          </select>
        </div>
      </div>
    </div>

    <div class="row">
      <h3 class="table-form-title">CONTACT INFORMATION</h3>
      <div class="col-md-3">
        <div class="form-group">
          <label>Email</label>
          <input type="text" class="form-control" ng-model="data.Reservation.email">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Phone</label>
          <input type="text" class="form-control" ng-model="data.Reservation.phone">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Mobile <i class="required">*</i></label>
          <input type="text" class="form-control" ng-model="data.Reservation.mobile" data-validation-engine="validate[required]">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Fax</label>
          <input type="text" class="form-control" ng-model="data.Reservation.fax">
        </div>
      </div>
    </div>
    
    <div class="row">
      <h3 class="table-form-title">IDENTITY INFORMATION</h3>
      <div class="col-md-3">
        <div class="form-group">
          <label>Identity Type</label>
          <select class="form-control" ng-model="data.Reservation.identity">
            <option value="">Select Identity Type</option>
            <option value="Drivers License">Drivers License</option>
            <option value="Passport">Passport</option>
          </select>
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>ID Number</label>
          <input type="text" class="form-control" ng-model="data.Reservation.identityIdNumber">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Nationality</label>
          <input type="text" class="form-control" ng-model="data.Reservation.nationality" ng-init="data.Reservation.nationality = 'Filipino'" data-validation-engine="validate[required]">
        </div>
      </div>
      
      <div class="col-md-3">
        <div class="form-group">
          <label>Gender</label>
          <select class="form-control"ng-model="data.Reservation.gender" ng-init="changeTitle(data.Reservation.gender,'gender')" ng-change="changeTitle(data.Reservation.gender,'')">
            <option value="null"></option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
      </div>
    </div>
    
    <div class="row">
      <h3 class="table-form-title">BILLING INFORMATION</h3>
      <div class="col-md-4">
        <div class="form-group">
          <label>Bill To</label>
          <select class="form-control" ng-model="data.Reservation.billTo">
            <option value=""></option>
            <option value="company">Company</option>
            <option value="group owner">Group Owner</option>
            <option value="guest">Guest</option>
          </select>
        </div>
      </div>
    </div>  
    
    <div class="row">
      <h3 class="table-form-title">OTHER INFORMATION</h3>
      <div class="col-md-4">
        <div class="form-group">
          <label>Company</label> 
          <input id="searchFiled_TXT" class="input form-control" ng-keydown="autoCompleteInput()" ng-model="data.Company.name" ng-init="search()" ng-keyup="autoCompleteInput()" ng-change="getCompany()"/>
        </div>
      </div>

      <div class="col-md-8">
        <div class="form-group">
          <label>Company Contact Person</label>
          <input type="text" class="form-control" ng-model="data.Company.contactPersonName">
        </div>
      </div>
      
      <div class="col-md-3">
        <div class="form-group">
          <label>Email</label>
          <input type="text" class="form-control" ng-model="data.Company.email">
        </div>
      </div>
      
      <div class="col-md-3">
        <div class="form-group">
          <label>Mobile</label>
          <input type="text" class="form-control" ng-model="data.Company.mobile">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Phone</label>
          <input type="text" class="form-control" ng-model="data.Company.phone">
        </div>
      </div>

      <div class="col-md-3">
        <div class="form-group">
          <label>Fax</label>
          <input type="text" class="form-control" ng-model="data.Company.fax">
        </div>
      </div>
      
      <div class="col-md-12">
        <div class="form-group">
          <label>Address</label>
          <input type="text" class="form-control" ng-model="data.Company.address">
        </div>
      </div>
    </div>
    
    <div class="row">
      <h3 class="table-form-title">ROOMS INFORMATION</h3>
      <div class="col-md-3">
        <a href="javascript:void(0)" class="btn btn-primary btn-block btn-xs" ng-click="selectRoom()">SELECT AVAILABLE ROOMS</a>
      </div>
      
      <div class="clearfix"></div>
      <hr>

      <div style="padding:0px 15px 0px 15px">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th class="w30px"></th>
              <th class="w60px">RM #</th>
              <th>RM TYPE</th>
              <th>ACCOMODATION</th>
              <th class="w120px text-right">RATE</th>
              <th class="w30px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="room in selectedRooms">
              <td>{{ $index+1 }}</td>
              <td>{{ room.code }}</td>
              <td class="uppercase">{{ room.type }}</td>
              <td class="uppercase">{{ room.accomodation }}</td>
              <th class="text-right italic">{{ room.rate*data.Reservation.nights | currency:'PHP ' }}</th>
              <td>
                <div class="btn-group btn-group-xs">
                  <a href="javascript:void(0)" class="btn btn-sm btn-danger" ng-click="removeSelectedRoom(room)"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
            <tr ng-if="selectedRooms==''">
              <td colspan="6" class="text-center">No selected room(s)</td>
            </tr>
          </tbody>
        </table>  
      </div>
    </div>
    </form>
    <hr>
    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-primary btn-block btn-sm" ng-click="save()"><i class="fa fa-save"></i> SAVE BOOKING</button>
      </div>
    </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/jquery-ui-autocomplete/jquery-ui.min.js"></script>

<?php echo $this->element('hotel/bookings/add/select-room-modal') ?>