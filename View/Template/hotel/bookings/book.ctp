  <div class="panel panel-primary">
      <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW RESERVATION</div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label>Arrival Date</label>
              <input type="text" class="form-control input-sm datepicker" id="arrival-date" ng-model="arrivalDate" ng-change="checkAvailable()">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
            <label>Departure Date</label>
              <input type="text" class="form-control input-sm datepicker" id="departure-date" ng-model="departureDate" ng-change="checkAvailable()">
            </div>
          </div>
        </div>
        
        <hr>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>RM TYPE</th>
                  <th>AVAILABLE RM.</th>
                  <th>ACCOMODATION</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="data in availableRooms">
                  <td class="uppercase">{{ data.RoomType.name }}</td>
                  <td>{{ data.Room }}</td>
                  <td class="uppercase">{{ data.Accomodation.name }}</td>
                  <td>
                    <button class="btn btn-primary btn-sm btn-block" ng-click="bookNow(data)">BOOK NOW!</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>

<?php echo $this->element('hotel/bookings/book/book-now-modal') ?>
