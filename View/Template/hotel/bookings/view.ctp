<link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/css.css">

<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW BOOKING</div>
  <div class="panel-body" style="padding-top:0px">
		<form id="form">
  		<div class="row">
  			<h3 class="table-form-title" style="margin:0px 0px 10px 0px">GUEST INFORMATION</h3>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>Title</label>
            <select class="form-control" ng-model="data.Reservation.title" data-validation-engine="validate[required]" ng-change="changeTitle(data.Reservation.title,'')" ng-init="changeTitle(data.Reservation.title,'title')">
              <option value="null"></option>
              <option value="Mr.">Mr.</option>
              <option value="Ms.">Ms.</option>
              <option value="Mrs.">Mrs.</option>
            </select>
  				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>Last Name <i class="required">*</i></label>
  					<input type="text" class="form-control " ng-model="data.Reservation.lastName" data-validation-engine="validate[required]">
  				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>First Name <i class="required">*</i></label>
  					<input type="text" class="form-control " ng-model="data.Reservation.firstName" data-validation-engine="validate[required]">
  				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>Middle Name</label>
  					<input type="text" class="form-control " ng-model="data.Reservation.middleName">
  				</div>
  			</div>
  			
  			<div class="col-md-12">
  				<div class="form-group">
  					<label>Address <i class="required">*</i></label>
  					<input type="text" class="form-control " ng-model="data.Reservation.address" data-validation-engine="validate[required]">
  				</div>
  			</div>
  		</div>
		
  		<div class="row">
  			<h3 class="table-form-title">STAY INFORMATION</h3>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>Arrival <i class="required">*</i></label>
  					<input type="text" class="form-control" ng-model="data.Reservation.arrival" data-validation-engine="validate[required]" disabled>
  				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>Departure <i class="required">*</i></label>
  					<input type="text" class="form-control" ng-model="data.Reservation.departure" data-validation-engine="validate[required]" ng-readonly="true">
  				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>Night(s) <i class="required">*</i></label>
  					<input type="text" class="form-control" ng-model="data.Reservation.nights" disabled>
  				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
  					<label>Booking Source <i class="required">*</i></label> 
  					<select class="form-control" ng-model="data.Reservation.bookingSource" ng-options="opt.id as opt.value for opt in bookingSources" data-validation-engine="validate[required]">
            </select>
  				</div>
  			</div>
  		</div>

      <div class="row">
        <h3 class="table-form-title">CONTACT INFORMATION</h3>
        <div class="col-md-3">
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" ng-model="data.Reservation.email">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" ng-model="data.Reservation.phone">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Mobile <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Reservation.mobile" data-validation-engine="validate[required]">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Fax</label>
            <input type="text" class="form-control" ng-model="data.Reservation.fax">
          </div>
        </div>
      </div>
		
      <div class="row">
        <h3 class="table-form-title">IDENTITY INFORMATION</h3>
        <div class="col-md-3">
          <div class="form-group">
            <label>Identity Type</label>
            <select class="form-control" ng-model="data.Reservation.identity">
              <option value="">Select Identity Type</option>
              <option value="Drivers License">Drivers License</option>
              <option value="Passport">Passport</option>
            </select>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>ID Number</label>
            <input type="text" class="form-control" ng-model="data.Reservation.identityIdNumber">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Nationality</label>
            <input type="text" class="form-control" ng-model="data.Reservation.nationality" ng-init="data.Reservation.nationality = 'Filipino'" data-validation-engine="validate[required]">
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="form-group">
            <label>Gender</label>
            <select class="form-control" ng-model="data.Reservation.gender" ng-init="changeTitle(data.Reservation.gender,'gender')" ng-change="changeTitle(data.Reservation.gender,'')">
              <option value="null"></option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
          </div>
        </div>
      </div>
  		
      <div class="row">
        <h3 class="table-form-title">BILLING INFORMATION</h3>
        <div class="col-md-4">
          <div class="form-group">
            <label>Bill To</label>
            <select class="form-control" ng-model="data.Reservation.billTo">
              <option value="company">Company</option>
              <option value="group owner">Group Owner</option>
              <option value="guest">Guest</option>
            </select>
          </div>
        </div>
      </div>
  		
      <div class="row">
        <h3 class="table-form-title">OTHER INFORMATION</h3>
        <div class="col-md-4">
          <div class="form-group">
            <label>Company</label>
              <input id="searchFiled_TXT" class="input form-control " ng-keydown="autoCompleteInput()" ng-model="data.Company.name" ng-init="search()" ng-change="getCompany()" ng-keyup="autoCompleteInput()"/>
          </div>
        </div>

        <div class="col-md-8">
          <div class="form-group">
            <label>Company Contact Person</label>
            <input type="text" class="form-control " ng-model="data.Company.contactPersonName">
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" ng-model="data.Company.email">
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="form-group">
            <label>Mobile</label>
            <input type="text" class="form-control" ng-model="data.Company.mobile">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" ng-model="data.Company.phone">
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Fax</label>
            <input type="text" class="form-control" ng-model="data.Company.fax">
          </div>
        </div>
        
        <div class="col-md-12">
          <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control " ng-model="data.Company.address">
          </div>
        </div>
      </div>
  		
  		<div class="row">
  			<h3 class="table-form-title">ROOMS INFORMATION</h3>
  			<div class="col-md-3">
  				<a href="javascript:void(0)" class="btn btn-primary btn-block btn-xs {{ data.Reservation.status=='expired'? 'disabled':'' }}" ng-click="selectAvailableRoom()" >SELECT AVAILABLE ROOMS</a>
  			</div>
  			
  			<div class="clearfix"></div>
  			<hr>
        
  			<div style="padding:0px 15px 0px 15px">
  				<table class="table table-bordered table-striped center vcenter">
  					<thead>
  						<tr>
  							<th class="w30px"></th>
  							<th class="w60px">RM #</th>
  							<th>RM TYPE</th>
  							<th>ACCOMODATION</th>
                <th class="w120px text-right">RATE</th>
  							<th class="w120px">STATUS</th>
  							<th class="w90px"></th>
  						</tr>
  					</thead>
  					<tbody>
  						<tr ng-repeat="room in data.ReservationRoom">
  							<td>{{ $index + 1 }}</td>
  							<td class="">{{ room.Room.name }}</td>
  							<td class="">{{ room.Room.RoomType.name }}</td>
  							<td class="">{{ room.Room.Accomodation.name }}</td>
                <th class="text-right">{{ room.Room.RoomType.rate * (room.extensions + room.nights) | currency:'PHP ' }}</th>
  							<td class="">
                  <span class="label label-success" ng-init="data.Reservation.checked='true'" ng-if="room.checkedIn">CHECKED IN</span>
                  <span class="label label-warning" ng-if="!room.checkedIn && room.status==''">PENDING</span>
                  <span class="label label-warning" ng-if="room.checkedIn && room.close">CHECKED OUT</span>
                  <span class="label label-danger" ng-if="!room.checkedIn && room.status=='expired'">EXPIRED</span>
                </td>
  							<td>
  								<div class="btn-group btn-group-xs" ng-if="room.checkedIn">
  									<a ng-click="viewFolio(room.Folio.Guest.id)" class="btn btn-warning"><i class="fa fa-eye"></i></a>
  									<a ng-click="editFolio()" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <a ng-click="deleteFolio(room.Folio)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
  								</div>

                  <div class="btn-group btn-group-xs" ng-if="!room.checkedIn">
                    <a ng-click="checkInReservationRoom(room)" class="btn btn-success {{ room.status=='expired'||data.Reservation.arrivalNot? 'disabled':'' }}"><i class="fa fa-check"></i></a>
                    <a ng-click="deleteReservationRoom(room)" class="btn btn-danger {{ room.status=='expired'||data.Reservation.arrivalNot? 'disabled':'' }}"><i class="fa fa-trash"></i></a>
                  </div>
  							</td>
  						</tr>
  					</tbody>
  				</table>	
  			</div>
		  </div>
		
		</form>
		<hr>

  	  <div class="row">
      <div class="col-md-12">
        <div class="btn-group btn-group-sm pull-right btn-min">
          <button class="btn btn-danger btn-sm btn-min" ng-click="deleteReservation(data.Reservation)"><i class="fa fa-trash"></i> DELETE</button>
          <button class="btn btn-primary btn-sm btn-min" ng-click="updatebooking()"><i class="fa fa-save"></i> UPDATE</button>
        </div>
       </div> 
  	  </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo $this->base ?>/assets/plugins/jquery-ui-autocomplete/jquery-ui.min.js"></script>

<?php echo $this->element('hotel/bookings/view/check-in-modal') ?>
<?php echo $this->element('hotel/bookings/view/view-folio-modal') ?>
<?php echo $this->element('hotel/bookings/add/select-room-modal') ?>
