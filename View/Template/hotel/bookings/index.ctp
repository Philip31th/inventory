<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> BOOKINGS</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-change="load({ search: searchTxt })">
        </div> 
      </div>

      <div class="col-md-3">
        <a href="#/hotel/booking/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> NEW BOOKING</a>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover center vcenter">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th class="text-left">NAME</th>
              <th class="w60px">ROOMS</th>
              <th class="w90px">ARRIVAL</th>
              <th class="w90px">DEPARTURE</th>
              <th class="w90px">EXTENSIONS</th>
              <th class="w150px">TYPE</th>
              <th class="w120px">STATUS</th>
              <th class="w70px"></th>
            </tr>
          </thead>
          <tbody>
            <tr class="{{ booking.visible? '' : 'danger' }}" ng-repeat="booking in bookings">
              <td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
              <td class="uppercase text-left">{{ booking.name }}</td>
              <td class="uppercase">{{ booking.rooms }}</td>
              <td class="uppercase">{{ booking.arrival }}</td>
              <td class="uppercase">{{ booking.departure }}</td>
              <td class="">{{ booking.extensions }} {{ booking.extensions!=''?'day(s)':''}} </td>
              <td class="uppercase">{{ booking.source }}</td>
              <td class="uppercase">
                <span class="label label-danger" ng-if="booking.rooms <= 0">INVALID BOOKING</span>
                <span class="label label-warning" ng-if="booking.pendingRooms > 0 && booking.rooms > 0 && booking.status==''">{{ booking.pendingRooms }} PENDING ARRIVAL</span>
                <span class="label label-success" ng-if="booking.pendingRooms == 0 && booking.rooms > 0">CHECKED IN</span>
                <span class="label label-danger" ng-if="booking.pendingRooms > 0 && booking.rooms > 0 && booking.status=='expired'">{{ booking.pendingRooms }} EXPIRED RESERVATION</span>
              </td>
              <td>
                <div class="btn-group btn-group-xs">
                  <a href="#/hotel/booking/view/{{ booking.id }}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                  <a class="btn btn-danger" ng-click="remove(booking)"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    <div class="clearfix"></div>

	</div>
</div>
