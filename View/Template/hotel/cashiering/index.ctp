<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> CASHIERING</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
         <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ search: searchTxt })">
        </div> 
      </div>
      
      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover center vcenter">
          <thead>
            <tr>
              <th class="w30px"></th>
              <th class="w70px">FOLIO #</th>
              <th>GUEST NAME</th>
              <th class="w200px">ROOM #</th>
              <th class="text-right w90px">ARRIVAL</th>
              <th class="text-right w90px">DEPARTURE</th>
              <th class="text-right w90px">EXTENSIONS</th>
              <th class="w120px text-right">AMOUNT</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="cashier in cashierings">
              <td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
              <td>{{ cashier.code }}</td>
              <td class="uppercase">{{ cashier.name }}</td>
              <td class="uppercase">RM. {{ cashier.room }} - {{ cashier.type }}</td>
              <td class="text-right uppercase">{{ cashier.arrival }}</td>
              <td class="text-right uppercase">{{ cashier.departure }}</td>
              <td class="text-right">{{ cashier.extended != ''? cashier.extended + ' ' + 'day(s)':'' }}</td>
              <th class="text-right">{{ cashier.Transactions.amount | currency:'PHP ' }}</th>
              <td>
                <div class="btn btn-group btn-xs">
                 <?php if (hasAccess('hotel/cashiering/view', $currentUser)): ?>
                  <a href="#/hotel/cashiering/view/{{ cashier.id }}" class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a>
                 <?php endif ?>
                 <?php if (hasAccess('hotel/cashiering/soa', $currentUser)): ?>
                  <a href="#/hotel/cashiering/soa/{{ cashier.id }}" class="btn btn-primary btn-xs"><i class="fa fa-file-text-o"></i></a>
                 <?php endif ?>
                </div> 
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

     <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'bar',page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
</div>