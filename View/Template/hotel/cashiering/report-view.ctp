<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW CASHIERING REPORT</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Folio #:</dt>
          <dd>{{ data[0].code }}</dd>

          <dt>Guest Name:</dt>
          <dd>{{ data[0].guest }}</dd>

          <dt>Room #:</dt>
          <dd>{{ data[0].room }}</dd>

          <dt>Arrival:</dt>
          <dd>{{ data[0].arrival }}</dd>

          <dt>Departure:</dt>
          <dd>{{ data[0].departure }}</dd>

          <dt>Payment:</dt>
          <dd>{{ data[0].Transaction.paid }}</dd>
        </dl>
      </div>  
    </div>

    <div class="clearfix"></div>
    <hr>

    <div class="row">
        <div class="col-md-3 pull-left">
          <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i> PRINT REPORT</a>
        </div>
    </div>
  </div>
</div>
