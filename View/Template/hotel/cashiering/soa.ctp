<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> STATEMENT OF ACCOUNT</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">       
          <table class="table vcenter">
          <tr>
            <td class="w150px">ACCOUNT NAME:</td>
            <th class="uppercase italic"> {{ data.guest }}</th>
            <td class="w150px">DATE OF ARRIVAL:</td>
            <td class="uppercase italic"> {{ data.arrival }}</td>                   
          <tr>
          </tr>   
            <td class="w150px">ACCOUNT #:</td>
            <td class="uppercase italic">{{ data.code }}</td>
            <td class="w150px">DATE OF DEPARTURE:</td>
            <td class="uppercase italic">{{ data.departure }}</td>
          </tr>
          </table> 
        </div>
      </div> 

        <table class="table table-bordered table-striped table-hover center vcenter">
          <thead>
            <tr>
              <th>PARTICULARS</th>
              <th class="text-right">AMOUNT</th>
            </tr>
          </thead>
          <tbody> 
           <tr ng-repeat="item in data.Transaction.charges">
              <td class="uppercase">{{ item.title }}</td>
              <td class="text-right">{{ item.amount | number:2 }}</td>
           </tr>
            <tr>
             <th class="text-right italic">TOTAL VAT</th>
             <th class="text-right italic">{{  (data.Transaction.amount) * 0.12 | number:2 }}</th>
           </tr>    
            <tr>
              <th class="text-right italic"> TOTAL PAYABLES</th>
              <th class="text-right italic">{{ (data.Transaction.amount) | number:2 }}</th>
            </tr>  
            <tr>
              <th class="text-right italic"> TOTAL PAID AMOUNT</th>
              <th class="text-right italic">{{ (data.Transaction.paid) | number:2 }}</th>
            </tr>  
            <tr>
              <th class="text-right italic"> CURRENT BALANCE</th>
              <th class="text-right italic">{{ (data.Transaction.balance) | number:2 }}</th>
            </tr>
          </tbody>
          <tfoot>       
          </tfoot>
        </table>

      <div class="clearfix"></div>
      <hr>
      <div class="row">
        <div class="pull-right col-md-3">
         <button class="btn btn-danger btn-sm btn-block w300px" ng-click="print()"><i class="fa fa-print"></i> PRINT</button>
        </div>
      </div> 

    </div>
    </div>
</div>
