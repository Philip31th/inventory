<div class="panel panel-primary">
      <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> HOTEL CASHIERING REPORT</div>
      <div class="panel-body">
        <div class="col-md-3">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control search" id="search" placeholder="SEARCH DATE" ng-model="searchTxt" ng-change="load({ date: searchTxt, business: 1  })">
          </div> 
        </div>

        <div class="col-md-3">
          <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="openCalendar($event, 'startTime')"><i class="fa fa-clock-o"></i></button>
            </span>
            <input type="text" disabled class="form-control" datetime-picker="h a" ng-model="dates.startTime" is-open="open.startTime" enable-date="false" timepicker-options="timeOptions" ng-click="openCalendar($event, 'startTime')"/>
          </div> 
        </div>

        <div class="col-md-3">
          <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="openCalendar($event, 'endTime')"><i class="fa fa-clock-o"></i></button>
            </span>
            <input type="text" disabled class="form-control" datetime-picker="h a" ng-model="dates.endTime" is-open="open.endTime" enable-date="false" timepicker-options="timeOptions" ng-click="openCalendar($event, 'endTime')"/>
          </div> 
        </div>

          <div class="col-md-3">         
            <button class="btn btn-primary btn-sm btn-block" ng-click="searchTime()"><i class="fa fa-search"></i> SEARCH</button>
          </div>  

        <div class="clearfix"></div>
        <hr>
        <div class="col-md-12">
          <table class="table table-bordered center">
            <thead>
              <tr>
                <!-- <th class="w30px">#</th> -->
                <th class="w80px">FOLIO #</th>
                <th>COMPANY</th>
                <th>GUEST NAME</th>
                <th>ROOM</th>
                <th class="text-right">PAYMENT</th>
                <th class="text-right">BALANCE</th>
                <th class="text-center">STATUS</th>
                <th class="text-center">REMARKS</th>
                <!-- <th class="w30px"></th> -->
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="cashier in datas">
                <!-- <td>{{ $index + 1 }}</td> -->
                <td>{{ cashier.code }}</td>
                <td class="uppercase">{{ cashier.company }}</td>
                <td class="uppercase">{{ cashier.guest }}</td>
                <td class="uppercase">{{ cashier.room }}</td>
                <td class="uppercase text-right">{{ cashier.Transaction.paid|number:2 }}</td>
                <td class="uppercase text-right">{{ cashier.Transaction.balance|number:2 }}</td>
                <td class="text-center">
                <span class="label label-{{ cashier.status=='IN-HOUSE'? 'success':'info' }}">{{ cashier.status }}</span>
                </td>
                <td class="uppercase">{{ cashier.Transaction.paymentType }} {{ cashier.transferedBills? '- (T)':'' }}</td>
              </tr>
              <tr ng-if="datas==''">
                <td colspan="8">No transactions found.</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="clearfix"></div>
        <hr>

        <div class="col-md-3">
          <table class="table vcenter">
            <thead>
              <tr><th></th></tr>
              <tr>
                <th class="text-center italic" colspan="2">S U M M A R Y</th>
              </tr>
              <tr>  
                <th class="text-right">PAYMENT TYPE</th>
                <th class="text-right">AMOUNT</th>
              </tr>  
            </thead>
            <tbody>
              <tr>
                <td class="text-right">CASH</td>
                <td class="text-right">{{ summary.TotalCash | number: 2 }}</td>
              </tr>
              <tr>
                <td class="text-right">CARD</td>
                <td class="text-right">{{ summary.TotalCard | number: 2 }}</td>
              </tr>
              <tr>
                <td class="text-right">DEBIT</td>
                <td class="text-right">{{ summary.TotalDebit | number: 2 }}</td>
              </tr>
              <tr>
                <td class="text-right">SEND BILL</td>
                <td class="text-right">{{ summary.TotalSendBill | number: 2 }}</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th class="text-right">TOTAL AMOUNT</th>
                <th class="text-right">{{ summary.Total | number: 2 }}</th>
              </tr>
            </tfoot>
          </table>
        </div>    

        <div class="clearfix"></div>
        <hr>

        <div class="col-md-3 pull-right">
          <a href="javascript:void(0)" class="btn btn-danger btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i> PRINT REPORT</a>
        </div>
      </div>
  </div>
</div>
