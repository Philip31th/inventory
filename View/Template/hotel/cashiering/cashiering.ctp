<div>
  <div class="panel panel-primary">
      <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> CASHIERING REPORT</div>
      <div class="panel-body">
        <div class="col-md-4 pull-right">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="strSearch" ng-change="load()">
          </div> 
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="col-md-12">
          <table class="table table-bordered center">
            <thead>
              <tr>
                <th class="w30px"></th>
                <th>NAME</th>
                <th>ROOM</th>
                <th class="text-right">PAYMENT</th>
                <th class="text-right">BALANCE</th>
                <th class="text-center">STATUS</th>
                <!-- <th class="w30px"></th> -->
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="cashier in datas">
                <td>{{ $index + 1 }}</td>
                <td class="uppercase">{{ cashier.guest }}</td>
                <td class="uppercase">RM# {{ cashier.room }} - {{ cashier.type }}</td>
                <td class="uppercase text-right">{{ cashier.Transaction.paid|number:2 }}</td>
                <td class="uppercase text-right">{{ cashier.Transaction.balance|number:2 }}</td>
                <td class="text-center">
                  <span class="label label-success" ng-if="cashier.Transaction.balance == 0">PAID</span>
                  <span class="label label-success" ng-if="cashier.Transaction.balance != 0">UNPAID</span>
                </td>
               <!--  <td>
                  <a href="#/hotel/report/cashiering/view/{{ cashier.id }}" class="btn btn-success btn-xs no-border-radius"><i class="fa fa-eye"></i></a>
                </td> -->
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-md-3 pull-right">
          <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block" onclick="printTable(base + 'print/cashiering-report')"><i class="fa fa-print"></i> PRINT REPORT</a>
        </div>
      </div>
  </div>
</div>
<script>
  $('.datepicker').datepicker({format: 'yyyy-mm-dd', autoclose: true});
</script>