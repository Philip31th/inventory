<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> CASHIERING</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="text-left bg-info" colspan="4">GUEST FOLIO</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td class="w120px bggray">FOLIO #</td>
              <td>{{ data.code }}</td>
              
              <td class="w120px bggray">ROOM #</td>
              <td class="w120px text-right">{{ data.room }}</td>
            </tr>

            <tr>
              <td class="bggray">COMPANY NAME</td>
              <td class="uppercase">{{ data.company }}</td>
              
              <td class="bggray">ROOM TYPE</td>
              <td class="uppercase text-right">{{ data.roomType }}</td>
            </tr>

            <tr>
              <td class="bggray">GUEST'S NAME</td>
              <th class="uppercase">{{ data.guest }}</th>
              
              <td class="bggray">ROOM RATE</td>
              <td class="text-right">{{ data.roomRate | currency:'PHP '  }}</td>
            </tr>

            <tr>
              <td class="bggray">ADDRESS</td>
              <td class="uppercase">{{ data.address }}</td>
              
              <td class="bggray">ARRIVAL</td>
              <td class="text-right">{{ data.arrival }}</td>
            </tr>

            <tr>
              <td class="bggray">VEHICLE MADE</td>
              <td class="uppercase">{{ data.vehicleMade }}</td>

              <td class="bggray">DEPARTURE</td>
              <td class="text-right">{{ data.departure }}</td>
            </tr>

            <tr>
              <td class="bggray">VEHICLE COLOR</td>
              <td>{{ data.vehiclecolor }}</td>

              <td class="bggray">SOURCE</td>
              <td class="text-right uppercase">{{ data.bookingSource }}</td>
            </tr>

            <tr>
              <td class="bggray">VEHICLE PLATE NO.</td>
              <td>{{ data.vehicleplateNo }}</td>
              
              <td class="bggray">STATUS</td>
              <td class="uppercase text-right">
                <span class="label label-{{ date.closed? 'danger' : 'success' }}">{{ data.closed? 'CHECKED OUT' : 'CHECKED IN' }}</span>
              </td>
            </tr>

             <tr>
              <td class="bggray">CONTACT #</td>
              <td>{{ data.mobile }}</td>
              
              <td class="text-right">TAXABLE AMOUNT</td>
              <th class="uppercase text-right">
                {{ data.taxableAmount | number:2  }}
              </th>

            </tr>

            <tr>
              <td class="bggray">SPECIAL REQUEST</td>
              <td class="uppercase">
                {{ data.specialRequest }}
              </td>
              
              <td class="text-right">TAX RATE</td>
              <td class="uppercase text-right">1.12</td>
            </tr>

            <tr>
              <td class="bggray">REMARKS</td>
              <td class="uppercase">
                {{ data.remarks }}
              </td>

              <td class="text-right">VAT</td>
              <th class="uppercase text-right">
                {{ data.vat | number:2   }}
              </th>
            </tr>
            <tr>
              <td></td>
              <td class="uppercase">
              </td>

              <td class="text-right">TOTAL CHARGES</td>
              <th class="uppercase text-right">{{ (data.Transactions.totalAmount+data.ReceivedTransactions.totalAmount) | number:2 }}</th>           
            </tr>

            <tr>
              <td class="text-right" colspan="3">TOTAL DISCOUNTS</td>
              <th class="uppercase text-right">{{ (data.Transactions.totalDiscount+data.ReceivedTransactions.totalDiscount) | number: 2 }}</th>
            </tr>

            <tr>
              <td class="text-right" colspan="3">DISCOUNT TYPE</td>
              <td class="uppercase text-right">{{ data.discountTypes }}</td>
              <!-- <td class="uppercase text-right">{{   }}</td> -->
            </tr>

            <tr>
              <td class="text-right" colspan="3">DISCOUNT PERCENTAGE</td>
              <td class="uppercase text-right">{{  (data.Transactions.discountPercent+data.ReceivedTransactions.discountPercent) }}</td>
            </tr>

            <tr>
              <td class="text-right" colspan="3">DEPOSITS RECEIVED</td>
              <th class="uppercase text-right">{{ (data.Transactions.totalPayment+data.ReceivedTransactions.totalPayment) | number:2  }}</th>
            </tr>

            <tr>
              <td class="text-right" colspan="3">BALANCE</td>
              <th class="uppercase text-right">{{ (data.Transactions.totalBalance+data.ReceivedTransactions.totalBalance) | number:2 }}</th>
            </tr>
          </tbody>
        </table>
      </div>

    <div ng-if="!data.transferedBills">
      <!-- CHARGES -->
      <div class="col-md-12" >
        <table class="table table-bordered table-striped table-hover vcenter center">
          <thead>
            <tr>
              <th class="bg-info text-left" colspan="10">CHARGES</th>
            </tr>
            <tr>
              <th class="text-center w30px">CODE</th>
              <th>BUSINESS</th>
              <th>PARTICULARS</th>
              <th class="text-right w100px">AMOUNT</th>
              <th class="text-right w100px">DISCOUNT</th>
              <th class="text-right w100px">INITIAL PAYMENT</th>
              <th class="text-right w100px">PAYMENT</th>
              <th class="text-right w100px">BALANCE</th>
              <th class="w120px">STATUS</th> 
              <th class="w120px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="transaction in data.Transactions.charges">
              <td>{{  transaction.code }}</td>
              <td class="uppercase">{{  transaction.business }}</td>
              <td class="uppercase text-center">{{  transaction.particulars }}</td>
              <th class="text-right">{{ transaction.totalAmount   | number: 2 }}</th>
              <th class="text-right">{{ transaction.totalDiscount | number: 2 }}</th>
              <th class="text-right">{{ transaction.totalInitialPayment | number: 2 }}</th>
              <th class="text-right">{{ transaction.totalPayment  | number: 2 }}</th>
              <th class="text-right">{{ transaction.totalBalance  | number: 2 }}</th>
              <td>
                <span class="label label-{{ transaction.totalBalance>0? 'danger' : 'success' }}">{{ transaction.totalBalance>0? 'PENDING PAYMENT' : 'TRANSACTION PAID' }}</span>
              </td> 
              <td>
                <div class="btn-group btn-group-xs">
                  <button disabled class="btn btn-primary" ng-if="transaction.totalBalance <= 0"><i class="fa fa-plus"></i></button>  

                  <!-- if transaction pending --> 
                  <button  class="btn btn-primary" ng-if="transaction.totalBalance > 0" title="CLICK TO ADD PAYMENT" ng-click="addPayment(transaction)"><i class="fa fa-plus"></i></button> 
            
                   <button class="btn btn-success" title="CLICK TO VIEW TRANSACTION" ng-click="viewTransactions(transaction)"><i class="fa fa-eye"></i></button>

                  <?php if (hasAccess('hotel/cashiering/delete', $currentUser)): ?>
                    <a class="btn btn-danger" title="DELETE" ng-if="transaction.deletable" ng-click="removeCharge(transaction)"><i class="fa fa-trash"></i></a>
                  <?php endif ?>

                </div>
              </td>
            </tr>
          </tbody>
          <tfoot class="bg-info">
            <th colspan="3" class="text-right">TOTAL</th>
            <th class="text-right">{{ data.Transactions.totalAmount | number: 2}}</th>
            <th class="text-right">{{ data.Transactions.totalDiscount | number: 2}}</th>
            <th class="text-right">{{ data.Transactions.totalInitialPayment | number: 2}}</th>
            <th class="text-right">{{ data.Transactions.chargesTotalPayment | number: 2 }}</th>
            <th class="text-right">{{ data.Transactions.totalBalance | number: 2 }}</th>
            <th class="text-right"></t>
            <td></td>
          </tfoot>
        </table>
      </div>

      <!-- RECEIVED CHARGES -->
      <div class="col-md-12" ng-if="data.receivedTransactions">
        <table class="table table-bordered table-striped table-hover vcenter center">
          <thead>
            <tr>
              <th class="bg-info text-left" colspan="11">RECEIVED CHARGES</th>
            </tr>
            <tr>
              <th class="text-center w30px">CODE</th>
              <th class="text-right w80px">FOLIO #</th>
              <th class="text-right">BUSINESS</th>
              <th>PARTICULARS</th>
              <th class="text-right w100px">AMOUNT</th>
              <th class="text-right w100px">DISCOUNT</th>
              <th class="text-right w100px">INITIAL PAYMENT</th>
              <th class="text-right w100px">PAYMENT</th>
              <th class="text-right w100px">BALANCE</th>
              <th class="w120px">STATUS</th> 
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <!--  main charges -->
            <tr ng-repeat="rtransaction in data.ReceivedTransactions.charges">
              <td>{{  rtransaction.code }}</td>
              <td>{{ rtransaction.folioCode  }}</td>
              <td class="uppercase">{{  rtransaction.business }}</td>
              <td class="uppercase">{{  rtransaction.particulars }}</td>
              <th class="text-right">{{ rtransaction.totalAmount | number: 2 }}</th>
              <th class="text-right">{{ rtransaction.totalDiscount | number: 2 }}</th>
              <th class="text-right">{{ rtransaction.TotalChildFolioPayment | number: 2 }}</th>
              <th class="text-right">{{ rtransaction.totalPayment  | number: 2 }}</th>
              <th class="text-right">{{ rtransaction.totalBalance  | number: 2 }}</th>
              <td>
                <span class="label label-{{ rtransaction.totalBalance>0? 'danger' : 'success' }}">{{ rtransaction.totalBalance>0? 'PENDING PAYMENT' : 'TRANSACTION PAID' }}</span>
              </td> 
              <td>
                <div class="btn-group btn-group-xs">
                  <!-- if transaction paid -->
                  <a disabled class="btn btn-primary" ng-if="rtransaction.totalBalance <= 0" title="CLICK TO ADD PAYMENT"><i class="fa fa-plus"></i></a>

                  <!-- if transaction pending -->
                  <a class="btn btn-primary" ng-if="rtransaction.totalBalance > 0" title="CLICK TO ADD PAYMENT" ng-click="addPayment(rtransaction)"><i class="fa fa-plus"></i></a>
                  
                  <a class="btn btn-success" title="CLICK TO VIEW TRANSACTION" ng-click="viewPayment(rtransaction)"><i class="fa fa-eye"></i></a>
                </div>
              </td>
            </tr>
          </tbody>
          <tfoot class="bg-info">
            <th colspan="4" class="text-right">TOTAL</th>
            <th class="text-right">{{ data.ReceivedTransactions.totalAmount | number: 2}}</th>
            <th class="text-right">{{ data.ReceivedTransactions.totalDiscount | number: 2}}</th>
            <th class="text-right">{{ data.ReceivedTransactions.totalInitialPayment | number: 2 }}</th>
            <th class="text-right">{{ data.ReceivedTransactions.totalPayment | number: 2 }}</th>
            <th class="text-right">{{ data.ReceivedTransactions.totalBalance | number: 2 }}</th>
            <th class="text-right">{{ }}</th>
            <td></td>
          </tfoot>  
        </table>
      </div>

      <!-- PAYMENTS -->
      <div class="col-md-12" ng-if="data.cPayments.payments != null">
        <table class="table table-bordered table-striped table-hover vcenter center">
          <thead>
            <tr> 
              <th class="bg-info text-left" colspan="8">PAYMENTS</th>
            </tr>
            <tr>
              <th class="text-center w30px">CHARGE CODE</th>
              <th class="text-center w90px">OR #</th>
              <th class="text-right w150px">DATE</th>
              <th class="text-right w90px">BUSINESS</th>
              <th class="text-center w150px">PAYMENT TYPE</th>
              <th class="text-right w120px">AMOUNT</th>
              <th class="text-right w120px">CHANGE</th>
              <th class="w50px"></th>
            </tr>
          </thead>

          <tbody>
            <tr ng-repeat="cpayment in data.cPayments.payments">
              <td>{{ cpayment.code }}</td>
              <td class="uppercase">{{ cpayment.orNumber }}</td>
              <td class="text-right uppercase">{{ cpayment.date }}</td>
              <td class="text-right uppercase">{{ cpayment.business }}</td>
              <th class="text-center uppercase">{{ cpayment.type }}</th>
              <th class="text-right">{{ cpayment.amount - cpayment.change | number: 2 }}</th>
              <th class="text-right">{{ cpayment.change | number: 2 }}</th>
              <td class="text-center">
                  <a class="btn btn-danger btn-xs" title="DELETE" ng-if="!cpayment.paidInOtherBusiness" ng-click="removePayment(cpayment)"><i class="fa fa-trash"></i></a>
              </td>  
            </tr>
          </tbody>
          <tfoot class="bg-info">
            <th colspan="5" class="text-right">TOTAL</th>
            <th class="text-right">{{ data.Transactions.totalPayment + data.ReceivedTransactions.totalPayment | number:2 }}</th>
            <th class="text-right">{{ data.Transactions.totalChange + data.ReceivedTransactions.totalChange | number:2 }}</th>
            <td></td>
          </tfoot>
        </table>
      </div>
  </div>    
    </div>

    <div class="clearfix"></div>
    <hr>

    <div class="row">
      <div class="col-md-12">

        <div class="btn-group btn-group-sm pull-left w200px" ng-if="!data.transferedBills">
          <button type="button" class="btn btn-danger btn-min"  ng-click="print()"><i class="fa fa-print"></i> PRINT</button>
        </div>

        <div class="btn-group btn-group-sm pull-left" ng-if="data.transferedBills">
          <label>Status:</label> 
          <span class="label label-warning">BILLS TRANSFERED</span>
        </div>


        <div class="btn-group btn-group-sm pull-right btn-min">
          <button type="button" ng-if="data.History" ng-if="!data.transferedBills" class="btn btn-primary btn-min" ng-click="viewHistory(data.guestId)"><i class="fa fa-eye"></i> VIEW HISTORY</button>

          <button type="button" ng-if="!data.transferedBills" class="btn btn-success btn-min" ng-click="addCharges()"><i class="fa fa-plus"></i> ADD CHARGES</button>

          <button type="button" ng-if="!data.transferedBills && data.Transactions.totalBalance>0 || data.ReceivedTransactions.totalBalance>0" class="btn btn-info btn-min" ng-click="transferBill()"><i class="fa fa-share"></i> TRANSFER BILL</button>

          <button type="button" class="btn btn-primary btn-min {{ data.Transactions.totalBalance+data.ReceivedTransactions.totalBalance > 0 && !data.transferedBills? 'disabled':''}}" ng-click="checkOut()"><i class="fa fa-check"></i> CHECK OUT</button>
        </div>        
      </div>
    </div>
    
  </div>
</div>

<?php echo $this->element('hotel/cashiering/view/add-payment-modal') ?>
<?php echo $this->element('hotel/cashiering/view/add-charge-modal') ?>
<?php echo $this->element('hotel/cashiering/view/change-discount-modal') ?>
<?php echo $this->element('hotel/cashiering/view/view-transaction-modal') ?>
<?php echo $this->element('hotel/cashiering/view/transfer-bill-modal') ?>
<?php echo $this->element('hotel/cashiering/view/view-history-modal') ?>
<?php echo $this->element('hotel/cashiering/view/view-transaction-history-modal') ?>

