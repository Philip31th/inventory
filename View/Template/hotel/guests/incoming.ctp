<div>
	<div class="panel panel-primary">
	    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> INCOMING GUESTS</div>
	    <div class="panel-body">
        <div class="col-md-4 pull-right">
          <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="strSearch" ng-change="load()">
          </div> 
        </div>
        <div class="clearfix"></div>
        <hr>
  			<div class="col-md-12">
  				<table class="table table-bordered center">
  					<thead>
  						<tr>
  							<th class="w30px"></th>
  							<th class="w150px">RM #</th>
  							<th>GUEST NAME</th>
  							<th class="w90px">ARRIVAL</th>
  						</tr>
  					</thead>
  					<tbody>
  						<tr ng-repeat="arrival in datas">
  							<td>{{ $index + 1 }}</td>
  							<td class="uppercase">RM. {{ arrival.code }} - {{ arrival.type }}</td>
  							<td class="uppercase">{{ arrival.guest }}</td>
  							<td class="uppercase">{{ arrival.arrival }}</td>
  						</tr>
  					</tbody>
  				</table>
  			</div>
	    </div>
	</div>
</div>

<script>
$('.datepicker').datepicker({format: 'yyyy-mm-dd', autoclose: true});
</script>
