<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW GUEST </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Guest:</dt>
          <dd>{{ data[0].name }}</dd>

          <dt>Room #:</dt>
          <dd>{{  data[0].room }}</dd>

          <dt>Room Type:</dt>
          <dd>{{  data[0].room_type }}</dd>

          <dt>Arrival:</dt>
          <dd>{{ data[0].arrival }}</dd>

          <dt>Departure:</dt>
          <dd>{{ data[0].departure }}</dd>
        </dl>
      </div>

      <div class="clearfix"></div>
      <hr>

<!--       <div class="col-md-3 pull-left">
       <button class="btn btn-success btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i>PRINT</button>
      </div> -->

     <!--  <div class="col-md-3 pull-right">
        <button class="btn btn-danger btn-sm btn-block" ng-click="remove(data.Guest)"><i class="fa fa-trash"></i> DELETE</button>
      </div> -->
      <?php if (hasAccess('hotel/guests/edit', $currentUser)): ?>
        <div class="col-md-3 pull-right">
          <a href="#/hotel/guestslists/edit/{{ data[0].id }}" class="btn btn-primary btn-sm btn-block"><i class="fa fa-pencil"></i> EDIT</a>
        </div>
      <?php endif ?>  
    </div>
  </div>
</div>
