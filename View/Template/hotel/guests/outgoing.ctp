<div>
	<div class="panel panel-primary">
	    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> OUTGOING GUESTS</div>
	    <div class="panel-body">
        <div class="col-md-4 pull-right">
          <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="strSearch" ng-change="load()">
          </div> 
        </div>
        <div class="clearfix"></div>
        <hr>
  			<div class="col-md-12">
  				<table class="table table-bordered center">
  					<thead>
  						<tr>
  							<th class="w30px"></th>
  							<th class="w200px">RM #</th>
  							<th>GUEST NAME</th>
                <th class="w90px">DEPARTURE</th>
                <th class="w90px">EXTENSION</th>
  							<!-- <th class="w70px"></th> -->
  						</tr>
  					</thead>
  					<tbody>
  						<tr ng-repeat="departure in datas">
  							<td>{{ $index + 1 }}</td>
  							<td class="uppercase">RM. {{ departure.code }} - {{ departure.type }}</td>
  							<td class="uppercase">{{ departure.guest }}</td>
                <td class="uppercase">{{ departure.departure }}</td>
                <td class="">{{ departure.extended }}</td>
  						<!-- 	<td>
  								<div class="btn-group btn-group-xs">
  									<a class="btn btn-success" alt="VIEW"><i class="fa fa-eye"></i></a>
                    <a class="btn btn-warning" alt="CHECK-OUT"><i class="fa fa-check"></i></a>
  								</div>
  							</td> -->
  						</tr>
  					</tbody>
  				</table>
  			</div>
	    </div>
	</div>
</div>
