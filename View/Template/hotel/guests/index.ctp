<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> GUEST LIST</div>
  <div class="panel-body">
  	<div class="row">
			<div class="col-md-4 pull-right">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
					<input date type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-change="load({ search: searchTxt })">
				</div> 
			</div>

<!--       <div class="col-md-3 pull-left">
         <button class="btn btn-danger btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i>PRINT</button>
      </div> -->

			<div class="clearfix"></div>
			<hr>

			<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover center">
					<thead>
						<tr>
							<th class="w30px">#</th>
							<th>RM #</th>
							<th>RM TYPE</th>
							<th>GUEST</th>
							<th>ARRIVAL</th>
							<th>DEPARTURE</th>
							<!-- <th class="w90px"></th> -->
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="guest in guests">
							<td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
							<td>{{ guest.room }}</td>
							<td class="uppercase">{{ guest.type }}</td>
							<td class="uppercase">{{ guest.name }} </td>
							<td class="uppercase">{{ guest.arrival }}</td>
							<td class="uppercase">{{ guest.departure }}</td>
							<!-- <td class="text-center">
							  <div class="btn-group btn-group-xs">
							    <a href="#/hotel/guestslists/view/{{ guest.id }}" class="btn btn-success"><i class="fa fa-eye"></i></a>

                   <?php if (hasAccess('hotel/guests/edit', $currentUser)): ?>
                      <a href="#/hotel/guestslists/edit/{{ guest.id }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                   <?php endif ?>

							  </div>
							</td> -->
						</tr>
					</tbody>
				</table>
			</div>
  	</div>

    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    <div class="clearfix"></div>
    
  </div>
</div>