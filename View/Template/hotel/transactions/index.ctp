<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> HOTEL TRANSACTIONS</div>
  <div class="panel-body">
    <div class="row>">
      <div class="col-md-12">
        <div class="col-md-4 pull-right">
          <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
            <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load( {code: 'hotel', search: searchTxt })">
          </div> 
        </div>
        <div class="col-md-3">
          <a href="#/hotel/transactions/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> NEW TRANSACTIONS</a>
        </div>
        </div>
      
      <div class="clearfix"></div>
      
      <hr>
      <div class="col-md-12">
        <table class="table table-bordered center">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th>CODE</th>
              <th>PARTICULARS</th>
              <th class="text-right">AMOUNT</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="transaction in transactions" ng-if="transactions != '' ">
              <td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
              <td class="uppercase">{{ transaction.code }}</td>
              <td class="uppercase">{{ transaction.particulars }}</td>
              <td class="uppercase text-right">{{ transaction.amount|number:2 }}</td>
              <td>
                <div class="btn-group btn-group-xs">
                  <a href="#/hotel/transactions/view/{{ transaction.id }}" class="btn btn-success " ><i class="fa fa-eye"></i></a>
                  <a href="#/hotel/transactions/edit/{{ transaction.id}}" class="btn btn-primary "><i class="fa fa-pencil"></i></a>
  			          <a href="javascript:void(0)" ng-click="remove(transaction)" class="btn btn-danger " title="DELETE"><i class="fa fa-trash"></i></a>
                </div>
              </td>
            </tr>

            <tr ng-if="transactions == '' ">
              <td colspan="5">No record/s found.</td>
            </tr>

          </tbody>
        </table>
      </div>
    </div> 

    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'hotel', page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>

    <div class="clearfix"></div>
     
  </div>
</div>