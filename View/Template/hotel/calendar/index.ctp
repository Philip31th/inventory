<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> CALENDAR VIEW <i class="uppercase pull-right">({{ startDate|dateFormat:'MMM.dd.yyyy' }} - {{ endDate|dateFormat:'MMM.dd.yyyy' }})</i></div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
          <input date type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="startDate" ng-change="load(startDate)">
        </div> 
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-2 pull-left">
        <button class="btn btn-primary btn-sm btn-block" ng-click="prev()">« PREV</button>
      </div>
      <div class="col-md-2 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="next()">NEXT »</button>
      </div>

      <div class="clearfix" style="margin-bottom:15px"></div>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover center vcenter">
          <thead>
            <tr>
              <th class="w30px"></th>
              <th class="w60px">RM #</th>
              <th>RM TYPE</th>
              <th class="w90px uppercase" ng-repeat="date in dates">{{ date.date | dateFormat:'MMM.dd' }}</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="room in datas">
              <td>{{ $index+1 }}</td>
              <td>{{ room.name }}</td>
              <td class="uppercase">{{ room.type }}</td>
              <td ng-init="status = room.availability[ room.id + '-' + (date.date|dateFormat:'yyyy-MM-dd') ]['status']" ng-click="cellClick(status)" class="w90px uppercase cellClick" ng-repeat="date in dates">
                <span ng-if="status == 'occupied'" class="label label-danger italic">
                  <a href="#/hotel/cashiering/view/{{ room.availability[ room.id + '-' + (date.date|dateFormat:'yyyy-MM-dd') ]['folioId'] }}" style="color: #FFF">{{ status }}</a>
                </span>
                <span ng-if="status == 'reserved'" class="label label-danger italic">{{ status }}</span>
                <!-- ng-click="cellClick(room, date, status) -->
                <span ng-if="status == 'available'" class="label label-success italic ">
                  <a href="#/bookings/book/{{ date.date }}" style="color: #FFF; text-decoration: none">
                    {{ status }}
                  </a>
                </span>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th class="w30px"></th>
              <th class="w60px">RM #</th>
              <th>RM TYPE</th>
              <th class="w90px uppercase" ng-repeat="date in dates">{{ date.date | dateFormat:'MMM.dd' }}</th>
            </tr>
          </tfoot>
        </table>
      </div>

      <div class="col-md-2 pull-left">
        <button class="btn btn-primary btn-sm btn-block" ng-click="prev()">« PREV</button>
      </div>
      <div class="col-md-2 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="next()">NEXT »</button>
      </div>
      <div class="clearfix" style="margin-bottom:5px"></div>
    </div>
  </div>
</div>

<?php echo $this->element('hotel/bookings/book/book-now-modal'); ?>

<style>
  .cellClick {
    cursor: pointer;
  }
  .cellClick:hover {
    background:#428bca !important;
  }
</style>

