<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW LEDGER</div>
  <div class="panel-body">
      <div class="col-md-12">
        <div class="row">

          <div class="col-md-12">
            <dl class="dl-horizontal dl-data dl-bordered">
              <dt>Company:</dt>
              <dd class="uppercase">{{ total.company }}</dd>

              <dt>Total Debit:</dt>
              <dd>{{ total.debit | number:2 }}</dd>

              <dt>Total Credit:</dt>
              <dd>{{ total.payments | number:2 }}</dd>
          </div>

          <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
              <tr>
                <th class="text-center w30px"></th>
                <th class="text-right w150px">DATE</th>
                <th class="text-center">PARTICULARS</th>
                <th class="text-right w100px">AMOUNT</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="item in datas">
                <td>{{ $index + 1 }}</td>
                <td class="text-right">{{ item.date }}</td>
                <td class="text-center uppercase">{{ item.particulars }}</td>
                <td class="text-right">{{ item.amount | number: 2 }}</td>
              </tr>
            </tbody>
            <tfoot>
						  <th class="text-right" colspan="3">TOTAL AMOUNT</th>
						  <th class="text-right">{{ total.payments | number:2 }}</th>
					  </tfoot>
          </table>
        </div>  
      </div>
      
      <!-- <div class="clearfix"></div>
      <hr> -->
      
      <!-- <div class="col-md-3 pull-right">
        <div class="row">
          <button class="btn btn-danger btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i> PRINT</button>
        </div>
      </div> -->
		
    </div>
</div>
