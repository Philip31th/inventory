<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> CITY LEDGER</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load( {search: searchTxt })">
        </div> 
      </div>
      
      <div class="clearfix"></div>  
      
    <div class="col-md-12">
      <hr>
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td class="w30px"></td>
              <th class="text-left">COMPANY</th>
              <th class="text-right">DEBIT</th>
              <th class="text-right">CREDIT</th> 
              <th class="text-right">BALANCE</th> 
              <td class="text-center w30px"></td>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="item in datas.company">
              <td>{{ $index + 1 }}</td>
              <td class="text-left uppercase">{{ item.company }}</td>
              <td class="text-right">{{ item.debit | number: 2 }}</td>
              <td class="text-right">{{ item.credit | number: 2 }}</td>
              <td class="text-right">{{ item.credit - item.debit | number: 2 }}</td>
               <td class="text-center">
                  <div class="btn-group btn-group-xs">
                    <a href="#/hotel/reports/city-ledger/view/{{ item.coy }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                  </div>  
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr class="text-right">
              <th colspan="2" class="text-right">TOTAL</th>
              <th class="text-right">{{ datas.totalDebit | number:2 }}</th>
              <th class="text-right">{{ datas.totalCredit | number:2 }}</th>
              <th class="text-right">{{ datas.totalBalance | number:2 }}</th>
              <th></th>
            </tr>
          </tfoot>
        </table>
    </div>  
    </div>      
      
    <div class="clearfix"></div>
    <hr>

    <div class="col-md-3 pull-right">
      <div class="row">
        <button class="btn btn-danger btn-sm btn-block" ng-click="prints()"><i class="fa fa-print"></i> PRINT</button>
      </div>
    </div>
    
  </div>
</div>      