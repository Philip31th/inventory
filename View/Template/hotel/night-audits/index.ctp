<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NIGHT AUDIT REPORT</div>
  <div class="panel-body">
    <!--HOUSE USE-->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr class="bg-info">
              <td class="w30px">#</td>
              <th class="text-right">HOUSE USE</th>
              <th class="text-right w120px" >NO. OF ROOMS</th>
              <th class="text-right w120px">OCCUPIED</th>
              <th class="text-right w120px">DIR</th>
              <th class="text-right w120px">OUT OF ORDER</th>
              <th class="text-right w200px">TOTAL ROOMS AVAILABLE</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="item in datas.audits" class="text-right">
              <td>{{ $index + 1 }}</td>
              <td>{{ item.room }}</td>
              <td>{{ item.total_rooms }}</td>
              <td>{{ item.total_occupied }}</td>
              <td>{{ item.dirty }}</td>
              <td>{{ item.out_of_order }}</td>
              <td>{{ item.total_available }}</td> 
            </tr>
          </tbody>
          <tfoot>
            <tr class="bg-info">
              <th colspan="2" class="text-right">TOTAL</th>
              <th class="text-right">{{ datas.grandTotalRooms }}</th>
              <th class="text-right">{{ datas.grandTotalOccupied }}</th>
              <th class="text-right">0</th>
              <th class="text-right">0</th>
              <th class="text-right">{{ datas.grandTotalAvailable }}</th>
            </tr> 
          </tfoot>
        </table>
      </div>       
    </div>
      
    <!-- ROOM REVENUE -->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr class="bg-info">
              <td class="w30px">#</td>
              <th class="text-right">ROOM REVENUE</th>
              <th class="text-right w120px">SOLD</th>
              <th class="text-right w120px">RACK</th>
              <th class="text-right w120px">GRO</th>
              <th class="text-right w120px">DISC</th>
              <th class="text-right w200px">REVENUE</th>
            </tr>
          </thead>
          <tbody >
            <tr ng-repeat="item in datas.audits">
              <td class="text-right">{{ $index + 1 }}</td>
              <td class="text-right">{{ item.room }}</td>
              <td class="text-right">{{ item.total_occupied }}</td>
              <td class="text-right">{{ item.rate | number: 2 }}</td>
              <td class="text-right">{{ item.gro | number: 2 }}</td>
              <td class="text-right">{{ item.discount | number: 2 }}</td>
              <td class="text-right">{{ item.revenue | number: 2 }}</td> 
            </tr>
          </tbody>
          <tfoot>
             <tr class="text-right bg-info">
              <th colspan="2" class="text-right">TOTAL</th>
              <th class="text-right">{{ datas.grandTotalOccupied }}</th>
              <th class="text-right"></th>
              <th class="text-right">{{ datas.grandGro | number: 2 }}</th>
              <th class="text-right">{{ datas.grandDisc | number: 2 }}</th>
              <th class="text-right">{{ datas.grandRevenue | number: 2 }}</th>
            </tr>
          </tfoot>
        </table>
      </div>       
    </div>

    <!-- EXTRA PERSON -->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td class="w30px">#</td>
              <th class="text-right">EXTRA PERSON</th>
              <th class="text-right w120px">O</th>
              <th class="text-right w120px">RACK</th>
              <th class="text-right w120px">GRO</th>
              <th class="text-right w120px">DISC</th>
              <th class="text-right w200px">REVENUE</th>
            </tr>
          </thead>
          <tbody >
            <tr class="text-right">
              <td></td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td> 
            </tr>
          </tbody>
          <tfoot>
             <tr class="text-right bg-info">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
          </tfoot>
        </table>
      </div>       
    </div>

    <!-- STATISTICS -->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr class="bg-info">
              <th class="text-right">STATISTICS</th>
              <th class="text-right">DOMESTIC</th>
              <th class="text-right">FOREIGN</th>
              <th class="text-right">ACTUAL</th>
            </tr>           
          </thead>
          <tbody >
            <tr>
              <td>Stay Over</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Individual)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Corporate)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Gift Voucher)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Travel Agency)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Walk-ins</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Complimentary</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>VIP/Confidential</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>&emsp; No Show</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th>&emsp;<i> HouseCount</i></th>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>&emsp; Confirmed Departure</td>
              <td rowspan="2"></td>
              <td rowspan="2"></td>
              <td></td>
            </tr>
            <tr>
              <td>&emsp; Indefinite Departure</td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3">Average Room Rate</td>
              <td class="text-right"></td>
            </tr>
            <tr>
              <td colspan="3">No. of Rooms Sold</td>
              <td class="text-right">{{ datas.grandTotalOccupied }}</td>
            </tr>
            <tr>
              <td colspan="3">No. of Saleable Rooms</td>
              <td class="text-right">{{ datas.grandTotalRooms - datas.grandOoo }}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr class="bg-info">
              <th colspan="3" class="text-right">PERCENTAGE OF OCCUPANCY</th>
              <td class="text-right"></td>
            </tr>
          </tfoot>
        </table>
      </div>       
    </div>

    <div class="col-md-3 pull-right">
      <div class="row">
        <button class="btn btn-danger btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i> PRINT</button>
    </div>

    </div>
  </div>
</div>