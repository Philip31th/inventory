<div>
	<div class="panel panel-primary">
	    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> CHARGES</div>
	    <div class="panel-body">
			<div class="col-md-4 pull-right">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
					<input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="strSearch" ng-keyup="search(strSearch)">
					
				</div> 
			</div>
			<div class="col-md-3">
				<a href="#/charges/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> ADD CHARGES</a>
			</div>
			<div class="clearfix"></div>
			<hr>
			<div class="col-md-12">
				<table class="table table-bordered center">
					<thead>
						<tr>
							<th>NAME</th>	
							<th class="w90px">AMOUNT</th>
							<th class="w90px"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="data in data">
							<td>{{data.name}}</td>
							<td>{{data.amount|number:2}}</td>							
							<td>
								<div class="btn-group btn-group-xs">
									<a href="#/charges/edit/{{data.id}}" class="btn btn-primary no-border-radius" ng-if="highLevel"><i class="fa fa-pencil"></i></a>
									<a href="javascript:void(0)" ng-click="remove(data)" class="btn btn-danger no-border-radius" title="DELETE" ng-if="highLevel"><i class="fa fa-trash"></i></a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
	    </div>
	</div>
</div>
