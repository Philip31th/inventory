<div>
	<div class="panel panel-primary">
	    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT ROOM CHARGES</div>
	    <div class="panel-body">
		<form id="form">
	    	<div class="col-md-4">
				<div class="form-group">
					<label>CODE</label>
					<input type="text" class="form-control" data-validation-engine="validate[required]" ng-model="data.Charge.code">
				</div>
			</div>	

	    	<div class="col-md-4">
				<div class="form-group">
					<label>NAME</label>
					<input type="text" class="form-control" data-validation-engine="validate[required]" ng-model="data.Charge.name">
				</div>
			</div>	
			
			<div class="col-md-4">
				<div class="form-group">
					<label>AMOUNT</label>
					<input type="text" class="form-control decimal" data-validation-engine="validate[required]" ng-model="data.Charge.amount">
				</div>
			</div>	
			
			<div class="col-md-4 pull-right">
				<button class="btn btn-primary btn-sm btn-block" ng-click="save()">SAVE</button>
			</div>
		</form>
	    </div>
	</div>
</div>
<script>
$('.decimal').inputmask({alias: 'decimal', rightAlign: false});
$('.integer').inputmask({alias: 'integer', rightAlign: false});
$('#form').validationEngine('attach');
</script>
