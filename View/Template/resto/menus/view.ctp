<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW RESTAURANT MENU </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name: </dt>
          <dd>{{ data.Menu.name }}</dd>

          <dt>Amount: </dt>
          <dd>{{ data.Menu.amount | currency:'PHP ' }}</dd>

          <dt>Description: </dt>
          <dd>{{ data.Menu.description }}</dd>

        </dl>

      <div class="clearfix"></div>
      <hr>

      <div class="btn-group btn-group-sm pull-right btn-min">
        <a href="#/resto/menu/edit/{{ data.Menu.id }}" class="btn btn-primary btn-sm btn-min"><i class="fa fa-edit"></i> EDIT</a>
        <button class="btn btn-danger btn-sm btn-min" ng-click="remove(data.Menu)"><i class="fa fa-trash"></i> DELETE</button>
      </div> 
      
     </div> 
    </div>
  </div>
</div>
