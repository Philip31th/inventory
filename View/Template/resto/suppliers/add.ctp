<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW RESTAURANT SUPPLIER</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <form id="form">
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label>Name <i class="required">*</i></label>
                <input type="text" class="form-control" ng-model="data.Supplier.name" data-validation-engine="validate[required]">
              </div>
            </div>
          
            <div class="col-md-4">
              <div class="form-group">
                <label>Contact #</label>
                <input type="text" class="form-control" ng-model="data.Supplier.contactNumber">
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label>Address</label>
                <textarea class="form-control" ng-model="data.Supplier.address"></textarea>
              </div>
            </div> 
            
          </div>
        </form>
      </div>  

      <div class="clearfix"></div>
      <hr> 
      
      <!-- SUPPLIER ITEMS -->
      <div class="col-md-12">
        <table class="table table-bordered" id="items">
          <thead>
            <tr>
              <th class="bg-info" colspan="6">SUPPLIER ITEMS</th>
            </tr>
            <tr>
              <th class="text-center w30px">#</th>
              <th class="text-center">NAME</th>
              <th class="text-center">PRICE</th>
              <th class="text-center">QTY</th>
              <th class="text-center">DESCRIPTION</th>
              <td class="text-center w30px"></td>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="item in data.SupplierItem" >
              <td class="w30px">{{ $index + 1 }}</td>
              <td class="text-center">{{ item.name }}</td>
              <td class="text-center">{{ item.price | number: 2 }}</td>
              <td class="text-center">{{ item.quantity }}</td>
              <td class="text-center">{{ item.description }}</td>
              <td class="text-center">
                <div class="btn-group btn-group-xs">
                  <button ng-click="removeItem(data.SupplierItem.indexOf(item),item)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                </div>
              </td>
            </tr>
            <tr ng-if="data.SupplierItem==''">
              <td colspan="6" class="text-center">No items.</td>
            </tr>
          </tbody>
        </table>
        <p class="pull-right"><label>TOTAL: PHP {{ totalSuppliesAmount() | number: 2 }}</label> </p>
      </div>

      <div class="clearfix"></div>
      <hr> 

      <div class="col-md-12">
        <div class="btn-group btn-group-sm pull-right btn-sm">
          <button type="button" class="btn btn-success" ng-click="addItem()"><i class="fa fa-plus"></i> ADD ITEM</button>
          <button type="button" class="btn btn-primary" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
        </div> 
      </div>

      </div>
     </div> 
  </div>
</div>

<?php echo $this->element('suppliers/add-supplier-item-modal') ?>