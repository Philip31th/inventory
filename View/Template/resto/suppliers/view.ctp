<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW RESTAURANT SUPPLIER </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name :</dt>
          <dd>{{ data.Supplier.name }}</dd>

          <dt>Contact #:</dt>
          <dd>{{ data.Supplier.contactNumber==null? 'n/a':data.Supplier.contactNumber }}</dd>

          <dt>Address :</dt>
          <dd>{{ data.Supplier.address==null? 'n/a':data.Supplier.address }}</dd>

          <dt>Supply Started :</dt>
          <dd>{{ data.Supplier.started }}</dd>

          <dt>Total Amount :</dt>
          <dd>PHP {{ totals.totalAmount | number: 2 }}</dd>

          <dt>Total Payments :</dt>
          <dd>PHP {{ totals.totalPayments | number: 2 }}</dd>

          <dt>Total Balance :</dt>
          <dd>PHP {{ totals.totalAmount - totals.totalPayments | number: 2 }}<dd>

        </dl>

      <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="bg-info" colspan="7">SUPPLIER ITEMS</th>
              </tr>
              <tr>
                <th class="text-center w30px">#</th>
                <th class="text-center">DATETIME</th>
                <th class="text-center">NAME</th>
                <th class="text-center">PRICE</th>
                <th class="text-center">QTY</th>
                <th class="text-center">DESCRIPTION</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="item in data.SupplierItem" >
                <td class="w30px">{{ $index + 1 }}</td>
                <td class="text-center">{{ item.started }}</td>
                <td class="text-center">{{ item.name }}</td>
                <td class="text-center">{{ item.price | number: 2 }}</td>
                <td class="text-center">{{ item.quantity }}</td>
                <td class="text-center">{{ item.description }}</td>
              </tr>
              <tr ng-if="data.SupplierItem==''">
                <td colspan="6" class="text-center">No items.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
        
      <div class="clearfix"></div>
      <hr>

      <div class="row">
        <div class="col-md-12">
          <div class="btn-group btn-group-sm pull-right btn-min">
            <a href="#/resto/supplier/edit/{{ data.Supplier.id }}" class="btn btn-success btn-min"><i class="fa fa-edit"></i> EDIT</a>
            <a href="javascript:void(0)" ng-click="remove(data.Supplier)" class="btn btn-danger btn-min" title="DELETE"><i class="fa fa-trash"></i> DELETE</a>
          </div> 
        </div>
      </div> 
      </div>
    </div>
  </div>
</div>
