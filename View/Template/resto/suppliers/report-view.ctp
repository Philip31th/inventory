<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> RESTAURANT SUPPLIER PAYMENTS VIEW</div>
  <div class="panel-body">
    <div class="row">

      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Supplier: </dt>
          <dd>{{ supplier.Supplier.name }}</dd>

          <dt>Total Amount: </dt>
          <dd>PHP {{ totals.totalAmount | number: 2 }}</dd>

          <dt>Balance: </dt>
          <dd>PHP {{ totals.totalAmount - totals.totalPayments | number: 2 }}</dd>

          <dt>Status: </dt>
          <dd>
            <span class="label label-{{ totals.totalAmount-totals.totalPayments <= 0? 'success' : 'danger' }}">{{ totals.totalAmount-totals.totalPayments <= 0? 'PAID' : 'UNPAID' }}</span>
          </dd>
       </dl>
      </div>

      <div class="col-md-12"> 
        <table class="table table-bordered table-striped table-hover center">
          <thead>
            <tr>
              <th>OR # </th>
              <th>DATETIME</th>
              <th>PAYMENT TYPE</th>
              <th>PAYMENTS</th>
              <th>CHANGE</th>
              <th class="w30px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="payment in payments">
              <td class="uppercase">{{ payment.orNumber }}</td>
              <td class="uppercase">{{ payment.datetime }}</td>
              <td class="uppercase">{{ payment.paymentType }}</td>
              <th class="uppercase">{{ payment.amount - payment.change | number: 2 }}</th>
              <th class="uppercase">{{ payment.change | number: 2 }}</th>
              <td>
                <a class="btn btn-danger btn-xs" title="DELETE" id="removePayment" ng-click="removePayment(payment)"><i class="fa fa-trash"></i></a>   
              </td>
            </tr>
            <tr ng-if="payments==''">
              <td colspan="6">No payments recorded.</td>
            </tr>
          </tbody>
          <tfoot>
            <tr ng-if="payments!=''">
              <th colspan="3" class="text-right">TOTAL</th>
              <th class="italic">PHP {{ totals.totalPayments | number: 2 }}</th>
              <th class="italic">PHP {{ totals.totalChange | number: 2 }}</th>
              <td></td>
            </tr>
          </tfoot>
        </table>
      </div>

      <div class="clearfix"></div><hr>

        <div class="col-md-3 pull-right">
          <button class="btn btn-primary btn-sm btn-block" id="addPayment" ng-if="totals.totalAmount-totals.totalPayments > 0" ng-click="addPayment()"><i class="fa fa-plus"></i> ADD PAYMENT</button>
        </div>

    </div>
  </div>  
</div>

<?php echo $this->element('hotel/cashiering/view/add-payment-modal') ?>