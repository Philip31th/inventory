<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW RESTAURANT TRANSACTION</div>
  <div class="panel-body">
		<form id="form">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>FOLIO #</label>
							<select class="form-control" ng-model="data.Folio.code" ng-options="opt.code as opt.value for opt in folios">
		          <option value="">SELECT FOLIO</option>
		        </select>
					</div>
				</div>
				
				<div class="col-md-12">
		      <div class="form-group">
		        <label>TITLE</label>
		        <input type="text" class="form-control" ng-model="data.Transaction.title">
		      </div>
		    </div>

		    <div class="col-md-12">
		      <div class="form-group">
		        <label>PARTICULARS</label>
		        <input type="text" class="form-control" ng-model="data.Transaction.particulars">
		      </div>
		    </div>
				
				<div class="clearfix"></div>
				<hr>

		    <div class="col-md-12">
				  <table class="table table-bordered">
					  <thead>
						 <tr>
	              <th class="bg-info text-left" colspan="9">TRANSACTION ITEMS</th>
	            </tr>
							<tr>
							  <th class="w30px">#</th>
								<th class="text-center">PARTICULARS</th>
								<th class="w120px text-right">AMOUNT</th>
								<th class="w70px text-center"></th>
							</tr>
						</thead>
					  <tbody>
						  <tr ng-repeat="item in data.TransactionSub" ng-if="data.TransactionSub!=''">
						    <td>{{ $index + 1}}</td>
						    <td class="text-center uppercase">{{ item.particulars }}</td>
						    <td class="text-right">{{ item.amount|number:2 }}</td>
						    <td>
						    	<div class="btn-group btn-group-xs">
						    		<button class="btn btn-primary" title="EDIT" ng-click="editItem(item)"><i class="fa fa-edit"></i></button>
				            <button ng-click="removeItem($index,item)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
				          </div>
						    </td>
						  </tr>
					</tbody>
					<tfoot class="bg-info">
            <tr>
              <th colspan="2" class="text-right">TOTAL BILLS</th>
              <th class="text-right italic">{{ totalBills() | number: 2 | currency: 'PHP '}}</th>
              <th></th>
            </tr>
          </tfoot>
				</table>
			</div>
        
        <div class="col-md-12">
          <div class="btn-group btn-group-sm pull-right btn-min">
            <button type="button" class="btn btn-success btn-min" ng-click="addItem()"><i class="fa fa-plus"></i> ADD ITEM</button>
            <button type="button" class="btn btn-primary btn-min" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
          </div> 
        </div>
      
		</form>
    </div>
	</div>
</div>


<?php echo $this->element('transactions/transaction-item-modal') ?>