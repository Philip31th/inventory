<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> RESTAURANT RECEIVABLES</div>
  <div class="panel-body">
    <div class="row>">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load( {code: 'restaurant', search: searchTxt })">
        </div> 
      </div>
<!--       <div class="col-md-3">
        <a href="#/resto/transactions/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> NEW TRANSACTION</a>
      </div> -->
      
      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover center">
          <thead>
            <tr>
              <th>FOLIO #</th>
              <th>DATE</th>
              <th>PARTICULARS</th>
              <th class="text-right">AMOUNT</th>
              <th class="text-right">INITIAL PAYMENT</th>
              <th class="text-right">PAYMENT</th>
              <th class="text-right">RECEIVABLES</th>
              <th class="w30px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="transaction in receivables">
              <td class="uppercase">{{ transaction.folio_code }}</td>
              <td class="text-center">{{ transaction.datetime }}</td>
              <td class="uppercase">{{ transaction.title }}</td>
              <td class="uppercase text-right">{{ transaction.totalAmount | number:2 }}</td>
              <td class="uppercase text-right">{{ transaction.initPayment | number:2 }}</td>
              <td class="uppercase text-right">{{ transaction.totalPayment | number:2 }}</td>
              <th class="uppercase text-right">{{ transaction.totalBalance | number:2 }}</th>
              <td>
                <div class="btn-group btn-group-xs">
                  <a href="#/resto/reports/receivables/view/{{ transaction.id }}" class="btn btn-success " ><i class="fa fa-eye"></i></a>
                </div>
              </td>
            </tr>

            <tr ng-if="receivables == '' ">
              <td colspan="8">No receivables found.</td>
            </tr>

          </tbody>
          <tfoot>
            <tr>
              <th colspan="6" class="text-right">TOTAL RECEIVABLES</th>
              <th class="text-right">PHP {{ total | number: 2 }}</th>
              <td></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div> 

    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'restaurant', page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>

    <div class="clearfix"></div>
     
  </div>
</div>