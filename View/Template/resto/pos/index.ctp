<div class="row">
  <div class="col-md-12">
      
      <div class="row">
        <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="javascript:void(0)">
              <div class="stats-icon">{{ tables.Available }}</i></div>
              <div class="stats-label">AVAILABLE TABLES</div>
            </a>
          </div>
        </div>
        
       <div class="col-md-3 image">
          <div class="stats bg-primary">
            <a href="javascript:void(0)">
              <div class="stats-icon">{{ tables.Occupied }}</i></div>
              <div class="stats-label">OCCUPIED TABLES</div>
            </a>
          </div>
        </div>  
      </div>
      
      <hr>
      <h3 class="page-title" style="font-weight:100">TABLES</h3>
      <hr>
      
     <div class="row">
        
          <div class="col-md-3 image" ng-repeat="table in tables.Tables">
            <div class="pos-stats bg-primary">
              <a href="javascript:void(0)">

                  <table class="table pos-table">
                    <tr>
                      <td rowspan="4" class="text-center">
                        <span class="stats-icon bg-primary">
                          <p id="table-index" style="">{{ $index + 1 }}</p>
                        </span></td>
                    </tr>
                    <tr class="pos-drawer text-center">
                      <td  id="pos-drawer1">
                          <div class="btn-group btn-group-xs">
                            <a ng-if="!table.occupied" href="#/resto/pos/transactions/add/{{table.tableId}}" class="btn btn-info" title="ADD TRANSACTION"><i class="fa fa-plus"></i></a>

                            <a ng-if="table.occupied" href="#/resto/pos/table/view/{{ table.tableId }}" class="btn btn-success" title="VIEW TRANSACTION"><i class="fa fa-eye"></i></a>

                            <a ng-if="table.paid || table.folio" ng-click="cleanTable(table.tableTransactionId,table.folio,table.Folio.folioTransactionId)" class="btn btn-warning" title="CLEAN TABLE"><i class="fa fa-check"></i></a>
                          </div> 
                      </td>
                    </tr>
                    <tr class="pos-drawer text-center">
                      <td  id="pos-drawer2">
                        <span ng-if="table.occupied&&!table.folio" class="label label-{{ table.paid? 'success' : 'danger'}}">{{ table.paid? 'PAID' : 'PENDING' }}</span>

                        <span class="label label-{{ table.occupied? 'danger' : 'success'}}">{{ table.occupied? 'OCCUPIED' : 'AVAILABLE' }}</span>
                      </td>
                    </tr>
                  </table>
              </a>
            </div>
          </div>
        
      </div>
           
      
    </div>
</div>