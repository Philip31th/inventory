<div class="panel panel-primary" ng-init="paymentStatus()">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW TABLE </div>
  <div class="panel-body">
    <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                <label>Folio #</label>
                <select disabled class="form-control" id="selectFolio" ng-if="paymentStatus()=='PAID'">
                <option value="">Select Folio</option>
              </select>

              <select class="form-control" id="selectFolio" ng-model="data.Folio.code" ng-options="opt.code as opt.value for opt in folios" ng-change="saveFolioTransactions()" ng-if="paymentStatus()!='PAID'">
                <option value="">Select Folio</option>
              </select>
            </div>
          </div>
          
      <form id="pos_form">
          <div class="col-md-12">
            <div class="form-group">
              <label>Order<i class="required">*</i></label>
              <select class="form-control" ng-model="menuId" id="menuId" ng-options="opt.id as opt.value for opt in menus" ng-change="getMenuData(menuId)"  data-validation-engine="validate[required]" ng-init="getMenuData(menuId)">
                <option value=""></option>
              </select>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label>Amount<i class="required">*</i></label>
              <input disabled amount type="text" ng-init="menu.Menu.amount=''" class="form-control" ng-model="menu.Menu.amount">
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label>Quantity<i class="required">*</i></label>
              <input type="text" number class="form-control" ng-model="quantity" data-validation-engine="validate[required]">
            </div>
          </div>
          
          <div class="col-md-12">
            <div class="btn-group btn-group-sm btn-min pull-right">
              <button type="button" class="btn btn-warning" id="addOrderBt" ng-click="saveOrder()"><i class="fa fa-plus"></i> SAVE ORDER</button>
              <button type="button" class="btn btn-danger" id="closeOrderBt"><i class="fa fa-close"></i> CLOSE</button>
            </div>
          </div>
      </form>  

      <div class="clearfix"></div>
      <hr>

      <!-- ORDER LIST -->
      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th colspan="8" class="bg-info">LIST OF ORDERS</th>
            </tr>
            <tr>
                <th class="w30px">#</th>
                <th class="text-center">NAME</th>
                <th class="w100px text-right">AMOUNT</th>
                <th class="w30px text-right">QTY</th>
                <th class="w120px text-right">TOTAL AMOUNT</th>
                <th class="w120px text-right">PAYMENT</th>
                <th class="w120px text-right">BALANCE</th>
                <th class="w70px text-center"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="item in data.TransactionSub">
              <td>{{ $index + 1}}</td>
              <td class="text-center uppercase">{{ item.particulars }}</td>
              <td class="text-right">{{ item.amount | number:2 }}</td>
              <td class="text-right">{{ item.totalQuantity }}</td>
              <td class="text-right">{{ item.totalAmount | number:2 }}</td>
              <td class="text-right">{{ item.totalPayments | number:2 }}</td>
              <td class="text-right">{{ item.totalAmount - item.totalPayments | number:2 }}</td>
              <td class="text-center">
                <div class="btn-group btn-group-xs">

                  <button  class="btn btn-primary {{ item.totalAmount - item.totalPayments > 0? '':'disabled' }}" title="CLICK TO ADD PAYMENT" ng-click="addPayment(item)"><i class="fa fa-plus"></i></button> 

                  <button ng-click="removeOrder($index,item)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                </div>
              </td>
            </tr>    
          </tbody>
          <tfoot class="bg-info">
            <tr>
              <th colspan="4" class="text-right">TOTAL</th>
              <th class="text-right">PHP {{ total | number: 2 }}</th>
              <th class="text-right">PHP {{ totalPayments | number:2 }}</th>
              <th class="text-right">PHP {{ total - totalPayments | number: 2 }}</th>
              <th></th>
           </tr>   
          </tfoot>
        </table>
      </div>
      <!--.-->
      
      <div class="col-md-12" ng-if="data.TransactionPayment!=''">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th class="bg-info text-left" colspan="7">PAYMENTS</th>
            </tr>
            <tr>
              <th class="text-center w30px">#</th>
              <th class="text-center w120px">OR #</th>
              <th class="text-center w120px">DATE</th>
              <th class="text-center">PAYMENT TYPE</th>
              <th class="text-right w120px">AMOUNT</th>
              <th class="text-right w120px">CHANGE</th>
              <th class="w70px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="payment in data.TransactionPayment">
              <td>{{ $index + 1 }}</td>
              <td class="uppercase text-center">{{ payment.orNumber }}</td>
              <td class="text-center uppercase">{{ payment.date | date:'yyyy-MM-dd' }}</td>
              <th class="text-center uppercase">{{ payment.paymentType }}</th>
              <th class="text-right">{{ payment.amount | number: 2 }}</th>
              <th class="text-right">{{ payment.change | number: 2 }}</th>
              <td class="text-center">
                <div class="btn-group btn-group-xs">
                  <a class="btn btn-danger" title="DELETE" ng-click="removePayment($index,payment)"><i class="fa fa-trash"></i></a>
                </div>  
                </div>
              </td>  
            </tr>
          </tbody>
          <tfoot class="bg-info">
            <tr>
              <th colspan="4" class="text-right">TOTAL PAYMENTS</th>
              <th class="text-right italic">PHP {{ totalPayments| number: 2 }}</th>
              <th class="text-right italic">PHP {{ totalChange | number: 2 }}</th>
              <th></th>
            </tr>
          </tfoot>
        </table>
      </div>
      
      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">

        <div class="pull-left">
          <label>Status: </label>
          <span class="label label-{{ paymentStatus()=='PAID'? 'success' : 'danger'}}">{{ paymentStatus()=='PAID'? 'PAID' : 'PENDING' }}</span>
         <!--                
          <label ng-if="data.folio!=''"> &emsp;|&emsp; Folio: </label>
          <span ng-if="data.folio!=''">
            {{ data.folio.Folio.code + " - " + data.folio.Folio.Guest.firstName + " " + data.folio.Folio.Guest.lastName }}

            {{ data.Folio.code }} -->
          </span>            
        </div>

        <div class="btn-group btn-group-sm pull-right btn-min">
<!--           <button type="button" ng-if="data.folio==''||paymentStatus()!='PAID'" class="btn btn-success btn-min" id="addPayment" ng-click="addPayment()"><i class="fa fa-plus"></i> ADD PAYMENT</button> -->

          <button type="button" class="btn btn-warning btn-min" ng-click="addOrder()" id="addOrder"><i class="fa fa-plus"></i> ADD ORDER</button>

          <!-- <button type="button" class="btn btn-primary btn-min" ng-click="save()"><i class="fa fa-save"></i> SAVE</button> -->
          
          <button type="button" ng-if="paymentStatus()=='PAID' || data.Folio.code!=null" class="btn btn-success btn-min" ng-click="cleanTable(data.tableTransactionId,status)"><i class="fa fa-check"></i> Clean Table </button>
        </div>        
      </div>
        
    </div>
  </div>
</div>    

<?php echo $this->element('hotel/cashiering/view/add-payment-modal') ?>
<?php echo $this->element('transactions/transaction-item-modal') ?>