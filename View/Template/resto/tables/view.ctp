<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW RESTAURANT TABLE </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name: </dt>
          <dd>{{ data.Table.name }}</dd>

          <dt>Amount: </dt>
          <dd>{{ data.Table.description }}</dd>

        </dl>

        <div class="clearfix"></div>
        <hr>

        <div class="btn-group btn-group-sm pull-right btn-min">
          <a href="#/resto/table/edit/{{ data.Table.id }}" class="btn btn-primary btn-sm btn-min"><i class="fa fa-edit"></i> EDIT</a>
          <button class="btn btn-danger btn-sm btn-min" ng-click="remove(data.Table)"><i class="fa fa-trash"></i> DELETE</button>
        </div>
      </div>    
    </div>
  </div>
</div>
