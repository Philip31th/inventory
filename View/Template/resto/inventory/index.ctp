<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> RESTAURANT INVENTORY </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ code : 'restaurant',search: searchTxt })">
        </div> 
      </div>

      <div class="col-md-8 btn-group btn-group-sm pull-left btn-min">
        <a href="#/resto/inventory/add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> NEW ITEM</a>
        <a href="#/resto/inventory/daily-consumption" class="btn btn-danger btn-sm"><i class="fa fa-list-ul"></i> DAILY CONSUMPTION</a>
      </div>
      
      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr> 
              <th class="w30px">#</th>
              <th class="uppercase text-center">NAME</th>
              <th class="text-center w120px">STOCKS</th>
              <th class="text-center w120px">COST</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
             <tr ng-repeat="item in items"> 

               <td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
               <td class="uppercase text-center">{{ item.name }}</td>
               <td class="text-center uppercase">{{ item.quantity | number:2 }} {{ item.unit }}</td>
               <th class="text-center italic">{{ item.cost  | number:2 | currency: 'PHP '}}</th>
               <td class="text-center">
                 <div class="btn-group btn-group-xs">
                    <a href="#/resto/inventory/view/{{item.id}}" class="btn btn-success" title="VIEW"><i class="fa fa-eye"></i></a>
                    <a href="#/resto/inventory/edit/{{item.id}}" class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-danger" title="DELETE" ng-click="remove(item)"><i class="fa fa-trash"></i></a>
                  </div>
               </td>
             </tr>
          </tbody>
        </table>
      </div>
    </div>
    
    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'restaurant',page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    
    <div class="clearfix"></div>

  </div>
</div>

