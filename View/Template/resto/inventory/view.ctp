<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW RESTAURANT INVENTORY </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Name:</dt>
          <dd>{{ data.Inventory.name }}</dd>

          <dt>Department:</dt>
          <dd>{{ data.Department.name }}</dd>

          <dt>Description:</dt>
          <dd>{{ data.Inventory.description }}</dd>
        </dl>

      <div class="clearfix"></div>
      <hr>
       
    <!-- inventory received/delivered -->
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="bg-info" colspan="6">INVENTORY ITEMS</th>
            </tr>
            <tr>
              <th class="">#</th>
              <th class="text-left w180px">DATETIME</th>
              <th>QUANTITY</th>
              <th>COST</th></th>
              <th class="text-center">STATUS</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="item in data.InventorySub">
              <td class="w30px">{{ $index + 1 }}</td>
              <td class="text-left">{{ item.date }}</td>              
              <td class="text-left">{{ item.quantity | number: 2 }}</td>
              <td class="text-left">{{ item.cost}}</td>
              <td class="w90px text-center">
                <span class="label label-{{ item.type? 'success' : 'danger' }}">{{ item.type? 'RECEIVED' : 'CONSUMED' }}</span>
              </td>
            </tr>
          </tbody>

          <tfoot>    
            <tr class="bg-info">
              <th class="text-right" colspan="2">TOTAL STOCKS</th>
              <th>
                <p class="text-left pull-left">{{ data.TotalQuantity | number: 2 }} {{ data.Inventory.unit }}</p> 
                <p class="text-right pull-right">AVERAGE COST</p>
              </th>
              <th class="text-left" colspan="2">{{ data.Inventory.averageCost }}</th>
            </tr>     
          </tfoot>
        </table>
      </div>
    </div>

      <div class="clearfix"></div>
      <hr>

      <div class="row">
        <div class="col-md-12">
          <div class="btn-group btn-group-sm pull-right btn-min">
            <a href="#/resto/inventory/edit/{{ data.Inventory.id }}" class="btn btn-success btn-min"><i class="fa fa-edit"></i> EDIT</a>
            <a href="javascript:void(0)" ng-click="remove(data.Inventory)" class="btn btn-danger btn-min" title="DELETE"><i class="fa fa-trash"></i> DELETE</a>
          </div> 
        </div>
      </div>
     </div>
  </div>
</div>
