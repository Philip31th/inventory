<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> DAILY CONSUMPTION</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
          <table class="table table-striped table-hover">
            <thead>
              <tr class="bg-info"> 
                <th class="w30px">#</th>
                <th class="uppercase text-left">NAME</th>
                <th class="text-left">DESCRIPTION</th>
                <th class="text-center">UNIT</th>
                <th class="text-center">STOCKS</th>
                <th class="text-center">COST</th>
                <th class="text-center w90px">USED</th>
              </tr>
            </thead>
            <tbody>
               <tr ng-repeat="item in items"> 
                 <td>{{ $index + 1 }}</td>
                 <td class="uppercase text-left">{{ item.name }}</td>
                 <td class="uppercase text-left">{{ item.description }}</td>
                 <td class="uppercase text-center">{{ item.unit }}</td>
                 <td class="text-center">{{ item.quantity | number: 2 }}</td>
                 <td class="text-center">{{ item.cost | number:2 }}</td>
                 <td class="text-center">
                     <input amount class="form-control" type="text" ng-model="item.delivered" ng-init="item.delivered = 0" >
                     <input type="hidden" ng-model="item.type" ng-init="item.type = false"/>
                 </td>
               </tr>
            </tbody>
          </table>
        </div>
      </div>
    <div class="clearfix"></div>
    <hr>

    <div class="btn-group btn-group-sm pull-right btn-min">
      <button type="button" class="btn btn-success btn-sm btn-min" ng-click="viewDailyConsumption()"><i class="fa fa-eye"></i> VIEW CONSUMPTIONS</button>
      <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="saveDailyConsumption(item)"><i class="fa fa-save"></i> SAVE</button>
    </div>
  </div>

</div>

<?php echo $this->element('inventories/view-daily-consumptions'); ?>