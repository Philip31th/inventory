<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW BAR TRANSACTION</div>
  <div class="panel-body">
		<form id="form">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						  <label>Folio #</label>
							<select class="form-control" ng-model="data.Folio.code" ng-options="opt.code as opt.value for opt in folios">
		          <option value="">Select Folio</option>
		        </select>
					</div>
				</div>
        
		    <div class="col-md-12">
		      <div class="form-group">
		        <label>Order<i class="required">*</i></label>
		        <select class="form-control" ng-model="menuId" id="menuId" ng-options="opt.id as opt.value for opt in menus" ng-change="getMenuData(menuId)"  data-validation-engine="validate[required]" ng-init="getMenuData(menuId)">
              <option value=""></option>
            </select>
		      </div>
		    </div>
		    
		    <div class="col-md-6">
		      <div class="form-group">
		        <label>Amount<i class="required">*</i></label>
		        <input disabled type="text" class="form-control" ng-model="menu.Menu.amount">
		      </div>
		    </div>
		    
		    <div class="col-md-6">
		      <div class="form-group">
		        <label>Quantity<i class="required">*</i></label>
		        <input number type="text" class="form-control" ng-model="quantity" data-validation-engine="validate[required]">
		      </div>
		    </div>
		    
		    <div class="col-md-3 pull-right">
		      <button type="button" class="btn btn-success btn-sm btn-block" id="addOrderBt" ng-click="saveOrder()"><i class="fa fa-plus"></i> ADD ORDER</button>
		    </div>
		  </form>  
				<div class="clearfix"></div>
				<hr>

		    <div class="col-md-12">
		      <label ng-if="data.TransactionSub==''">No orders yet</label>
				  <table class="table table-bordered" ng-if="data.TransactionSub!=''" ng-init="totalAmount = 0">
					  <thead>
						 <tr>
	              <th class="bg-info text-left" colspan="8">ORDERS</th>
	            </tr>
							<tr>
							  <th class="w30px">#</th>
								<th class="text-center">NAME</th>
								<th class="w120px text-right">AMOUNT</th>
								<th class="w120px text-right">QUANTITY</th>
								<th class="w120px text-right">TOTAL AMOUNT</th>
								<th class="w30px text-center"></th>
							</tr>
					</thead>
					  <tbody>
					  <tr ng-repeat="item in data.TransactionSub">
					    <td>{{ $index + 1}}</td>
					    <td class="text-center uppercase">{{ item.particulars }}</td>
					    <td class="text-right">{{ item.amount | number:2 }}</td>
					    <td class="text-right">{{ item.quantity }}</td>
					    <td class="text-right" ng-init="totalAmount = item.totalAmount">{{ item.totalAmount | number:2 }}</td>
					    <td>
					    	<div class="btn-group btn-group-xs">
			            <button ng-click="removeOrder($index)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
			          </div>
					    </td>
					  </tr>
					</tbody>
					<tfoot class="bg-info">
            <tr>
              <th colspan="4" class="text-right">TOTAL BILLS</th>
              <th class="text-right italic">PHP {{ total | number:2 }}</th>
              <th></th>
            </tr>
           </tfoot>
				</table>
			</div>
        
        <div class="col-md-3 pull-right">
           <button type="button" class="btn btn-primary btn-sm btn-block" ng-if="data.TransactionSub!=''" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
          </div> 
        </div>
    </div>
	</div>
</div>


<?php echo $this->element('transactions/transaction-item-modal') ?>