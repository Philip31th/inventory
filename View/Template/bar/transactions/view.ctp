<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> VIEW BAR TRANSACTION</div>
  <div class="panel-body">
     <div class="col-md-12">
       <div class="row">
        <dl class="dl-horizontal dl-bordered">
          <dt>Code:</dt>
          <dd>{{ data.Transaction.code }}</dd>

          <dt>Title:</dt>
          <dd>{{ data.Transaction.title }}</dd>

          <dt> Business:</dt>
          <dd>{{ data.Business.name }}</dd>
          
          <dt>Particulars:</dt>
          <dd class="propercase">{{ data.Transaction.particulars  }}</dd>
          
          <dt>Date:</dt>
          <dd class="propercase">{{ data.Transaction.date  }}</dd>

          <dt>Guest Name:</dt>
          <dd class="propercase">{{ data.Guest.name }}</dd>
        </dl>
      </div>
    </div>
    
    <div class="clearfix"></div><hr>
    
      <div class="col-md-12">
        <div class="row">
          <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
              <tr>
                <th class="bg-info text-left" colspan="9">TRANSACTION ITEMS</th>
              </tr>
              <tr>
                <th class="text-center w30px">#</th>
                <th>PARTICULARS</th>
                <th class="text-right w100px">AMOUNT</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="transactionsub in data.TransactionSub" ng-if="data.TransactionSub!=''">
                <td>{{  $index + 1 }}</td>
                <td class="uppercase">{{  transactionsub.particulars }}</td>
                <td class="text-right">{{ transactionsub.amount | number: 2 }}</td>
              </tr>
              <tr ng-if="data.TransactionSub==''" >
                <td colspan="3">No transaction items yet.</td>
              </tr>
            </tbody>
            <tfoot class="bg-info">
						  <th class="text-right" colspan="2">TOTAL AMOUNT</th>
						  <th class="text-right">{{ data.TotalAmount | number:2 | currency: 'PHP '}}</th>
					  </tfoot>
          </table>
        </div>  
      </div>
      
      <div class="clearfix"></div>
      <hr>
      
      <div class="row">
        <div class="col-md-12">
          <div class="btn-group btn-group-sm pull-right btn-min">
            <a href="#/bar/transactions/edit/{{ data.Transaction.id }}" class="btn btn-success btn-min"><i class="fa fa-edit"></i> EDIT</a>
			      <a href="javascript:void(0)" ng-click="remove(data)" class="btn btn-danger btn-min" title="DELETE"><i class="fa fa-trash"></i> DELETE</a>
          </div> 
        </div>
      </div>
		
    </div>
</div>
