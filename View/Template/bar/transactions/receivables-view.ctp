<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> BAR RECEIVABLES VIEW</div>
  <div class="panel-body">
    <div class="row>">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Folio #: </dt>
          <dd>{{ receivables[0].folio_code }}</dd>

          <dt>Transaction Date: </dt>
          <dd>{{ receivables[0].datetime }}</dd>

          <dt>Particulars: </dt>
          <dd>{{ receivables[0].title }}</dd>

          <dt>Total Amount: </dt>
          <dd>PHP {{ receivables[0].totalAmount | number: 2 }}</dd>

          <dt>Total Payment: </dt>
          <dd>PHP {{ receivables[0].totalPayment | number: 2 }}</dd>

          <dt>Total Receivables: </dt>
          <dd>PHP {{ receivables[0].totalBalance | number: 2 }}</dd>
       </dl>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover center">
          <thead>
            <tr>
              <th colspan="4" class="bg-info text-left">TRANSACTION ITEMS</th>
            </tr>
            <tr>
              <th>PARTICULARS</th>
              <th>AMOUNT</th>
              <th>QTY</th>
              <th>TOTAL AMOUNT</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="rcv in receivables[0].TransactionSub">
              <td class="uppercase">{{ rcv.particulars }}</td>
              <td>{{ rcv.amount | number: 2 }}</td>
              <td>{{ rcv.quantity }}</td>
              <td>{{ rcv.amount*rcv.quantity | number: 2 }}</td>
            </tr> 
            <tr ng-if="receivables[0].TransactionSub==''"><td colspan="5">No items recorded.</td></tr>
          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>

      <div class="clearfix"></div>
      <hr>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover center">
          <thead>
            <tr>
              <th colspan="5" class="bg-info text-left">PAYMENTS</th>
            </tr>
            <tr>
              <th>DATE</th>
              <th>OR #</th>
              <th>PAYMENT TYPE</th>
              <th>AMOUNT</th>
              <th>CHANGE</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="rcv in receivables[0].TransactionPayment">
              <td>{{ rcv.date }}</td>
              <td>{{ rcv.orNumber }}</td>
              <td class="uppercase">{{ rcv.paymentType }}</td>
              <td>{{ rcv.amount | number: 2 }}</td>
              <td>{{ rcv.change | number: 2 }}</td>
            </tr>
            <tr ng-if="receivables[0].TransactionPayment==''"><td colspan="5">No payments recorded.</td></tr>
          </tbody>
        </table>
      </div>
    </div>      
  </div>
</div>