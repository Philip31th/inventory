<div class="panel panel-primary">
    <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW BAR TABLES</div>
    <div class="panel-body">
      <form id="form">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Name <i class="required">*</i></label>
              <input type="text" class="form-control" ng-model="data.Table.name" data-validation-engine="validate[required]">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Description</label>
              <textarea class="form-control" ng-model="data.Table.description"></textarea>
            </div>
          </div>
        </div>
      </form>

      <div class="clearfix"></div>
      <hr>

      <div class="row">
        <div class="col-md-3 pull-right">
          <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-download"></i> SAVE</button>
        </div>
      </div>
    </div>
   </div>
</div>