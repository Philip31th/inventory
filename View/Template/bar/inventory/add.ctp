<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> NEW BAR INVENTORY ITEM</div>
  <div class="panel-body">
    <form id="form">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Inventory.name" data-validation-engine="validate[required]">
          </div>
        </div>
        
        
        
        <div class="col-md-4">
          <div class="form-group">
            <label>Unit</label>
            <!--<input lass="form-control" type="text"/>-->
            <select class="form-control" ng-model="data.Inventory.unit">
              <option value=""></option>
              <option value="l">L</option>
              <option value="ml">ML</option>
              <option value="kl">KL</option>
              <option value="g">G</option>
              <option value="mg">MG</option>
            </select>
          </div>
        </div>
            
        <div class="col-md-4">
          <div class="form-group">
            <label>Quantity <i class="required">*</i></label>
            <input number type="text" class="form-control" ng-model="data.InventorySub.quantity" data-validation-engine="validate[required]">
          </div>
        </div>
        
         <div class="col-md-4">
          <div class="form-group">
            <label>Cost <i class="required">*</i></label>
            <input amount type="text" class="form-control" ng-model="data.InventorySub.cost" data-validation-engine="validate[required]">
          </div>
        </div>
        
        <div class="col-md-12">
          <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" ng-model="data.Inventory.description"></textarea>
          </div>
        </div>
      </div>
    </form>

    <div class="clearfix"></div>
    <hr>
        
    <div class="row">
      <div class="col-md-3 pull-right">
        <button class="btn btn-primary btn-sm btn-block" ng-click="save()"><i class="fa fa-save"></i> SAVE</button>
      </div>
    </div> 
  </div>
</div>