<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT BAR INVENTORY</div>
  <div class="panel-body">
    
    <!-- inventory details -->
    <form id="form">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Name <i class="required">*</i></label>
            <input type="text" class="form-control" ng-model="data.Inventory.name" data-validation-engine="validate[required]">
          </div>
        </div>
        
        <div class="col-md-12">
          <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" ng-model="data.Inventory.description"></textarea>
          </div>
        </div>
      </div>
    </form>

    <div class="clearfix"></div>
    <hr>
           
        <!-- inventory received/delivered -->
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="bg-info" colspan="7">INVENTORY ITEMS</th>
            </tr>
            <tr>
              <th class="">#</th>
              <th class="text-left w180px">DATETIME</th>
              <th>QUANTITY</th>
              <th>COST</th></th>
              <th class="text-center">STATUS</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="item in data.InventorySub">
              <td class="w30px">{{ $index + 1 }}</td>
              <td class="text-left">{{ item.date }}</td>              
              <td class="text-left">{{ item.quantity | number: 2 }}</td>
              <td class="text-left">{{ item.cost}}</td>
              <td class="w90px text-center">
                <span class="label label-{{ item.type? 'success' : 'danger' }}">{{ item.type? 'RECEIVED' : 'CONSUMED' }}</span>
              </td>
              <td class="text-center w70px">
                <div class="btn-group btn-group-xs">
                  <button class="btn btn-primary" title="EDIT" ng-click="editItem(item)"><i class="fa fa-edit"></i></button>
                  <button ng-click="remove(item)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                </div>
              </td>
            </tr>
          </tbody>

          <tfoot>    
            <tr class="bg-info">
              <th class="text-right" colspan="2">TOTAL STOCKS</th>
              <th>
                <p class="text-left pull-left">{{ data.TotalQuantity | number: 2 }} {{ data.Inventory.unit }}</p> 
                <p class="text-right pull-right">AVERAGE COST</p>
              </th>
              <th class="text-left" colspan="3">{{ data.Inventory.averageCost }}</th>
            </tr>     
          </tfoot>
        </table>

      </div>
    </div>
    <hr>

    <div class="btn-group btn-group-sm pull-right btn-min">
      <button type="button" class="btn btn-success btn-sm btn-min" ng-click="addItem()"><i class="fa fa-plus"></i> ADD ITEM</button>
      <!--<button  class="btn btn-danger btn-sm btn-min" ng-click="deliverItem()"><i class="fa fa-truck"></i> DELIVER ITEM</button>-->
      <button type="button" class="btn btn-primary btn-sm btn-min" ng-click="update()"><i class="fa fa-save"></i> UPDATE</button>
    </div>

  </div>
</div>

<?php echo $this->element('inventories/inventory-item-modal'); ?>




