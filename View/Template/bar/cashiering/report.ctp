  <div class="panel panel-primary">
      <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> BAR CASHIERING REPORT</div>
      <div class="panel-body">
        <div class="col-md-3">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" class="form-control search" id="search" placeholder="SEARCH DATE" ng-model="searchTxt" ng-change="load({ date: searchTxt, business: 3  })">
          </div> 
        </div>

        <div class="col-md-3">
          <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="openCalendar($event, 'startTime')"><i class="fa fa-clock-o"></i></button>
            </span>
            <input type="text" disabled class="form-control" datetime-picker="h a" ng-model="dates.startTime" is-open="open.startTime" enable-date="false" timepicker-options="timeOptions" ng-click="openCalendar($event, 'startTime')"/>
          </div> 
        </div>

        <div class="col-md-3">
          <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="openCalendar($event, 'endTime')"><i class="fa fa-clock-o"></i></button>
            </span>
            <input type="text" disabled class="form-control" datetime-picker="h a" ng-model="dates.endTime" is-open="open.endTime" enable-date="false" timepicker-options="timeOptions" ng-click="openCalendar($event, 'endTime')"/>
          </div> 
        </div>

          <div class="col-md-3">         
            <button class="btn btn-primary btn-sm btn-block" ng-click="searchTime()"><i class="fa fa-search"></i> SEARCH</button>
          </div>  

        <div class="clearfix"></div>
        <hr>

        <div class="col-md-12">
          <table class="table table-bordered center">
            <thead>
              <tr class="bg-info"><th colspan="5" class="text-left">NON-GUEST TRANSACTIONS</th></tr>
              <tr>
                <th>CODE</th>
                <th>PARTICULARS</th>
                <th class="text-center">PAYMENT</th>
                <th class="text-center">BALANCE</th>
                <th class="text-center">REMARKS</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="trans in non_guest.Transactions.charges">
                <td>{{ trans.code }}</td>
                <td>{{ trans.title }}</td>
                <td class="uppercase text-center">{{ trans.paid|number:2 }}</td>
                <td class="uppercase text-center">{{ trans.balance|number:2 }}</td>
                <td class="uppercase">{{ trans.paymentType }}</td>
              </tr>
              <tr ng-if="non_guest==''"><td colspan="5">No transactions</td></tr>
            </tbody>
          </table>
        </div>

        <div class="col-md-12">
          <table class="table table-bordered center">
            <thead>
              <tr class="bg-info"><th colspan="8"  class="text-left">GUEST TRANSACTIONS</th></tr>
              <tr>
                <th class="w80px">FOLIO #</th>
                <th>COMPANY</th>
                <th>GUEST NAME</th>
                <th>ROOM</th>
                <th class="text-right">PAYMENT</th>
                <th class="text-right">BALANCE</th>
                <th class="text-center">STATUS</th>
                <th class="text-center">REMARKS</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="cashier in datas">
                <td>{{ cashier.code }}</td>
                <td class="uppercase">{{ cashier.company }}</td>
                <td class="uppercase">{{ cashier.guest }}</td>
                <td class="uppercase">{{ cashier.room }}</td>
                <td class="uppercase text-right">{{ cashier.Transaction.paid|number:2 }}</td>
                <td class="uppercase text-right">{{ cashier.Transaction.balance|number:2 }}</td>
                <td class="text-center">
                <span class="label label-{{ cashier.status=='IN-HOUSE'? 'success':'info' }}">{{ cashier.status }}</span>
                </td>
                <td class="uppercase">{{ cashier.Transaction.paymentType }} {{ cashier.transferedBills? '- (T)':'' }}</td>
              </tr>
              <tr ng-if="datas==''"><td colspan="8">No transactions</td></tr>
            </tbody>
          </table>
        </div>

        
        <div class="clearfix"></div>
        <hr>

        <div class="col-md-4">
          <table class="table table-bordered center">
            <thead>
              <tr>
                <th class="bg-info text-left" colspan="2">SUMMARY</th>
              </tr>
              <tr>  
                <th>PAYMENT TYPE</th>
                <th class="text-right">AMOUNT</th>
              </tr>  
            </thead>
            <tbody>
              <tr>
                <td>CASH</td>
                <td class="text-right">{{ summary.TotalCash + non_guest.Summary.TotalCash | number: 2 }}</td>
              </tr>
              <tr>
                <td>CARD</td>
                <td class="text-right">{{ summary.TotalCard + non_guest.Summary.TotalCard | number: 2 }}</td>
              </tr>
              <tr>
                <td>DEBIT</td>
                <td class="text-right">{{ summary.TotalDebit + non_guest.Summary.TotalDebit | number: 2 }}</td>
              </tr>
              <!-- <tr>
                <td>RECEIVABLES</td>
                <td class="text-right">{{ summary.TotalReceivable | number: 2 }}</td>
              </tr> -->
            </tbody>
            <tfoot>
              <tr class="bg-info">
                <th class="text-right">TOTAL AMOUNT</th>
                <th class="text-right">{{ summary.Total + non_guest.Summary.Total | number: 2 }}</th>
              </tr>
            </tfoot>
          </table>
        </div>    

        <div class="clearfix"></div>
        <hr>

        <div class="col-md-3 pull-right">
          <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block" ng-click="print()"><i class="fa fa-print"></i> PRINT REPORT</a>
        </div>
      </div>
  </div>
</div>