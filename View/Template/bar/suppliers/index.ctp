<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> BAR SUPPLIERS</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ code: 'bar', search: searchTxt })">
        </div> 
      </div>

      <div class="col-md-3 pull-left">
        <a ui-sref="" href="#/bar/supplier/add" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> NEW SUPPLIER</a>
      </div>

      <div class="clearfix"></div>
      <hr>
      
      <div class="col-md-12"> 
        <table class="table table-bordered table-striped table-hover center">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th>NAME</th>
              <th>CONTACT #</th>
              <th>ADDRESS</th>
              <th class="w90px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="supplier in suppliers">
              <td>{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
              <td class="uppercase">{{ supplier.name }}</td>
              <td class="uppercase">{{ supplier.contact }}</td>
              <td class="uppercase">{{ supplier.address }}</td>
              <td>
                <div class="btn-group btn-group-xs">
                  <a href="#/bar/supplier/view/{{ supplier.id }}" class="btn btn-success" title="VIEW"><i class="fa fa-eye"></i></a>
                  <a href="#/bar/supplier/edit/{{ supplier.id }}" class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></a>
                  <a class="btn btn-danger" title="DELETE" ng-click="remove(supplier)"><i class="fa fa-trash"></i></a>
                </div>
             </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    
    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'bar',page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    <div class="clearfix"></div>

  </div>  
</div>