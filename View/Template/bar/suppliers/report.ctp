<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> BAR SUPPLIERS PAYABLES</div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ code: 'bar', search: searchTxt })">
        </div> 
      </div>

      <div class="col-md-3">
        <button class="btn btn-success btn-sm btn-block" id="viewSuppliesPaymentHistory" ng-click="viewSuppliesPaymentHistory()"><i class="fa fa-eye"></i> VIEW ALL SUPPLIES PAYMENTS</button>
      </div>

      <div class="clearfix"></div>
      <hr>
      
      <div class="col-md-12"> 
        <table class="table table-bordered table-striped table-hover center">
          <thead>
            <tr>
              <th>NAME</th>
              <th>AMOUNT</th>
              <th>PAYMENTS</th>
              <th>PAYABLES</th>
              <th class="w70px"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="supplier in suppliers">
              <td class="uppercase">{{ supplier.name }}</td>
              <td class="uppercase">{{ supplier.totalAmount | number: 2 }}</td>
              <td class="uppercase">{{ supplier.totalPayments | number: 2 }}</td>
              <th class="uppercase">{{ supplier.totalPayables | number: 2 }}</th>
              <td>
                <div class="btn-group btn-group-xs">
                  <button  class="btn btn-primary {{ supplier.totalAmount - supplier.totalPayments > 0? '':'disabled' }}" title="CLICK TO ADD PAYMENT" ng-click="addPayment(supplier)"><i class="fa fa-plus"></i></button>  
                  <a href="#/bar/reports/supplier/view/{{ supplier.id }}" class="btn btn-xs btn-success" title="VIEW"><i class="fa fa-eye"></i></a>
                </div>       
              </td>
            </tr>
            <tr ng-if="suppliers==''">
              <td colspan="5">No payables found.</td>
            </tr>
          </tbody>
          <tfoot ng-if="suppliers!=''">
            <tr>
              <th colspan="3" class="text-right">TOTAL PAYABLES</th>
              <th class="italic">PHP {{ totalPayables | number: 2 }}</th>
              <td></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>

    <ul class="pagination pull-right">
      <li class="prevPage"><a href="javascript:void(0)">&laquo;</a></li>
      <li class="pagination-page" ng-repeat="page in pages"><a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'bar',page: page.number, search: searchTxt })">{{ page.number }}</a></li>
      <li class="nextPage"><a href="javascript:void(0)">&raquo;</a></li>
    </ul>
    <div class="clearfix"></div>

  </div>  
</div>

<?php echo $this->element('hotel/cashiering/view/add-payment-modal') ?>
<?php echo $this->element('suppliers/bar-supplies-payment-history-modal') ?>