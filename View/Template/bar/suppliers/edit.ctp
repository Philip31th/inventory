<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> EDIT BAR SUPPLIER</div>
  <div class="panel-body">
    <form id="form">
      <!-- SUPPLIER DETAILS -->
      <div class="row">
        <div class="col-md-12>">
          <div class="col-md-8">
            <div class="form-group">
              <label>Name <i class="required">*</i></label>
              <input type="text" class="form-control" ng-model="data.Supplier.name" data-validation-engine="validate[required]">
            </div>
          </div>
        
          <div class="col-md-4">
            <div class="form-group">
              <label>Contact #</label>
              <input number type="text" class="form-control" ng-model="data.Supplier.contactNumber">
            </div>
          </div>
  
          <div class="col-md-12">
            <div class="form-group">
              <label>Address</label>
              <textarea class="form-control" ng-model="data.Supplier.address"></textarea>
            </div>
          </div>          
      </div>
      </div>
    </form>  
          
    <div class="row"></div>  
    <hr>
      
    <!-- SUPPLIER ITEMS -->
    <div class="row">
        <div class="col-md-12">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="bg-info" colspan="7">SUPPLIER ITEMS</th>
              </tr>
              <tr>
                <th class="text-center w30px">#</th>
                <!-- <th class="text-center">DATE</th> -->
                <th class="text-center">NAME</th>
                <th class="text-center">PRICE</th>
                <th class="text-center">QTY</th>
                <th class="text-center">DESCRIPTION</th>
                <td class="text-center w60px"></td>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="item in data.SupplierItem">
                <td class="w30px">{{ $index + 1 }}</td>
                <!-- <td class="text-center" date="dd MMM yyyy HH:mm">{{ item.created }}</td> -->
                <td class="text-center">{{ item.name }}</td>
                <td class="text-center">{{ item.price | number: 2 }}</td>
                <td class="text-center">{{ item.quantity }}</td>
                <td class="text-center">{{ item.description }}</td>
                <td class="text-center w70px">
                  <div class="btn-group btn-group-xs">
                    <button class="btn btn-primary" title="EDIT" ng-click="editItem(item)"><i class="fa fa-edit"></i></button>
                    <button ng-click="removeItem(item,$index)" class="btn btn-danger" title="DELETE"><i class="fa fa-trash"></i></button>
                  </div>
                </td>
              </tr>
              <tr ng-if="data.SupplierItem==''">
                <td colspan="7" class="text-center">No items found.</td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>

    <div class="clearfix"></div>
    <label>TOTAL: PHP {{ totalSuppliesAmount() | number: 2 }}</label>
    <hr>
    
    <div class="btn-group btn-group-sm pull-right btn-min">
      <button type="button" class="btn btn-success btn-min" ng-click="addItem()"><i class="fa fa-plus"></i> ADD ITEM</button>
      <button type="button" class="btn btn-primary btn-min" ng-click="update()"><i class="fa fa-save"></i> UPDATE</button>
    </div>  

  </div>
</div>


<?php echo $this->element('suppliers/add-supplier-item-modal') ?>
<?php echo $this->element('suppliers/edit-supplier-item-modal') ?>