<div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-dot-circle-o"></i> BAR MENU MANAGEMENT</div>
  <div class="panel-body">
  	<div class="row">
    	<div class="col-md-3">
				<a href="#/bar/menu/add" class="btn btn-primary btn-sm btn-block modal-form"><i class="fa fa-plus"></i> NEW MENU</a>
			</div>

      <div class="col-md-4 pull-right">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control search" placeholder="SEARCH HERE" ng-model="searchTxt" ng-keyup="load({ code: 'bar', search: searchTxt })">
        </div>
      </div>

      <div class="clearfix"></div>
   		<hr>

   		<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th class="text-center w30px">#</th>
							<th class="">NAME</th>
              <th class="w120px text-right">AMOUNT</th>
              <th class="w90px"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="menu in menus">
							<td class="text-center">{{ (paginator.page - 1) * paginator.limit + $index + 1 }}</td>
							<td class="uppercase">{{ menu.name }}</td>
              <th class="text-right uppercase italic">{{ menu.amount | currency:'PHP ' }}</th>
							<td class="text-center">
								<div class="btn-group btn-group-xs">
                  <a href="#/bar/menu/view/{{ menu.id }}" class="btn btn-success" title="VIEW"><i class="fa fa-eye"></i></a>
									<a href="#/bar/menu/edit/{{ menu.id }}" class="btn btn-primary" title="EDIT"><i class="fa fa-edit"></i></a>
									<a href="javascript:void(0)" ng-click="remove(menu)" class="btn btn-danger no-border-radius" title="DELETE"><i class="fa fa-trash"></i></a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
   		</div>
  	</div>

    <ul class="pagination pull-right">
        <li class="prevPage">
          <a href="javascript:void(0)">&laquo;</a>
        </li>
        <li class="pagination-page" ng-repeat="page in pages">
          <a href="javascript:void(0)" class="text-center" ng-click="load({ code: 'bar', page: page.number, search: searchTxt })">{{ page.number }}</a>
        </li>
        <li class="nextPage">
          <a href="javascript:void(0)">&raquo;</a>
        </li>
    </ul>
    <div class="clearfix"></div>

  </div>
</div>