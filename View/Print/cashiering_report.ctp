<!DOCTYPE html>
<html>
<head>
  <title>HOTEL CASHIERING REPORT</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
<!-- <?php print_r($this->request->params['pass']) ?> -->
  <div class="main col-md-12">
    <table class="table table-bordered center">
      <thead>
        <tr>
          <th class="w80px">FOLIO #</th>
          <th>COMPANY</th>
          <th>GUEST NAME</th>
          <th>ROOM</th>
          <th class="text-right">PAYMENT</th>
          <th class="text-right">BALANCE</th>
          <th class="text-center">STATUS</th>
          <th class="text-center">REMARKS</th>
        </tr>
      </thead>
      <tbody>

        <?php foreach($cashiering as $cashier): ?>

        <tr>
          <td><?php echo $cashier['code'];?></td>
          <td class="uppercase"><?php echo $cashier['company'];?></td>
          <td class="uppercase"><?php echo $cashier['guest'];?></td>
          <td class="uppercase"><?php echo $cashier['room'] ?></td>
          <td class="uppercase text-right"><?php echo number_format($cashier['Transaction']['paidAmount'],2)?></td>
          <td class="uppercase text-right"><?php echo number_format($cashier['Transaction']['balance'],2)?></td>
          <td class="text-center uppercase">
            <?php echo $cashier['status'];?></span>
          </td>
          <td class="uppercase">
            <?php echo $cashier['Transaction']['paymentType'];
              if($cashier['transferedBills']) {
                echo '(T)';
              }
            ?>
          </td>
        </tr>

        <?php endforeach ?>

        <?php if(empty($cashiering)){
          echo '<tr><td colspan="9">No record found</td></tr>';
        }
          
         ?>
      </tbody>
    </table>

     <div class="pull-left col-md-4">
       <label>PREPARED BY:</label>
       <br><br>
       <label class="italic uppercase"><?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?></label>
     </div> 
     <!-- summary table  -->
     <div class="pull-right col-md-4">
      <table border="0" class="left" width="200px">
        <thead>
          <tr>
            <th class="text-left w500px"></th>
            <th class="w300px"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>CASH:</th>
            <th class="text-right">
              <?php echo number_format($summary['TotalCash'],2)?></th>
          </tr>
          <tr>
            <th>CARD:</th>
            <th class="text-right">
              <?php echo number_format($summary['TotalCard'],2)?></th>
          </tr>
          <tr>
            <th>DEBIT:</th>
            <th class="text-right">
              <?php echo number_format($summary['TotalDebit'],2)?></th>
          </tr>
          <tr>
            <th>SEND BILL:</th>
            <th class="text-right">
              <?php echo number_format($summary['TotalSendBill'],2)?></th>
          </tr>
          <tr>
            <th>TOTAL:</th>
            <th class="text-right">
              <?php echo number_format($summary['Total'],2)?></th>
          </tr>
        </tbody>
      </table>
    </div>    

    <div class="clearfix"></div>
    <hr>

    <div class="pull-right" style="font-size: 10px">
      Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
    </div>
    
  </div>
</body>
</html>
