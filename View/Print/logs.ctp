<!DOCTYPE html>
<html>
<head>
  <title>USER LOGS</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="main col-md-12">
    <div class="col-md-12">
        <table class="table table-bordered center">
          <thead>
            <tr>
              <th class="w30px">#</th>
              <th class="text-ri'">DATETIME</th>
              <th class="text-right">ROLE</th>
              <th class="text-right">USER</th>
              <th class="text-right">ACTION</th>
              <th class="text-right">DESCRIPTION</th>
            </tr>
          </thead>
          <tbody>
             <?php $ctr = 0; ?>
             <?php foreach($logs as $log): ?>
             
            <tr>
              <td><?php echo $ctr+=1; ?></td>
              <td class="uppercase text-right"><?php echo $log['date']?></td>
              <td class="uppercase text-right"><?php echo $log['role']?></td>
              <td class="text-right uppercase"><?php echo $log['user']?></td>
              <td class="text-right uppercase"><?php echo $log['action']?></td>
              <td class="text-right uppercase"><?php echo $log['description']?></td>
            </tr>
            
             <?php endforeach ?>
             
          </tbody>
        </table>
      </div>

      <div class="pull-left" style="font-size: 10px">
        Printed By: <?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?>
      </div>
      <div class="pull-right" style="font-size: 10px">
        Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
      </div>
  </div>
</body>
</html>
