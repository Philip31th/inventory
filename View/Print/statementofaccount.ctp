<!DOCTYPE html>
<html>
<head>
  <title>STATEMENT OF ACCOUNT</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="main col-md-12">
    <div class="row">
      <div class="col-md-12">
        <table class="table vcenter">
          <tr>
            <td class="w150px">ACCOUNT NAME:</td>
            <th class="uppercase italic"> <?php echo $cashiering['guest']?> </th>
            <td class="w150px">DATE OF ARRIVAL:</td>
            <td class="uppercase italic"><?php echo $cashiering['arrival']?></td>                   
          <tr>
          </tr>   
            <td class="w150px">ACCOUNT #:</td>
            <td class="uppercase italic"><?php echo $cashiering['code']?></td>
            <td class="w150px">DATE OF DEPARTURE:</td>
            <td class="uppercase italic"><?php echo $cashiering['departure']?></td>
          </tr>   
        </table>
      </div>
    </div>  
      
      <table class="table table-bordered center vcenter">
            <thead>
              <tr>
                <th>PARTICULARS</th>
                <th class="text-right">AMOUNT</th>
              </tr>
            </thead>
            <tbody>
            <!--  MAIN TRANSACTIONS -->
            <?php $ctr = 0; $totalVat = 0; $totalAmount = 0;?>

            <?php foreach($cashiering['Transaction']['charges'] as $items): ?>
                <tr>
                  <td class="uppercase"><?php  echo $items['title'] ?></td>
                  <td class="text-right"><?php echo number_format($items['amount'],2) ?></td>
                </tr>
                <!-- get Total VAT -->
                <?php 
                  $totalAmount += $items['amount'];
                  $totalVat = number_format($totalAmount*0.12,2);
                ?>

              <?php endforeach ?><!-- // end main transactions -->
                
            </tbody>
            <tfoot>       
              <tr>
                <th class="text-right italic"> TOTAL VAT</th>
                <th class="text-right italic">                  
                  <?php echo number_format($cashiering['Transaction']['amount']* 0.12,2) ?>
                </th>
              </tr>     
              <tr>
              <tr>
                <th class="text-right italic"> TOTAL PAYABLES</th>
                <th class="text-right italic"><?php echo number_format($cashiering['Transaction']['amount'],2) ?></th>
              </tr>
              <tr>
                <th class="text-right italic"> TOTAL PAID AMOUNT</th>
                <th class="text-right italic"><?php echo number_format($cashiering['Transaction']['paid'],2) ?></th>
              </tr>  
              <tr>
                <th class="text-right italic"> CURRENT BALANCE</th>
                <th class="text-right italic"><?php echo number_format($cashiering['Transaction']['balance'],2) ?></th>
              </tr>          
            </tfoot>
          </table>  

          <div class="pull-left" style="font-size: 10px">
            Printed By: <?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?>
          </div>
          <div class="pull-right" style="font-size: 10px">
            Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
          </div>
  </div>
</body>
</html>
