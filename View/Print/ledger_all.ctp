<!DOCTYPE html>
<html>
<head>
  <title>CITY LEDGER</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="main col-md-12">
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td class="w30px"></td>
              <th class="text-left">COMPANY</th>
              <th class="text-right">DEBIT</th>
              <th class="text-right">CREDIT</th> 
              <th class="text-right">BALANCE</th> 
            </tr>
          </thead>
          <tbody>
            <?php $ctr = 0; ?>
            <?php foreach($companies['company'] as $company): ?>
              
            <tr>
              <td><?php echo $ctr+=1  ?></td>
              <td class="text-left uppercase"><?php echo $company['company']?></td>
              <td class="text-right"><?php echo number_format($company['debit'],2)?></td>
              <td class="text-right"><?php echo number_format($company['credit'],2)?></td>
              <td class="text-right"><?php echo number_format($company['credit'] - $company['debit'],2)?></td>
            </tr>
            
             <?php endforeach ?>
             
            <tr class="text-right">
              <th colspan="2" class="text-right">TOTAL</th>
              <th class="text-right"><?php echo number_format($companies['totalDebit'],2) ?></th>
              <th class="text-right"><?php echo number_format($companies['totalCredit'],2) ?></th>
              <th class="text-right"><?php echo number_format($companies['totalBalance'],2) ?></th>
            </tr>
          </tbody>
        </table>
      </div>  
    </div>  

    <div class="pull-left" style="font-size: 10px">
      Printed By: <?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?>
    </div>
    <div class="pull-right" style="font-size: 10px">
      Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
    </div>
    
  </div>
</body>
</html>
