<!DOCTYPE html>
<html>
<head>
  <title>CASHIERING REPORT</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="row">
  <!-- <?php print_r($non_guest) ?> -->

    <div class="col-md-12">
      <table class="table table-bordered center">
        <thead>
          <tr><th colspan="5" class="text-left">NON-GUEST TRANSACTIONS</th></tr>
          <tr>
            <th>CODE</th>
            <th>PARTICULARS</th>
            <th class="text-center">PAYMENT</th>
            <th class="text-center">BALANCE</th>
            <th class="text-center">REMARKS</th>
          </tr>
        </thead>
        <tbody>
          <?php $ctr = 0; ?>
          <?php if (!empty($non_guest)||$non_guest!=null) {
          foreach($non_guest['Transactions']['charges'] as $trans): ?>

          <tr>
            <td><?php echo $trans['code'] ?></td>
            <td><?php echo $trans['title'] ?></td>
            <td class="uppercase text-center"><?php echo number_format($trans['paid'],2) ?></td>
            <td class="uppercase text-center"><?php echo number_format($trans['balance'],2) ?></td>
            <td class="uppercase"><?php echo $trans['paymentType'] ?></td>
          </tr>

          <?php endforeach; }?>

          <?php if(empty($non_guest)||empty($non_guest['Transactions']['charges'])){
            echo '<tr><td colspan="9">No transactions</td></tr>';
          }
          
         ?>
        </tbody>
      </table>
    </div>

    <div class="col-md-12">
      <table class="table table-bordered center">
        <thead>
          <tr><th colspan="8"  class="text-left">GUEST TRANSACTIONS</th></tr>
          <tr>
            <th class="w80px">FOLIO #</th>
            <th>COMPANY</th>
            <th>GUEST NAME</th>
            <th>ROOM</th>
            <th class="text-right">PAYMENT</th>
            <th class="text-right">BALANCE</th>
            <th class="text-center">STATUS</th>
            <th class="text-center">REMARKS</th>
          </tr>
        </thead>
        <tbody>

          <?php foreach($cashiering as $cashier): ?>

          <tr>
            <td><?php echo $cashier['code'] ?></td>
            <td class="uppercase"><?php echo $cashier['company'] ?></td>
            <td class="uppercase"><?php echo $cashier['guest'] ?></td>
            <td class="uppercase"><?php echo $cashier['room'] ?></td>
            <td class="uppercase text-right"><?php echo number_format($cashier['Transaction']['paid'],2) ?></td>
            <td class="uppercase text-right"><?php echo number_format($cashier['Transaction']['balance'],2) ?></td>
            <td class="text-center"><?php echo $cashier['status'] ?></td>
            <td class="uppercase">
              <?php echo $cashier['Transaction']['paymentType'];
                if($cashier['transferedBills']) {
                  echo ' (T)';
                }
              ?>
            </td>
          </tr>

          <?php endforeach ?>

          <?php if ($cashiering==null) {
            echo "<tr><td colspan='8'>No transactions</td></tr>";
            }?>

        </tbody>
      </table>
    </div>  

    <div class="pull-left col-md-4">
       <label>PREPARED BY:</label>
       <br><br>
       <label class="italic uppercase"><?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?></label>
     </div> 

     <!-- summary table  -->
     <div class="pull-right col-md-4">
      <table border="0" class="left" width="200px">
        <thead>
          <tr>
            <th class="text-left w500px"></th>
            <th class="w300px"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>CASH:</th>
            <th class="text-right">
              <?php echo number_format($summary['TotalCash'] + $non_guest['Summary']['TotalCash'],2)?></th>
          </tr>
          <tr>
            <th>CARD:</th>
            <th class="text-right">
              <?php echo number_format($summary['TotalCard'] + $non_guest['Summary']['TotalCard'],2)?></th>
          </tr>
          <tr>
            <th>DEBIT:</th>
            <th class="text-right">
              <?php echo number_format($summary['TotalDebit'] + $non_guest['Summary']['TotalDebit'],2)?></th>
          </tr>
          <tr>
            <th>TOTAL:</th>
            <th class="text-right">
              <?php echo number_format($summary['Total'] + $non_guest['Summary']['Total'],2)?></th>
          </tr>
        </tbody>
      </table>
    </div>    

    <div class="clearfix"></div>
    <hr>

    <div class="pull-right" style="font-size: 10px">
      Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
    </div>
  </div>  
</body>
</html>
