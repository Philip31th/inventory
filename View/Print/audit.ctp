<!DOCTYPE html>
<html>
<head>
  <title>NIGHT AUDIT</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="main col-md-12">

    <!--HOUSE USE-->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td class="w30px">#</td>
              <th class="text-right">HOUSE USE</th>
              <th class="text-right w120px" >NO. OF ROOMS</th>
              <th class="text-right w120px">OCCUPIED</th>
              <th class="text-right w120px">DIR</th>
              <th class="text-right w120px">OUT OF ORDER</th>
              <th class="text-right w200px">TOTAL ROOMS AVAILABLE</th>
            </tr>
          </thead>
          <tbody >
            
            <?php $ctr = 0; ?>
            <?php foreach($audits['audits'] as $row): ?>
 
             <tr class="text-right">
              <td><?php echo $ctr+=1 ?></td>
              <td><?php echo $row['room'] ?></td>
              <td><?php echo $row['total_rooms'] ?></td>
              <td><?php echo $row['total_occupied'] ?></td>
              <td><?php echo $row['dirty'] ?></td>
              <td><?php echo $row['out_of_order'] ?></td>
              <td><?php echo $row['total_available'] ?></td> 
            </tr>
            
            <?php endforeach ?>  
            
            <tr>
              <th colspan="2" class="text-right">TOTAL</th>
              <th class="text-right"><?php echo $audits['grandTotalRooms']  ?></th>
              <th class="text-right"><?php echo $audits['grandTotalOccupied']  ?></th>
              <th class="text-right"><?php echo $audits['grandTotalDir']  ?></th>
              <th class="text-right"><?php echo $audits['grandTotalOoo']  ?></th>
              <th class="text-right"><?php echo $audits['grandTotalAvailable']  ?></th>
            </tr>
          </tbody>
        </table>
      </div>       
    </div>
      
    <!-- ROOM REVENUE -->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td class="w30px">#</td>
              <th class="text-right">ROOM REVENUE</th>
              <th class="text-right w120px">SOLD</th>
              <th class="text-right w120px">RACK</th>
              <th class="text-right w120px">GRO</th>
              <th class="text-right w120px">DISC</th>
              <th class="text-right w200px">REVENUE</th>
            </tr>
          </thead>
          <tbody >
            
            <?php $ctr = 0; ?>
            <?php foreach($audits['audits'] as $row): ?>
            
            <tr>
              <td class="text-right"><?php echo $ctr+=1 ?></td>
              <td class="text-right"><?php echo $row['room'] ?></td>
              <td class="text-right"><?php echo $row['total_occupied'] ?></td>
              <td class="text-right"><?php echo number_format($row['rate'],2) ?></td>
              <td class="text-right"><?php echo number_format($row['gro'],2) ?></td>
              <td class="text-right"><?php echo number_format($row['discount'],2) ?></td>
              <td class="text-right"><?php echo number_format($row['revenue'],2) ?></td> 
            </tr>
            
            <?php endforeach ?>    
            
            <tr class="text-right">
              <th colspan="2" class="text-right">TOTAL</th>
              <th class="text-right"><?php echo $audits['grandTotalOccupied'] ?></th>
              <th class="text-right"></th>
              <th class="text-right"><?php echo number_format($audits['grandGro'],2) ?></th>
              <th class="text-right"><?php echo number_format($audits['grandDisc'],2) ?></th>
              <th class="text-right"><?php echo number_format($audits['grandRevenue'],2) ?></th>
            </tr>
          </tbody>
        </table>
      </div>       
    </div>

    <!-- EXTRA PERSON -->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <td class="w30px">#</td>
              <th class="text-right">EXTRA PERSON</th>
              <th class="text-right w120px">O</th>
              <th class="text-right w120px">RACK</th>
              <th class="text-right w120px">GRO</th>
              <th class="text-right w120px">DISC</th>
              <th class="text-right w200px">REVENUE</th>
            </tr>
          </thead>
          <tbody >
            <tr class="text-right">
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
            <tr class="text-right">
              <th colspan="2"></th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>       
    </div>

    <!-- STATISTICS -->
    <div class="col-md-12">
      <div class="row">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th class="text-right">STATISTICS</th>
              <th class="text-right">DOMESTIC</th>
              <th class="text-right">FOREIGN</th>
              <th class="text-right">ACTUAL</th>
            </tr>           
          </thead>
          <tbody >
            <tr>
              <td>Stay Over</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Individual)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Corporate)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Gift Voucher)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Today's Expected Arrival (Travel Agency)</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Walk-ins</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Complimentary</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>VIP/Confidential</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>&emsp; No Show</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th>&emsp;<i> HouseCount</i></th>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>&emsp; Confirmed Departure</td>
              <td rowspan="2"></td>
              <td rowspan="2"></td>
              <td></td>
            </tr>
            <tr>
              <td>&emsp; Indefinite Departure</td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3">Average Room Rate</td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3">No. of Rooms Sold</td>
              <td class="text-right"><?php echo $audits['grandTotalOccupied']?></td>
            </tr>
            <tr>
              <td colspan="3">No. of Saleable Rooms</td>
              <td class="text-right"><?php echo $audits['grandTotalRooms'] - $audits['grandTotalOoo']?></td>
            </tr>
            <tr>
              <th colspan="3" class="text-right">PERCENTAGE OF OCCUPANCY</th>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>       
    </div>

    <div class="pull-left" style="font-size: 10px">
      Printed By: <?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?>
    </div>
    <div class="pull-right" style="font-size: 10px">
      Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
    </div>

  </div>
</body>
</html>
