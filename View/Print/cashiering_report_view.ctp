<!DOCTYPE html>
<html>
<head>
  <title>HOTEL CASHIERING REPORT</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="main col-md-12">
      <div class="col-md-12">
        <dl class="dl-horizontal dl-data dl-bordered">
          <dt>Folio #:</dt>
          <dd><?php echo $cashiering['0']['code'] ?></dd>

          <dt>Guest Name:</dt>
          <dd><?php echo $cashiering['0']['guest'] ?></dd>

          <dt>Room #:</dt>
          <dd><?php echo $cashiering['0']['room'] ?></dd>

          <dt>Arrival:</dt>
          <dd><?php echo $cashiering['0']['arrival'] ?></dd>

          <dt>Departure:</dt>
          <dd><?php echo $cashiering['0']['departure'] ?></dd>

          <dt>Payment:</dt>
          <dd><?php echo $cashiering['0']['Transaction']['paid'] ?></dd>
        </dl>
      </div>

      <div class="pull-left" style="font-size: 10px">
        Printed By: <?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?>
      </div>
      <div class="pull-right" style="font-size: 10px">
        Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
      </div>
  </div>
</body>
</html>
