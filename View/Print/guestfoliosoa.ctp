<!DOCTYPE html>
<html>
<head>
  <title>GUEST FOLIO</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/print.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="main col-md-12">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th class="text-left" colspan="4">GUEST FOLIO</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="w120px bggray">FOLIO #</td>
              <td><?php echo $cashiering['code'] ?></td>
              
              <td class="w120px bggray">ROOM #</td>
              <td class="w120px text-right"><?php echo $cashiering['room'] ?></td>
            </tr>

            <tr>
              <td class="bggray">COMPANY NAME</td>
              <td class="uppercase"><?php echo $cashiering['company'] ?></td>
              
              <td class="bggray">ROOM TYPE</td>
              <td class="uppercase text-right"><?php echo $cashiering['roomType'] ?></td>
            </tr>

            <tr>
              <td class="bggray">GUEST'S NAME</td>
              <td class="uppercase"><?php echo $cashiering['guest'] ?></td>
              
              <td class="bggray">ROOM RATE</td>
              <td class="text-right"><?php echo 'PHP '. number_format($cashiering['roomRate'],2) ?></td>
            </tr>

            <tr>
              <td class="bggray">ADDRESS</td>
              <td class="uppercase"><?php echo $cashiering['address'] ?></td>
              
              <td class="bggray">ARRIVAL</td>
              <td class="text-right"><?php echo $cashiering['arrival'] ?></td>
            </tr>

            <tr>
              <td class="bggray">VEHICLE MADE</td>
              <td class="text-right"><?php ?></td>

              <td class="bggray">DEPARTURE</td>
              <td class="text-right"><?php echo $cashiering['departure'] ?></td>
            </tr>

            <tr>
              <td class="bggray">VEHICLE COLOR</td>
              <td><?php ?></td>
              
              <td class="bggray">SOURCE</td>
              <td class="text-right uppercase"><?php echo $cashiering['bookingSource'] ?></td>
            </tr>

            <tr>
              <td class="bggray">VEHICLE PLATE NO.</td>
              <td><?php ?></td>
              
              <td class="bggray">STATUS</td>
              <td class="uppercase text-right">
                <?php  $status = '';
                     if ($tmp['Folio']['closed']) $status = 'CHECKED OUT';
                     else $status = 'CHECKED IN';
                  ?>
                  <span><?php echo $status; ?></span>
              </td>
            </tr>

            <tr>
              <td class="bggray">CONTACT #</td>
              <td><?php echo $cashiering['mobile'] ?></td>
              
              <td class="text-right">TAXABLE AMOUNT</td>

              <?php $totalAmount = 0;?>
              <?php foreach($cashiering['Transactions']['charges'] as $row): ?>
                <?php 
                  $totalAmount += $row['totalAmount']; 
                  $vat = $totalAmount*0.12;
                  $totalTaxable = $totalAmount-($vat);
                ?>
              <?php endforeach ?>

              <td class="uppercase text-right">
              <?php echo number_format($totalTaxable,2) ?>
              </td> 
            </tr>

            <tr>
              <td class="bggray">SPECIAL REQUEST</td>
              <td>
                <?php echo $cashiering['specialRequest'] ?>
              </td>

              <td class="text-right">TAX RATE</td>
              <td class="uppercase text-right">1.12</td>
            </tr>
            <tr>
              <td class="bggray">REMARKS</td>
              <td class=""><?php echo $cashiering['remarks']?></td>

              <td class="text-right">VAT</td>
              <td class="uppercase text-right">
              <?php echo number_format(($cashiering['Transactions']['totalAmount']+$cashiering['ReceivedTransactions']['totalAmount'])*0.12,2);?>
            </tr>
            
            <tr>
              <td class="text-right" colspan="3">TOTAL CHARGES</td>
              <td class="uppercase text-right"><?php echo number_format($cashiering['Transactions']['totalAmount']+$cashiering['ReceivedTransactions']['totalAmount'],2) ?></td>
            </tr>

             <tr>
              <td class="text-right" colspan="3">TOTAL DISCOUNTS</td>
              <td class="uppercase text-right">
              <?php echo number_format($cashiering['Transactions']['totalDiscount']+$cashiering['ReceivedTransactions']['totalDiscount'],2) ?>
              </td>
            </tr>

            <tr>(
              <td class="text-right" colspan="3">DISCOUNT TYPE</td>
              <td class="uppercase text-right"><?php echo $cashiering['discountTypes']?></td>
            </tr>

            <tr>
              <td class="text-right" colspan="3">DISCOUNT PERCENTAGE</td>
              <td class="uppercase text-right"><?php echo number_format($cashiering['Transactions']['discountPercent']+$cashiering['ReceivedTransactions']['discountPercent'])?></td>
            </tr>

            <tr>
              <td class="text-right" colspan="3">DEPOSITS RECEIVED</td>
              <td class="uppercase text-right"><?php echo number_format($cashiering['Transactions']['totalPayment']+$cashiering['ReceivedTransactions']['totalPayment'],2) ?></td>
            </tr>

            <tr>
              <td class="text-right" colspan="3">BALANCE</td>
              <td class="uppercase text-right"><?php echo number_format($cashiering['Transactions']['totalBalance']+$cashiering['ReceivedTransactions']['totalBalance'],2) ?></td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
              <tr>
                <th class=" text-left" colspan="9">CHARGES</th>
              </tr>
              <tr>
                <th class="text-center w30px">CODE</th>
                <th class="text-center">BUSINESS</th>
                <th class="text-center">PARTICULARS</th>
                <th class="text-right w100px">AMOUNT</th>
                <th class="text-right w100px">DISCOUNT</th>
                <th class="text-right w150px">INITIAL PAYMENT</th>
                <th class="text-right w100px">PAYMENT</th>
                <th class="text-right w100px">BALANCE</th>
                <th class="text-center w120px">STATUS</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($cashiering['Transactions']['charges'] as $row): ?>
                <tr>
                  <td class="text-center"><?php echo $row['code'] ?></td>
                  <td class="text-center uppercase"><?php echo $row['business'] ?></td>
                  <td class="text-center uppercase"><?php echo $row['particulars'] ?></td>
                  <th class="text-right"><?php echo number_format($row['totalAmount'],2) ?></th>
                  <th class="text-right"><?php echo number_format($row['totalDiscount'],2) ?></th>
                  <th class="text-right"><?php echo number_format($row['totalbarPayment'],2) ?></th>
                  <th class="text-right"><?php echo number_format($row['totalPayment'],2) ?></th>
                  <th class="text-right"><?php echo number_format($row['totalBalance'],2) ?></th>
                  <td class="text-center">
                    <?php  $status = '';
                       if ($row['totalBalance']<=0) $status = 'TRANSACTION PAID';
                       else $status = 'PENDING PAYMENT'; ?>
                    <span><?php echo $status; ?></span>
                  </td>
                </tr>
              <?php endforeach ?>  
            </tbody>
            <tfoot>
              <th colspan="3" class="text-right">TOTAL</th>
              <th class="text-right"><?php echo number_format($cashiering['Transactions']['totalAmount'],2)?></th>
              <th class="text-right"><?php echo number_format($cashiering['Transactions']['totalDiscount'],2)?></th>
              <th class="text-right"><?php echo number_format($cashiering['Transactions']['totalInitialPayment'],2)?></th>
              <th class="text-right"><?php echo number_format($cashiering['Transactions']['chargesTotalPayment'],2)?></th>
              <th class="text-right"><?php echo number_format($cashiering['Transactions']['totalBalance'],2)?></th>
              <th class="text-right"></th>
          </tfoot>
        </table>
      </div> 

      <?php  if ($cashiering['ReceivedTransactions']['charges'] != null ) { ?>      
      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover vcenter center">
          <thead>
            <tr>
              <th class=" text-left" colspan="10">RECEIVED CHARGES</th>
            </tr>
            <tr>
              <th class="text-center w30px">CODE</th>
              <th class="text-right">FOLIO #</th>
              <th class="text-right">BUSINESS</th>
              <th class="text-center">PARTICULARS</th>
              <th class="text-right w100px">AMOUNT</th>
              <th class="text-right w100px">DISCOUNT</th>
              <th class="text-right w100px">INITIAL PAYMENT</th>
              <th class="text-right w100px">PAYMENT</th>
              <th class="text-right w100px">BALANCE</th>
              <th class="w120px">STATUS</th>
            </tr>
          </thead>
          <tbody>
            <?php $count =1 ; 
              if(!empty($cashiering['ReceivedTransactions']['charges'])) {
            ?>
            <?php foreach($cashiering['ReceivedTransactions']['charges'] as $row): ?>
              <tr>
                <td class="text-center"><?php echo $row['code'] ?></td>
                <td class="text-right"><?php echo $row['folioCode'] ?></td>
                <td class="text-right uppercase"><?php echo $row['business'] ?></td>
                <td class="text-center uppercase"><?php echo $row['particulars'] ?></td>
                <th class="text-right"><?php echo number_format($row['totalAmount'],2) ?></th>
                <th class="text-right"><?php echo number_format($row['totalDiscount'],2) ?></th>
                <th class="text-right"><?php echo number_format($row['TotalChildFolioPayment'],2) ?></th>
                <th class="text-right"><?php echo number_format($row['totalPayment'],2) ?></th>
                <th class="text-right"><?php echo number_format($row['totalBalance'],2) ?></th>
                <td class="text-right">
                  <?php  $status = '';
                     if ($row['totalBalance']<=0) $status = 'TRANSACTION PAID';
                    else $status = 'PENDING PAYMENT'; ?>
                  <span><?php echo $status; ?></span>
                </td>
              </tr>
            <?php endforeach; }?>
          </tbody>
          <tfoot>
            <th colspan="4" class="text-right">TOTAL</th>
            <th class="text-right"><?php echo number_format($cashiering['ReceivedTransactions']['totalAmount'],2) ?></th>
            <th class="text-right"><?php echo number_format($cashiering['ReceivedTransactions']['totalDiscount'],2) ?></th>
            <th class="text-right"><?php echo number_format($cashiering['ReceivedTransactions']['totalInitialPayment'],2) ?></th>
            <th class="text-right"><?php echo number_format($cashiering['ReceivedTransactions']['totalPayment'],2) ?></th>
            <th class="text-right"><?php echo number_format($cashiering['ReceivedTransactions']['totalBalance'],2) ?></th>
            <td></td>
          </tfoot>  
        </table>
      </div>  
      <?php } ?>

      <?php  if ($cashiering['Transactions']['charges'][0]['payments'] != null ) { ?>
      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover vcenter center">
          <thead>
            <tr>
              <th class=" text-left" colspan="9">PAYMENTS </th>
            </tr>
            <tr>
              <th class="text-center w30px">CHARGE CODE</th>
              <th class="text-center w90px">OR #</th>
              <th class="text-right w150px">DATE</th>
              <th class="text-right w90px">BUSINESS</th>
              <th class="text-right w150px">PAYMENT TYPE</th>
              <th class="text-right w120px">AMOUNT</th>
              <th class="text-right w120px">CHANGE</th>
            </tr>
          </thead>
          <tbody>
            <?php $index = 0;?>
            <?php foreach($cashiering['cPayments']['payments'] as $row): ?>
                <tr>
                  <td class="text-center uppercase"><?php echo $row['code']?></td>
                  <td class="text-center"><?php echo $row['orNumber']?></td>
                  <td class="text-right uppercase"><?php echo $row['date']?></td>
                  <td class="text-right uppercase"><?php echo $row['business']?></td>
                  <th class="text-right uppercase"><?php echo $row['type']?></th>
                  <th class="text-right"><?php echo number_format($row['amount']-$row['change'],2)?></th>
                  <th class="text-right"><?php echo number_format($row['change'],2)?></th>
                </tr>
            <?php endforeach; ?>
          </tbody>
          <tfoot>
            <th colspan="5" class="text-right">TOTAL</th>
            <th class="text-right"><?php echo number_format($cashiering['Transactions']['totalPayment'] + $cashiering['ReceivedTransactions']['totalPayment'],2) ?></th>
            <th class="text-right"><?php echo number_format($cashiering['Transactions']['totalChange'] + $cashiering['ReceivedTransactions']['totalChange'],2) ?></th>
          </tfoot>
        </table>
      </div>  
      <?php } ?>    

    </div>
    <div class="pull-left" style="font-size: 10px">
      Printed By: <?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?>
    </div>
    <div class="pull-right" style="font-size: 10px">
      Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
    </div>

  </div>  
</body>
</html>
