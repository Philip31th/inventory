<!DOCTYPE html>
<html>
<head>
  <title>CITY LEDGER</title>
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/plugins/bootstrap-3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->base ?>/assets/css/style.css">
</head>
<body ng-app="ehrms" style="background-color:white">
  <div class="main col-md-12">
    <div class="col-md-12">
      <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
              <dl class="dl-horizontal dl-data">
                <dt>Company:</dt>
                <dd class="uppercase"><?php echo $payments['company']?></dd>

                <dt>Total Debit:</dt>
                <dd><?php echo number_format($payments['debit'],2)?></dd>

                <dt>Total Credit:</dt>
                <dd><?php echo number_format($payments['payments'],2)?></dd>
            </div>

          <table class="table table-bordered table-striped table-hover vcenter center">
            <thead>
               <tr>
                <th class="w30px"></th>  
                <th class="text-right w150px">DATE</th>
                <th class="text-center">PARTICULARS</th>
                <th class="text-right w100px">AMOUNT</th>
              </tr>
            </thead>
            <tbody>
               <?php $ctr = 0; ?>
               <?php foreach($transactions as $data): ?>
             
              <tr>
                <td><?php echo $ctr+=1; ?></td>
                <td class="text-right"><?php echo $data['date']?></td>
                <td class="text-center uppercase"><?php echo $data['particulars']?></td>
                <td class="text-right"><?php echo number_format($data['amount'],2)?></td>
              </tr>
              
              <?php endforeach ?>
              
            </tbody>
            <tfoot>
						  <th class="text-right" colspan="3">TOTAL AMOUNT</th>
						  <th class="text-right"><?php echo number_format($payments['payments'],2)?></th>
					  </tfoot>
          </table>
        </div>  
    </div>
    
    <div class="clearfix"></div>
    <hr>
      
      <div class="pull-left" style="font-size: 10px">
        Printed By: <?php echo $this->Session->read('Auth.User.firstName'). ' ' .$this->Session->read('Auth.User.lastName');?>
      </div>
      <div class="pull-right" style="font-size: 10px">
        Datetime: <?php echo Date('Y/m/d h:i:s A') ?>
      </div>
    
        </div>
</body>
</html>
